# 3. Bindless Design

Date: 2024-11-24

## Status

Accepted

## Context

With [2. GPU Driven Rendering](0002-gpu-driven-rendering.md) we want to reduce
the amount of work done on the CPU.

## Decision

We design around a bindless design.

## Consequences

- Reducing the amount of bindings frees up the CPU for other work.
- Less binding might reduce the complexity of CPU-side code. On the other side,
  those bindings do some validation which we will miss or doing ourself, which
  might add complexity back again.
