# 2. GPU Driven Rendering

Date: 2024-11-24

## Status

Accepted

## Context

Rendering with its high performance requirements poses a challenge to both the
hardware and software involved. While the capabilities of the hardware increase
in an incredible rate, software development can't just rest on those shoulders.
We need to design the library to exploit those hardware capabilities.

## Decision

We design around a
[GPU Driven Rendering](https://vkguide.dev/docs/gpudriven/gpu_driven_engines/)
design.

## Consequences

- Reducing the interactions between CPU and GPU, in particular the amount of data
  transferred, will reduce latencies and frees up the CPU for other work.
- As (one of) the state of the art rendering approaches, we should find enough
  resources online.
