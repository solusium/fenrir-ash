use {ash::vk::LogicOp, fenrir_hal::logical::Operation};

#[inline]
pub(crate) fn ash_to_hal(logical_operation: LogicOp) -> Operation {
    if LogicOp::CLEAR == logical_operation {
        Operation::Clear
    } else if LogicOp::AND == logical_operation {
        Operation::And
    } else if LogicOp::AND_REVERSE == logical_operation {
        Operation::AndReverse
    } else if LogicOp::COPY == logical_operation {
        Operation::Copy
    } else if LogicOp::AND_INVERTED == logical_operation {
        Operation::AndInverted
    } else if LogicOp::NO_OP == logical_operation {
        Operation::NoOp
    } else if LogicOp::XOR == logical_operation {
        Operation::Xor
    } else if LogicOp::OR == logical_operation {
        Operation::Or
    } else if LogicOp::NOR == logical_operation {
        Operation::Nor
    } else if LogicOp::EQUIVALENT == logical_operation {
        Operation::Equivalent
    } else if LogicOp::INVERT == logical_operation {
        Operation::Invert
    } else if LogicOp::OR_REVERSE == logical_operation {
        Operation::OrReverse
    } else if LogicOp::COPY_INVERTED == logical_operation {
        Operation::CopyInverted
    } else if LogicOp::OR_INVERTED == logical_operation {
        Operation::OrInverted
    } else if LogicOp::NAND == logical_operation {
        Operation::Nand
    } else {
        Operation::Set
    }
}
