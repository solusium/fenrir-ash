use {
    crate::{
        result::{device_lost, out_of_device_memory, out_of_host_memory, unexpected_error},
        trace_called, trace_calling,
    },
    abi_stable::std_types::RArc,
    ash::{
        prelude::VkResult,
        vk::{FenceCreateFlags, FenceCreateInfo},
    },
    fenrir_error::{invalid_argument, state_not_recoverable, Error},
    futures_core::Stream,
    sleipnir::{pin, task::yield_now},
    smallvec::SmallVec,
    std::{
        future::Future,
        ops::Deref,
        pin::Pin,
        slice::from_ref,
        task::{Context, Poll},
    },
    tracing::{span, Level},
};

#[allow(dead_code)]
pub(crate) enum WaitFor {
    All,
    Any,
}

pub(crate) struct CreateInfo {
    flags: Option<FenceCreateFlags>,
}

#[allow(dead_code)]
pub(crate) struct Fence {
    inner: ash::vk::Fence,
    create_info: CreateInfo,
    device: RArc<crate::device::Guard>,
}

pub(crate) struct SmallWait<'a, const N: usize> {
    fences: &'a [Fence],
    wait_for: WaitFor,
}

pub(crate) fn small_wait<const N: usize>(
    fences: &[Fence],
    wait_for: WaitFor,
) -> Result<SmallWait<'_, N>, Error> {
    let one_device = fences.first().map_or(true, |fence| {
        fences.iter().all(|other| fence.device == other.device)
    });

    if one_device {
        Ok(SmallWait { fences, wait_for })
    } else {
        Err(invalid_argument(
            "fences created on different devices are not allowed.",
        ))
    }
}

impl CreateInfo {
    #[inline]
    fn ash(&self) -> FenceCreateInfo<'_> {
        let mut create_info = FenceCreateInfo::default();

        if let Some(flags) = self.flags {
            create_info = create_info.flags(flags);
        }

        create_info
    }
}

impl Fence {
    pub(crate) fn new(
        device: RArc<crate::device::Guard>,
        flags: Option<FenceCreateFlags>,
    ) -> Result<Self, Error> {
        let create_info = CreateInfo { flags };

        let ash_create_info = create_info.ash();

        let trace_args = (device.deref(), ash_create_info);

        trace_calling(Self::format_create_fence, trace_args);

        let fence_result = unsafe { device.create_fence(&ash_create_info, None) };

        trace_called(Self::format_create_fence, trace_args, Some(&fence_result));

        let inner = match fence_result {
            Ok(fence_) => Ok(fence_),
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }?;

        Ok(Self {
            inner,
            create_info,
            device,
        })
    }

    #[cold]
    #[inline]
    fn format_create_fence(args: (&crate::device::Guard, FenceCreateInfo)) -> String {
        let (device, create_info) = args;

        format!("create_fence(&self: {device:?}, create_info: {create_info:?}, allocation_callbacks: None)")
    }

    #[cold]
    #[inline]
    fn format_destroy_fence(args: (&crate::device::Guard, ash::vk::Fence)) -> String {
        let (device, fence) = args;

        format!("destroy_fence(&self: {device:?}, fence: {fence:?}, allocation_callbacks: None)")
    }

    #[cold]
    #[inline]
    fn format_get_fence_state(args: (&crate::device::Guard, ash::vk::Fence)) -> String {
        let (device, fence) = args;

        format!("get_fence_status(&self: {device:?}, fence: {fence:?})")
    }

    #[cold]
    #[inline]
    fn format_reset_fences(args: (&crate::device::Guard, &[ash::vk::Fence])) -> String {
        let (device, fences) = args;

        format!("reset_fences(&self: {device:?}, fences: {fences:?})")
    }

    #[cold]
    #[inline]
    fn format_wait_for_fences(
        args: (&crate::device::Guard, &[ash::vk::Fence], bool, u64),
    ) -> String {
        let (device, fences, wait_all, timeout) = args;

        format!("wait_for_fences(&self: {device:?}, fences: {fences:?}, wait_fall: {wait_all:?}, timeout: {timeout:?})")
    }

    #[allow(dead_code)]
    pub(crate) fn is_ready(&self) -> Result<bool, Error> {
        let wait_for_fences_result =
            unsafe { self.device.wait_for_fences(from_ref(&self.inner), false, 0) };

        match wait_for_fences_result {
            Ok(()) => Ok(true),
            Err(result) => match result {
                ash::vk::Result::TIMEOUT => Ok(false),
                error => Err(match error {
                    ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                    ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                    ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                    error => unexpected_error(error),
                }),
            },
        }
    }

    fn sync_wait(&self, timeout: u64) -> Result<(), Error> {
        let span = span!(Level::TRACE, module_path!());

        let _span_guard = span.enter();

        let device = &*self.device;

        let fences = [self.inner];

        let wait_all = true;

        let wait_for_fences_trace_args = (device, &fences[..], wait_all, timeout);

        let wait_for_fences = || {
            let wait_for_fences_span = span!(Level::TRACE, "ash::Device::wait_for_fences");

            let _wait_for_fences_span_guard = wait_for_fences_span.enter();

            trace_calling(Self::format_wait_for_fences, wait_for_fences_trace_args);

            let result = unsafe { device.wait_for_fences(&fences[..], wait_all, timeout) };

            trace_called(
                Self::format_wait_for_fences,
                wait_for_fences_trace_args,
                Some(&result),
            );

            result
        };

        match wait_for_fences() {
            Ok(()) => Ok(()),
            Err(vk_result) => Err(match vk_result {
                ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    pub(crate) fn reset(&self) -> Result<(), Error> {
        let trace_args = (self.device.deref(), from_ref(&self.inner));

        trace_calling(Self::format_reset_fences, trace_args);

        let reset_result = unsafe { self.device.reset_fences(from_ref(&self.inner)) };

        trace_called(Self::format_reset_fences, trace_args, Some(&reset_result));

        match reset_result {
            Ok(result) => Ok(result),
            Err(vk_result) => Err(match vk_result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                error => unexpected_error(error),
            }),
        }
    }
}

impl Deref for Fence {
    type Target = ash::vk::Fence;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl Drop for Fence {
    fn drop(&mut self) {
        let drop_span = span!(Level::TRACE, module_path!());

        let _drop_span_guard = drop_span.enter();

        let device = &*self.device;

        let fence = self.inner;

        let trace_args = (device, fence);

        let fence_status_result = || {
            let get_fence_status_span = span!(Level::TRACE, "ash::Device::get_fence_status");

            let _get_fence_status_span_guard = get_fence_status_span.enter();

            trace_calling(Self::format_get_fence_state, trace_args);

            let fence_status_result_ = unsafe { device.get_fence_status(fence) };

            trace_called(
                Self::format_get_fence_state,
                trace_args,
                Some(&fence_status_result_),
            );

            fence_status_result_
        };

        match fence_status_result() {
            Err(result) => unreachable!(
                "{}: expected Ok(bool), got Err({result:?})",
                Self::format_get_fence_state(trace_args)
            ),
            Ok(signaled) => {
                if signaled {
                    if let Err(code) = self.sync_wait(u64::MAX) {
                        unreachable!(
                            "{}: expected Ok(()), got Err({code:?})",
                            Self::format_wait_for_fences((device, &[fence][..], true, u64::MAX))
                        );
                    }
                }
            }
        }

        let destroy_fence_span = span!(Level::TRACE, "ash::Device::destroy_fence");

        let _destroy_fence_span_guard = destroy_fence_span.enter();

        trace_calling(Self::format_destroy_fence, trace_args);

        unsafe { device.destroy_fence(fence, None) }

        trace_called(
            Self::format_destroy_fence,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

unsafe impl Send for Fence {}
unsafe impl Sync for Fence {}

impl Stream for Fence {
    type Item = Result<(), Error>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let wait_for_fences =
            || unsafe { self.device.wait_for_fences(from_ref(&self.inner), true, 0) };

        let mut wait_for_fences_result = wait_for_fences();

        while let Err(result) = wait_for_fences_result {
            match result {
                ash::vk::Result::TIMEOUT => {
                    let yield_future = yield_now();

                    pin!(yield_future);

                    if Poll::Pending == yield_future.poll(cx) {
                        return Poll::Pending;
                    }

                    wait_for_fences_result = wait_for_fences();
                }
                error => {
                    return Poll::Ready(Some(Err(match error {
                        ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                        ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                        ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                        error => state_not_recoverable(&format!("unexpected error: {error}")),
                    })))
                }
            }
        }

        match self.reset() {
            Ok(result) => Poll::Ready(Some(Ok(result))),
            Err(code) => Poll::Ready(Some(Err(code))),
        }
    }
}

impl<const N: usize> Future for SmallWait<'_, N> {
    type Output = Result<Option<usize>, Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        match self.fences.first() {
            None => Poll::Ready(Ok(None)),
            Some(first_fence) => {
                let fences = self
                    .fences
                    .iter()
                    .map(|fence| fence.inner)
                    .collect::<SmallVec<[ash::vk::Fence; 16]>>();

                let wait_for_all = match self.wait_for {
                    WaitFor::All => true,
                    WaitFor::Any => false,
                };

                let device = &first_fence.device;

                let check_fences = || unsafe { device.wait_for_fences(&fences, wait_for_all, 0) };

                while let Err(result) = check_fences() {
                    match result {
                        ash::vk::Result::TIMEOUT => {
                            let yield_future = yield_now();

                            pin!(yield_future);

                            if Poll::Pending == yield_future.poll(cx) {
                                return Poll::Pending;
                            }
                        }
                        result => {
                            return Poll::Ready(Err(match result {
                                ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => {
                                    out_of_device_memory()
                                }
                                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                                error => {
                                    state_not_recoverable(&format!("unexpected error: {error}"))
                                }
                            }))
                        }
                    }
                }

                match self.wait_for {
                    WaitFor::All => {
                        let resets_result = self.fences.iter().try_for_each(Fence::reset);

                        match resets_result {
                            Ok(()) => Poll::Ready(Ok(None)),
                            Err(code) => Poll::Ready(Err(code)),
                        }
                    }
                    WaitFor::Any => {
                        let results_result = self
                            .fences
                            .iter()
                            .map(|fence| {
                                let wait_result = unsafe {
                                    first_fence.device.wait_for_fences(from_ref(fence), true, 0)
                                };

                                match wait_result {
                                    Ok(()) => Ok(true),
                                    Err(result) => match result {
                                        ash::vk::Result::TIMEOUT => Ok(false),
                                        result => Err(match result {
                                            ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                                            ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => {
                                                out_of_device_memory()
                                            }
                                            ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => {
                                                out_of_host_memory()
                                            }
                                            error => unexpected_error(error),
                                        }),
                                    },
                                }
                            })
                            .collect::<Result<SmallVec<[bool; N]>, Error>>();

                        match results_result {
                            Err(code) => Poll::Ready(Err(code)),
                            Ok(results) => match results.iter().position(|result| *result) {
                                Some(index) => match self.fences[index].reset() {
                                    Ok(()) => Poll::Ready(Ok(Some(index))),
                                    Err(code) => Poll::Ready(Err(code)),
                                },
                                None => Poll::Ready(Err(state_not_recoverable(
                                    "At least one fence should be ready at this point.",
                                ))),
                            },
                        }
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use {
        crate::{
            fence::Fence,
            instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
        },
        ash::vk::FenceCreateFlags,
        tokio::test,
    };

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                let fence = Fence::new(device.guard(), None);

                assert!(fence.is_ok());
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn is_ready() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                {
                    let fence = Fence::new(device.guard(), None);

                    assert!(fence.is_ok());

                    let is_ready = fence.unwrap().is_ready();

                    assert!(is_ready.is_ok());

                    assert!(!is_ready.unwrap());
                }

                {
                    let fence = Fence::new(device.guard(), Some(FenceCreateFlags::SIGNALED));

                    assert!(fence.is_ok());

                    let is_ready = fence.unwrap().is_ready();

                    assert!(is_ready.is_ok());
                }
            }
        }
    }
}
