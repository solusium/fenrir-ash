use {ash::vk::PolygonMode, fenrir_hal::polygon::Mode};

#[inline]
pub(crate) fn ash_to_hal(mode: PolygonMode) -> Mode {
    if PolygonMode::FILL == mode {
        Mode::Fill
    } else if PolygonMode::LINE == mode {
        Mode::Line
    } else {
        Mode::Point
    }
}
