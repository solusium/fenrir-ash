use {ash::vk::ColorComponentFlags, fenrir_hal::color::Component};

#[inline]
pub(crate) fn hal_to_ash(components: &[Component]) -> ColorComponentFlags {
    let mut flags = ColorComponentFlags::default();

    for component in components {
        flags |= match component {
            Component::R => ColorComponentFlags::R,
            Component::G => ColorComponentFlags::G,
            Component::B => ColorComponentFlags::B,
            Component::A => ColorComponentFlags::A,
        }
    }

    flags
}
