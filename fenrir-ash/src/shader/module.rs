use {
    crate::{
        device::{Device, Guard},
        result::{out_of_device_memory, out_of_host_memory, unexpected_error},
        trace_called, trace_calling,
    },
    abi_stable::std_types::RArc,
    ash::vk::{Result as AshResult, ShaderModule, ShaderModuleCreateInfo, ShaderStageFlags},
    bytemuck::try_cast_slice,
    core::ops::Deref,
    fenrir_error::state_not_recoverable,
    fenrir_hal::shader::{Module as HalModule, Stage as HalStage},
    fenrir_shader_compiler::{Error, Language, Metadata, Stage},
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
#[allow(dead_code)]
pub(crate) struct Module {
    inner: ShaderModule,
    stage: ShaderStageFlags,
    spirv: Vec<u8>,
    entry_point: String,
    device: Device,
}

impl Module {
    #[inline]
    pub(crate) async fn new(device: Device, shader: &HalModule<'_, '_>) -> Result<Self, Error> {
        let entry_point = shader.entry_point().to_string();

        let shader_stage = match shader.stage() {
            HalStage::Compute => Stage::Compute,
            HalStage::Fragment => Stage::Fragment,
            HalStage::Vertex => Stage::Vertex,
        };

        let spirv = match shader.language() {
            Language::GLSL => {
                device
                    .compiler()
                    .compile(
                        Metadata::build(
                            shader.code(),
                            Language::GLSL,
                            Language::SPIRV,
                            shader_stage,
                        )
                        .build(),
                    )
                    .await
            }
            Language::SPIRV => Ok(shader.code().to_vec()),
        }?;

        let spirv_slice = match try_cast_slice::<u8, u32>(&spirv) {
            Ok(spirv_slice_) => Ok(spirv_slice_),
            Err(error) => Err(Error::FenrirError(state_not_recoverable(&format!(
                "failed to read SPIR-V: {error}"
            )))),
        }?;

        let create_info = ShaderModuleCreateInfo::default().code(spirv_slice);

        let stage = match shader.stage() {
            HalStage::Compute => ShaderStageFlags::COMPUTE,
            HalStage::Fragment => ShaderStageFlags::FRAGMENT,
            HalStage::Vertex => ShaderStageFlags::VERTEX,
        };

        let device_guard = device.guard();

        let create_shader_module = || {
            debug_span!("create_shader_module");

            unsafe { device_guard.create_shader_module(&create_info, None) }
        };

        let trace_args = (&device_guard, &create_info);

        trace_calling(Self::format_create_shader_module, trace_args);

        let shader_module_result = create_shader_module();

        trace_called(
            Self::format_create_shader_module,
            trace_args,
            Some(&shader_module_result),
        );

        match shader_module_result {
            Ok(inner) => Ok(Self {
                inner,
                stage,
                spirv,
                entry_point,
                device,
            }),
            Err(error) => Err(Error::FenrirError(match error {
                AshResult::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                AshResult::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                AshResult::ERROR_INVALID_SHADER_NV => fenrir_error::Error::InvalidShader,
                _ => unexpected_error(error),
            })),
        }
    }

    #[inline]
    pub(crate) fn hal<'a>(&'a self) -> HalModule<'a, 'a> {
        let stage = if ShaderStageFlags::VERTEX == self.stage {
            HalStage::Vertex
        } else if ShaderStageFlags::FRAGMENT == self.stage {
            HalStage::Fragment
        } else {
            HalStage::Compute
        };

        let code = self.spirv.as_slice();

        let lanuage = Language::SPIRV;

        let entry_point = self.entry_point.as_str();

        HalModule::<'a, 'a>::from((stage, code, lanuage, entry_point))
    }

    #[inline]
    pub(crate) fn entry_point(&self) -> &str {
        &self.entry_point
    }

    #[cold]
    #[inline]
    fn format_create_shader_module(args: (&RArc<Guard>, &ShaderModuleCreateInfo)) -> String {
        let (device, create_info) = args;

        format!("create_shader_module(&self: {device:?}, shader: {create_info:?}, allocation_callbacks: None)")
    }

    #[cold]
    #[inline]
    fn format_destroy_shader_module(args: (&RArc<Guard>, ShaderModule)) -> String {
        let (device, module) = args;

        format!("destroy_shader_module(&self: {device:?}, shader: {module:?}, allocation_callbacks: None)")
    }

    #[inline]
    pub(crate) const fn stage(&self) -> ShaderStageFlags {
        self.stage
    }
}

impl Deref for Module {
    type Target = ShaderModule;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl Drop for Module {
    #[inline]
    fn drop(&mut self) {
        let device = self.device.guard();

        let destroy_shader_module = || {
            debug_span!("destroy_shader_module");

            unsafe { device.destroy_shader_module(self.inner, None) };
        };

        let args = (&device, self.inner);

        trace_calling(Self::format_destroy_shader_module, args);

        destroy_shader_module();

        trace_called(
            Self::format_destroy_shader_module,
            args,
            None::<&Result<(), AshResult>>,
        );
    }
}
