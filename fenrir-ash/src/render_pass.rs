use {
    crate::{
        command::{
            buffer::Recorder, Buffer as CommandBuffer, PipelineBarrier, Pool as CommandPool,
        },
        device::Device,
        failed_to_cast,
        graphics::{pipeline::Guard as GraphicsPipelineGuard, Pipeline},
        image::MemoryBarrier,
        queue::Family,
        rendering::Info as RenderingInfo,
        Attachment,
    },
    abi_stable::{
        rtry,
        sabi_trait::TD_CanDowncast,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
        },
    },
    alloc::sync::{Arc, Weak},
    ash::vk::{
        CommandBufferInheritanceRenderingInfo, CommandBufferLevel, Extent2D, ImageAspectFlags,
        ImageLayout, ImageSubresourceRange, Offset2D, PipelineBindPoint, QueueFlags, Rect2D,
        RenderingFlags, RenderingInfo as AshRenderingInfo, SampleCountFlags,
    },
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::{RwLock, RwLockReadGuard},
    core::{
        convert::{From, Into},
        marker::PhantomData,
        ops::Range,
    },
    fenrir_error::{
        argument_out_of_domain, invalid_argument, not_supported, state_not_recoverable, Error,
    },
    fenrir_hal::{
        color::Attachment as HalColorAttachment,
        geometry::Point2D,
        graphics::pipeline::{
            stable_abi::Trait_TO as HalPipelineTrait, Metadata as GraphicsPipelineMetadata,
            Pipeline as HalPipeline,
        },
        pipeline::BindPoint,
        render_pass::{
            stable_abi::{Trait, Trait_TO},
            Metadata as HalRenderPassMetadata,
        },
        Device as HalDevice, RenderPass as HalRenderPass,
    },
    fenrir_shader_compiler::Error as CompilerError,
    futures_channel::mpsc::channel,
    futures_util::{future::join_all, stream, SinkExt, StreamExt as FuturesStreamExt},
    sleipnir::ffi::stream::StreamExt,
    smallvec::SmallVec,
};

extern crate alloc;

#[derive(Debug)]
struct CommandBufferResult {
    pub inner: Result<Arc<CommandBuffer>, Error>,
}

impl Default for CommandBufferResult {
    #[inline]
    fn default() -> Self {
        Self {
            inner: Err(state_not_recoverable("this should not be in use")),
        }
    }
}

pub(crate) struct Metadata {
    render_area: Rect2D,
    layer_count: u32,
    view_mask: u32,
    bind_point: PipelineBindPoint,
    color_attachments: SmallVec<[Attachment; 3]>,
    queue_family: Family,
    device: HalDevice,
}

#[derive(Debug)]
pub(crate) struct RenderPass {
    rendering_info: RenderingInfo,
    bind_point: PipelineBindPoint,
    color_attachments: RwLock<SmallVec<[Attachment; 3]>>,
    queue_family: Family,
    device: HalDevice,
    pipelines: Arc<RwLock<Vec<Weak<GraphicsPipelineGuard>>>>,
}

impl Metadata {
    #[allow(dead_code)]
    pub(crate) const fn new(
        render_area: Rect2D,
        layer_count: u32,
        view_mask: u32,
        bind_point: PipelineBindPoint,
        color_attachments: SmallVec<[Attachment; 3]>,
        queue_family: Family,
        device: HalDevice,
    ) -> Self {
        Self {
            render_area,
            layer_count,
            view_mask,
            bind_point,
            color_attachments,
            queue_family,
            device,
        }
    }

    #[inline]
    fn usize_to_u32(value: usize) -> Result<u32, Error> {
        u32::try_from(value).map_or_else(
            |_| {
                Err(argument_out_of_domain(
                    Range {
                        start: u32::MIN.into(),
                        end: (u64::from(u32::MAX) + 1).into(),
                    },
                    value.into(),
                ))
            },
            Ok,
        )
    }
}

impl RenderPass {
    #[inline]
    pub(crate) fn new(metadata: Metadata) -> Result<Self, Error> {
        let device = metadata.device.clone();

        let queue_family = metadata.queue_family;

        let color_attachments = RwLock::new(metadata.color_attachments);

        let rendering_info = AshRenderingInfo::default()
            .render_area(metadata.render_area)
            .layer_count(metadata.layer_count)
            .view_mask(metadata.view_mask)
            .color_attachments(&[])
            .try_into()?;

        let bind_point = metadata.bind_point;

        let pipelines = Arc::new(RwLock::new(Vec::new()));

        Ok(Self {
            rendering_info,
            bind_point,
            color_attachments,
            queue_family,
            device,
            pipelines,
        })
    }

    #[inline]
    async fn add_color_attachment(&self, attachment: Attachment) {
        self.color_attachments.write().await.push(attachment);
    }

    #[inline]
    pub(crate) async fn color_attachments(&self) -> RwLockReadGuard<SmallVec<[Attachment; 3]>> {
        self.color_attachments.read().await
    }

    #[inline]
    async fn metadata(&self) -> HalRenderPassMetadata {
        let color_attachments = self
            .color_attachments
            .read()
            .await
            .iter()
            .map(|attachment| attachment.clone().into())
            .collect();

        let offset = self.rendering_info.render_area().offset;
        let extent = self.rendering_info.render_area().extent;

        let render_area = fenrir_hal::geometry::Rect2D::new(
            Point2D::new(offset.x as isize, offset.y as isize),
            fenrir_hal::geometry::Extent2D::new(extent.width as usize, extent.height as usize),
        );
        let bind_point = match self.bind_point {
            PipelineBindPoint::GRAPHICS => BindPoint::Graphics,
            PipelineBindPoint::COMPUTE => BindPoint::Compute,
            PipelineBindPoint::RAY_TRACING_KHR => BindPoint::RayTracing,
            _ => unreachable!(),
        };

        HalRenderPassMetadata::new(
            self.device.clone(),
            color_attachments,
            render_area,
            self.rendering_info.layer_count() as usize,
            self.rendering_info.view_mask() as usize,
            bind_point,
        )
    }

    #[inline]
    async fn new_graphics_pipeline<'a>(
        &'a self,
        graphics_pipeline_metadata: GraphicsPipelineMetadata<'a, 'a>,
    ) -> Result<Pipeline, CompilerError> {
        let this = Self {
            rendering_info: self.rendering_info.clone(),
            bind_point: self.bind_point,
            color_attachments: RwLock::new(self.color_attachments.read().await.clone()),
            queue_family: self.queue_family.clone(),
            device: self.device.clone(),
            pipelines: self.pipelines.clone(),
        };

        let pipeline = Box::pin(Pipeline::new(graphics_pipeline_metadata, this)).await?;

        {
            let mut pipelines = self.pipelines.write().await;

            let maybe_empty_entry = pipelines
                .iter_mut()
                .find(|weak_pipeline| 0 == weak_pipeline.strong_count());

            match maybe_empty_entry {
                Some(empty_entry) => {
                    *empty_entry = Arc::downgrade(&pipeline.guard());
                }
                None => pipelines.push(Arc::downgrade(&pipeline.guard())),
            }
        }

        Ok(pipeline)
    }

    #[inline]
    async fn remove_color_attachment(&self, color_attachment: Attachment) -> Result<(), Error> {
        {
            let mut color_attachments = self.color_attachments.write().await;

            let (index, _) = color_attachments
                .iter()
                .enumerate()
                .find(|(_, current_color_attachment)| {
                    **current_color_attachment == color_attachment
                })
                .ok_or_else(|| {
                    invalid_argument(&format!(
                        "color attachment {color_attachment:?} not found in color attachments of render pass",
                    ))
                })?;

            color_attachments.remove(index);
        }

        Ok(())
    }

    #[inline]
    async fn render(&self) -> Result<(), Error> {
        {
            let queue_family_supports_rendering = self
                .queue_family
                .flags()
                .iter()
                .any(|flag| QueueFlags::GRAPHICS == *flag);

            if !queue_family_supports_rendering {
                return Err(invalid_argument(&format!(
                    "rendering on following queue family without which does not have QueueFlags::Graphics \
                    not possible: {:?}", self.queue_family
                )));
            }
        }

        let pipelines = self
            .pipelines
            .read()
            .await
            .iter()
            .filter_map(Weak::upgrade)
            .collect::<Vec<_>>();

        if pipelines.is_empty() {
            return Ok(());
        }

        let command_pool = self.queue_family.command_pool().await?;

        let mut command_buffer = command_pool.buffer(CommandBufferLevel::PRIMARY, None)?;

        self.render_pipelines(&command_pool, pipelines, command_buffer.record()?)
            .await?;

        command_buffer.submit().await?;

        command_pool.await
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn render_area(&self) -> &Rect2D {
        self.rendering_info.render_area()
    }

    #[inline]
    async fn render_pipeline(
        pipeline: &Arc<GraphicsPipelineGuard>,
        command_buffer_arc: Arc<CommandBuffer>,
    ) -> CommandBufferResult {
        CommandBufferResult {
            inner: Self::render_pipeline_impl(pipeline, command_buffer_arc).await,
        }
    }

    #[inline]
    async fn render_pipelines<'a>(
        &self,
        command_pool: &CommandPool,
        pipelines: Vec<Arc<GraphicsPipelineGuard>>,
        recorder: Recorder<'a>,
    ) -> Result<(), Error> {
        let color_attachments = self.color_attachments.read().await;

        recorder
            .pipeline_barrier(PipelineBarrier::default().add_image_memory_barriers(
                color_attachments.iter().map(|attachment| {
                    MemoryBarrier::new(
                        attachment.view().image().clone(),
                        ImageSubresourceRange::default()
                            .aspect_mask(ImageAspectFlags::COLOR)
                            .base_mip_level(0)
                            .level_count(1)
                            .base_array_layer(0)
                            .layer_count(1),
                        ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                    )
                }),
            ))
            .await;

        {
            let color_attachment_rendering_infos =
                join_all(color_attachments.iter().map(Attachment::rendering_info)).await;

            let rendering_info = self
                .rendering_info
                .ash()
                .flags(RenderingFlags::CONTENTS_SECONDARY_COMMAND_BUFFERS)
                .layer_count(1)
                .color_attachments(&color_attachment_rendering_infos);

            {
                let _renderer = recorder.renderer(rendering_info);

                let color_attachment_formats = color_attachments
                    .iter()
                    .map(|attachment| attachment.view().format())
                    .collect::<SmallVec<[_; 3]>>();

                let inheritance_info = Some(
                    CommandBufferInheritanceRenderingInfo::default()
                        .flags(RenderingFlags::CONTENTS_SECONDARY_COMMAND_BUFFERS)
                        .view_mask(self.rendering_info.view_mask())
                        .color_attachment_formats(&color_attachment_formats)
                        .rasterization_samples(SampleCountFlags::TYPE_1),
                );

                let command_buffers = command_pool
                    .buffers(
                        CommandBufferLevel::SECONDARY,
                        inheritance_info,
                        pipelines.len(),
                    )?
                    .into_iter()
                    .map(Arc::new);

                let (sender, receiver) = channel(pipelines.len());

                let senders = pipelines.iter().map(|_| sender.clone()).collect::<Vec<_>>();

                drop(sender);

                {
                    stream::iter(pipelines)
                        .zip(stream::iter(command_buffers))
                        .zip(stream::iter(senders))
                        .for_each_parallel(
                            None,
                            Some(recorder.buffer().device().instance().runtime()),
                            |((pipeline, command_buffer), mut sender)| async move {
                                let render_pipeline_result =
                                    Self::render_pipeline(&pipeline, command_buffer).await;

                                if let Err(error) = sender.send(render_pipeline_result).await {
                                    unreachable!(
                                        "failed to send the results of recording the drawing of \
                                        graphics pipelines: {error}"
                                    );
                                }
                            },
                        )
                        .await;

                    let secondary_command_buffers = receiver
                        .collect::<Vec<_>>()
                        .await
                        .into_iter()
                        .map(|command_buffer_result| command_buffer_result.inner)
                        .collect::<Result<Vec<_>, Error>>()?
                        .into_iter()
                        .map(|command_buffer| {
                            Arc::into_inner(command_buffer).map_or_else(
                                || {
                                    Err(state_not_recoverable(
                                        "expected no other strong reference to an Arc",
                                    ))
                                },
                                Ok,
                            )
                        })
                        .collect::<Result<Vec<_>, Error>>()?;

                    recorder.execute(secondary_command_buffers)?;
                }
            }
        }

        drop(color_attachments);

        Ok(())
    }

    #[inline]
    async fn render_pipeline_impl(
        pipeline: &Arc<GraphicsPipelineGuard>,
        command_buffer_arc: Arc<CommandBuffer>,
    ) -> Result<Arc<CommandBuffer>, Error> {
        let mut command_buffer = Arc::into_inner(command_buffer_arc).map_or_else(
            || {
                Err(state_not_recoverable(
                    "expected no other strong reference to an Arc",
                ))
            },
            Ok,
        )?;

        {
            let recorder = command_buffer.record()?;

            recorder.draw(pipeline).await;
        }

        Ok(Arc::new(command_buffer))
    }
}

impl TryFrom<HalRenderPassMetadata> for Metadata {
    type Error = Error;

    #[inline]
    fn try_from(metadata: HalRenderPassMetadata) -> Result<Self, Self::Error> {
        let offset = [
            metadata.render_area().offset().x(),
            metadata.render_area().offset().y(),
        ]
        .map(|component| {
            i32::try_from(component).map_or_else(
                |_| {
                    Err(argument_out_of_domain(
                        Range {
                            start: i32::MIN.into(),
                            end: (i64::from(i32::MAX) + 1).into(),
                        },
                        component.into(),
                    ))
                },
                Ok,
            )
        })
        .into_iter()
        .try_fold(SmallVec::<[i32; 2]>::new(), |mut vector, next| {
            vector.push(next?);

            Ok(vector)
        })?;

        let extent = [
            metadata.render_area().extent().width(),
            metadata.render_area().extent().height(),
        ]
        .map(Self::usize_to_u32)
        .into_iter()
        .try_fold(SmallVec::<[u32; 2]>::new(), |mut vector, next| {
            vector.push(next?);

            Ok(vector)
        })?;

        let render_area = Rect2D::default()
            .offset(Offset2D::default().x(offset[0]).y(offset[1]))
            .extent(Extent2D::default().width(extent[0]).height(extent[1]));

        let layer_count = Self::usize_to_u32(metadata.layer_count())?;

        let view_mask = Self::usize_to_u32(metadata.view_mask())?;

        let bind_point = match metadata.bind_point() {
            BindPoint::Graphics => PipelineBindPoint::GRAPHICS,
            BindPoint::Compute => PipelineBindPoint::COMPUTE,
            BindPoint::RayTracing => PipelineBindPoint::RAY_TRACING_KHR,
        };

        let color_attachments = metadata
            .color_attachments()
            .iter()
            .map(|attachment| Attachment::try_from(attachment.clone()))
            .collect::<Result<SmallVec<[_; 3]>, Error>>()?;

        let device = metadata.device();

        let device_state = device.state();

        let device_state_obj = device_state.obj;

        let queue_family = match device_state_obj.downcast_into::<Device>() {
            Err(error) => Err(failed_to_cast(
                &device.state(),
                PhantomData::<crate::Device>,
                error,
            )),
            Ok(device) => match metadata.bind_point() {
                BindPoint::Graphics | BindPoint::RayTracing => device.graphics_queue_family(),
                BindPoint::Compute => device.compute_queue_family(),
            },
        }?;

        Ok(Self {
            render_area,
            layer_count,
            view_mask,
            bind_point,
            color_attachments,
            queue_family,
            device,
        })
    }
}

impl TryInto<HalRenderPassMetadata> for Metadata {
    type Error = Error;

    #[inline]
    fn try_into(self) -> Result<HalRenderPassMetadata, Self::Error> {
        let device = self.device.clone();

        let color_attachments = self.color_attachments.into_iter().map(Into::into).collect();

        let offset = self.render_area.offset;
        let extent = self.render_area.extent;
        let render_area = fenrir_hal::geometry::Rect2D::new(
            Point2D::new(offset.x as isize, offset.y as isize),
            fenrir_hal::geometry::Extent2D::new(extent.width as usize, extent.height as usize),
        );

        let layer_count = self.layer_count as usize;

        let view_mask = self.view_mask as usize;

        let bind_point = match self.bind_point {
            PipelineBindPoint::GRAPHICS => Ok(BindPoint::Graphics),
            PipelineBindPoint::COMPUTE => Ok(BindPoint::Compute),
            PipelineBindPoint::RAY_TRACING_KHR => Ok(BindPoint::RayTracing),
            unsupported_bind_point => Err(not_supported(&format!(
                "render pass bind point '{unsupported_bind_point:?}' not supported"
            ))),
        }?;

        Ok(HalRenderPassMetadata::new(
            device,
            color_attachments,
            render_area,
            layer_count,
            view_mask,
            bind_point,
        ))
    }
}

impl Clone for RenderPass {
    #[inline]
    fn clone(&self) -> Self {
        Self {
            rendering_info: self.rendering_info.clone(),
            bind_point: self.bind_point,
            color_attachments: RwLock::new(self.color_attachments.read_blocking().clone()),
            queue_family: self.queue_family.clone(),
            device: self.device.clone(),
            pipelines: self.pipelines.clone(),
        }
    }
}

impl Eq for RenderPass {}

#[allow(clippy::from_over_into)]
impl Into<HalRenderPass> for RenderPass {
    #[inline]
    fn into(self) -> HalRenderPass {
        HalRenderPass::new(Trait_TO::from_ptr(RArc::new(self), TD_CanDowncast))
    }
}

impl PartialEq for RenderPass {
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.rendering_info == other.rendering_info,
            self.bind_point == other.bind_point,
            self.queue_family == other.queue_family,
            self.device == other.device,
        ];

        values.into_iter().all(|value| value)
    }
}

unsafe impl Send for RenderPass {}
unsafe impl Sync for RenderPass {}

impl Trait for RenderPass {
    #[inline]
    fn add_color_attachment(
        &self,
        color_attachment: HalColorAttachment,
    ) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move {
            self.add_color_attachment(rtry!(TryFrom::try_from(color_attachment)))
                .await;

            ROk(())
        }
        .into_ffi()
    }

    #[inline]
    fn metadata(&self) -> BorrowingFfiFuture<HalRenderPassMetadata> {
        async move { self.metadata().await }.into_ffi()
    }

    #[inline]
    fn new_graphics_pipeline<'a>(
        &'a self,
        graphics_pipeline_metadata: GraphicsPipelineMetadata<'a, 'a>,
    ) -> BorrowingFfiFuture<'a, RResult<HalPipeline, CompilerError>> {
        async move {
            match Self::new_graphics_pipeline(self, graphics_pipeline_metadata).await {
                Err(error) => RErr(error),
                Ok(pipeline) => {
                    ROk(HalPipelineTrait::from_ptr(RArc::new(pipeline), TD_CanDowncast).into())
                }
            }
        }
        .into_ffi()
    }

    #[inline]
    fn remove_color_attachment(
        &self,
        color_attachment: HalColorAttachment,
    ) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move {
            self.remove_color_attachment(rtry!(TryFrom::try_from(color_attachment)))
                .await
                .into()
        }
        .into_ffi()
    }

    #[inline]
    fn render(&self) -> BorrowingFfiFuture<'_, RResult<(), Error>> {
        async move { Self::render(self).await.into() }.into_ffi()
    }
}

#[cfg(test)]
mod tests {
    use {
        crate::{
            attachment::{ClearColorValue, ClearValue},
            graphics::Pipeline,
            image::View,
            instance::tests::{devices_and_dependencies, load_shader_compilers},
            render_pass::{Metadata as RenderPassMetadata, RenderPass},
            Attachment, Image,
        },
        abi_stable::{sabi_trait::TD_CanDowncast, std_types::RArc},
        ash::vk::{
            AttachmentLoadOp, AttachmentStoreOp, Extent2D, Offset2D, PipelineBindPoint, QueueFlags,
            Rect2D,
        },
        fenrir_hal::{
            color::{
                blend::State as ColorBlendState,
                component::{Mapping, Swizzle},
            },
            device::{stable_abi::Trait_TO, Device},
            graphics::pipeline::Metadata as GraphicsPipelineMetadata,
            render_pass::stable_abi::Trait,
            shader::{Module, Stage},
            texture::{
                stable_abi::Trait as HalTextureTrait, Aspect, Metadata as HalTextureMetadata,
                SubresourceRange, Usage,
            },
            Device as HalDevice, Viewport,
        },
        fenrir_shader_compiler::Language,
        std::iter::once,
        tokio::test,
    };

    #[inline]
    #[allow(clippy::cast_precision_loss)]
    async fn default_graphics_pipeline(render_pass: &RenderPass, device: &HalDevice) -> Pipeline {
        let shader_codes: [&str; 1] = ["#version 450\n\
            \n\
            vec2 positions[3] = vec2[](\n\
              vec2(0.0, -0.5),\n\
              vec2(0.5, 0.5),\n\
              vec2(-0.5, 0.5)\n\
            );\n\
            \n\
            void main() {\n\
              gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
            }"];

        let render_area = render_pass.render_area();

        let pipeline_result = render_pass
            .new_graphics_pipeline(
                GraphicsPipelineMetadata::builder(device.clone())
                    .add_shader(Module::new(
                        Stage::Vertex,
                        shader_codes[0].as_bytes(),
                        Language::GLSL,
                        "main",
                    ))
                    .set_viewports(&[Viewport::new(
                        render_area.offset.x as f32,
                        render_area.offset.y as f32,
                        render_area.extent.width as f32,
                        render_area.extent.height as f32,
                        0.0,
                        1.0,
                    )])
                    .set_scissors(&[fenrir_hal::geometry::Rect2D::new(
                        fenrir_hal::geometry::Point2D::new(
                            render_area.offset.x as isize,
                            render_area.offset.y as isize,
                        ),
                        fenrir_hal::geometry::Extent2D::new(
                            render_area.extent.width as usize,
                            render_area.extent.height as usize,
                        ),
                    )])
                    .build(),
            )
            .await;

        assert!(pipeline_result.is_ok());

        pipeline_result.unwrap()
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn render() {
        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                let fenrir_hal_device = Device::new(Trait_TO::from_ptr(
                    RArc::new(device.clone()),
                    TD_CanDowncast,
                ));

                for queue_family in device.queue_families() {
                    let render_area = Rect2D {
                        offset: Offset2D::default().x(0).y(0),
                        extent: Extent2D::default().width(1).height(1),
                    };

                    let image_metadata_result = HalTextureMetadata::new(
                        fenrir_hal_device.clone(),
                        render_area.extent.width as usize,
                        render_area.extent.height as usize,
                        1,
                        vec![Usage::ColorAttachment],
                        "color_attachment".into(),
                    )
                    .try_into();

                    assert!(image_metadata_result.is_ok());

                    let image_result = Image::new(image_metadata_result.unwrap()).await;

                    assert!(image_result.is_ok());

                    let hal_view_result = image_result.unwrap().view(
                        Mapping::new(
                            Swizzle::Identity,
                            Swizzle::Identity,
                            Swizzle::Identity,
                            Swizzle::Identity,
                        ),
                        SubresourceRange::new(&[Aspect::Color], 0, 1, 0, 1),
                    );

                    assert!(hal_view_result.is_ok());

                    let hal_view = hal_view_result.unwrap();

                    let view_result = hal_view.inner().obj.downcast_as::<View>();

                    assert!(view_result.is_ok());

                    let color_attachments = once(Attachment::new(
                        view_result.unwrap().clone(),
                        None,
                        AttachmentLoadOp::CLEAR,
                        AttachmentStoreOp::STORE,
                        ClearValue::Color(ClearColorValue::Float32([0.0, 0.0, 0.0, 0.0])),
                        ColorBlendState::builder().enabled(false).build(),
                    ))
                    .collect();

                    let metadata = RenderPassMetadata::new(
                        render_area,
                        1,
                        0,
                        PipelineBindPoint::GRAPHICS,
                        color_attachments,
                        queue_family.clone(),
                        fenrir_hal_device.clone(),
                    );

                    let render_pass_result = RenderPass::new(metadata);

                    assert!(render_pass_result.is_ok());

                    let render_pass = render_pass_result.unwrap();

                    let _pipeline =
                        default_graphics_pipeline(&render_pass, &fenrir_hal_device).await;

                    {
                        let queue_family_has_graphics = queue_family
                            .flags()
                            .iter()
                            .any(|flag| QueueFlags::GRAPHICS == *flag);

                        assert_eq!(
                            queue_family_has_graphics,
                            Trait::render(&render_pass).await.is_ok()
                        );
                    }
                }
            }
        }
    }
}
