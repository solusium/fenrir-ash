use {ash::vk::CullModeFlags, fenrir_hal::cull::Mode};

#[inline]
pub(crate) fn ash_to_hal(cull_mode: CullModeFlags) -> Mode {
    if CullModeFlags::NONE == cull_mode {
        Mode::None
    } else if CullModeFlags::FRONT == cull_mode {
        Mode::Front
    } else if CullModeFlags::BACK == cull_mode {
        Mode::Back
    } else {
        Mode::FrontAndBack
    }
}
