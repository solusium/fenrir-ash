use {
    ash::vk::Rect2D as AshRect2D,
    fenrir_hal::geometry::{Extent2D, Point2D, Rect2D as HalRect2D},
};

pub(crate) const fn ash_to_hal(scissor: &AshRect2D) -> HalRect2D {
    HalRect2D::new(
        Point2D::new(scissor.offset.x as isize, scissor.offset.y as isize),
        Extent2D::new(
            scissor.extent.width as usize,
            scissor.extent.height as usize,
        ),
    )
}
