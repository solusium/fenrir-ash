use {
    crate::{instance::Guard, trace_called, trace_calling, Instance},
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            PhysicalDevice, PhysicalDeviceExtendedDynamicState3PropertiesEXT,
            PhysicalDeviceProperties, PhysicalDeviceProperties2,
            PhysicalDeviceVertexAttributeDivisorPropertiesEXT, TRUE,
        },
    },
    educe::Educe,
    float_eq::float_eq,
    tracing::debug_span,
};

extern crate alloc;

#[derive(Clone, Debug, Educe)]
#[educe(Deref)]
pub(crate) struct Properties {
    #[educe(Deref)]
    properties: PhysicalDeviceProperties,
    dynamic_primitive_topology_unrestricted: bool,
    max_vertex_attrib_divisor: u32,
}

impl Properties {
    #[inline]
    pub(crate) fn new(instance: &Instance, physical_device: PhysicalDevice) -> Self {
        let instance_guard = instance.guard();

        let mut dynamic_state_3 = PhysicalDeviceExtendedDynamicState3PropertiesEXT::default();

        let mut vertex_attribute_divisor =
            PhysicalDeviceVertexAttributeDivisorPropertiesEXT::default();

        let properties = Self::get_physical_device_properties2(
            &instance_guard,
            physical_device,
            &mut dynamic_state_3,
            &mut vertex_attribute_divisor,
        );

        let dynamic_primitive_topology_unrestricted =
            TRUE == dynamic_state_3.dynamic_primitive_topology_unrestricted;

        let max_vertex_attrib_divisor = vertex_attribute_divisor.max_vertex_attrib_divisor;

        Self {
            properties,
            dynamic_primitive_topology_unrestricted,
            max_vertex_attrib_divisor,
        }
    }

    #[inline]
    pub(crate) fn dynamic_state_3(&self) -> PhysicalDeviceExtendedDynamicState3PropertiesEXT<'_> {
        PhysicalDeviceExtendedDynamicState3PropertiesEXT::default()
            .dynamic_primitive_topology_unrestricted(self.dynamic_primitive_topology_unrestricted)
    }

    #[cold]
    #[inline]
    fn format_get_physical_device_properties2(
        args: (&Arc<Guard>, PhysicalDevice, &PhysicalDeviceProperties2),
    ) -> String {
        let (instance, physical_device, properties) = args;

        let dynamic_state_3 = unsafe {
            *(properties.p_next as *const PhysicalDeviceExtendedDynamicState3PropertiesEXT)
        };

        let vertex_attribute_divisor = unsafe {
            *(dynamic_state_3.p_next as *const PhysicalDeviceVertexAttributeDivisorPropertiesEXT)
        };

        let dynamic_state_3_string = format!(
            "PhysicalDeviceExtendedDynamicState3PropertiesEXT {{ s_type: {:?}, p_next: \
            {vertex_attribute_divisor:?}, dynamic_primitive_topology_unrestricted: {:?} }}",
            dynamic_state_3.s_type, dynamic_state_3.dynamic_primitive_topology_unrestricted
        );

        let properties_string = format!(
            "PhysicalDeviceProperties2{{ s_type: {:?}, p_next: \
            {dynamic_state_3_string:?}, properties: {:?} }}",
            properties.s_type, properties.properties
        );

        format!(
            "get_physical_device_properties2(&self: {instance:?}, physical_device: \
            {physical_device:?}, props: {properties_string:?})"
        )
    }

    fn get_physical_device_properties2(
        instance_guard: &Arc<Guard>,
        physical_device: PhysicalDevice,
        dynamic_state_3: &mut PhysicalDeviceExtendedDynamicState3PropertiesEXT,
        vertex_attribute_divisor: &mut PhysicalDeviceVertexAttributeDivisorPropertiesEXT,
    ) -> PhysicalDeviceProperties {
        let mut properties2 = PhysicalDeviceProperties2::default()
            .push_next(dynamic_state_3)
            .push_next(vertex_attribute_divisor);

        trace_calling(
            Self::format_get_physical_device_properties2,
            (instance_guard, physical_device, &properties2),
        );

        {
            let _span = debug_span!("get_physical_device_properties2");

            unsafe {
                instance_guard.get_physical_device_properties2(physical_device, &mut properties2);
            }
        }

        trace_called(
            Self::format_get_physical_device_properties2,
            (instance_guard, physical_device, &properties2),
            None::<&VkResult<()>>,
        );

        properties2.properties
    }

    #[inline]
    pub(crate) fn vertex_attribute_divisor(
        &self,
    ) -> PhysicalDeviceVertexAttributeDivisorPropertiesEXT<'_> {
        PhysicalDeviceVertexAttributeDivisorPropertiesEXT::default()
            .max_vertex_attrib_divisor(self.max_vertex_attrib_divisor)
    }
}

impl PartialEq for Properties {
    #[inline]
    #[allow(clippy::too_many_lines)]
    fn eq(&self, other: &Self) -> bool {
        let comparisons = [
            self.properties.api_version == other.properties.api_version,
            self.properties.driver_version == other.properties.driver_version,
            self.properties.vendor_id == other.properties.vendor_id,
            self.properties.device_id == other.properties.device_id,
            self.properties.device_type == other.properties.device_type,
            self.properties.device_name == other.properties.device_name,
            self.properties.pipeline_cache_uuid == other.properties.pipeline_cache_uuid,
            self.properties.limits.max_image_dimension1_d
                == other.properties.limits.max_image_dimension1_d,
            self.properties.limits.max_image_dimension2_d
                == other.properties.limits.max_image_dimension2_d,
            self.properties.limits.max_image_dimension3_d
                == other.properties.limits.max_image_dimension3_d,
            self.properties.limits.max_image_dimension_cube
                == other.properties.limits.max_image_dimension_cube,
            self.properties.limits.max_image_array_layers
                == other.properties.limits.max_image_array_layers,
            self.properties.limits.max_texel_buffer_elements
                == other.properties.limits.max_texel_buffer_elements,
            self.properties.limits.max_uniform_buffer_range
                == other.properties.limits.max_uniform_buffer_range,
            self.properties.limits.max_storage_buffer_range
                == other.properties.limits.max_storage_buffer_range,
            self.properties.limits.max_push_constants_size
                == other.properties.limits.max_push_constants_size,
            self.properties.limits.max_memory_allocation_count
                == other.properties.limits.max_memory_allocation_count,
            self.properties.limits.max_sampler_allocation_count
                == other.properties.limits.max_sampler_allocation_count,
            self.properties.limits.buffer_image_granularity
                == other.properties.limits.buffer_image_granularity,
            self.properties.limits.sparse_address_space_size
                == other.properties.limits.sparse_address_space_size,
            self.properties.limits.max_bound_descriptor_sets
                == other.properties.limits.max_bound_descriptor_sets,
            self.properties.limits.max_per_stage_descriptor_samplers
                == other.properties.limits.max_per_stage_descriptor_samplers,
            self.properties
                .limits
                .max_per_stage_descriptor_uniform_buffers
                == other
                    .properties
                    .limits
                    .max_per_stage_descriptor_uniform_buffers,
            self.properties
                .limits
                .max_per_stage_descriptor_storage_buffers
                == other
                    .properties
                    .limits
                    .max_per_stage_descriptor_storage_buffers,
            self.properties.limits.max_descriptor_set_sampled_images
                == other.properties.limits.max_descriptor_set_sampled_images,
            self.properties.limits.max_descriptor_set_storage_images
                == other.properties.limits.max_descriptor_set_storage_images,
            self.properties.limits.max_descriptor_set_input_attachments
                == other.properties.limits.max_descriptor_set_input_attachments,
            self.properties.limits.max_per_stage_resources
                == other.properties.limits.max_per_stage_resources,
            self.properties.limits.max_descriptor_set_samplers
                == other.properties.limits.max_descriptor_set_samplers,
            self.properties.limits.max_descriptor_set_uniform_buffers
                == other.properties.limits.max_descriptor_set_uniform_buffers,
            self.properties
                .limits
                .max_descriptor_set_uniform_buffers_dynamic
                == other
                    .properties
                    .limits
                    .max_descriptor_set_uniform_buffers_dynamic,
            self.properties.limits.max_descriptor_set_storage_buffers
                == other.properties.limits.max_descriptor_set_storage_buffers,
            self.properties
                .limits
                .max_descriptor_set_storage_buffers_dynamic
                == other
                    .properties
                    .limits
                    .max_descriptor_set_storage_buffers_dynamic,
            self.properties.limits.max_descriptor_set_sampled_images
                == other.properties.limits.max_descriptor_set_sampled_images,
            self.properties.limits.max_descriptor_set_storage_images
                == other.properties.limits.max_descriptor_set_storage_images,
            self.properties.limits.max_vertex_input_attributes
                == other.properties.limits.max_vertex_input_attributes,
            self.properties.limits.max_vertex_input_bindings
                == other.properties.limits.max_vertex_input_bindings,
            self.properties.limits.max_vertex_input_attribute_offset
                == other.properties.limits.max_vertex_input_attribute_offset,
            self.properties.limits.max_vertex_input_binding_stride
                == other.properties.limits.max_vertex_input_binding_stride,
            self.properties.limits.max_vertex_output_components
                == other.properties.limits.max_vertex_output_components,
            self.properties.limits.max_tessellation_generation_level
                == other.properties.limits.max_tessellation_generation_level,
            self.properties.limits.max_tessellation_patch_size
                == other.properties.limits.max_tessellation_patch_size,
            self.properties
                .limits
                .max_tessellation_control_per_vertex_input_components
                == other
                    .properties
                    .limits
                    .max_tessellation_control_per_vertex_input_components,
            self.properties.limits.max_vertex_output_components
                == other.properties.limits.max_vertex_output_components,
            self.properties
                .limits
                .max_tessellation_control_per_patch_output_components
                == other
                    .properties
                    .limits
                    .max_tessellation_control_per_patch_output_components,
            self.properties
                .limits
                .max_tessellation_control_total_output_components
                == other
                    .properties
                    .limits
                    .max_tessellation_control_total_output_components,
            self.properties
                .limits
                .max_tessellation_evaluation_input_components
                == other
                    .properties
                    .limits
                    .max_tessellation_evaluation_input_components,
            self.properties
                .limits
                .max_tessellation_evaluation_output_components
                == other
                    .properties
                    .limits
                    .max_tessellation_evaluation_output_components,
            self.properties.limits.max_geometry_shader_invocations
                == other.properties.limits.max_geometry_shader_invocations,
            self.properties.limits.max_geometry_input_components
                == other.properties.limits.max_geometry_input_components,
            self.properties.limits.max_geometry_output_components
                == other.properties.limits.max_geometry_output_components,
            self.properties.limits.max_geometry_output_vertices
                == other.properties.limits.max_geometry_output_vertices,
            self.properties.limits.max_geometry_total_output_components
                == other.properties.limits.max_geometry_total_output_components,
            self.properties.limits.max_fragment_input_components
                == other.properties.limits.max_fragment_input_components,
            self.properties.limits.max_fragment_output_attachments
                == other.properties.limits.max_fragment_output_attachments,
            self.properties.limits.max_fragment_dual_src_attachments
                == other.properties.limits.max_fragment_dual_src_attachments,
            self.properties.limits.max_compute_shared_memory_size
                == other.properties.limits.max_compute_shared_memory_size,
            self.properties.limits.max_compute_work_group_count
                == other.properties.limits.max_compute_work_group_count,
            self.properties.limits.max_compute_work_group_invocations
                == other.properties.limits.max_compute_work_group_invocations,
            self.properties.limits.max_compute_work_group_size
                == other.properties.limits.max_compute_work_group_size,
            self.properties.limits.sub_pixel_precision_bits
                == other.properties.limits.sub_pixel_precision_bits,
            self.properties.limits.sub_texel_precision_bits
                == other.properties.limits.sub_texel_precision_bits,
            self.properties.limits.mipmap_precision_bits
                == other.properties.limits.mipmap_precision_bits,
            self.properties.limits.max_draw_indexed_index_value
                == other.properties.limits.max_draw_indexed_index_value,
            self.properties.limits.max_draw_indirect_count
                == other.properties.limits.max_draw_indirect_count,
            float_eq!(
                self.properties.limits.max_sampler_lod_bias,
                other.properties.limits.max_sampler_lod_bias,
                r2nd <= f32::EPSILON
            ),
            float_eq!(
                self.properties.limits.max_sampler_anisotropy,
                other.properties.limits.max_sampler_anisotropy,
                r2nd <= f32::EPSILON
            ),
            self.properties.limits.max_viewports == other.properties.limits.max_viewports,
            self.properties.limits.max_viewport_dimensions
                == other.properties.limits.max_viewport_dimensions,
            float_eq!(
                self.properties.limits.viewport_bounds_range,
                other.properties.limits.viewport_bounds_range,
                rmax_all <= f32::EPSILON
            ),
            self.properties.limits.viewport_sub_pixel_bits
                == other.properties.limits.viewport_sub_pixel_bits,
            self.properties.limits.min_memory_map_alignment
                == other.properties.limits.min_memory_map_alignment,
            self.properties.limits.min_texel_buffer_offset_alignment
                == other.properties.limits.min_texel_buffer_offset_alignment,
            self.properties.limits.min_uniform_buffer_offset_alignment
                == other.properties.limits.min_uniform_buffer_offset_alignment,
            self.properties.limits.min_storage_buffer_offset_alignment
                == other.properties.limits.min_storage_buffer_offset_alignment,
            self.properties.limits.min_texel_offset == other.properties.limits.min_texel_offset,
            self.properties.limits.max_texel_offset == other.properties.limits.max_texel_offset,
            self.properties.limits.min_texel_gather_offset
                == other.properties.limits.min_texel_gather_offset,
            self.properties.limits.max_texel_gather_offset
                == other.properties.limits.max_texel_gather_offset,
            float_eq!(
                self.properties.limits.min_interpolation_offset,
                other.properties.limits.min_interpolation_offset,
                r2nd <= f32::EPSILON
            ),
            float_eq!(
                self.properties.limits.max_interpolation_offset,
                other.properties.limits.max_interpolation_offset,
                r2nd <= f32::EPSILON
            ),
            self.properties.limits.sub_pixel_interpolation_offset_bits
                == other.properties.limits.sub_pixel_interpolation_offset_bits,
            self.properties.limits.max_framebuffer_width
                == other.properties.limits.max_framebuffer_width,
            self.properties.limits.max_framebuffer_height
                == other.properties.limits.max_framebuffer_height,
            self.properties.limits.max_framebuffer_layers
                == other.properties.limits.max_framebuffer_layers,
            self.properties.limits.framebuffer_color_sample_counts
                == other.properties.limits.framebuffer_color_sample_counts,
            self.properties.limits.framebuffer_depth_sample_counts
                == other.properties.limits.framebuffer_depth_sample_counts,
            self.properties.limits.framebuffer_stencil_sample_counts
                == other.properties.limits.framebuffer_stencil_sample_counts,
            self.properties
                .limits
                .framebuffer_no_attachments_sample_counts
                == other
                    .properties
                    .limits
                    .framebuffer_no_attachments_sample_counts,
            self.properties.limits.max_color_attachments
                == other.properties.limits.max_color_attachments,
            self.properties.limits.sampled_image_color_sample_counts
                == other.properties.limits.sampled_image_color_sample_counts,
            self.properties.limits.sampled_image_integer_sample_counts
                == other.properties.limits.sampled_image_integer_sample_counts,
            self.properties.limits.sampled_image_depth_sample_counts
                == other.properties.limits.sampled_image_depth_sample_counts,
            self.properties.limits.sampled_image_stencil_sample_counts
                == other.properties.limits.sampled_image_stencil_sample_counts,
            self.properties.limits.storage_image_sample_counts
                == other.properties.limits.storage_image_sample_counts,
            self.properties.limits.max_sample_mask_words
                == other.properties.limits.max_sample_mask_words,
            self.properties.limits.timestamp_compute_and_graphics
                == other.properties.limits.timestamp_compute_and_graphics,
            float_eq!(
                self.properties.limits.timestamp_period,
                other.properties.limits.timestamp_period,
                r2nd <= f32::EPSILON
            ),
            self.properties.limits.max_clip_distances == other.properties.limits.max_clip_distances,
            self.properties.limits.max_cull_distances == other.properties.limits.max_cull_distances,
            self.properties.limits.max_combined_clip_and_cull_distances
                == other.properties.limits.max_combined_clip_and_cull_distances,
            self.properties.limits.discrete_queue_priorities
                == other.properties.limits.discrete_queue_priorities,
            float_eq!(
                self.properties.limits.point_size_range,
                other.properties.limits.point_size_range,
                rmax_all <= f32::EPSILON
            ),
            float_eq!(
                self.properties.limits.line_width_range,
                other.properties.limits.line_width_range,
                rmax_all <= f32::EPSILON
            ),
            float_eq!(
                self.properties.limits.point_size_granularity,
                other.properties.limits.point_size_granularity,
                r2nd <= f32::EPSILON
            ),
            float_eq!(
                self.properties.limits.line_width_granularity,
                other.properties.limits.line_width_granularity,
                r2nd <= f32::EPSILON
            ),
            self.properties.limits.strict_lines == other.properties.limits.strict_lines,
            self.properties.limits.standard_sample_locations
                == other.properties.limits.standard_sample_locations,
            self.properties.limits.optimal_buffer_copy_offset_alignment
                == other.properties.limits.optimal_buffer_copy_offset_alignment,
            self.properties
                .limits
                .optimal_buffer_copy_row_pitch_alignment
                == other
                    .properties
                    .limits
                    .optimal_buffer_copy_row_pitch_alignment,
            self.properties.limits.non_coherent_atom_size
                == other.properties.limits.non_coherent_atom_size,
            self.properties
                .sparse_properties
                .residency_standard2_d_block_shape
                == other
                    .properties
                    .sparse_properties
                    .residency_standard2_d_block_shape,
            self.properties
                .sparse_properties
                .residency_standard2_d_multisample_block_shape
                == other
                    .properties
                    .sparse_properties
                    .residency_standard2_d_multisample_block_shape,
            self.properties
                .sparse_properties
                .residency_standard3_d_block_shape
                == other
                    .properties
                    .sparse_properties
                    .residency_standard3_d_block_shape,
            self.properties.sparse_properties.residency_aligned_mip_size
                == other
                    .properties
                    .sparse_properties
                    .residency_aligned_mip_size,
            self.properties
                .sparse_properties
                .residency_non_resident_strict
                == other
                    .properties
                    .sparse_properties
                    .residency_non_resident_strict,
            self.dynamic_primitive_topology_unrestricted
                == other.dynamic_primitive_topology_unrestricted,
        ];

        comparisons.into_iter().all(|comparison| comparison)
    }
}

unsafe impl Send for Properties {}
unsafe impl Sync for Properties {}
