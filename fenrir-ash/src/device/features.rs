use {
    crate::{
        device_does_not_support, instance::Guard as InstanceGuard, trace_called, trace_calling,
    },
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            PhysicalDevice, PhysicalDeviceExtendedDynamicState2FeaturesEXT,
            PhysicalDeviceExtendedDynamicState3FeaturesEXT, PhysicalDeviceFeatures,
            PhysicalDeviceFeatures2, PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT,
            PhysicalDeviceVertexInputDynamicStateFeaturesEXT, PhysicalDeviceVulkan12Features,
            PhysicalDeviceVulkan13Features,
        },
    },
    core::ops::DerefMut,
    educe::Educe,
    fenrir_error::{state_not_recoverable, Error},
    regex::{Captures, Regex},
    smallvec::SmallVec,
    tracing::debug_span,
};

extern crate alloc;

enum FeatureCheck<'a> {
    Vulkan12Feature { name: &'a str, available: bool },
    Vulkan13Feature { name: &'a str, available: bool },
}

#[derive(Clone, Debug)]
enum PNext<'a> {
    DynamicState2(PhysicalDeviceExtendedDynamicState2FeaturesEXT<'a>),
    DynamicState3(PhysicalDeviceExtendedDynamicState3FeaturesEXT<'a>),
    PageableDeviceLocalMemory(PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT<'a>),
    VertexInputDynamicState(PhysicalDeviceVertexInputDynamicStateFeaturesEXT<'a>),
    Vulkan12(PhysicalDeviceVulkan12Features<'a>),
    Vulkan13(PhysicalDeviceVulkan13Features<'a>),
}

#[derive(Clone, Debug, Educe)]
#[educe(Deref)]
pub(crate) struct Features {
    #[educe(Deref)]
    inner: PhysicalDeviceFeatures,
    physical_device: PhysicalDevice,
    instance: Arc<InstanceGuard>,
}

#[derive(Educe)]
#[educe(Deref)]
pub(crate) struct Features2<'a> {
    #[educe(Deref)]
    inner: PhysicalDeviceFeatures2<'a>,
    vulkan_1_2: PhysicalDeviceVulkan12Features<'a>,
    vulkan_1_3: PhysicalDeviceVulkan13Features<'a>,
    dynamic_state_2: PhysicalDeviceExtendedDynamicState2FeaturesEXT<'a>,
    dynamic_state_3: Option<PhysicalDeviceExtendedDynamicState3FeaturesEXT<'a>>,
    pageable_device_local_memory: Option<PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT<'a>>,
    vertex_input_dynamic_state: Option<PhysicalDeviceVertexInputDynamicStateFeaturesEXT<'a>>,
}

impl Features {
    #[inline]
    pub(super) fn new(
        instance: Arc<InstanceGuard>,
        physical_device: PhysicalDevice,
    ) -> Result<Self, Error> {
        let mut p_nexts = [
            PNext::Vulkan12(PhysicalDeviceVulkan12Features::default()),
            PNext::Vulkan13(PhysicalDeviceVulkan13Features::default()),
        ];

        let inner = {
            let features2 =
                Self::get_physical_device_features(&instance, physical_device, &mut p_nexts);

            features2.features
        };

        match (p_nexts[0].clone(), p_nexts[1].clone()) {
            (PNext::Vulkan12(vulkan_1_2), PNext::Vulkan13(vulkan_1_3)) => {
                Self::check_required_features(vulkan_1_2, vulkan_1_3)
            }
            _ => Err(state_not_recoverable("Unexpected branch reached")),
        }?;

        Ok(Self {
            inner,
            physical_device,
            instance,
        })
    }

    #[inline]
    fn check_required_features<'a>(
        vulkan_1_2: PhysicalDeviceVulkan12Features<'a>,
        vulkan_1_3: PhysicalDeviceVulkan13Features<'a>,
    ) -> Result<(), Error> {
        let required_feature_checks =
            Self::collect_required_features_checks(vulkan_1_2, vulkan_1_3);

        let features_checks_succeeded = |feature_check: &FeatureCheck<'_>| match feature_check {
            FeatureCheck::Vulkan12Feature { name: _, available }
            | FeatureCheck::Vulkan13Feature { name: _, available } => *available,
        };

        let all_required_features_available = required_feature_checks
            .iter()
            .all(features_checks_succeeded);

        if all_required_features_available {
            Ok(())
        } else {
            let features_not_available = |feature_check: &FeatureCheck<'_>| match feature_check {
                FeatureCheck::Vulkan12Feature { name: _, available }
                | FeatureCheck::Vulkan13Feature { name: _, available } => !*available,
            };

            let features_to_string = |feature_check| match feature_check {
                FeatureCheck::Vulkan12Feature { name, available: _ } => {
                    format!("PhysicalDeviceVulkan12Features::{name}")
                }
                FeatureCheck::Vulkan13Feature { name, available: _ } => {
                    format!("PhysicalDeviceVulkan13Features::{name}")
                }
            };

            let missing_required_features = required_feature_checks
                .into_iter()
                .filter(features_not_available)
                .map(features_to_string)
                .collect::<Vec<String>>();

            Err(device_does_not_support(&format!(
                "{missing_required_features:?}"
            )))
        }
    }

    #[inline]
    fn collect_required_features_checks<'a>(
        vulkan_1_2: PhysicalDeviceVulkan12Features<'a>,
        vulkan_1_3: PhysicalDeviceVulkan13Features<'a>,
    ) -> [FeatureCheck<'a>; 3] {
        let required_vulkan_1_2_features = Self::required_vulkan_1_2_features();

        let required_vulkan_1_3_features = Self::required_vulkan_1_3_features();

        [
            FeatureCheck::Vulkan12Feature {
                name: "timeline_semaphore",
                available: required_vulkan_1_2_features.timeline_semaphore
                    == vulkan_1_2.timeline_semaphore,
            },
            FeatureCheck::Vulkan13Feature {
                name: "dynamic_rendering",
                available: required_vulkan_1_3_features.dynamic_rendering
                    == vulkan_1_3.dynamic_rendering,
            },
            FeatureCheck::Vulkan13Feature {
                name: "synchronization2",
                available: required_vulkan_1_3_features.synchronization2
                    == vulkan_1_3.synchronization2,
            },
        ]
    }

    #[inline]
    pub(crate) fn dynamic_state_2(
        &self,
    ) -> Result<PhysicalDeviceExtendedDynamicState2FeaturesEXT<'_>, Error> {
        let mut p_nexts = [PNext::DynamicState2(
            PhysicalDeviceExtendedDynamicState2FeaturesEXT::default(),
        )];

        let _ =
            Self::get_physical_device_features(&self.instance, self.physical_device, &mut p_nexts);

        match p_nexts[0] {
            PNext::DynamicState2(dynamic_state_2) => Ok(dynamic_state_2),
            _ => Err(state_not_recoverable("reached unreachable code")),
        }
    }

    #[inline]
    pub(crate) fn dynamic_state_3(
        &self,
    ) -> Result<PhysicalDeviceExtendedDynamicState3FeaturesEXT<'_>, Error> {
        let mut p_nexts = [PNext::DynamicState3(
            PhysicalDeviceExtendedDynamicState3FeaturesEXT::default(),
        )];

        let _ =
            Self::get_physical_device_features(&self.instance, self.physical_device, &mut p_nexts);

        match p_nexts[0] {
            PNext::DynamicState3(dynamic_state_3) => Ok(dynamic_state_3),
            _ => Err(state_not_recoverable("reached unreachable code")),
        }
    }

    #[inline]
    pub(crate) fn features2(&self, extensions: &[&str]) -> Features2<'_> {
        Features2::new(&self.instance, self.physical_device, extensions)
    }

    #[cold]
    #[inline]
    fn format_get_physical_device_features2(
        args: (
            &Arc<InstanceGuard>,
            PhysicalDevice,
            &PhysicalDeviceFeatures2,
            &[PNext<'_>],
        ),
    ) -> String {
        let (instance, physical_device, features, p_nexts) = args;

        let regex_result = Regex::new(r"p_next: (0x[a-zA-Z0-9]+),");

        let p_next_chain = p_nexts.last().map_or_else(
            || "0x0".to_string(),
            |first_p_next| {
                let mut p_next_chain = format!("{first_p_next:?}");

                if let Ok(regex) = regex_result.as_ref() {
                    if 1 < p_nexts.len() {
                        p_nexts[0..(p_nexts.len() - 1)]
                            .iter()
                            .rev()
                            .for_each(|p_next| {
                                p_next_chain = regex
                                    .replace_all(&p_next_chain, |_: &Captures| {
                                        format!("p_next: {p_next:?}")
                                    })
                                    .to_string();
                            });
                    }
                }

                p_next_chain
            },
        );

        let features_string = Regex::new(r"p_next: (0x[a-zA-Z0-9]+),").map_or_else(
            |_| format!("{features:?}"),
            |regex| {
                regex
                    .replace_all(&format!("{features:?}"), |_: &Captures| {
                        format!("p_next: {p_next_chain:?}")
                    })
                    .to_string()
            },
        );

        format!("get_physical_device_features2(&self: {instance:?}, physical_device: {physical_device:?}, features: {features_string:?})")
    }

    #[inline]
    fn get_physical_device_features<'a>(
        instance: &Arc<InstanceGuard>,
        physical_device: PhysicalDevice,
        p_nexts: &'a mut [PNext<'_>],
    ) -> PhysicalDeviceFeatures2<'a> {
        let mut features2 = PhysicalDeviceFeatures2::default();

        let p_nexts_copy = p_nexts.iter().cloned().collect::<SmallVec<[PNext; 6]>>();

        p_nexts.iter_mut().for_each(|p_next| match p_next {
            PNext::DynamicState2(dynamic_state2) => {
                features2 = features2.push_next(dynamic_state2);
            }
            PNext::DynamicState3(dynamic_state3) => {
                features2 = features2.push_next(dynamic_state3);
            }
            PNext::PageableDeviceLocalMemory(device_local_memory) => {
                features2 = features2.push_next(device_local_memory);
            }
            PNext::VertexInputDynamicState(vertex_input_dynamic_state) => {
                features2 = features2.push_next(vertex_input_dynamic_state);
            }
            PNext::Vulkan12(vulkan12) => {
                features2 = features2.push_next(vulkan12);
            }
            PNext::Vulkan13(vulkan13) => {
                features2 = features2.push_next(vulkan13);
            }
        });

        trace_calling(
            Self::format_get_physical_device_features2,
            (instance, physical_device, &features2, &p_nexts_copy),
        );

        {
            let _span = debug_span!("get_physical_device_features2");

            unsafe { instance.get_physical_device_features2(physical_device, &mut features2) };
        }

        trace_called(
            Self::format_get_physical_device_features2,
            (instance, physical_device, &features2, &p_nexts_copy),
            None::<&VkResult<()>>,
        );

        features2
    }

    #[inline]
    fn required_vulkan_1_2_features<'a>() -> PhysicalDeviceVulkan12Features<'a> {
        PhysicalDeviceVulkan12Features::default().timeline_semaphore(true)
    }

    #[inline]
    fn required_vulkan_1_3_features<'a>() -> PhysicalDeviceVulkan13Features<'a> {
        PhysicalDeviceVulkan13Features::default()
            .synchronization2(true)
            .dynamic_rendering(true)
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn vulkan_1_2(&self) -> Result<PhysicalDeviceVulkan12Features<'_>, Error> {
        let mut p_nexts = [PNext::Vulkan12(PhysicalDeviceVulkan12Features::default())];

        {
            let _ = Self::get_physical_device_features(
                &self.instance,
                self.physical_device,
                &mut p_nexts,
            );
        }

        match p_nexts[0] {
            PNext::Vulkan12(vulkan_1_2) => Ok(vulkan_1_2),
            _ => Err(state_not_recoverable("reached unreachable code")),
        }
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn vulkan_1_3(&self) -> Result<PhysicalDeviceVulkan13Features<'_>, Error> {
        let mut p_nexts = [PNext::Vulkan13(PhysicalDeviceVulkan13Features::default())];

        {
            let _ = Self::get_physical_device_features(
                &self.instance,
                self.physical_device,
                &mut p_nexts,
            );
        }

        match p_nexts[0] {
            PNext::Vulkan13(vulkan_1_3) => Ok(vulkan_1_3),
            _ => Err(state_not_recoverable("reached unreachable code")),
        }
    }

    #[inline]
    pub(crate) fn vertex_input_dynamic_state(
        &self,
    ) -> Result<PhysicalDeviceVertexInputDynamicStateFeaturesEXT<'_>, Error> {
        let mut p_nexts = [PNext::VertexInputDynamicState(
            PhysicalDeviceVertexInputDynamicStateFeaturesEXT::default(),
        )];

        let _ =
            Self::get_physical_device_features(&self.instance, self.physical_device, &mut p_nexts);

        match p_nexts[0] {
            PNext::VertexInputDynamicState(vertex_input_dynamic_state) => {
                Ok(vertex_input_dynamic_state)
            }
            _ => Err(state_not_recoverable("reached unreachable code")),
        }
    }
}

impl Features2<'_> {
    #[inline]
    fn new(
        instance: &Arc<InstanceGuard>,
        physical_device: PhysicalDevice,
        extensions: &[&str],
    ) -> Self {
        let mut p_nexts = [
            PNext::VertexInputDynamicState(
                PhysicalDeviceVertexInputDynamicStateFeaturesEXT::default(),
            ),
            PNext::PageableDeviceLocalMemory(
                PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT::default(),
            ),
            PNext::DynamicState3(PhysicalDeviceExtendedDynamicState3FeaturesEXT::default()),
            PNext::DynamicState2(PhysicalDeviceExtendedDynamicState2FeaturesEXT::default()),
        ]
        .into_iter()
        .filter(|p_next| {
            let p_next_extension = match p_next {
                PNext::DynamicState2(_) => "VK_EXT_extended_dynamic_state2",
                PNext::DynamicState3(_) => "VK_EXT_extended_dynamic_state3",
                PNext::PageableDeviceLocalMemory(_) => "VK_EXT_pageable_device_local_memory",
                PNext::VertexInputDynamicState(_) => "VK_EXT_vertex_input_dynamic_state",
                PNext::Vulkan12(_) | PNext::Vulkan13(_) => "",
            };

            extensions
                .iter()
                .any(|extension| p_next_extension == *extension)
        })
        .collect::<SmallVec<[PNext; 6]>>();

        let features = {
            let features2 =
                Features::get_physical_device_features(instance, physical_device, &mut p_nexts);

            features2.features
        };

        p_nexts.push(PNext::Vulkan13(Features::required_vulkan_1_3_features()));

        p_nexts.push(PNext::Vulkan12(Features::required_vulkan_1_2_features()));

        let mut vertex_input_dynamic_state = None;
        let mut pageable_device_local_memory = None;
        let mut dynamic_state_3 = None;
        let mut dynamic_state_2 = PhysicalDeviceExtendedDynamicState2FeaturesEXT::default();
        let mut vulkan_1_3 = PhysicalDeviceVulkan13Features::default();
        let mut vulkan_1_2 = PhysicalDeviceVulkan12Features::default();

        p_nexts.into_iter().for_each(|p_next| match p_next {
            PNext::DynamicState2(physical_device_extended_dynamic_state2_features_ext) => {
                dynamic_state_2 = physical_device_extended_dynamic_state2_features_ext;
            }
            PNext::DynamicState3(physical_device_extended_dynamic_state3_features_ext) => {
                dynamic_state_3 = Some(physical_device_extended_dynamic_state3_features_ext);
            }
            PNext::PageableDeviceLocalMemory(
                physical_device_pageable_device_local_memory_features_ext,
            ) => {
                pageable_device_local_memory =
                    Some(physical_device_pageable_device_local_memory_features_ext);
            }
            PNext::VertexInputDynamicState(vertex_input_dynamic_state_features_ext) => {
                vertex_input_dynamic_state = Some(vertex_input_dynamic_state_features_ext);
            }
            PNext::Vulkan12(physical_device_vulkan12_features) => {
                vulkan_1_2 = physical_device_vulkan12_features;
            }
            PNext::Vulkan13(physical_device_vulkan13_features) => {
                vulkan_1_3 = physical_device_vulkan13_features;
            }
        });

        let inner = PhysicalDeviceFeatures2::default().features(features);

        Self {
            inner,
            vulkan_1_2,
            vulkan_1_3,
            dynamic_state_2,
            dynamic_state_3,
            pageable_device_local_memory,
            vertex_input_dynamic_state,
        }
    }
}

impl Eq for Features {}

impl PartialEq for Features {
    #[allow(clippy::too_many_lines)]
    fn eq(&self, other: &Self) -> bool {
        let comparisons = [
            self.robust_buffer_access == other.robust_buffer_access,
            self.full_draw_index_uint32 == other.full_draw_index_uint32,
            self.image_cube_array == other.image_cube_array,
            self.independent_blend == other.independent_blend,
            self.geometry_shader == other.geometry_shader,
            self.tessellation_shader == other.tessellation_shader,
            self.sample_rate_shading == other.sample_rate_shading,
            self.dual_src_blend == other.dual_src_blend,
            self.logic_op == other.logic_op,
            self.multi_draw_indirect == other.multi_draw_indirect,
            self.draw_indirect_first_instance == other.draw_indirect_first_instance,
            self.depth_clamp == other.depth_clamp,
            self.depth_bias_clamp == other.depth_bias_clamp,
            self.fill_mode_non_solid == other.fill_mode_non_solid,
            self.depth_bounds == other.depth_bounds,
            self.wide_lines == other.wide_lines,
            self.large_points == other.large_points,
            self.alpha_to_one == other.alpha_to_one,
            self.multi_viewport == other.multi_viewport,
            self.sampler_anisotropy == other.sampler_anisotropy,
            self.texture_compression_etc2 == other.texture_compression_etc2,
            self.texture_compression_astc_ldr == other.texture_compression_astc_ldr,
            self.texture_compression_bc == other.texture_compression_bc,
            self.occlusion_query_precise == other.occlusion_query_precise,
            self.pipeline_statistics_query == other.pipeline_statistics_query,
            self.vertex_pipeline_stores_and_atomics == other.vertex_pipeline_stores_and_atomics,
            self.fragment_stores_and_atomics == other.fragment_stores_and_atomics,
            self.shader_tessellation_and_geometry_point_size
                == other.shader_tessellation_and_geometry_point_size,
            self.shader_image_gather_extended == other.shader_image_gather_extended,
            self.shader_storage_image_extended_formats
                == other.shader_storage_image_extended_formats,
            self.shader_storage_image_multisample == other.shader_storage_image_multisample,
            self.shader_storage_image_read_without_format
                == other.shader_storage_image_read_without_format,
            self.shader_storage_image_write_without_format
                == other.shader_storage_image_write_without_format,
            self.shader_uniform_buffer_array_dynamic_indexing
                == other.shader_uniform_buffer_array_dynamic_indexing,
            self.shader_sampled_image_array_dynamic_indexing
                == other.shader_sampled_image_array_dynamic_indexing,
            self.shader_storage_buffer_array_dynamic_indexing
                == other.shader_storage_buffer_array_dynamic_indexing,
            self.shader_storage_image_array_dynamic_indexing
                == other.shader_storage_image_array_dynamic_indexing,
            self.shader_clip_distance == other.shader_clip_distance,
            self.shader_cull_distance == other.shader_cull_distance,
            self.shader_float64 == other.shader_float64,
            self.shader_int64 == other.shader_int64,
            self.shader_int16 == other.shader_int16,
            self.shader_resource_residency == other.shader_resource_residency,
            self.shader_resource_min_lod == other.shader_resource_min_lod,
            self.sparse_binding == other.sparse_binding,
            self.sparse_residency_buffer == other.sparse_residency_buffer,
            self.sparse_residency_image2_d == other.sparse_residency_image2_d,
            self.sparse_residency_image3_d == other.sparse_residency_image3_d,
            self.sparse_residency2_samples == other.sparse_residency2_samples,
            self.sparse_residency4_samples == other.sparse_residency4_samples,
            self.sparse_residency8_samples == other.sparse_residency8_samples,
            self.sparse_residency16_samples == other.sparse_residency16_samples,
            self.sparse_residency_aliased == other.sparse_residency_aliased,
            self.variable_multisample_rate == other.variable_multisample_rate,
            self.inherited_queries == other.inherited_queries,
            /*     self.vulkan_1_2.s_type == other.vulkan_1_2.s_type,
                 self.vulkan_1_2.p_next == other.vulkan_1_2.p_next,
                 self.vulkan_1_2.sampler_mirror_clamp_to_edge
                     == other.vulkan_1_2.sampler_mirror_clamp_to_edge,
                 self.vulkan_1_2.draw_indirect_count == other.vulkan_1_2.draw_indirect_count,
                 self.vulkan_1_2.storage_buffer8_bit_access
                     == other.vulkan_1_2.storage_buffer8_bit_access,
                 self.vulkan_1_2.uniform_and_storage_buffer8_bit_access
                     == other.vulkan_1_2.uniform_and_storage_buffer8_bit_access,
                 self.vulkan_1_2.storage_push_constant8 == other.vulkan_1_2.storage_push_constant8,
                 self.vulkan_1_2.shader_buffer_int64_atomics
                     == other.vulkan_1_2.shader_buffer_int64_atomics,
                 self.vulkan_1_2.shader_shared_int64_atomics
                     == other.vulkan_1_2.shader_shared_int64_atomics,
                 self.vulkan_1_2.shader_float16 == other.vulkan_1_2.shader_float16,
                 self.vulkan_1_2.shader_int8 == other.vulkan_1_2.shader_int8,
                 self.vulkan_1_2.descriptor_indexing == other.vulkan_1_2.descriptor_indexing,
                 self.vulkan_1_2
                     .shader_input_attachment_array_dynamic_indexing
                     == other
                         .vulkan_1_2
                         .shader_input_attachment_array_dynamic_indexing,
                 self.vulkan_1_2
                     .shader_uniform_texel_buffer_array_dynamic_indexing
                     == other
                         .vulkan_1_2
                         .shader_uniform_texel_buffer_array_dynamic_indexing,
                 self.vulkan_1_2
                     .shader_storage_texel_buffer_array_dynamic_indexing
                     == other
                         .vulkan_1_2
                         .shader_storage_texel_buffer_array_dynamic_indexing,
                 self.vulkan_1_2
                     .shader_uniform_buffer_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_uniform_buffer_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .shader_sampled_image_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_sampled_image_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .shader_storage_buffer_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_storage_buffer_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .shader_storage_image_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_storage_image_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .shader_input_attachment_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_input_attachment_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .shader_uniform_texel_buffer_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_uniform_texel_buffer_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .shader_storage_texel_buffer_array_non_uniform_indexing
                     == other
                         .vulkan_1_2
                         .shader_storage_texel_buffer_array_non_uniform_indexing,
                 self.vulkan_1_2
                     .descriptor_binding_uniform_buffer_update_after_bind
                     == other
                         .vulkan_1_2
                         .descriptor_binding_uniform_buffer_update_after_bind,
                 self.vulkan_1_2
                     .descriptor_binding_sampled_image_update_after_bind
                     == other
                         .vulkan_1_2
                         .descriptor_binding_sampled_image_update_after_bind,
                 self.vulkan_1_2
                     .descriptor_binding_storage_image_update_after_bind
                     == other
                         .vulkan_1_2
                         .descriptor_binding_storage_image_update_after_bind,
                 self.vulkan_1_2
                     .descriptor_binding_storage_buffer_update_after_bind
                     == other
                         .vulkan_1_2
                         .descriptor_binding_storage_buffer_update_after_bind,
                 self.vulkan_1_2
                     .descriptor_binding_uniform_texel_buffer_update_after_bind
                     == other
                         .vulkan_1_2
                         .descriptor_binding_uniform_texel_buffer_update_after_bind,
                 self.vulkan_1_2
                     .descriptor_binding_storage_texel_buffer_update_after_bind
                     == other
                         .vulkan_1_2
                         .descriptor_binding_storage_texel_buffer_update_after_bind,
                 self.vulkan_1_2
                     .descriptor_binding_update_unused_while_pending
                     == other
                         .vulkan_1_2
                         .descriptor_binding_update_unused_while_pending,
                 self.vulkan_1_2.descriptor_binding_partially_bound
                     == other.vulkan_1_2.descriptor_binding_partially_bound,
                 self.vulkan_1_2.descriptor_binding_variable_descriptor_count
                     == other
                         .vulkan_1_2
                         .descriptor_binding_variable_descriptor_count,
                 self.vulkan_1_2.runtime_descriptor_array == other.vulkan_1_2.runtime_descriptor_array,
                 self.vulkan_1_2.sampler_filter_minmax == other.vulkan_1_2.sampler_filter_minmax,
                 self.vulkan_1_2.scalar_block_layout == other.vulkan_1_2.scalar_block_layout,
                 self.vulkan_1_2.imageless_framebuffer == other.vulkan_1_2.imageless_framebuffer,
                 self.vulkan_1_2.uniform_buffer_standard_layout
                     == other.vulkan_1_2.uniform_buffer_standard_layout,
                 self.vulkan_1_2.shader_subgroup_extended_types
                     == other.vulkan_1_2.shader_subgroup_extended_types,
                 self.vulkan_1_2.separate_depth_stencil_layouts
                     == other.vulkan_1_2.separate_depth_stencil_layouts,
                 self.vulkan_1_2.host_query_reset == other.vulkan_1_2.host_query_reset,
                 self.vulkan_1_2.timeline_semaphore == other.vulkan_1_2.timeline_semaphore,
                 self.vulkan_1_2.buffer_device_address == other.vulkan_1_2.buffer_device_address,
                 self.vulkan_1_2.buffer_device_address_capture_replay
                     == other.vulkan_1_2.buffer_device_address_capture_replay,
                 self.vulkan_1_2.buffer_device_address_multi_device
                     == other.vulkan_1_2.buffer_device_address_multi_device,
                 self.vulkan_1_2.vulkan_memory_model == other.vulkan_1_2.vulkan_memory_model,
                 self.vulkan_1_2.vulkan_memory_model_device_scope
                     == other.vulkan_1_2.vulkan_memory_model_device_scope,
                 self.vulkan_1_2
                     .vulkan_memory_model_availability_visibility_chains
                     == other
                         .vulkan_1_2
                         .vulkan_memory_model_availability_visibility_chains,
                 self.vulkan_1_2.shader_output_viewport_index
                     == other.vulkan_1_2.shader_output_viewport_index,
                 self.vulkan_1_2.shader_output_layer == other.vulkan_1_2.shader_output_layer,
                 self.vulkan_1_2.subgroup_broadcast_dynamic_id
                     == other.vulkan_1_2.subgroup_broadcast_dynamic_id,
                 self.vulkan_1_3.s_type == other.vulkan_1_3.s_type,
                 self.vulkan_1_3.p_next == other.vulkan_1_3.p_next,
                 self.vulkan_1_3.robust_image_access == other.vulkan_1_3.robust_image_access,
                 self.vulkan_1_3.inline_uniform_block == other.vulkan_1_3.inline_uniform_block,
                 self.vulkan_1_3
                     .descriptor_binding_inline_uniform_block_update_after_bind
                     == other
                         .vulkan_1_3
                         .descriptor_binding_inline_uniform_block_update_after_bind,
                 self.vulkan_1_3.pipeline_creation_cache_control
                     == other.vulkan_1_3.pipeline_creation_cache_control,
                 self.vulkan_1_3.private_data == other.vulkan_1_3.private_data,
                 self.vulkan_1_3.shader_demote_to_helper_invocation
                     == other.vulkan_1_3.shader_demote_to_helper_invocation,
                 self.vulkan_1_3.shader_terminate_invocation
                     == other.vulkan_1_3.shader_terminate_invocation,
                 self.vulkan_1_3.subgroup_size_control == other.vulkan_1_3.subgroup_size_control,
                 self.vulkan_1_3.compute_full_subgroups == other.vulkan_1_3.compute_full_subgroups,
                 self.vulkan_1_3.synchronization2 == other.vulkan_1_3.synchronization2,
                 self.vulkan_1_3.texture_compression_astc_hdr
                     == other.vulkan_1_3.texture_compression_astc_hdr,
                 self.vulkan_1_3.shader_zero_initialize_workgroup_memory
                     == other.vulkan_1_3.shader_zero_initialize_workgroup_memory,
                 self.vulkan_1_3.dynamic_rendering == other.vulkan_1_3.dynamic_rendering,
                 self.vulkan_1_3.shader_integer_dot_product
                     == other.vulkan_1_3.shader_integer_dot_product,
                 self.vulkan_1_3.maintenance4 == other.vulkan_1_3.maintenance4,
                 self.dynamic_state_2.s_type == other.dynamic_state_2.s_type,
                 self.dynamic_state_2.p_next == other.dynamic_state_2.p_next,
                 self.dynamic_state_2.extended_dynamic_state2
                     == other.dynamic_state_2.extended_dynamic_state2,
                 self.dynamic_state_2.extended_dynamic_state2_logic_op
                     == other.dynamic_state_2.extended_dynamic_state2_logic_op,
                 self.dynamic_state_2
                     .extended_dynamic_state2_patch_control_points
                     == other
                         .dynamic_state_2
                         .extended_dynamic_state2_patch_control_points,
                 self.dynamic_state_3.s_type == other.dynamic_state_3.s_type,
                 self.dynamic_state_3.p_next == other.dynamic_state_3.p_next,
                 self.dynamic_state_3
                     .extended_dynamic_state3_tessellation_domain_origin
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_tessellation_domain_origin,
                 self.dynamic_state_3
                     .extended_dynamic_state3_depth_clamp_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_depth_clamp_enable,
                 self.dynamic_state_3.extended_dynamic_state3_polygon_mode
                     == other.dynamic_state_3.extended_dynamic_state3_polygon_mode,
                 self.dynamic_state_3
                     .extended_dynamic_state3_rasterization_samples
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_rasterization_samples,
                 self.dynamic_state_3.extended_dynamic_state3_sample_mask
                     == other.dynamic_state_3.extended_dynamic_state3_sample_mask,
                 self.dynamic_state_3
                     .extended_dynamic_state3_alpha_to_coverage_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_alpha_to_coverage_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_alpha_to_one_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_alpha_to_one_enable,
                 self.dynamic_state_3.extended_dynamic_state3_logic_op_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_logic_op_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_color_blend_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_color_blend_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_color_blend_equation
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_color_blend_equation,
                 self.dynamic_state_3
                     .extended_dynamic_state3_color_write_mask
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_color_write_mask,
                 self.dynamic_state_3
                     .extended_dynamic_state3_rasterization_stream
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_rasterization_stream,
                 self.dynamic_state_3
                     .extended_dynamic_state3_conservative_rasterization_mode
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_conservative_rasterization_mode,
                 self.dynamic_state_3
                     .extended_dynamic_state3_extra_primitive_overestimation_size
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_extra_primitive_overestimation_size,
                 self.dynamic_state_3
                     .extended_dynamic_state3_depth_clip_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_depth_clip_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_sample_locations_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_sample_locations_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_color_blend_advanced
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_color_blend_advanced,
                 self.dynamic_state_3
                     .extended_dynamic_state3_provoking_vertex_mode
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_provoking_vertex_mode,
                 self.dynamic_state_3
                     .extended_dynamic_state3_line_rasterization_mode
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_line_rasterization_mode,
                 self.dynamic_state_3
                     .extended_dynamic_state3_line_stipple_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_line_stipple_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_depth_clip_negative_one_to_one
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_depth_clip_negative_one_to_one,
                 self.dynamic_state_3
                     .extended_dynamic_state3_viewport_w_scaling_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_viewport_w_scaling_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_viewport_swizzle
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_viewport_swizzle,
                 self.dynamic_state_3
                     .extended_dynamic_state3_coverage_to_color_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_coverage_to_color_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_coverage_to_color_location
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_coverage_to_color_location,
                 self.dynamic_state_3
                     .extended_dynamic_state3_coverage_modulation_mode
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_coverage_modulation_mode,
                 self.dynamic_state_3
                     .extended_dynamic_state3_coverage_modulation_table_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_coverage_modulation_table_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_coverage_modulation_table
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_coverage_modulation_table,
                 self.dynamic_state_3
                     .extended_dynamic_state3_coverage_reduction_mode
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_coverage_reduction_mode,
                 self.dynamic_state_3
                     .extended_dynamic_state3_representative_fragment_test_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_representative_fragment_test_enable,
                 self.dynamic_state_3
                     .extended_dynamic_state3_shading_rate_image_enable
                     == other
                         .dynamic_state_3
                         .extended_dynamic_state3_shading_rate_image_enable,
                 self.pageable_device_local_memory.s_type == other.pageable_device_local_memory.s_type,
                 self.pageable_device_local_memory.p_next == other.pageable_device_local_memory.p_next,
                 self.pageable_device_local_memory
                     .pageable_device_local_memory
                     == other
                         .pageable_device_local_memory
                         .pageable_device_local_memory,
            */
        ];

        comparisons.into_iter().all(|comparison| comparison)
    }
}

unsafe impl Send for Features {}
unsafe impl Sync for Features {}

impl DerefMut for Features2<'_> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.inner.p_next = <*mut PhysicalDeviceVulkan12Features>::cast(&mut self.vulkan_1_2);

        self.vulkan_1_2.p_next = <*mut PhysicalDeviceVulkan13Features>::cast(&mut self.vulkan_1_3);

        let mut p_next = &mut self.vulkan_1_3.p_next;

        *p_next =
            <*mut PhysicalDeviceExtendedDynamicState2FeaturesEXT>::cast(&mut self.dynamic_state_2);

        p_next = &mut self.dynamic_state_2.p_next;

        if let Some(dynamic_state_3) = self.dynamic_state_3.as_mut() {
            *p_next = <*mut PhysicalDeviceExtendedDynamicState3FeaturesEXT>::cast(dynamic_state_3);

            p_next = &mut dynamic_state_3.p_next;
        }

        if let Some(pageable_device_local_memory) = self.pageable_device_local_memory.as_mut() {
            *p_next = <*mut PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT>::cast(
                pageable_device_local_memory,
            );

            p_next = &mut pageable_device_local_memory.p_next;
        }

        if let Some(vertex_input_dynamic_state) = self.vertex_input_dynamic_state.as_mut() {
            *p_next = <*mut PhysicalDeviceVertexInputDynamicStateFeaturesEXT>::cast(
                vertex_input_dynamic_state,
            );
        }

        &mut self.inner
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use {
        crate::{
            device::Features,
            instance::tests::{init_tracing, instance_and_dependencies, load_shader_compilers},
        },
        tokio::test,
    };

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn eq() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (instance, _) = instance_and_dependencies(shader_compiler);

            let instance_guard = instance.guard();

            let physical_devices_result = unsafe { instance_guard.enumerate_physical_devices() };

            assert!(physical_devices_result.is_ok());

            physical_devices_result
                .unwrap()
                .into_iter()
                .for_each(|physical_device| {
                    assert_eq!(
                        Features::new(instance_guard.clone(), physical_device),
                        Features::new(instance_guard.clone(), physical_device)
                    );
                });
        }
    }
}
