#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate,
    clippy::type_complexity
)]

pub(crate) use crate::{device::Device, instance::Instance};
use {
    abi_stable::{
        export_root_module,
        prefix_type::PrefixTypeTrait,
        sabi_extern_fn,
        sabi_trait::TD_CanDowncast,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
            RVec, Tuple2,
        },
        DynTrait,
    },
    ash::prelude::VkResult,
    bytemuck::bytes_of,
    core::{
        any::TypeId,
        fmt::{Debug, Display},
        marker::PhantomData,
    },
    fenrir_error::{invalid_argument, not_supported, Error},
    fenrir_hal::instance::{
        module::{Module, ModuleRef},
        Inner, Metadata,
    },
    sleipnir::ffi::runtime::Handle,
    tracing::{enabled, error, metadata::LevelFilter, trace, Level},
    tracing_subscriber::fmt::format::FmtSpan,
};

mod attachment;
mod color;
mod command;
mod compare;
mod cull;
mod depth;
mod device;
mod draw;
mod fence;
mod graphics;
mod image;
mod instance;
mod logical;
mod polygon;
mod queue;
mod render_pass;
mod rendering;
mod result;
mod scissor;
mod semaphore;
mod shader;
mod stencil;
mod viewport;

type Attachment = crate::attachment::Attachment;
type Depth = crate::depth::Depth;
type Image = crate::image::Image;
type RenderPass = crate::render_pass::RenderPass;

#[export_root_module]
fn instantiate_root_module() -> ModuleRef {
    Module {
        new,
        devices,
        handle,
    }
    .leak_into_prefix()
}

#[inline]
#[must_use]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
pub fn new(
    metadata: Metadata,
    runtime: Handle,
    allocator_manager: fenrir_hal::allocator::manager::Manager,
) -> RResult<DynTrait<'static, RArc<()>, Inner>, Error> {
    let maybe_tracing_level = match metadata.tracing_level {
        1 => Some(LevelFilter::ERROR),
        2 => Some(LevelFilter::WARN),
        3 => Some(LevelFilter::INFO),
        4 => Some(LevelFilter::DEBUG),
        5 => Some(LevelFilter::TRACE),
        _ => None,
    };

    if let Some(tracing_level) = maybe_tracing_level {
        let _init_result = tracing_subscriber::fmt()
            .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
            .with_max_level(tracing_level)
            .try_init();
    }

    match Instance::new(&metadata.into(), runtime, allocator_manager) {
        Ok(instance) => ROk(DynTrait::from_ptr(RArc::new(instance))),
        Err(code) => RErr(code),
    }
}

#[inline]
#[must_use]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
pub fn devices(
    instance: DynTrait<'static, RArc<()>, Inner>,
) -> RResult<RVec<fenrir_hal::device::stable_abi::Trait_TO<RArc<()>>>, Error> {
    instance.downcast_as::<Instance>().map_or_else(
        |error| RErr(failed_to_cast(&instance, PhantomData::<Instance>, error)),
        |instance_| match instance_.devices() {
            Err(code) => RErr(code),
            Ok(devices) => ROk(devices
                .into_iter()
                .map(|device| {
                    fenrir_hal::device::stable_abi::Trait_TO::from_ptr(
                        RArc::new(device),
                        TD_CanDowncast,
                    )
                })
                .collect()),
        },
    )
}

#[inline]
#[must_use]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
pub fn handle(
    instance: DynTrait<'static, RArc<()>, Inner>,
) -> RResult<Tuple2<*const u8, usize>, Error> {
    instance.downcast_as::<Instance>().map_or_else(
        |error| RErr(failed_to_cast(&instance, PhantomData::<Instance>, error)),
        |instance_| {
            let bytes = bytes_of(instance_.handle());

            ROk((bytes.as_ptr(), bytes.len()).into())
        },
    )
}

#[inline]
#[must_use]
pub(crate) fn device_does_not_support(feature: &str) -> Error {
    not_supported(&format!("device doesn't support {feature}"))
}

#[inline]
#[must_use]
pub(crate) fn failed_to_cast<T, E, U>(source: &T, _: PhantomData<U>, error: E) -> Error
where
    T: Debug,
    E: Display,
    U: 'static,
{
    let target = TypeId::of::<U>();

    invalid_argument(&format!(
        "failed to cast {source:?} to a {target:?}: {error}"
    ))
}

#[inline]
pub(crate) fn trace_called<ARGS, R>(
    f: fn(ARGS) -> String,
    args: ARGS,
    maybe_result: Option<&VkResult<R>>,
) where
    R: Debug,
{
    match maybe_result {
        None => {
            if enabled!(Level::TRACE) {
                trace!("Called {}", f(args));
            }
        }
        Some(result) => {
            if (result.is_ok() && enabled!(Level::TRACE))
                || (result.is_err() && enabled!(Level::ERROR))
            {
                let message = format!("Called {} -> {result:?}", f(args));

                if result.is_ok() {
                    trace!(message);
                } else {
                    error!(message);
                }
            }
        }
    }
}

#[inline]
pub(crate) fn trace_calling<ARGS>(f: fn(ARGS) -> String, args: ARGS) {
    if enabled!(Level::TRACE) {
        trace!("Calling {}", f(args));
    }
}
