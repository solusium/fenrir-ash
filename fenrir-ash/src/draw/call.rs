mod id;

pub(crate) type ID = crate::draw::call::id::ID;

#[derive(Debug, Eq, PartialEq)]
pub(crate) struct Call {}

impl Default for Call {
    #[inline]
    fn default() -> Self {
        Self {}
    }
}
