use {
    crate::{
        draw::{call::ID, Call},
        graphics::pipeline::{
            ColorBlend, DepthStencil, DynamicStates, InputAssembly, Multisample, Rasterization,
            Tessellation, VertexInput, Viewport,
        },
    },
    alloc::vec::Vec,
    async_lock::{RwLock, RwLockReadGuard},
    core::ops::Deref,
};

extern crate alloc;

#[derive(Debug)]
pub(crate) struct Calls {
    inner: RwLock<Vec<(ID, Call)>>,
}

impl Calls {
    #[inline]
    pub(crate) fn new(
        color_blend: ColorBlend,
        depth_stencil: DepthStencil,
        dynamic_states: DynamicStates,
        input_assembly: InputAssembly,
        multisample: Multisample,
        rasterization: Rasterization,
        tessellation: Tessellation,
        vertex_input: VertexInput,
        viewport: Viewport,
        view_mask: u32,
    ) -> Self {
        let vec = vec![(
            ID::new(
                color_blend,
                depth_stencil,
                dynamic_states,
                input_assembly,
                multisample,
                rasterization,
                tessellation,
                vertex_input,
                viewport,
                view_mask,
            ),
            Call {},
        )];

        Self {
            inner: RwLock::new(vec),
        }
    }

    #[inline]
    pub(crate) async fn read(&self) -> RwLockReadGuard<Vec<(ID, Call)>> {
        self.inner.read().await
    }
}

impl Default for Calls {
    #[inline]
    fn default() -> Self {
        Self {
            inner: RwLock::new(Vec::new()),
        }
    }
}

impl Eq for Calls {}

impl PartialEq for Calls {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.inner.read_blocking().deref() == other.inner.read_blocking().deref()
    }
}
