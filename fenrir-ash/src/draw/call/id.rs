use crate::graphics::pipeline::{
    ColorBlend, DepthStencil, DynamicStates, InputAssembly, Multisample, Rasterization,
    Tessellation, VertexInput, Viewport,
};

#[derive(Debug, Eq, PartialEq)]
pub(crate) struct ID {
    color_blend: ColorBlend,
    depth_stencil: DepthStencil,
    dynamic_states: DynamicStates,
    input_assembly: InputAssembly,
    multisample: Multisample,
    rasterization: Rasterization,
    tessellation: Tessellation,
    vertex_input: VertexInput,
    viewport: Viewport,
    view_mask: u32,
}

impl ID {
    #[inline]
    #[allow(clippy::too_many_arguments)]
    pub(crate) const fn new(
        color_blend: ColorBlend,
        depth_stencil: DepthStencil,
        dynamic_states: DynamicStates,
        input_assembly: InputAssembly,
        multisample: Multisample,
        rasterization: Rasterization,
        tessellation: Tessellation,
        vertex_input: VertexInput,
        viewport: Viewport,
        view_mask: u32,
    ) -> Self {
        Self {
            color_blend,
            depth_stencil,
            dynamic_states,
            input_assembly,
            multisample,
            rasterization,
            tessellation,
            vertex_input,
            viewport,
            view_mask,
        }
    }

    #[inline]
    pub(crate) const fn color_blend(&self) -> &ColorBlend {
        &self.color_blend
    }

    #[inline]
    pub(crate) const fn depth_stencil(&self) -> &DepthStencil {
        &self.depth_stencil
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn dynamic_states(&self) -> &DynamicStates {
        &self.dynamic_states
    }

    #[inline]
    pub(crate) const fn input_assembly(&self) -> &InputAssembly {
        &self.input_assembly
    }

    #[inline]
    pub(crate) const fn multisample(&self) -> &Multisample {
        &self.multisample
    }

    #[inline]
    pub(crate) const fn rasterization(&self) -> &Rasterization {
        &self.rasterization
    }

    #[inline]
    pub(crate) const fn tessellation(&self) -> &Tessellation {
        &self.tessellation
    }

    #[inline]
    pub(crate) const fn vertex_input(&self) -> &VertexInput {
        &self.vertex_input
    }

    #[inline]
    pub(crate) const fn viewport(&self) -> &Viewport {
        &self.viewport
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn view_mask(&self) -> u32 {
        self.view_mask
    }
}
