use {
    crate::{
        result::{device_lost, out_of_device_memory, out_of_host_memory, unexpected_error},
        trace_called, trace_calling,
    },
    abi_stable::std_types::RArc,
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            PipelineStageFlags2, SemaphoreCreateInfo, SemaphoreSignalInfo, SemaphoreSubmitInfo,
            SemaphoreTypeCreateInfo, SemaphoreWaitInfo,
        },
    },
    async_lock::RwLock,
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        ops::Deref,
        pin::{pin, Pin},
        task::{Context, Poll},
    },
    fenrir_error::Error,
    futures_core::stream::Stream,
    sleipnir::task::yield_now,
    tracing::trace_span,
};

extern crate alloc;

mod create_info;

type CreateInfo = crate::semaphore::create_info::CreateInfo;

#[derive(Debug, PartialEq)]
pub(crate) struct Guard {
    semaphore: ash::vk::Semaphore,
    device: RArc<crate::device::Guard>,
    timeline: u64,
}

pub(crate) struct Semaphore {
    inner: Arc<RwLock<Guard>>,
    create_info: CreateInfo,
    future: Option<Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>>,
}

pub(crate) struct SubmitInfo<'a> {
    submit_info: SemaphoreSubmitInfo<'a>,
}

impl Guard {
    #[inline]
    pub(crate) const fn timeline(&self) -> u64 {
        self.timeline
    }

    #[cold]
    #[inline]
    fn format_destroy_semaphore(args: (&crate::device::Guard, ash::vk::Semaphore)) -> String {
        let (device, semaphore) = args;

        format!("destroy_semaphore(&self: {device:?}, semaphore: {semaphore:?}, allocation_callbacks: None)")
    }
}

impl Semaphore {
    #[inline]
    pub(crate) fn new(device: RArc<crate::device::Guard>) -> Result<Self, Error> {
        let future = None;

        let timeline = 1;

        let create_info = crate::semaphore::CreateInfo::new();

        let mut ash_create_info_wrapper = create_info.ash();

        let ash_create_info = ash_create_info_wrapper.deref_mut();

        let create_semaphore = || {
            let _span = trace_span!("create_semaphore");

            unsafe { device.create_semaphore(ash_create_info, None) }
        };

        let trace_args = (device.deref(), ash_create_info);

        trace_calling(Self::format_create_semaphore, trace_args);

        let semaphore_ = create_semaphore();

        trace_called(Self::format_create_semaphore, trace_args, Some(&semaphore_));

        let inner = match semaphore_ {
            Ok(semaphore) => Ok(Arc::new(RwLock::new(Guard {
                semaphore,
                device,
                timeline,
            }))),
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }?;

        Ok(Self {
            inner,
            create_info,
            future,
        })
    }

    #[cold]
    #[inline]
    fn format_create_semaphore(args: (&crate::device::Guard, &SemaphoreCreateInfo)) -> String {
        let (device, create_info) = args;

        let s_type = create_info.s_type;

        let type_create_info = create_info.p_next.cast::<SemaphoreTypeCreateInfo>();

        let flags = create_info.flags;

        let create_info_string = format!(
            "SemaphoreCreateInfo {{ s_type: {s_type:?}, p_next: {type_create_info:?}, flags: \
            {flags:?} }}"
        );

        format!("create_semaphore(&self: {device:?}, create_info: {create_info_string}, allocation_callbacks: \
                None)")
    }

    #[cold]
    #[inline]
    fn format_signal_semaphore(args: (&crate::device::Guard, SemaphoreSignalInfo)) -> String {
        let (device, signal_info) = args;

        format!("signal_semaphore(&self: {device:?}, signal_info: {signal_info:?})")
    }

    #[cold]
    #[inline]
    fn format_wait_semaphores(
        args: (&RArc<crate::device::Guard>, &[ash::vk::Semaphore], &[u64]),
    ) -> String {
        let (device, semaphores, timelines) = args;

        let wait_info = SemaphoreWaitInfo::default()
            .semaphores(semaphores)
            .values(timelines);

        let timeout = u64::MAX;

        format!("wait_semaphore(&self: {device:?}, wait_info: {wait_info:?}, timeout: {timeout})")
    }

    #[inline]
    pub(crate) fn guard(&self) -> Arc<RwLock<Guard>> {
        self.inner.clone()
    }

    #[inline]
    fn poll_wait_future(
        &mut self,
        cx: &mut Context,
    ) -> Poll<&mut Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>> {
        if self.future.is_some() {
            Poll::Ready(self.future.as_mut().expect("we just checked this is Some"))
        } else {
            match pin!(self.inner.read()).poll(cx) {
                Poll::Pending => Poll::Pending,
                Poll::Ready(semaphore) => {
                    let device = semaphore.device.clone();
                    let timeline = semaphore.timeline;
                    let semaphore = semaphore.semaphore;

                    Poll::Ready(
                        self.future
                            .insert(Box::pin(Self::wait(device, semaphore, timeline))),
                    )
                }
            }
        }
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) async fn signal(&self) -> Result<(), Error> {
        let semaphore = self.inner.read().await;

        let device = &*semaphore.device;

        let signal_info = SemaphoreSignalInfo::default()
            .semaphore(**semaphore)
            .value(semaphore.timeline);

        let signal_semaphore = || {
            let _span = trace_span!("signal_semaphore");

            unsafe { device.signal_semaphore(&signal_info) }
        };

        let trace_args = (device, signal_info);

        trace_calling(Self::format_signal_semaphore, trace_args);

        let signal_result = signal_semaphore();

        trace_called(
            Self::format_signal_semaphore,
            trace_args,
            Some(&signal_result),
        );

        drop(semaphore);

        match signal_result {
            Ok(result) => Ok(result),
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    async fn wait(
        device: RArc<crate::device::Guard>,
        semaphore: ash::vk::Semaphore,
        timeline: u64,
    ) -> Result<(), Error> {
        let device = &device;

        let semaphores = [semaphore];

        let timelines = [timeline];

        let trace_args = (device, semaphores.as_slice(), timelines.as_slice());

        let check_semaphore = || async move {
            let wait_info = SemaphoreWaitInfo::default()
                .semaphores(&semaphores)
                .values(&timelines);

            unsafe { device.wait_semaphores(&wait_info, 0) }
        };

        let wait_semaphores = || async move {
            let _span = trace_span!("wait_semaphores");

            while let Err(result) = check_semaphore().await {
                match result {
                    ash::vk::Result::TIMEOUT => {
                        yield_now().await;
                    }
                    error => return Err(error),
                }
            }

            Ok(())
        };

        trace_calling(Self::format_wait_semaphores, trace_args);

        let wait_semaphores_result = wait_semaphores().await;

        trace_called(
            Self::format_wait_semaphores,
            trace_args,
            Some(&wait_semaphores_result),
        );

        match wait_semaphores_result {
            Ok(()) => Ok(()),
            Err(error) => Err(match error {
                ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }
}

impl SubmitInfo<'_> {
    #[inline]
    pub(crate) fn new(
        semaphore: ash::vk::Semaphore,
        value: u64,
        stage_mask: PipelineStageFlags2,
    ) -> Self {
        let submit_info = SemaphoreSubmitInfo::default()
            .semaphore(semaphore)
            .value(value)
            .stage_mask(stage_mask);

        Self { submit_info }
    }
}

impl Debug for Semaphore {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Semaphore")
            .field("semaphore", &self.inner)
            .field("create_info", &self.create_info)
            .finish_non_exhaustive()
    }
}

impl Deref for Guard {
    type Target = ash::vk::Semaphore;

    fn deref(&self) -> &Self::Target {
        &self.semaphore
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        let device = self.device.deref();

        let semaphore = self.semaphore;

        let trace_args = (device, semaphore);

        trace_calling(Self::format_destroy_semaphore, trace_args);

        unsafe { device.destroy_semaphore(semaphore, None) };

        trace_called(
            Self::format_destroy_semaphore,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

unsafe impl Send for Semaphore {}
unsafe impl Sync for Semaphore {}

impl Future for Semaphore {
    type Output = Result<(), Error>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        match this.poll_wait_future(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(wait_future) => match pin!(wait_future).poll(cx) {
                Poll::Pending => Poll::Pending,
                Poll::Ready(result) => {
                    this.future = None;

                    Poll::Ready(result)
                }
            },
        }
    }
}

impl Stream for Semaphore {
    type Item = Result<(), Error>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let this = self.get_mut();

        match this.poll_wait_future(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(wait_future) => match pin!(wait_future).poll(cx) {
                Poll::Pending => Poll::Pending,
                Poll::Ready(result) => {
                    if let Err(code) = result {
                        Poll::Ready(Some(Err(code)))
                    } else {
                        let semaphore_future = this.inner.write();

                        let pinned_semaphore_future = pin!(semaphore_future);

                        match pinned_semaphore_future.poll(cx) {
                            Poll::Pending => Poll::Pending,
                            Poll::Ready(mut semaphore) => {
                                this.future = None;

                                semaphore.timeline += 1;

                                Poll::Ready(Some(Ok(())))
                            }
                        }
                    }
                }
            },
        }
    }
}

impl<'a> Deref for SubmitInfo<'a> {
    type Target = SemaphoreSubmitInfo<'a>;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.submit_info
    }
}

unsafe impl Send for SubmitInfo<'_> {}
unsafe impl Sync for SubmitInfo<'_> {}

#[cfg(test)]
mod tests {
    use {
        crate::{
            instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
            semaphore::Semaphore,
        },
        futures_util::StreamExt,
        rand::{rng, Rng},
        tokio::test,
    };

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                let semaphore = Semaphore::new(device.guard());

                assert!(semaphore.is_ok());
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn signal_and_wait_semaphore() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            let mut rng = rng();

            for device in devices {
                let semaphore_result = Semaphore::new(device.guard());

                assert!(semaphore_result.is_ok());

                let mut semaphore = semaphore_result.unwrap();

                for _ in 1..rng.random_range(1..128) {
                    assert!(semaphore.signal().await.is_ok());

                    let next_result = semaphore.next().await;

                    assert!(next_result.is_some());

                    assert!(next_result.unwrap().is_ok());
                }
            }
        }
    }
}
