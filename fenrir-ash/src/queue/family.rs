use {
    crate::{
        device::{Guard, QueueCreateInfo},
        queue::Queue,
    },
    abi_stable::std_types::RArc,
    alloc::sync::Arc,
    ash::vk::{QueueFamilyProperties2, QueueFlags},
    async_lock::Mutex,
    core::{cmp::Ordering, fmt::Debug},
    fenrir_error::Error,
    sleipnir::ffi::runtime::Handle,
    smallvec::SmallVec,
};

extern crate alloc;

#[derive(Clone)]
pub(crate) struct Family {
    command_pools: Arc<Mutex<Vec<Arc<crate::command::pool::Guard>>>>,
    queues: SmallVec<[ash::vk::Queue; 16]>,
    flags: QueueFlags,
    create_info: QueueCreateInfo,
    device: RArc<Guard>,
    queue_submitter: crate::queue::submitter::Submitter,
    runtime: Handle,
}

impl Family {
    #[inline]
    pub(crate) fn new(
        properties: QueueFamilyProperties2<'_>,
        create_info: QueueCreateInfo,
        device: RArc<Guard>,
        queue_submitter: crate::queue::submitter::Submitter,
        runtime: Handle,
    ) -> Self {
        let flags = properties.queue_family_properties.queue_flags;

        let device_ref = &device;

        let queues = create_info
            .priorities()
            .iter()
            .enumerate()
            .map(|(usize_queue_index, _)| {
                let queue_index = usize_queue_index
                    .try_into()
                    .map_or_else(|_| unreachable!(), |queue_index| queue_index);

                unsafe { device_ref.get_device_queue(create_info.index(), queue_index) }
            })
            .collect();

        let command_pools = Arc::new(Mutex::new(Vec::new()));

        Self {
            command_pools,
            queues,
            flags,
            create_info,
            device,
            queue_submitter,
            runtime,
        }
    }

    #[inline]
    pub(crate) async fn command_pool(&self) -> Result<crate::command::pool::Pool, Error> {
        let mut command_pools = self.command_pools.lock().await;

        let maybe_guard = command_pools.pop();

        let guard = match maybe_guard {
            Some(guard) => Ok(guard),
            None => Ok(Arc::new(crate::command::pool::Guard::new(
                self.clone(),
                self.device.clone(),
            )?)),
        }?;

        drop(command_pools);

        Ok(crate::command::Pool::new(guard, self.runtime.clone()).await)
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn create_info(&self) -> &QueueCreateInfo {
        &self.create_info
    }

    #[inline]
    pub(crate) fn device(&self) -> RArc<Guard> {
        self.device.clone()
    }

    #[inline]
    pub(crate) fn flags(&self) -> SmallVec<[QueueFlags; 8]> {
        const AVAILABLE_FLAGS: [QueueFlags; 7] = [
            QueueFlags::GRAPHICS,
            QueueFlags::COMPUTE,
            QueueFlags::TRANSFER,
            QueueFlags::SPARSE_BINDING,
            QueueFlags::PROTECTED,
            QueueFlags::VIDEO_DECODE_KHR,
            QueueFlags::VIDEO_ENCODE_KHR,
        ];

        AVAILABLE_FLAGS
            .into_iter()
            .filter(|flag| self.flags.contains(*flag))
            .collect()
    }

    #[inline]
    pub(crate) const fn index(&self) -> u32 {
        self.create_info.index()
    }

    #[inline]
    pub(crate) fn len(&self) -> usize {
        self.queues.len()
    }

    #[inline]
    pub(crate) async fn push(&self, command_pool: Arc<crate::command::pool::Guard>) {
        self.command_pools.lock().await.push(command_pool);
    }

    #[inline]
    pub(crate) fn queues(&self) -> SmallVec<[Queue; 16]> {
        self.queues.iter().map(|queue| Queue::new(*queue)).collect()
    }

    #[inline]
    pub(crate) fn queue_submitter(&self) -> crate::queue::submitter::Submitter {
        self.queue_submitter.clone()
    }
}

impl Debug for Family {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Family")
            .field(
                "device",
                &format_args!("ash::Device{{ handle: {:?} }}", self.device.handle()),
            )
            .field("flags", &self.flags)
            .field("create_info", &self.create_info)
            .finish_non_exhaustive()
    }
}

impl Eq for Family {}

impl PartialEq for Family {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        (self.queues == other.queues)
            && (self.flags == other.flags)
            && (self.create_info == other.create_info)
            && (self.device == other.device)
    }
}

impl PartialOrd for Family {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Family {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        if self.device < other.device {
            Ordering::Less
        } else if self.device > other.device {
            Ordering::Greater
        } else if self.queues < other.queues {
            Ordering::Less
        } else if self.queues > other.queues {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

unsafe impl Send for Family {}
unsafe impl Sync for Family {}
