use {
    crate::{
        command,
        fence::{small_wait, Fence, WaitFor},
        queue,
        result::{device_lost, out_of_device_memory, out_of_host_memory, unexpected_error},
        semaphore, trace_called, trace_calling,
    },
    alloc::sync::Arc,
    ash::vk::{
        CommandBuffer, CommandBufferSubmitInfo, FenceCreateFlags, PipelineStageFlags2,
        SemaphoreSubmitInfo, SubmitInfo2,
    },
    async_lock::{Mutex, RwLock},
    core::{
        fmt::{Debug, Formatter},
        mem::take,
        slice::from_ref,
    },
    fenrir_error::{state_not_recoverable, Error},
    futures_channel::{
        mpsc::{unbounded, UnboundedReceiver, UnboundedSender},
        oneshot,
    },
    futures_util::{future::join_all, FutureExt, StreamExt},
    sleipnir::ffi::{runtime::Handle, task::JoinHandle},
    smallvec::SmallVec,
    std::slice::from_raw_parts,
    tracing::trace_span,
    vec_collections::{vecmap, AbstractVecMap, VecMap},
};

extern crate alloc;

type Fences = VecMap<[(queue::family::Family, SmallVec<[Fence; 16]>); 4]>;

#[derive(PartialEq)]
#[allow(clippy::large_enum_variant)]
enum Message {
    Submit(Metadata),
    Shutdown,
}

pub(crate) struct Metadata {
    queue_family: queue::family::Family,
    command_buffers: SmallVec<[CommandBuffer; 1]>,
    signal_semaphores: SmallVec<[(Arc<RwLock<semaphore::Guard>>, PipelineStageFlags2); 2]>,
}

#[derive(Debug)]
struct Implementation {
    sender: UnboundedSender<Message>,
    task: Option<Mutex<Task>>,
    runtime: Handle,
}

#[derive(Clone, Debug)]
pub(crate) struct Submitter {
    implementation: Arc<Implementation>,
}

struct Task {
    join_handle: Option<JoinHandle<()>>,
    return_receiver: Option<oneshot::Receiver<Result<(), Error>>>,
    error: Option<Error>,
}

impl Implementation {
    #[async_backtrace::framed]
    async fn drop(task_mutex: Mutex<Task>, sender: UnboundedSender<Message>) {
        let mut task = task_mutex.lock().await;

        if task.error.is_none() {
            let _send_resuult = sender.unbounded_send(Message::Shutdown);

            match take(&mut task.join_handle) {
                None => unreachable!(),
                Some(join_handle) => {
                    let _join_handle_result = join_handle.await;
                }
            }
            match take(&mut task.return_receiver) {
                None => unreachable!(),
                Some(return_receiver) => {
                    let _receiv_result = return_receiver.await;
                }
            }
        }
    }
}

impl Metadata {
    pub(crate) fn new(
        queue_family: queue::family::Family,
        command_buffers_: &[CommandBuffer],
        signal_semaphores_: &[(Arc<RwLock<semaphore::Guard>>, PipelineStageFlags2)],
    ) -> Self {
        let command_buffers = command_buffers_.into();

        let signal_semaphores = signal_semaphores_.into();

        Self {
            queue_family,
            command_buffers,
            signal_semaphores,
        }
    }
}

impl Submitter {
    pub(crate) fn new(runtime: Handle) -> Self {
        let error = None;

        let (task_return_sender, task_return_receiver) = oneshot::channel();

        let return_receiver = Some(task_return_receiver);

        let (sender, receiver) = unbounded();

        let join_handle = Some(runtime.spawn(Self::task(receiver, task_return_sender)));

        let task = Some(Mutex::new(Task {
            join_handle,
            return_receiver,
            error,
        }));

        let implementation = Arc::new(Implementation {
            sender,
            task,
            runtime,
        });

        Self { implementation }
    }

    #[cold]
    #[inline]
    fn format_queue_submit2(
        args: (
            &crate::device::Guard,
            ash::vk::Queue,
            &[SubmitInfo2],
            ash::vk::Fence,
        ),
    ) -> String {
        let (device, queue, submit_infos, fence) = args;

        let submit_infos_string = submit_infos.iter().map(Self::format_submit_info).fold(
            String::new(),
            |mut result, current| {
                if !result.is_empty() {
                    result += ", ";
                }

                result += &current;

                result
            },
        );

        format!("queue_submit2(&self: {device:?}, queue: {queue:?}, submits: [{submit_infos_string}], fence: {fence:?})")
    }

    #[inline]
    fn format_submit_info(submit_info: &SubmitInfo2) -> String {
        let wait_semaphores_infos = unsafe {
            from_raw_parts(
                submit_info.p_wait_semaphore_infos,
                submit_info.wait_semaphore_info_count as usize,
            )
        };

        let command_buffer_infos = unsafe {
            from_raw_parts(
                submit_info.p_command_buffer_infos,
                submit_info.command_buffer_info_count as usize,
            )
        };

        let signal_semaphore_infos = unsafe {
            from_raw_parts(
                submit_info.p_signal_semaphore_infos,
                submit_info.signal_semaphore_info_count as usize,
            )
        };

        format!(
            "SubmitInfo2 {{ s_type: {:?}, p_next: {:?}, flags: {:?}, wait_semaphore_info_count: {}, p_wait_semaphore_infos: {:?}, command_buffer_info_count: {}, p_command_buffer_infos: {:?}, signal_semaphore_info_count: {}, p_signal_semaphore_infos: {:?}",
            submit_info.s_type,
            submit_info.p_next,
            submit_info.flags,
            submit_info.wait_semaphore_info_count,
            wait_semaphores_infos,
            submit_info.command_buffer_info_count,
            command_buffer_infos,
            submit_info.signal_semaphore_info_count,
            signal_semaphore_infos
        )
    }

    #[allow(clippy::needless_pass_by_value)]
    fn get_queue_family_fences(
        fences: &mut Fences,
        queue_family: queue::family::Family,
    ) -> Result<&SmallVec<[Fence; 16]>, Error> {
        if fences.get(&queue_family).is_none() {
            let fences_result = (0..queue_family.len())
                .map(|_| Fence::new(queue_family.device(), Some(FenceCreateFlags::SIGNALED)))
                .collect::<Result<SmallVec<[Fence; 16]>, Error>>();

            match fences_result {
                Err(error) => {
                    return Err(error);
                }
                Ok(fences_) => {
                    if fences.insert(queue_family.clone(), fences_).is_some() {
                        unreachable!()
                    }
                }
            }
        }

        fences.get(&queue_family).map_or_else(|| unreachable!(), Ok)
    }

    async fn queue_submit(metadata: Metadata, index: usize, fences: &[Fence]) -> Result<(), Error> {
        let queues = metadata.queue_family.queues();

        let queue = &queues[index];

        let command_buffer_submit_infos = metadata
            .command_buffers
            .iter()
            .map(|command_buffer| command::buffer::SubmitInfo::new(*command_buffer))
            .collect::<SmallVec<[command::buffer::SubmitInfo; 1]>>();

        let signal_semaphore_submit_infos = join_all(metadata.signal_semaphores.iter().map(
            |(semaphore_rwlock, stage_mask)| async move {
                let semaphore = semaphore_rwlock.read().await;

                semaphore::SubmitInfo::new(**semaphore, semaphore.timeline(), *stage_mask)
            },
        ))
        .await;

        let command_buffer_infos = command_buffer_submit_infos
            .into_iter()
            .map(|command_buffer_submit_info| *command_buffer_submit_info)
            .collect::<SmallVec<[CommandBufferSubmitInfo; 1]>>();

        let signal_semaphore_infos = signal_semaphore_submit_infos
            .into_iter()
            .map(|signal_semaphore_submit_info| *signal_semaphore_submit_info)
            .collect::<SmallVec<[SemaphoreSubmitInfo; 2]>>();

        let submit_info = SubmitInfo2::default()
            .command_buffer_infos(&command_buffer_infos)
            .signal_semaphore_infos(&signal_semaphore_infos)
            .wait_semaphore_infos(&[]);

        let fence = &fences[index];

        fence.reset()?;

        let device = metadata.queue_family.device();

        let queue_submit2 = || {
            trace_span!("queue_submit2");

            unsafe { device.queue_submit2(**queue, from_ref(&submit_info), **fence) }
        };

        let trace_args = (&*device, **queue, from_ref(&submit_info), **fence);

        trace_calling(Self::format_queue_submit2, trace_args);

        let submit_result = queue_submit2();

        trace_called(Self::format_queue_submit2, trace_args, Some(&submit_result));

        match submit_result {
            Ok(result) => Ok(result),
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    pub(crate) async fn submit(&self, metadata: Metadata) -> Result<(), Error> {
        let send_result = self
            .implementation
            .sender
            .unbounded_send(Message::Submit(metadata));

        match send_result {
            Ok(result) => Ok(result),
            Err(_) => match self.implementation.task.as_ref() {
                None => unreachable!(),
                Some(task_mutex) => {
                    let mut task = task_mutex.lock().await;

                    match task.error.as_ref() {
                        Some(error) => Err(error.clone()),
                        None => match take(&mut task.join_handle) {
                            None => unreachable!(),
                            Some(join_handle) => match join_handle.await {
                                Err(_) => {
                                    let error = state_not_recoverable(
                                        "task paniced or got unexpectetly cancelled",
                                    );

                                    task.error = Some(error.clone());

                                    drop(task);

                                    Err(error)
                                }
                                Ok(()) => match take(&mut task.return_receiver) {
                                    None => unreachable!(),
                                    Some(return_receiver) => {
                                        if return_receiver.await.is_ok() {
                                            unreachable!()
                                        } else {
                                            let error = state_not_recoverable("");

                                            task.error = Some(error.clone());

                                            drop(task);

                                            Err(error)
                                        }
                                    }
                                },
                            },
                        },
                    }
                }
            },
        }
    }

    async fn task(
        mut receiver: UnboundedReceiver<Message>,
        task_return: oneshot::Sender<Result<(), Error>>,
    ) {
        let mut cache = Vec::new();

        let mut fences: VecMap<[(queue::family::Family, SmallVec<[Fence; 16]>); 4]> = vecmap!();

        while let Some(first_message) = receiver.next().await {
            cache.push(first_message);

            while let Some(Some(another_message)) = receiver.next().now_or_never() {
                cache.push(another_message);
            }

            if cache.iter().any(|message| Message::Shutdown == *message) {
                break;
            }

            cache.reverse();

            while let Some(Message::Submit(metadata)) = cache.pop() {
                match Self::get_queue_family_fences(&mut fences, metadata.queue_family.clone()) {
                    Err(error) => {
                        if task_return.send(Err(error)).is_err() {
                            unreachable!();
                        }

                        return;
                    }
                    Ok(queue_family_fences) => {
                        match small_wait::<16>(queue_family_fences, WaitFor::Any) {
                            Err(error) => {
                                if task_return.send(Err(error)).is_err() {
                                    unreachable!();
                                }

                                return;
                            }
                            Ok(small_wait_future) => match small_wait_future.await {
                                Err(error) => {
                                    if task_return.send(Err(error)).is_err() {
                                        unreachable!();
                                    }

                                    return;
                                }
                                Ok(maybe_index) => match maybe_index {
                                    None => {
                                        let task_return_send_result =
                                            task_return.send(Err(state_not_recoverable(
                                                "Expected queue index. Found None.",
                                            )));

                                        if task_return_send_result.is_err() {
                                            unreachable!();
                                        }

                                        return;
                                    }
                                    Some(index) => {
                                        let queue_submit_result = Self::queue_submit(
                                            metadata,
                                            index,
                                            queue_family_fences,
                                        )
                                        .await;

                                        if let Err(result) = queue_submit_result {
                                            if task_return.send(Err(result)).is_err() {
                                                unreachable!();
                                            }

                                            return;
                                        }
                                    }
                                },
                            },
                        }
                    }
                }
            }

            cache.clear();
        }

        if task_return.send(Ok(())).is_err() {
            unreachable!();
        }
    }
}

impl Drop for Implementation {
    #[inline]
    fn drop(&mut self) {
        match take(&mut self.task) {
            None => unreachable!(),
            Some(task) => {
                let sender = self.sender.clone();

                let _join_handle = self
                    .runtime
                    .spawn(async move { Self::drop(task, sender).await });
            }
        }
    }
}

unsafe impl Send for Message {}
unsafe impl Sync for Message {}

impl PartialEq for Metadata {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let different_queue_families = self.queue_family != other.queue_family;
        let different_command_buffers = self.command_buffers != other.command_buffers;

        if different_queue_families || different_command_buffers {
            false
        } else {
            let all_equal = self
                .signal_semaphores
                .iter()
                .zip(other.signal_semaphores.iter())
                .all(
                    |((left_arc, left_stage_mask), (right_arc, right_stage_mask))| {
                        (left_stage_mask == right_stage_mask) && Arc::ptr_eq(left_arc, right_arc)
                    },
                );

            all_equal
        }
    }
}

impl Debug for Task {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Task")
            .field("return_receiver", &self.return_receiver)
            .field("error", &self.error)
            .finish_non_exhaustive()
    }
}

unsafe impl Send for Task {}
