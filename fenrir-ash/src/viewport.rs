use {ash::vk::Viewport as AshViewport, fenrir_hal::Viewport as HalViewport};

#[inline]
pub(crate) const fn ash_to_hal(viewport: &AshViewport) -> HalViewport {
    HalViewport::new(
        viewport.x,
        viewport.y,
        viewport.width,
        viewport.height,
        viewport.min_depth,
        viewport.max_depth,
    )
}
