use {
    ash::vk::{
        AttachmentLoadOp, AttachmentStoreOp, ClearValue, ImageLayout, ImageView,
        RenderingAttachmentInfo, ResolveModeFlags,
    },
    core::{
        fmt::{Debug, Formatter},
        slice::from_raw_parts,
    },
    std::ptr::addr_of,
};

mod info;

pub(crate) type Info = crate::rendering::info::Info;

#[derive(Clone)]
pub(crate) struct AttachmentInfo {
    image_view: ImageView,
    image_layout: ImageLayout,
    resolve_mode: ResolveModeFlags,
    resolve_image_view: ImageView,
    resolve_image_layout: ImageLayout,
    load_operation: AttachmentLoadOp,
    store_operation: AttachmentStoreOp,
    clear_value: ClearValue,
}

impl AttachmentInfo {
    #[inline]
    #[allow(dead_code)]
    pub(crate) fn ash(&self) -> RenderingAttachmentInfo<'_> {
        RenderingAttachmentInfo::default()
            .image_view(self.image_view)
            .image_layout(self.image_layout)
            .resolve_mode(self.resolve_mode)
            .resolve_image_view(self.resolve_image_view)
            .resolve_image_layout(self.resolve_image_layout)
            .load_op(self.load_operation)
            .store_op(self.store_operation)
            .clear_value(self.clear_value)
    }
}

impl Debug for AttachmentInfo {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("AttachmentInfo")
            .field("image_view", &self.image_view)
            .field("image_layout", &self.image_layout)
            .field("resolve_mode", &self.resolve_mode)
            .field("resolve_image_view", &self.resolve_image_view)
            .field("resolve_image_layout", &self.resolve_image_layout)
            .field("load_op", &self.load_operation)
            .field("store_op", &self.store_operation)
            .field(
                "clear_value",
                &format_args!("ash::ClearValue{{ {:?} }}", unsafe {
                    from_raw_parts(
                        addr_of!(self.clear_value).cast::<u8>(),
                        size_of::<ClearValue>(),
                    )
                },),
            )
            .finish()
    }
}

impl Eq for AttachmentInfo {}

impl From<RenderingAttachmentInfo<'_>> for AttachmentInfo {
    #[inline]
    fn from(value: RenderingAttachmentInfo) -> Self {
        Self {
            image_view: value.image_view,
            image_layout: value.image_layout,
            resolve_mode: value.resolve_mode,
            resolve_image_view: value.resolve_image_view,
            resolve_image_layout: value.resolve_image_layout,
            load_operation: value.load_op,
            store_operation: value.store_op,
            clear_value: value.clear_value,
        }
    }
}

impl PartialEq for AttachmentInfo {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let clear_values_are_equal = unsafe {
            from_raw_parts(
                addr_of!(self.clear_value).cast::<u8>(),
                size_of::<ClearValue>(),
            ) == from_raw_parts(
                addr_of!(other.clear_value).cast::<u8>(),
                size_of::<ClearValue>(),
            )
        };

        let values = [
            self.image_view == other.image_view,
            self.image_layout == other.image_layout,
            self.resolve_mode == other.resolve_mode,
            self.resolve_image_view == other.resolve_image_view,
            self.resolve_image_layout == other.resolve_image_layout,
            self.load_operation == other.load_operation,
            self.store_operation == other.store_operation,
            clear_values_are_equal,
        ];

        values.into_iter().all(|value| value)
    }
}
