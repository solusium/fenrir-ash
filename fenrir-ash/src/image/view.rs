use {
    crate::{
        device::Guard as DeviceGuard,
        image::Image,
        result::{
            invalid_opaque_capture_address, out_of_device_memory, out_of_host_memory,
            unexpected_error,
        },
        trace_called, trace_calling,
    },
    abi_stable::{sabi_trait::TD_CanDowncast, std_types::RArc},
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            ComponentMapping, ComponentSwizzle, Format, ImageAspectFlags, ImageSubresourceRange,
            ImageType, ImageView, ImageViewCreateFlags, ImageViewCreateInfo, ImageViewType,
        },
    },
    core::ops::{Deref, Range},
    fenrir_error::{argument_out_of_domain, Error},
    fenrir_hal::{
        color::component::{Mapping, Swizzle},
        texture::{
            stable_abi::Trait_TO,
            view::{stable_abi::Trait as HalViewTrait, Metadata as HalViewMetadata},
            Aspect, SubresourceRange, Texture,
        },
    },
    smallvec::SmallVec,
    tracing::{debug_span, trace_span},
};

extern crate alloc;

#[derive(Debug, Eq, PartialEq)]
struct Guard {
    inner: ImageView,
    image: Image,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(super) struct Metadata {
    image: Image,
    components: Mapping,
    subresource_range: SubresourceRange,
}

#[derive(Clone, Debug)]
pub(crate) struct View {
    inner: Arc<Guard>,
    flags: ImageViewCreateFlags,
    inner_type: ImageViewType,
    format: Format,
    components: ComponentMapping,
    subresource_range: ImageSubresourceRange,
}

impl Metadata {
    #[inline]
    pub(super) const fn new(
        image: Image,
        components: Mapping,
        subresource_range: SubresourceRange,
    ) -> Self {
        Self {
            image,
            components,
            subresource_range,
        }
    }
}

impl Guard {
    #[cold]
    #[inline]
    fn format_destroy_image(args: (&DeviceGuard, ImageView)) -> String {
        let (device, view) = args;

        format!("destroy_image_view(&self: {device:?}, image_view: {view:?}, allocation_callbacks: None)")
    }
}

impl View {
    pub(super) fn new(metadata: Metadata) -> Result<Self, Error> {
        let image = metadata.image;

        let image_create_info = image.create_info();

        let guard = image.guard();

        let create_info = ImageViewCreateInfo::default()
            .image(**guard)
            .view_type(Self::get_view_type(image_create_info.image_type))
            .format(image_create_info.format)
            .components(Self::cast_components_fenrir_to_ash(metadata.components))
            .subresource_range(Self::cast_subresource_range_fenrir_to_ash(
                metadata.subresource_range,
            )?);

        let device = guard.device();

        let trace_args = (&*device, &create_info);

        let create_image_view = || unsafe {
            trace_span!("create_image_view");

            device.create_image_view(&create_info, None)
        };

        trace_calling(Self::format_create_image_view, trace_args);

        let view_result = create_image_view();

        trace_called(
            Self::format_create_image_view,
            trace_args,
            Some(&view_result),
        );

        let subresource_range = create_info.subresource_range;

        let components = create_info.components;

        let format = create_info.format;

        let inner_type = create_info.view_type;

        let flags = create_info.flags;

        let inner = Arc::new(Guard {
            inner: Self::cast_view_result(view_result)?,
            image,
        });

        Ok(Self {
            inner,
            flags,
            inner_type,
            format,
            components,
            subresource_range,
        })
    }

    #[inline]
    pub(crate) fn ash(&self) -> ImageView {
        self.inner.inner
    }

    #[inline]
    fn cast_components_fenrir_to_ash(components: Mapping) -> ComponentMapping {
        [
            components.r(),
            components.g(),
            components.b(),
            components.a(),
        ]
        .into_iter()
        .map(|swizzle| match swizzle {
            Swizzle::Identity => ComponentSwizzle::IDENTITY,
            Swizzle::Zero => ComponentSwizzle::ZERO,
            Swizzle::One => ComponentSwizzle::ONE,
            Swizzle::R => ComponentSwizzle::R,
            Swizzle::G => ComponentSwizzle::G,
            Swizzle::B => ComponentSwizzle::B,
            Swizzle::A => ComponentSwizzle::A,
        })
        .enumerate()
        .fold(
            ComponentMapping::default(),
            |components_, (index, swizzle)| match index {
                0 => ComponentMapping::default()
                    .r(swizzle)
                    .g(components_.g)
                    .b(components_.b)
                    .a(components_.a),
                1 => ComponentMapping::default()
                    .r(components_.r)
                    .g(swizzle)
                    .b(components_.b)
                    .a(components_.a),
                2 => ComponentMapping::default()
                    .r(components_.r)
                    .g(components_.g)
                    .b(swizzle)
                    .a(components_.a),
                3 => ComponentMapping::default()
                    .r(components_.r)
                    .g(components_.g)
                    .b(components_.b)
                    .a(swizzle),
                _ => unreachable!(),
            },
        )
    }

    #[inline]
    fn cast_subresource_range_fenrir_to_ash(
        subresource_range: SubresourceRange,
    ) -> Result<ImageSubresourceRange, Error> {
        let aspect_mask = subresource_range.aspects().iter().fold(
            ImageAspectFlags::default(),
            |aspect_mask_, aspect| match *aspect {
                Aspect::None => unreachable!(),
                Aspect::Color => aspect_mask_ | ImageAspectFlags::COLOR,
                Aspect::Depth => aspect_mask_ | ImageAspectFlags::DEPTH,
                Aspect::Stencil => aspect_mask_ | ImageAspectFlags::STENCIL,
            },
        );

        let values = [
            subresource_range.base_mip_level(),
            subresource_range.level_count(),
            subresource_range.base_array_layer(),
            subresource_range.layer_count(),
        ]
        .into_iter()
        .map(|usize_value| {
            usize_value.try_into().map_or_else(
                |_| {
                    Err(argument_out_of_domain(
                        Range {
                            start: 0u64.into(),
                            end: (u64::from(u32::MAX) + 1).into(),
                        },
                        usize_value.into(),
                    ))
                },
                Ok,
            )
        })
        .collect::<Result<SmallVec<[u32; 4]>, _>>()?;

        Ok(ImageSubresourceRange::default()
            .aspect_mask(aspect_mask)
            .base_mip_level(values[0])
            .level_count(values[1])
            .base_array_layer(values[2])
            .layer_count(values[3]))
    }

    #[inline]
    fn cast_view_result(
        view_result: ash::prelude::VkResult<ImageView>,
    ) -> Result<ImageView, Error> {
        match view_result {
            Ok(view) => Ok(view),
            Err(error) => Err(match error {
                ash::vk::Result::ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS => {
                    invalid_opaque_capture_address()
                }
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    pub(crate) const fn format(&self) -> Format {
        self.format
    }

    #[cold]
    #[inline]
    fn format_create_image_view(args: (&DeviceGuard, &ImageViewCreateInfo)) -> String {
        let (device, create_info) = args;

        format!(
            "create_image_view(&self: {device:?}, create_info: {create_info:?}, allocation_callbacks: None)"
        )
    }

    #[inline]
    fn get_view_type(image_type: ImageType) -> ImageViewType {
        if ImageType::TYPE_1D == image_type {
            ImageViewType::TYPE_1D
        } else if ImageType::TYPE_2D == image_type {
            ImageViewType::TYPE_2D
        } else {
            ImageViewType::TYPE_3D
        }
    }

    #[inline]
    pub(crate) fn image(&self) -> &Image {
        &self.inner.image
    }
}

impl Deref for View {
    type Target = ImageView;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.inner.inner
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        let device = self.image.guard().device();

        trace_calling(Self::format_destroy_image, (&device, self.inner));

        {
            debug_span!("destroy_image_view");

            unsafe { device.destroy_image_view(self.inner, None) };
        }

        trace_called(
            Self::format_destroy_image,
            (&device, self.inner),
            None::<&VkResult<()>>,
        );
    }
}

impl Eq for View {}

impl PartialEq for View {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.inner == other.inner,
            self.flags == other.flags,
            self.inner_type == other.inner_type,
            self.format == other.format,
            self.components.r == other.components.r,
            self.components.g == other.components.g,
            self.components.b == other.components.b,
            self.components.a == other.components.a,
            self.subresource_range.aspect_mask == other.subresource_range.aspect_mask,
            self.subresource_range.base_mip_level == other.subresource_range.base_mip_level,
            self.subresource_range.level_count == other.subresource_range.level_count,
            self.subresource_range.base_array_layer == other.subresource_range.base_array_layer,
            self.subresource_range.layer_count == other.subresource_range.layer_count,
        ];

        values.into_iter().all(|value| value)
    }
}

impl HalViewTrait for View {
    #[inline]
    fn metadata(&self) -> HalViewMetadata {
        let components = &self.components;

        let mapping = [components.r, components.g, components.b, components.a]
            .into_iter()
            .map(|component| {
                if ComponentSwizzle::IDENTITY == component {
                    Swizzle::Identity
                } else if ComponentSwizzle::ZERO == component {
                    Swizzle::Zero
                } else if ComponentSwizzle::ONE == component {
                    Swizzle::One
                } else if ComponentSwizzle::R == component {
                    Swizzle::R
                } else if ComponentSwizzle::G == component {
                    Swizzle::G
                } else if ComponentSwizzle::B == component {
                    Swizzle::B
                } else {
                    assert_eq!(ComponentSwizzle::A, component);

                    Swizzle::A
                }
            })
            .enumerate()
            .fold(
                Mapping::default(),
                |mapping, (index, swizzle)| match index {
                    0 => Mapping::new(swizzle, mapping.g(), mapping.b(), mapping.a()),
                    1 => Mapping::new(mapping.r(), swizzle, mapping.b(), mapping.a()),
                    2 => Mapping::new(mapping.r(), mapping.g(), swizzle, mapping.a()),
                    3 => Mapping::new(mapping.r(), mapping.g(), mapping.b(), swizzle),
                    _ => unreachable!(),
                },
            );

        let ash_subresource_range = &self.subresource_range;

        let aspects = [
            (ImageAspectFlags::COLOR, Aspect::Color),
            (ImageAspectFlags::DEPTH, Aspect::Depth),
            (ImageAspectFlags::STENCIL, Aspect::Stencil),
        ]
        .into_iter()
        .filter_map(|(aspect_flag, aspect)| {
            if (ash_subresource_range.aspect_mask & aspect_flag) == aspect_flag {
                Some(aspect)
            } else {
                None
            }
        })
        .collect::<SmallVec<[Aspect; 4]>>();

        let unexpected_large_value = "unexpected large value not representable by usize";

        let subresource_range = SubresourceRange::new(
            &aspects,
            ash_subresource_range
                .base_mip_level
                .try_into()
                .expect(unexpected_large_value),
            ash_subresource_range
                .level_count
                .try_into()
                .expect(unexpected_large_value),
            ash_subresource_range
                .base_array_layer
                .try_into()
                .expect(unexpected_large_value),
            ash_subresource_range
                .layer_count
                .try_into()
                .expect(unexpected_large_value),
        );

        let texture = Texture::new(Trait_TO::from_ptr(
            RArc::new(self.inner.image.clone()),
            TD_CanDowncast,
        ));

        HalViewMetadata::new(texture, mapping, subresource_range)
    }
}

unsafe impl Send for View {}
unsafe impl Sync for View {}
