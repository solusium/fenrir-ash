use {
    crate::Image,
    ash::vk::{
        AccessFlags2, ImageLayout, ImageMemoryBarrier2, ImageSubresourceRange, PipelineStageFlags2,
    },
};

pub(crate) struct MemoryBarrier {
    image: Image,
    subresource_range: ImageSubresourceRange,
    destination: ImageLayout,
}

impl MemoryBarrier {
    #[inline]
    pub(crate) const fn new(
        image: Image,
        subresource_range: ImageSubresourceRange,
        destination: ImageLayout,
    ) -> Self {
        Self {
            image,
            subresource_range,
            destination,
        }
    }

    pub(crate) async fn ash(&self) -> ImageMemoryBarrier2 {
        let determine_stage_flags = |layout| match layout {
            ImageLayout::PREINITIALIZED => PipelineStageFlags2::HOST,
            ImageLayout::TRANSFER_DST_OPTIMAL | ImageLayout::TRANSFER_SRC_OPTIMAL => {
                PipelineStageFlags2::TRANSFER
            }
            ImageLayout::COLOR_ATTACHMENT_OPTIMAL => PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT,
            ImageLayout::DEPTH_ATTACHMENT_OPTIMAL => {
                PipelineStageFlags2::EARLY_FRAGMENT_TESTS | PipelineStageFlags2::LATE_FRAGMENT_TESTS
            }
            ImageLayout::FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR => {
                PipelineStageFlags2::FRAGMENT_SHADING_RATE_ATTACHMENT_KHR
            }
            ImageLayout::SHADER_READ_ONLY_OPTIMAL => {
                PipelineStageFlags2::VERTEX_SHADER | PipelineStageFlags2::FRAGMENT_SHADER
            }
            ImageLayout::PRESENT_SRC_KHR => PipelineStageFlags2::NONE,
            _ => PipelineStageFlags2::ALL_COMMANDS,
        };

        let determine_access_flags = |layout| match layout {
            ImageLayout::PREINITIALIZED => AccessFlags2::HOST_WRITE,
            ImageLayout::COLOR_ATTACHMENT_OPTIMAL => {
                AccessFlags2::COLOR_ATTACHMENT_READ | AccessFlags2::COLOR_ATTACHMENT_WRITE
            }
            ImageLayout::DEPTH_ATTACHMENT_OPTIMAL => {
                AccessFlags2::DEPTH_STENCIL_ATTACHMENT_READ
                    | AccessFlags2::DEPTH_STENCIL_ATTACHMENT_WRITE
            }
            ImageLayout::FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR => {
                AccessFlags2::FRAGMENT_SHADING_RATE_ATTACHMENT_READ_KHR
            }
            ImageLayout::SHADER_READ_ONLY_OPTIMAL => {
                AccessFlags2::SHADER_READ | AccessFlags2::INPUT_ATTACHMENT_READ
            }
            ImageLayout::TRANSFER_DST_OPTIMAL => AccessFlags2::TRANSFER_WRITE,
            ImageLayout::TRANSFER_SRC_OPTIMAL => AccessFlags2::TRANSFER_READ,
            _ => AccessFlags2::empty(),
        };

        let old_layout = self.image.layout().await;

        ImageMemoryBarrier2::default()
            .src_stage_mask(determine_stage_flags(old_layout))
            .src_access_mask(determine_access_flags(old_layout))
            .dst_stage_mask(determine_stage_flags(self.destination))
            .dst_access_mask(determine_access_flags(self.destination))
            .old_layout(old_layout)
            .new_layout(self.destination)
            .image(self.image.ash())
            .subresource_range(self.subresource_range)
    }

    #[inline]
    pub(crate) const fn destination(&self) -> ImageLayout {
        self.destination
    }

    #[inline]
    pub(crate) const fn image(&self) -> &Image {
        &self.image
    }
}
