extern crate alloc;

use {
    crate::{
        device::Device,
        result::{
            extension_not_present, incompatible_driver, initialization_failed,
            out_of_device_memory, out_of_host_memory, unexpected_error,
        },
    },
    alloc::{ffi::CString, sync::Arc},
    ash::{
        prelude::VkResult,
        vk::{
            api_version_major, api_version_minor, api_version_patch, api_version_variant,
            make_api_version, DebugUtilsMessageSeverityFlagsEXT, DebugUtilsMessageTypeFlagsEXT,
            DebugUtilsMessengerCreateInfoEXT, InstanceCreateInfo, LayerProperties,
            PFN_vkDebugUtilsMessengerCallbackEXT, PhysicalDevice, API_VERSION_1_3,
        },
        Entry,
    },
    core::{
        ffi::{c_char, CStr},
        fmt::{Debug, Formatter},
        iter::once,
        ops::Deref,
        slice::from_raw_parts,
        str::from_utf8_unchecked,
    },
    educe::Educe,
    fenrir_ash_interface::instance::Handle,
    fenrir_error::{layer_not_present, state_not_recoverable, Error},
    fenrir_hal::shader::Compiler,
    sleipnir::ffi::runtime::Handle as RuntimeHandle,
    smallvec::SmallVec,
    tracing::{enabled, error, trace, Level},
};

pub(crate) struct Guard {
    instance: ash::Instance,
    runtime: RuntimeHandle,
}

#[derive(Clone, Educe)]
#[educe(Debug)]
#[allow(dead_code)]
pub(crate) struct Instance {
    allocator_manager: fenrir_hal::allocator::manager::Manager,
    queue_submitter: crate::queue::submitter::Submitter,
    handle: fenrir_ash_interface::instance::Handle,
    inner: Arc<Guard>,
    debug_utils_messenger_create_info: Option<DebugUtilsMessengerCreateInfoEXT<'static>>,
    application_info: crate::instance::ApplicationInfo,
    enabled_layers: SmallVec<[[c_char; 256]; 1]>,
    enabled_extensions: SmallVec<[[i8; 256]; 1]>,
    shader_compiler: Compiler,
    #[educe(Debug(ignore))]
    entry: Entry,
}

#[derive(Clone)]
struct ApplicationInfo {
    application_name: CString,
    application_version: u32,
    engine_name: CString,
    engine_version: u32,
    api_version: u32,
}

pub(crate) struct Metadata {
    name: String,
    major: usize,
    minor: usize,
    patch: usize,
    user_callback: PFN_vkDebugUtilsMessengerCallbackEXT,
    required_layers: SmallVec<[[c_char; 256]; 1]>,
    shader_compiler: Compiler,
}

impl ApplicationInfo {
    fn new(
        application_name: &str,
        application_version: u32,
        engine_name: &str,
        engine_version: u32,
        api_version: u32,
    ) -> Result<Self, Error> {
        let error_mapper = |_| state_not_recoverable("string contains internal 0 byte");

        let application_name = CString::new(application_name).map_err(error_mapper)?;

        let engine_name = CString::new(engine_name).map_err(error_mapper)?;

        Ok(Self {
            application_name,
            application_version,
            engine_name,
            engine_version,
            api_version,
        })
    }

    #[inline]
    fn ash(&self) -> ash::vk::ApplicationInfo<'_> {
        ash::vk::ApplicationInfo::default()
            .application_name(&self.application_name)
            .application_version(self.application_version)
            .engine_name(&self.engine_name)
            .engine_version(self.engine_version)
            .api_version(self.api_version)
    }
}

impl Guard {
    #[cold]
    #[inline]
    fn format_destroy_instance(&self) -> String {
        format!("destroy_instance(&self: {self:?}, allocation_callback: None)")
    }

    #[inline]
    pub(crate) fn runtime(&self) -> RuntimeHandle {
        self.runtime.clone()
    }

    #[inline]
    fn trace_called_destroy_instance(&self) {
        if enabled!(Level::TRACE) {
            trace!("Called {}", self.format_destroy_instance());
        }
    }

    #[inline]
    fn trace_calling_destroy_instance(&self) {
        if enabled!(Level::TRACE) {
            trace!("Calling {}", self.format_destroy_instance());
        }
    }
}

impl Instance {
    pub(crate) fn new(
        metadata: &Metadata,
        runtime: RuntimeHandle,
        allocator_manager: fenrir_hal::allocator::manager::Manager,
    ) -> Result<Self, Error> {
        let entry = Entry::linked();

        let shader_compiler = metadata.shader_compiler.clone();

        Self::trace_calling_enumerate_layer_properties();

        let layer_properties_result = unsafe { entry.enumerate_instance_layer_properties() };

        Self::trace_called_enumerate_layer_properties(&layer_properties_result);

        let layer_properties = match layer_properties_result {
            Ok(layer_properties_) => Ok(layer_properties_),
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }?;

        {
            let required_layer_not_found =
                metadata.required_layers.iter().position(|required_layer| {
                    !layer_properties
                        .iter()
                        .any(|layer_property| required_layer == &layer_property.layer_name)
                });

            if let Some(index) = required_layer_not_found {
                match unsafe { CStr::from_ptr(metadata.required_layers[index].as_ptr()) }.to_str() {
                    Err(_) => unreachable!(),
                    Ok(layer) => return Err(layer_not_present(layer)),
                }
            }
        }

        let mut debug_utils_messenger_create_info =
            Self::build_debug_utils_messenger_create_info(metadata.user_callback);

        let mut u32_version: [u32; 3] = [0, 1, 0];

        let version = [metadata.major, metadata.minor, metadata.patch];

        for version_info in u32_version.iter_mut().zip(version) {
            let usize_version_info = version_info.1;

            let u32_version_info = version_info.0;

            *u32_version_info =
                u32::try_from(usize_version_info).unwrap_or_else(|_| unreachable!());
        }

        let [u32_major, u32_minor, u32_patch] = u32_version;

        let application_info = ApplicationInfo::new(
            &metadata.name,
            make_api_version(0, u32_major, u32_minor, u32_patch),
            "fenrir",
            make_api_version(0, 0, 1, 0),
            API_VERSION_1_3,
        )?;

        let ash_application_info = application_info.ash();

        let enabled_layers = metadata.required_layers.clone();

        let enabled_layer_names = enabled_layers
            .iter()
            .map(|enabled_layer| enabled_layer.as_ptr())
            .collect::<SmallVec<[*const c_char; 1]>>();

        let enabled_extensions =
            Self::build_enabled_extensions(debug_utils_messenger_create_info.as_ref(), &entry)?;

        let enabled_extension_names = enabled_extensions
            .iter()
            .map(|enabled_extension| enabled_extension.as_ptr())
            .collect::<SmallVec<[*const c_char; 1]>>();

        let mut instance_create_info = InstanceCreateInfo::default()
            .application_info(&ash_application_info)
            .enabled_layer_names(&enabled_layer_names)
            .enabled_extension_names(&enabled_extension_names);

        instance_create_info = debug_utils_messenger_create_info.as_mut().map_or(
            instance_create_info,
            |debug_utils_messenger_create_info| {
                instance_create_info.push_next(debug_utils_messenger_create_info)
            },
        );

        Self::trace_calling_create_instance(&instance_create_info);

        let inner = match unsafe { entry.create_instance(&instance_create_info, None) } {
            Ok(instance) => Ok(Arc::new(Guard {
                instance,
                runtime: runtime.clone(),
            })),
            Err(error) => Err(match error {
                ash::vk::Result::ERROR_INCOMPATIBLE_DRIVER => incompatible_driver(),
                ash::vk::Result::ERROR_INITIALIZATION_FAILED => initialization_failed(&format!(
                    "{}",
                    ash::vk::Result::ERROR_INITIALIZATION_FAILED
                )),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }?;

        Self::trace_called_create_instance(&instance_create_info, &inner);

        let handle = Handle::new(&**inner);

        let queue_submitter = crate::queue::submitter::Submitter::new(runtime);

        Ok(Self {
            allocator_manager,
            queue_submitter,
            handle,
            inner,
            debug_utils_messenger_create_info,
            application_info,
            enabled_layers,
            enabled_extensions,
            shader_compiler,
            entry,
        })
    }

    #[inline]
    pub(crate) fn allocator_manager(&self) -> fenrir_hal::allocator::manager::Manager {
        self.allocator_manager.clone()
    }

    #[inline]
    fn build_debug_utils_messenger_create_info(
        user_callback: PFN_vkDebugUtilsMessengerCallbackEXT,
    ) -> Option<DebugUtilsMessengerCreateInfoEXT<'static>> {
        user_callback.map(|_| {
            DebugUtilsMessengerCreateInfoEXT::default()
                .message_severity(
                    DebugUtilsMessageSeverityFlagsEXT::VERBOSE
                        | DebugUtilsMessageSeverityFlagsEXT::INFO
                        | DebugUtilsMessageSeverityFlagsEXT::WARNING
                        | DebugUtilsMessageSeverityFlagsEXT::ERROR,
                )
                .message_type(
                    DebugUtilsMessageTypeFlagsEXT::GENERAL
                        | DebugUtilsMessageTypeFlagsEXT::VALIDATION
                        | DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
                )
                .pfn_user_callback(user_callback)
        })
    }

    #[inline]
    fn build_enabled_extensions(
        debug_utils_messenger_create_info: Option<&DebugUtilsMessengerCreateInfoEXT>,
        entry: &Entry,
    ) -> Result<SmallVec<[[i8; 256]; 1]>, Error> {
        if debug_utils_messenger_create_info.is_none() {
            Ok(SmallVec::new())
        } else {
            let maybe_extension =
                match unsafe { entry.enumerate_instance_extension_properties(None) } {
                    Ok(extensions) => Ok(extensions),
                    Err(error) => Err(match error {
                        ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                        ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                        _ => unexpected_error(error),
                    }),
                }?
                .into_iter()
                .find(|extension| {
                    unsafe { CStr::from_ptr(extension.extension_name.as_ptr()) }.to_str()
                        == Ok("VK_EXT_debug_utils")
                });

            maybe_extension.map_or_else(
                || Err(extension_not_present("VK_EXT_debug_utils")),
                |extension| {
                    Ok(once(extension.extension_name).collect::<SmallVec<[[i8; 256]; 1]>>())
                },
            )
        }
    }

    #[inline]
    pub(crate) fn devices(&self) -> Result<Vec<Device>, Error> {
        let instance = &self.inner;

        Self::trace_calling_enumerate_physical_devices(instance);

        let physical_devices_result = unsafe { instance.enumerate_physical_devices() };

        Self::trace_called_enumerate_physical_devices(instance, &physical_devices_result);

        match physical_devices_result {
            Err(error) => Err(match error {
                ash::vk::Result::ERROR_INITIALIZATION_FAILED => initialization_failed(&format!(
                    "{}",
                    ash::vk::Result::ERROR_INITIALIZATION_FAILED
                )),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
            Ok(physical_devices) => physical_devices
                .into_iter()
                .map(|physical_device| Device::new(physical_device, self))
                .collect(),
        }
    }

    #[inline]
    pub(crate) const fn compiler(&self) -> &Compiler {
        &self.shader_compiler
    }

    #[cold]
    #[inline]
    fn format_create_instance(instance_create_info: &InstanceCreateInfo) -> String {
        format!(
            "ash::Entry::create_instance(&self, create_info: &{}, allocation_callbacks: None)",
            Self::format_instance_create_info(instance_create_info)
        )
    }

    #[cold]
    #[inline]
    pub(crate) const fn format_enumerate_layer_properties() -> &'static str {
        "enumerate_instance_layer_properties(&self: ash::Entry }}"
    }

    #[cold]
    #[inline]
    fn format_enumerate_physical_devices(instance: &Arc<Guard>) -> String {
        format!("enumerate_physical_devices(&self: {instance:?})")
    }

    #[cold]
    #[inline]
    fn format_instance_create_info(instance_create_info: &InstanceCreateInfo) -> String {
        let debug_utils_messenger_create_info_string = if instance_create_info.p_next.is_null() {
            "0x0".to_string()
        } else {
            let debug_utils_messenger_create_info = unsafe {
                *instance_create_info
                    .p_next
                    .cast::<DebugUtilsMessengerCreateInfoEXT>()
            };

            format!("{debug_utils_messenger_create_info:?}")
        };

        let application_info = unsafe { *instance_create_info.p_application_info };

        let application_name = if !application_info.p_application_name.is_null() {
            unsafe {
                from_utf8_unchecked(CStr::from_ptr(application_info.p_application_name).to_bytes())
            }
        } else {
            ""
        };

        let engine_name = unsafe { CStr::from_ptr(application_info.p_engine_name) };

        let application_info_string = format!(
            "*ash::vk::ApplicationInfo{{ s_type: APPLICATION_INFO, p_next: \
            {debug_utils_messenger_create_info_string}, p_application_name: {application_name:?}, \
            application_version: {}.{}.{}, p_engine_name: {engine_name:?}, engine_version: \
            {}.{}.{}, api_version: {}.{}.{} }}",
            api_version_major(application_info.application_version),
            api_version_minor(application_info.application_version),
            api_version_patch(application_info.application_version),
            api_version_major(application_info.engine_version),
            api_version_minor(application_info.engine_version),
            api_version_patch(application_info.engine_version),
            api_version_major(application_info.api_version),
            api_version_minor(application_info.api_version),
            api_version_patch(application_info.api_version)
        );

        format!(
            "ash::vk::InstanceCreateInfo{{ s_type=INSTANCE_CREATE_INFO, p_next: \
            0x0, flags: , p_application_info: {application_info_string}, \
            enabled_layer_count: {}, pp_enabled_layer_names: {:?}, \
            enabled_extension_count: {}, pp_enabled_extension_names: {:?} }}",
            instance_create_info.enabled_layer_count,
            unsafe {
                from_raw_parts(
                    instance_create_info.pp_enabled_layer_names,
                    usize::try_from(instance_create_info.enabled_layer_count).unwrap(),
                )
                .iter()
                .map(|layer_name| CStr::from_ptr(*layer_name))
            }
            .collect::<Vec<_>>(),
            instance_create_info.enabled_extension_count,
            unsafe {
                from_raw_parts(
                    instance_create_info.pp_enabled_extension_names,
                    usize::try_from(instance_create_info.enabled_extension_count).unwrap(),
                )
                .iter()
                .map(|extension_name| CStr::from_ptr(*extension_name))
            }
            .collect::<Vec<_>>()
        )
    }

    #[inline]
    pub(crate) fn guard(&self) -> Arc<Guard> {
        self.inner.clone()
    }

    #[inline]
    pub(crate) const fn handle(&self) -> &Handle {
        &self.handle
    }

    #[inline]
    pub(crate) fn queue_submitter(&self) -> crate::queue::submitter::Submitter {
        self.queue_submitter.clone()
    }

    #[inline]
    pub(crate) fn runtime(&self) -> RuntimeHandle {
        self.inner.runtime.clone()
    }

    #[inline]
    fn trace_called_create_instance(
        instance_create_info: &InstanceCreateInfo,
        instance: &Arc<Guard>,
    ) {
        if enabled!(Level::TRACE) {
            trace!(
                "Called {} -> {instance:?}.",
                Self::format_create_instance(instance_create_info)
            );
        }
    }

    #[inline]
    fn trace_called_enumerate_layer_properties(result: &VkResult<Vec<LayerProperties>>) {
        if (result.is_ok() && enabled!(Level::TRACE)) || enabled!(Level::ERROR) {
            let message = format!(
                "Called {} -> {:?}",
                Self::format_enumerate_layer_properties(),
                result
            );

            if result.is_ok() {
                trace!(message);
            } else {
                error!(message);
            }
        }
    }

    #[inline]
    fn trace_called_enumerate_physical_devices(
        instance: &Arc<Guard>,
        result: &VkResult<Vec<PhysicalDevice>>,
    ) {
        if (result.is_ok() && enabled!(Level::TRACE)) || enabled!(Level::ERROR) {
            let message = format!(
                "Called {} -> {result:?}.",
                Self::format_enumerate_physical_devices(instance)
            );

            if result.is_ok() {
                trace!(message);
            } else {
                error!(message);
            }
        }
    }

    #[inline]
    fn trace_calling_create_instance(instance_create_info: &InstanceCreateInfo) {
        if enabled!(Level::TRACE) {
            trace!(
                "Calling {}.",
                Self::format_create_instance(instance_create_info)
            );
        }
    }

    #[inline]
    fn trace_calling_enumerate_layer_properties() {
        if enabled!(Level::TRACE) {
            trace!("Calling {}", Self::format_enumerate_layer_properties(),);
        }
    }

    #[inline]
    fn trace_calling_enumerate_physical_devices(instance: &Arc<Guard>) {
        if enabled!(Level::TRACE) {
            trace!(
                "Calling {}.",
                Self::format_enumerate_physical_devices(instance)
            );
        }
    }
}

impl Debug for ApplicationInfo {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("ApplicationInfo")
            .field("application_name", &self.application_name)
            .field(
                "application_version",
                &format!(
                    "{}.{}.{}.{}",
                    api_version_variant(self.application_version),
                    api_version_major(self.application_version),
                    api_version_minor(self.application_version),
                    api_version_patch(self.application_version),
                ),
            )
            .field("engine_name", &self.engine_name)
            .field(
                "engine_version",
                &format!(
                    "{}.{}.{}.{}",
                    api_version_variant(self.engine_version),
                    api_version_major(self.engine_version),
                    api_version_minor(self.engine_version),
                    api_version_patch(self.engine_version),
                ),
            )
            .field(
                "api_version",
                &format!(
                    "{}.{}.{}.{}",
                    api_version_variant(self.api_version),
                    api_version_major(self.api_version),
                    api_version_minor(self.api_version),
                    api_version_patch(self.api_version),
                ),
            )
            .finish()
    }
}

impl Debug for Guard {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Guard")
            .field(
                "instance",
                &format_args!(
                    "ash::Instance{{ ash::vk::Instance: {:?} }}",
                    self.instance.handle()
                ),
            )
            .finish_non_exhaustive()
    }
}

impl Deref for Guard {
    type Target = ash::Instance;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.instance
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        self.trace_calling_destroy_instance();

        unsafe { self.instance.destroy_instance(None) };

        self.trace_called_destroy_instance();
    }
}

impl Eq for Guard {}

impl PartialEq for Guard {
    fn eq(&self, other: &Self) -> bool {
        self.instance.handle() == other.instance.handle()
    }
}

unsafe impl Send for Instance {}
unsafe impl Sync for Instance {}

impl From<fenrir_hal::instance::Metadata> for Metadata {
    #[inline]
    fn from(metadata: fenrir_hal::instance::Metadata) -> Self {
        Self {
            name: metadata.name.into(),
            major: metadata.major,
            minor: metadata.minor,
            patch: metadata.patch,
            user_callback: None,
            required_layers: SmallVec::new(),
            shader_compiler: metadata.shader_compiler,
        }
    }
}

unsafe impl Send for Metadata {}
unsafe impl Sync for Metadata {}

#[cfg(test)]
pub(crate) mod tests {
    use {
        crate::{
            device::Device,
            instance::{Instance, Metadata},
        },
        ash::vk::{
            Bool32, DebugUtilsMessageSeverityFlagsEXT, DebugUtilsMessageTypeFlagsEXT,
            DebugUtilsMessengerCallbackDataEXT, FALSE,
        },
        async_lock::OnceCell,
        core::convert::Into,
        pretty_assertions::assert_eq,
        regex::Regex,
        sleipnir::ffi::runtime::Handle,
        smallvec::SmallVec,
        std::{
            env::{current_exe, set_var, var},
            ffi::{c_void, CStr},
            fs::read_dir,
            iter::once,
            mem::transmute,
            path::{Path, PathBuf},
            str::FromStr,
        },
        tokio::test,
        tracing::{error, info, level_filters::LevelFilter, trace, warn},
        tracing_subscriber::fmt::format::FmtSpan,
    };

    #[inline]
    unsafe extern "system" fn user_callback(
        flag: DebugUtilsMessageSeverityFlagsEXT,
        _: DebugUtilsMessageTypeFlagsEXT,
        data: *const DebugUtilsMessengerCallbackDataEXT,
        _: *mut c_void,
    ) -> Bool32 {
        let message_result = CStr::from_ptr((*data).p_message).to_str();

        assert!(message_result.is_ok());

        let message = message_result.unwrap();

        match flag {
            DebugUtilsMessageSeverityFlagsEXT::ERROR => error!("{message}"),
            DebugUtilsMessageSeverityFlagsEXT::INFO => info!("{message}"),
            DebugUtilsMessageSeverityFlagsEXT::VERBOSE => trace!("{message}"),
            DebugUtilsMessageSeverityFlagsEXT::WARNING => warn!("{message}"),
            _ => unreachable!(),
        }

        let allowed_messages = [
            "Override layer has override paths set to",
            "Deprecated warn_on_robust_oob setting was set, use gpuav_warn_on_robust_oob instead.",
             "Attempting to enable extension VK_EXT_debug_utils, but this extension is intended to support use by applications when debugging and it is strongly recommended that it be otherwise avoided."];

        if !allowed_messages
            .iter()
            .any(|message_| message.contains(message_))
        {
            assert_ne!(DebugUtilsMessageSeverityFlagsEXT::WARNING, flag);
            assert_ne!(DebugUtilsMessageSeverityFlagsEXT::ERROR, flag);
        }

        FALSE
    }

    #[inline]
    #[allow(clippy::future_not_send)]
    pub(crate) async fn init_tracing() {
        static TRACING_SUBSCRIBER: OnceCell<()> = OnceCell::new();

        assert!(TRACING_SUBSCRIBER
            .get_or_try_init(|| async move {
                set_var("VK_LAYER_MESSAGE_ID_FILTER", "0x675dc32e");

                tracing_subscriber::fmt()
                    .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
                    .with_max_level(LevelFilter::TRACE)
                    .try_init()
            })
            .await
            .is_ok());
    }

    #[inline]
    fn get_allocator_manager(runtime: &Handle) -> fenrir_hal::allocator::manager::Manager {
        let current_exe = current_exe().unwrap();

        let deps_dir = current_exe.parent().unwrap();

        let mut file_regex = "libfenrir_gpu_allocator".to_string();

        file_regex.push_str("(-[a-z0-9]+)?\\.so");

        let regex_result = Regex::new(&file_regex);

        assert!(regex_result.is_ok());

        let regex = regex_result.unwrap();

        let regex_ref = &regex;

        let maybe_allocator_manager = read_dir(deps_dir)
            .unwrap()
            .map(|entry_result| {
                let entry = entry_result.unwrap();

                entry.file_name().to_str().map_or_else(
                    || None,
                    |file_name| {
                        if regex_ref.is_match(file_name) {
                            let allocator_manager_result =
                                fenrir_hal::allocator::manager::Manager::new(
                                    &entry.path(),
                                    runtime.clone(),
                                );

                            allocator_manager_result.ok()
                        } else {
                            None
                        }
                    },
                )
            })
            .fold(None, |result, next| result.map_or(next, Some));

        assert!(maybe_allocator_manager.is_some());

        maybe_allocator_manager.unwrap()
    }

    #[inline]
    fn load_shader_compiler(name: &str) -> fenrir_shader_compiler::Compiler {
        let mainifest_dir_result = var("CARGO_MANIFEST_DIR");

        assert!(mainifest_dir_result.is_ok());

        let library_dir_result = PathBuf::from_str(&mainifest_dir_result.unwrap());

        assert!(library_dir_result.is_ok());

        let maybe_library_dir = library_dir_result.unwrap().parent().map(Path::to_path_buf);

        assert!(maybe_library_dir.is_some());

        let mut library_dir = maybe_library_dir.unwrap();

        library_dir.push("target");

        library_dir.push(mode());

        library_dir.push("deps");

        let maybe_entires = read_dir(&library_dir);

        assert!(maybe_entires.is_ok());

        let maybe_compiler = maybe_entires
            .unwrap()
            .filter_map(Result::ok)
            .filter(|entry| entry.file_name().to_string_lossy().contains(name))
            .filter_map(
                |entry| match fenrir_shader_compiler::Compiler::new(&entry.path()) {
                    Err(error) => {
                        error!("{entry:?}: {error}");
                        None
                    }
                    Ok(module) => Some(module),
                },
            )
            .find(|_| true);

        assert!(maybe_compiler.is_some());

        maybe_compiler.unwrap()
    }

    #[inline]
    pub(crate) fn load_shader_compilers() -> impl IntoIterator<Item = fenrir_hal::shader::Compiler>
    {
        ["naga"].map(load_shader_compiler).map(Into::into)
    }

    #[inline]
    #[cfg(debug_assertions)]
    const fn mode() -> &'static str {
        "debug"
    }

    #[inline]
    #[cfg(not(debug_assertions))]
    const fn mode() -> &'static str {
        "release"
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        init_tracing().await;

        let required_layers = once("VK_LAYER_KHRONOS_validation")
            .map(|layer| {
                let mut layer_c_chars: [i8; 256] = [0; 256];

                layer_c_chars
                    .iter_mut()
                    .zip(layer.bytes())
                    .for_each(|byte| {
                        let signed_byte = byte.0;

                        let unsigned_byte = byte.1;

                        let maybe_signed_byte = unsigned_byte.try_into();

                        assert!(maybe_signed_byte.is_ok());

                        *signed_byte = maybe_signed_byte.unwrap();
                    });

                layer_c_chars
            })
            .map(|mut layer| {
                layer[255] = 0;

                layer
            })
            .collect::<SmallVec<[[i8; 256]; 1]>>();

        let runtime = sleipnir::runtime::Handle::current().into();

        let allocator_manager = get_allocator_manager(&runtime);

        for shader_compiler in load_shader_compilers() {
            {
                // The case of an empty application name lead to crashes in the past.
                // Testing for regressions here.

                let metadata = Metadata {
                    name: String::new(),
                    major: 0,
                    minor: 1,
                    patch: 0,
                    user_callback: Some(user_callback),
                    required_layers: required_layers.clone(),
                    shader_compiler: shader_compiler.clone(),
                };

                let instance = Instance::new(&metadata, runtime.clone(), allocator_manager.clone());

                assert!(instance.is_ok());
            }

            let metadata = Metadata {
                name: "instance::tests::new".into(),
                major: 0,
                minor: 1,
                patch: 0,
                user_callback: Some(user_callback),
                required_layers: required_layers.clone(),
                shader_compiler,
            };

            let instance = Instance::new(&metadata, runtime.clone(), allocator_manager.clone());

            assert!(instance.is_ok());
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn layer_not_present() {
        init_tracing().await;

        let non_existent_layer = "2@€ÄöÜß";

        let required_layers = once(non_existent_layer.to_string())
            .map(|layer| {
                let mut layer_c_chars: [i8; 256] = [0; 256];

                layer_c_chars
                    .iter_mut()
                    .zip(layer.bytes())
                    .for_each(|byte| {
                        let signed_byte = byte.0;

                        let unsigned_byte = byte.1;

                        *signed_byte = unsafe { transmute::<u8, i8>(unsigned_byte) };
                    });

                layer_c_chars
            })
            .map(|mut layer| {
                layer[255] = 0;

                layer
            })
            .collect::<SmallVec<[[i8; 256]; 1]>>();

        let runtime = sleipnir::runtime::Handle::current().into();

        let allocator_manager = get_allocator_manager(&runtime);

        for shader_compiler in load_shader_compilers() {
            let metadata = Metadata {
                name: "instance::tests::layer_not_present".into(),
                major: 0,
                minor: 1,
                patch: 0,
                user_callback: Some(user_callback),
                required_layers: required_layers.clone(),
                shader_compiler,
            };

            let instance = Instance::new(&metadata, runtime.clone(), allocator_manager.clone());

            assert!(instance.is_err());
            assert_eq!(
                fenrir_error::layer_not_present(non_existent_layer),
                instance.err().unwrap()
            );
        }
    }

    pub(crate) fn instance_and_dependencies(
        shader_compiler: fenrir_hal::shader::Compiler,
    ) -> (Instance, Handle) {
        let required_layers = once("VK_LAYER_KHRONOS_validation")
            .map(|layer| {
                let mut layer_c_chars: [i8; 256] = [0; 256];

                layer_c_chars
                    .iter_mut()
                    .zip(layer.bytes())
                    .for_each(|byte| {
                        let signed_byte = byte.0;

                        let unsigned_byte = byte.1;

                        let maybe_signed_byte = unsigned_byte.try_into();

                        assert!(maybe_signed_byte.is_ok());

                        *signed_byte = maybe_signed_byte.unwrap();
                    });

                layer_c_chars
            })
            .map(|mut layer| {
                layer[255] = 0;

                layer
            })
            .collect::<SmallVec<[[i8; 256]; 1]>>();

        let metadata = Metadata {
            name: "instance::tests::instance_and_dependencies".into(),
            major: 0,
            minor: 1,
            patch: 0,
            user_callback: Some(user_callback),
            required_layers,
            shader_compiler,
        };

        let runtime = sleipnir::runtime::Handle::current().into();

        let allocator_manager = get_allocator_manager(&runtime);

        let instance = Instance::new(&metadata, runtime.clone(), allocator_manager);

        assert!(instance.is_ok());

        (instance.unwrap(), runtime)
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn devices() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (instance, _) = instance_and_dependencies(shader_compiler);

            let devices = instance.devices();

            assert!(devices.is_ok());

            assert!(!devices.unwrap().is_empty());
        }
    }

    pub(crate) fn devices_and_dependencies(
        shader_compiler: fenrir_hal::shader::Compiler,
    ) -> (Vec<Device>, Instance, Handle) {
        let (instance, runtime) = instance_and_dependencies(shader_compiler);

        let devices_result = instance.devices();

        assert!(devices_result.is_ok());

        let devices = devices_result.unwrap();

        assert!(!devices.is_empty());

        (devices, instance, runtime)
    }
}
