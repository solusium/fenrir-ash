use {
    crate::{
        device::features::Features,
        failed_to_cast,
        image::Image,
        instance::{self, Instance},
        queue::{self, submitter::Submitter},
        render_pass::RenderPass,
        result::{
            device_lost, out_of_device_memory, out_of_host_memory, too_many_objects,
            unexpected_error,
        },
        trace_called, trace_calling,
    },
    abi_stable::{
        rtry,
        sabi_trait::TD_Opaque,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
            RStr, RVec, Tuple2,
        },
    },
    alloc::sync::Arc,
    ash::vk::{
        api_version_major, api_version_minor, api_version_patch, AllocationCallbacks,
        DeviceCreateInfo, DeviceQueueCreateInfo, ExtensionProperties, PhysicalDevice,
        PhysicalDeviceFeatures2, PhysicalDeviceLimits, PhysicalDeviceType, QueueFamilyProperties2,
        QueueFlags,
    },
    async_ffi::{FfiFuture, FutureExt},
    bytemuck::bytes_of,
    core::{
        cmp::{min, Ordering},
        ffi::CStr,
        fmt::{Debug, Formatter},
        marker::PhantomData,
        ops::{Deref, DerefMut, Range},
        slice::from_raw_parts,
    },
    educe::Educe,
    fenrir_ash_interface::device::Handle,
    fenrir_error::{argument_out_of_domain, initialization_failed, not_supported, Error},
    fenrir_hal::{
        allocator::Allocator, device::Type, shader::Compiler, version::Version, Extension,
    },
    smallvec::SmallVec,
    tracing::{debug_span, enabled, trace, Level},
};

mod features;
mod properties;

extern crate alloc;

pub(crate) type Properties = crate::device::properties::Properties;

#[derive(Clone, Educe)]
#[educe(Debug)]
pub(crate) struct Device {
    allocator: Allocator,
    queue_families: SmallVec<[queue::family::Family; 4]>,
    properties: Properties,
    inner: RArc<Guard>,
    extension_names: SmallVec<[*const i8; 5]>,
    features: Features,
    shader_compiler: Compiler,
}

#[derive(Clone)]
pub(crate) struct Guard {
    handle: Handle,
    device: ash::Device,
    instance: Arc<instance::Guard>,
}

#[derive(Clone, Educe, PartialEq)]
#[educe(Debug)]
pub(crate) struct QueueCreateInfo {
    queue_family_index: u32,
    queue_priorities: SmallVec<[f32; 16]>,
}

impl Device {
    #[inline]
    pub(crate) fn new(
        physical_device: PhysicalDevice,
        fenrir_instance: &Instance,
    ) -> Result<Self, Error> {
        let shader_compiler = fenrir_instance.compiler().clone();

        let instance = fenrir_instance.guard();

        let properties = Properties::new(fenrir_instance, physical_device);

        let queue_family_properties_count =
            unsafe { instance.get_physical_device_queue_family_properties2_len(physical_device) };

        let mut queue_family_properties: SmallVec<[QueueFamilyProperties2; 4]> = SmallVec::new();

        queue_family_properties.resize(
            queue_family_properties_count,
            QueueFamilyProperties2::default(),
        );

        unsafe {
            instance.get_physical_device_queue_family_properties2(
                physical_device,
                &mut queue_family_properties,
            );
        };

        let features = Features::new(instance.clone(), physical_device)?;

        let extension_properties = Self::extensions(&instance, physical_device)?;

        let extension_names = [
            "VK_EXT_extended_dynamic_state2\0",
            "VK_EXT_extended_dynamic_state3\0",
            "VK_EXT_memory_priority\0",
            "VK_EXT_pageable_device_local_memory\0",
            "VK_EXT_vertex_input_dynamic_state\0",
        ]
        .into_iter()
        .filter_map(|extension_name| {
            let extension_name_pointer = extension_name.as_ptr().cast::<i8>();

            let extension_name_cstr = unsafe { CStr::from_ptr(extension_name_pointer) };

            let has_extension = extension_properties.iter().any(|extension_property| {
                extension_name_cstr
                    == unsafe { CStr::from_ptr(extension_property.extension_name.as_ptr()) }
            });

            if has_extension {
                Some(extension_name_pointer)
            } else {
                None
            }
        })
        .collect::<SmallVec<[*const i8; 5]>>();

        let queue_create_infos = Self::create_queue_create_infos(&queue_family_properties)?;

        let ash_device = Self::create_device(
            &queue_create_infos,
            &features,
            &extension_names,
            &instance,
            physical_device,
        )?;

        let handle = Handle::new(physical_device, &ash_device, *fenrir_instance.handle());

        let inner = RArc::new(Guard {
            handle,
            device: ash_device,
            instance,
        });

        let allocator_manager = fenrir_instance.allocator_manager();

        let allocator = allocator_manager
            .allocator_form_guard(fenrir_hal::device::guard::Guard::new(inner.clone()))?;

        let queue_submitter = fenrir_instance.queue_submitter();

        let queue_families = Self::get_queue_families(
            queue_family_properties,
            queue_create_infos,
            &inner,
            &queue_submitter,
            fenrir_instance,
        );

        Ok(Self {
            allocator,
            queue_families,
            properties,
            inner,
            extension_names,
            features,
            shader_compiler,
        })
    }

    #[inline]
    pub(crate) fn allocator(&self) -> Allocator {
        self.allocator.clone()
    }

    #[inline]
    pub(crate) const fn compiler(&self) -> &Compiler {
        &self.shader_compiler
    }

    #[inline]
    pub(crate) fn compute_queue_family(&self) -> Result<queue::family::Family, Error> {
        let compute_and_no_graphics_queue_family_result =
            self.queue_families.iter().find(|queue_family| {
                let queue_flags = queue_family.flags();

                let compute = queue_flags
                    .iter()
                    .find(|queue_flag| QueueFlags::COMPUTE == **queue_flag);

                let graphics = queue_flags
                    .iter()
                    .find(|queue_flag| QueueFlags::GRAPHICS == **queue_flag);

                compute.is_some() && graphics.is_none()
            });

        compute_and_no_graphics_queue_family_result.map_or_else(
            || {
                let compute_family_result = self.queue_families.iter().find(|queue_family| {
                    let queue_flags = queue_family.flags();

                    queue_flags
                        .iter()
                        .any(|queue_flag| QueueFlags::COMPUTE == *queue_flag)
                });

                compute_family_result.map_or_else(
                    || {
                        Err(not_supported(&format!(
                            "no queue family found which supports {:?}",
                            QueueFlags::COMPUTE,
                        )))
                    },
                    |queue_family| Ok(queue_family.clone()),
                )
            },
            |queue_family| Ok(queue_family.clone()),
        )
    }

    #[inline]
    fn create_device(
        queue_create_infos: &[QueueCreateInfo],
        features: &Features,
        extension_names: &[*const i8],
        instance: &Arc<instance::Guard>,
        physical_device: PhysicalDevice,
    ) -> Result<ash::Device, Error> {
        let ash_queue_create_infos = queue_create_infos
            .iter()
            .map(|queue_create_info| queue_create_info.ash())
            .collect::<SmallVec<[DeviceQueueCreateInfo; 4]>>();

        let extensions = extension_names
            .iter()
            .map(|extension_name| {
                let extensions_name_cstr = unsafe { CStr::from_ptr(*extension_name) };

                match extensions_name_cstr.to_str() {
                    Ok(extension) => Ok(extension),
                    Err(error) => Err(failed_to_cast(
                        &extensions_name_cstr,
                        PhantomData::<&str>,
                        error,
                    )),
                }
            })
            .collect::<Result<SmallVec<[_; 5]>, Error>>()?;

        let mut features2 = features.features2(&extensions);

        let create_info = DeviceCreateInfo::default()
            .push_next(features2.deref_mut())
            .queue_create_infos(&ash_queue_create_infos)
            .enabled_extension_names(extension_names);

        let create_device = || {
            let _span = debug_span!("create_device");

            unsafe { instance.create_device(physical_device, &create_info, None) }
        };

        let trace_args = (instance.deref(), physical_device, create_info);

        trace_calling(Self::format_create_device, trace_args);

        let ash_device_result = create_device();

        let ash_vk_device_result = match ash_device_result.as_ref() {
            Ok(ash_device) => Ok(ash_device.handle()),
            Err(result) => Err(*result),
        };

        trace_called(
            Self::format_create_device,
            trace_args,
            Some(&ash_vk_device_result),
        );

        match ash_device_result {
            Ok(ash_device) => Ok(ash_device),
            Err(error) => Err(match error {
                ash::vk::Result::ERROR_DEVICE_LOST => device_lost(),
                ash::vk::Result::ERROR_INITIALIZATION_FAILED => initialization_failed(&format!(
                    "{}",
                    ash::vk::Result::ERROR_INITIALIZATION_FAILED
                )),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                ash::vk::Result::ERROR_TOO_MANY_OBJECTS => too_many_objects(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    fn create_queue_create_infos(
        queue_family_properties: &[QueueFamilyProperties2],
    ) -> Result<SmallVec<[QueueCreateInfo; 4]>, Error> {
        queue_family_properties
            .iter()
            .enumerate()
            .map(|(index, queue_family_property)| {
                let queue_family_index = u32::try_from(index).map_err(|_| {
                    argument_out_of_domain(
                        Range {
                            start: u32::MIN.into(),
                            end: u32::MAX.into(),
                        },
                        index.into(),
                    )
                })?;

                let mut queue_priorities = SmallVec::<[f32; 16]>::new();

                let queue_count =
                    usize::try_from(queue_family_property.queue_family_properties.queue_count)
                        .map_err(|_| {
                            argument_out_of_domain(
                                Range {
                                    start: usize::MIN.into(),
                                    end: usize::MAX.into(),
                                },
                                queue_family_property
                                    .queue_family_properties
                                    .queue_count
                                    .into(),
                            )
                        })?;

                queue_priorities.resize(queue_count, 1.0);

                Ok(QueueCreateInfo {
                    queue_family_index,
                    queue_priorities,
                })
            })
            .collect::<Result<SmallVec<[QueueCreateInfo; 4]>, Error>>()
    }

    #[inline]
    pub(crate) fn device_type(&self) -> Type {
        match self.properties.device_type {
            PhysicalDeviceType::INTEGRATED_GPU => Type::IntegratedGPU,
            PhysicalDeviceType::DISCRETE_GPU => Type::DiscreteGPU,
            PhysicalDeviceType::VIRTUAL_GPU => Type::VirtualGPU,
            PhysicalDeviceType::CPU => Type::CPU,
            _ => Type::Other,
        }
    }

    #[inline]
    pub(crate) fn driver_version(&self) -> Result<Version, Error> {
        const NVIDIA: u32 = 4318;

        let ash_version = self.properties.driver_version;

        let get_version_components = || {
            if NVIDIA == self.properties.vendor_id {
                const MAJOR_OFFSET: u32 = 22;

                const MINOR_OFFSET: u32 = 14;

                const PATCH_OFFSET: u32 = 6;

                const BYTE_MASK: u32 = 0xFF;

                [
                    ash_version >> MAJOR_OFFSET,
                    (ash_version >> MINOR_OFFSET) & BYTE_MASK,
                    (ash_version >> PATCH_OFFSET) & BYTE_MASK,
                ]
            } else {
                [
                    api_version_major(ash_version),
                    api_version_minor(ash_version),
                    api_version_patch(ash_version),
                ]
            }
            .into_iter()
            .map(|u32_component| {
                u8::try_from(u32_component).map_or_else(
                    |_| {
                        Err(argument_out_of_domain(
                            Range {
                                start: u8::MIN.into(),
                                end: u8::MAX.into(),
                            },
                            u32_component.into(),
                        ))
                    },
                    Ok,
                )
            })
            .collect::<Result<SmallVec<[u8; 3]>, _>>()
        };

        let version_components = get_version_components()?;

        let (major, minor, patch) = (
            version_components[0],
            version_components[1],
            version_components[2],
        );

        Ok(Version::new(major, minor, patch))
    }

    #[inline]
    pub(super) fn extension_names(&self) -> Result<impl Iterator<Item = &str>, Error> {
        let extension_name_string_results = self.extension_names.iter().map(|extension_name| {
            let extension_name_cstr = unsafe { CStr::from_ptr(*extension_name) };
            (extension_name_cstr, extension_name_cstr.to_str())
        });

        let first_error = extension_name_string_results
            .clone()
            .filter_map(|(extension_name_cstr, extension_name_string_result)| {
                match extension_name_string_result {
                    Ok(_) => None,
                    Err(error) => Some((extension_name_cstr, error)),
                }
            })
            .find(|_| true);

        match first_error {
            Some((extension_name_cstr, error)) => Err(failed_to_cast(
                &extension_name_cstr,
                PhantomData::<&str>,
                error,
            )),
            None => Ok(extension_name_string_results
                .filter_map(|(_, extension_name_str_result)| extension_name_str_result.ok())),
        }
    }

    #[inline]
    fn extensions(
        instance: &Arc<instance::Guard>,
        physical_device: PhysicalDevice,
    ) -> Result<Vec<ExtensionProperties>, Error> {
        let trace_args = (instance.deref(), physical_device);

        let enumerate_device_extension_properties = || {
            let _span = debug_span!("enumerate_device_extension_properties");

            unsafe { instance.enumerate_device_extension_properties(physical_device) }
        };

        trace_calling(
            Self::format_enumerate_device_extension_properties,
            trace_args,
        );

        let extension_properties_result = enumerate_device_extension_properties();

        trace_called(
            Self::format_enumerate_device_extension_properties,
            trace_args,
            Some(&extension_properties_result),
        );

        extension_properties_result.map_err(|error| match error {
            ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
            ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
            error => unexpected_error(error),
        })
    }

    #[inline]
    pub(crate) const fn features(&self) -> &Features {
        &self.features
    }

    #[cold]
    #[inline]
    fn format_create_device(args: (&instance::Guard, PhysicalDevice, DeviceCreateInfo)) -> String {
        let (instance, physical_device, create_info) = args;

        let physical_device_features =
            unsafe { *(create_info.p_next.cast::<PhysicalDeviceFeatures2>()) };

        let queue_create_infos = unsafe {
            from_raw_parts(
                create_info.p_queue_create_infos,
                usize::try_from(create_info.queue_create_info_count).unwrap(),
            )
        }
        .iter()
        .map(|queue_create_info| {
            let queue_priorities = unsafe {
                from_raw_parts(
                    queue_create_info.p_queue_priorities,
                    usize::try_from(queue_create_info.queue_count).unwrap(),
                )
            };

            format!(
                "DeviceQueueCreateInfo {{ s_type: StructureType, p_next: 0x0, flags: , \
                queue_family_index: {}, queue_count: {}, p_queue_priorities: {:?} }}",
                queue_create_info.queue_family_index,
                queue_create_info.queue_count,
                queue_priorities
            )
        })
        .enumerate()
        .fold(
            String::new(),
            |queue_create_infos, (index, queue_create_info)| {
                queue_create_infos + if 0 < index { ", " } else { "" } + &queue_create_info
            },
        );

        let extension_names = unsafe {
            from_raw_parts(
                create_info.pp_enabled_extension_names,
                create_info.enabled_extension_count as usize,
            )
        }
        .iter()
        .map(|extensions_name| unsafe { CStr::from_ptr(*extensions_name) })
        .collect::<Vec<_>>();

        let device_create_info = format!(
            "DeviceCreateInfo {{ s_type: DEVICE_CREATE_INFO, p_next: {physical_device_features:?}, \
            flags:, queue_create_info_count: {}, p_queue_create_infos: [{queue_create_infos}], \
            enabled_layer_count: 0, pp_enabled_layer_names: 0x0, enabled_extension_count: \
            {}, pp_enabled_extension_names: {extension_names:?}, p_enabled_features: 0x0 }}",
            create_info.queue_create_info_count,
            extension_names.len()
        );

        format!(
            "create_device(self: {instance:?}, physical_device: {physical_device:?}, create_info: \
            {device_create_info:?}, allocator_callbacks: {:?})",
            None::<AllocationCallbacks>
        )
    }

    #[cold]
    #[inline]
    fn format_enumerate_device_extension_properties(
        args: (&instance::Guard, PhysicalDevice),
    ) -> String {
        let (instance, physical_device) = args;

        format!(
            "enumerate_device_extension_properties(&self: {instance:?}, physical_device: \
            {physical_device:?})"
        )
    }

    #[inline]
    fn get_queue_families(
        queue_family_properties: SmallVec<[QueueFamilyProperties2; 4]>,
        queue_create_infos: SmallVec<[QueueCreateInfo; 4]>,
        device: &RArc<Guard>,
        queue_submitter: &Submitter,
        instance: &Instance,
    ) -> SmallVec<[crate::queue::Family; 4]> {
        queue_family_properties
            .into_iter()
            .zip(queue_create_infos)
            .map(|(queue_family_property, create_info)| {
                crate::queue::Family::new(
                    queue_family_property,
                    create_info,
                    device.clone(),
                    queue_submitter.clone(),
                    instance.runtime(),
                )
            })
            .collect()
    }

    #[inline]
    pub(crate) fn graphics_queue_family(&self) -> Result<queue::family::Family, Error> {
        let queue_family_result = self.queue_families.iter().find(|queue_family| {
            let queue_flags = queue_family.flags();

            queue_flags
                .iter()
                .any(|queue_flag| QueueFlags::GRAPHICS == *queue_flag)
        });

        queue_family_result.map_or_else(
            || {
                Err(not_supported(&format!(
                    "no queue family found which supports {:?}",
                    QueueFlags::GRAPHICS,
                )))
            },
            |queue_family| Ok(queue_family.clone()),
        )
    }

    #[inline]
    pub(crate) fn guard(&self) -> RArc<Guard> {
        self.inner.clone()
    }

    #[inline]
    pub(crate) fn handle(&self) -> &Handle {
        self.inner.handle()
    }

    #[inline]
    pub(crate) fn name(&self) -> (*const u8, usize) {
        let bytes = bytes_of(&self.properties.device_name);

        (bytes.as_ptr(), bytes.len())
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn limits(&self) -> &PhysicalDeviceLimits {
        &self.properties.limits
    }

    #[inline]
    pub(crate) const fn properties(&self) -> &Properties {
        &self.properties
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn queue_families(&self) -> &[queue::family::Family] {
        &self.queue_families
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn transfer_queue_family(&self) -> Result<queue::family::Family, Error> {
        let transfer_and_no_graphics_queue_family_result =
            self.queue_families.iter().find(|queue_family| {
                let queue_flags = queue_family.flags();

                let transfer = queue_flags
                    .iter()
                    .find(|queue_flag| QueueFlags::TRANSFER == **queue_flag);

                let graphics = queue_flags
                    .iter()
                    .find(|queue_flag| QueueFlags::GRAPHICS == **queue_flag);

                transfer.is_some() && graphics.is_none()
            });

        transfer_and_no_graphics_queue_family_result.map_or_else(
            || {
                self.compute_queue_family().map_or_else(
                    |_| {
                        self.graphics_queue_family().map_or_else(
                            |_| {
                                Err(not_supported(&format!(
                                    "no queue family found which supports {:?}",
                                    QueueFlags::TRANSFER,
                                )))
                            },
                            Ok,
                        )
                    },
                    Ok,
                )
            },
            |queue_family| Ok(queue_family.clone()),
        )
    }
}

impl Guard {
    #[cold]
    #[inline]
    fn format_destroy_device(&self) -> String {
        format!("destroy_device(&self: {self:?}, allocation_callback: None)")
    }

    #[inline]
    pub(crate) const fn handle(&self) -> &Handle {
        &self.handle
    }

    #[inline]
    pub(crate) fn instance(&self) -> Arc<crate::instance::Guard> {
        self.instance.clone()
    }

    #[inline]
    pub(crate) fn physical_device(&self) -> PhysicalDevice {
        self.handle.physical_device()
    }

    #[inline]
    fn trace_called_destroy_device(&self) {
        if enabled!(Level::TRACE) {
            trace!("Called {}", self.format_destroy_device());
        }
    }

    #[inline]
    fn trace_calling_destroy_device(&self) {
        if enabled!(Level::TRACE) {
            trace!("Calling {}", self.format_destroy_device());
        }
    }
}

impl QueueCreateInfo {
    #[inline]
    fn ash(&self) -> DeviceQueueCreateInfo<'_> {
        DeviceQueueCreateInfo::default()
            .queue_family_index(self.queue_family_index)
            .queue_priorities(&self.queue_priorities)
    }

    #[inline]
    #[must_use]
    pub(crate) const fn index(&self) -> u32 {
        self.queue_family_index
    }

    #[inline]
    #[must_use]
    pub(crate) fn priorities(&self) -> &[f32] {
        &self.queue_priorities
    }
}

impl Eq for Device {}

impl PartialEq for Device {
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.queue_families == other.queue_families,
            self.properties == other.properties,
            self.features == other.features,
        ];

        values.into_iter().all(|value| value)
    }
}

impl fenrir_hal::device::stable_abi::Trait for Device {
    #[inline]
    fn device_type(&self) -> Type {
        self.device_type()
    }

    #[inline]
    fn driver_version(&self) -> RResult<Version, Error> {
        self.driver_version().into()
    }

    #[inline]
    fn extensions(&self) -> RResult<RVec<Extension>, Error> {
        ROk(rtry!(Self::extensions(
            &self.inner.instance,
            self.inner.physical_device()
        ))
        .into_iter()
        .map(|extension_property| {
            let spec_version = extension_property.spec_version;

            let bytes = bytes_of(&extension_property.extension_name);
            let length = bytes
                .iter()
                .position(|&byte| b'\0' == byte)
                .unwrap_or(extension_property.extension_name.len());

            Extension {
                name: unsafe { RStr::from_raw_parts(bytes.as_ptr(), length) }.into(),
                version: Version::new(
                    api_version_major(spec_version).try_into().unwrap(),
                    api_version_minor(spec_version).try_into().unwrap(),
                    api_version_patch(spec_version).try_into().unwrap(),
                ),
            }
        })
        .collect())
    }

    #[inline]
    fn guard(&self) -> fenrir_hal::device::guard::Guard {
        fenrir_hal::device::guard::Guard::new(self.guard())
    }

    #[inline]
    fn handle(&self) -> fenrir_hal::device::Handle {
        self.handle().as_core_handle()
    }

    #[inline]
    fn name(&self) -> Tuple2<*const u8, usize> {
        self.name().into()
    }

    #[inline]
    fn new_render_pass(
        &self,
        metadata: &fenrir_hal::render_pass::Metadata,
    ) -> RResult<fenrir_hal::render_pass::stable_abi::Trait_TO<RArc<()>>, Error> {
        match metadata.clone().try_into() {
            Err(error) => RErr(error),
            Ok(metadata_) => ROk(fenrir_hal::render_pass::stable_abi::Trait_TO::from_ptr(
                RArc::new(rtry!(RenderPass::new(metadata_))),
                TD_Opaque,
            )),
        }
    }

    #[inline]
    fn new_texture(
        &self,
        metadata: &fenrir_hal::texture::Metadata,
    ) -> FfiFuture<RResult<fenrir_hal::texture::stable_abi::Trait_TO<RArc<()>>, Error>> {
        let metadata_result = metadata.clone().try_into();

        async move {
            match metadata_result {
                Err(error) => RErr(error),
                Ok(metadata_) => ROk(fenrir_hal::texture::stable_abi::Trait_TO::from_ptr(
                    RArc::new(rtry!(Image::new(metadata_).await)),
                    TD_Opaque,
                )),
            }
        }
        .into_ffi()
    }

    #[inline]
    fn queue_families(&self) -> RVec<fenrir_hal::queue::Family> {
        self.queue_families
            .iter()
            .map(|queue_family| {
                let index = queue_family.index().try_into().unwrap();

                let length = queue_family.len();

                let mut flags_: SmallVec<[fenrir_hal::queue::family::Flags; 8]> = queue_family
                    .flags()
                    .into_iter()
                    .map(|flag| match flag {
                        QueueFlags::GRAPHICS => fenrir_hal::queue::family::Flags::Graphics,
                        QueueFlags::COMPUTE => fenrir_hal::queue::family::Flags::Compute,
                        QueueFlags::TRANSFER => fenrir_hal::queue::family::Flags::Transfer,
                        QueueFlags::SPARSE_BINDING => {
                            fenrir_hal::queue::family::Flags::SparseBinding
                        }
                        QueueFlags::VIDEO_DECODE_KHR => {
                            fenrir_hal::queue::family::Flags::VideoDecode
                        }
                        QueueFlags::VIDEO_ENCODE_KHR => {
                            fenrir_hal::queue::family::Flags::VideoEncode
                        }
                        QueueFlags::PROTECTED => fenrir_hal::queue::family::Flags::ProtectedMemory,
                        _ => fenrir_hal::queue::family::Flags::Unknown,
                    })
                    .collect();

                let flag_count = min(8, flags_.len());

                flags_.resize(8, fenrir_hal::queue::family::Flags::Unknown);

                let flags: Tuple2<[fenrir_hal::queue::family::Flags; 8], usize> =
                    ((&flags_[0..8]).try_into().unwrap(), flag_count).into();

                fenrir_hal::queue::Family {
                    index,
                    length,
                    flags,
                }
            })
            .collect()
    }
}

unsafe impl Send for Device {}
unsafe impl Sync for Device {}

impl Debug for Guard {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Guard")
            .field(
                "device",
                &format_args!(
                    "ash::Device{{ ash::vk::Device: {:?} }}",
                    self.device.handle()
                ),
            )
            .field("instance", &self.instance)
            .finish_non_exhaustive()
    }
}

impl Deref for Guard {
    type Target = ash::Device;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.device
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        self.trace_calling_destroy_device();

        {
            let _span = debug_span!("destroy_device");

            unsafe { self.device.destroy_device(None) };
        }

        self.trace_called_destroy_device();
    }
}

impl Eq for Guard {}

impl fenrir_hal::device::guard::stable_abi::Trait for Guard {
    #[inline]
    fn handle(&self) -> fenrir_hal::device::Handle {
        fenrir_hal::device::Handle::new(bytes_of(&self.handle))
    }
}

impl PartialEq for Guard {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.device.handle() == other.device.handle()
    }
}

impl PartialOrd for Guard {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.device.handle().cmp(&other.device.handle()))
    }
}

impl Ord for Guard {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.device.handle().cmp(&other.device.handle())
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use {
        crate::instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
        pretty_assertions::assert_eq,
        tokio::test,
    };

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn eq() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            let device_count_minus_one = if devices.is_empty() {
                0
            } else {
                devices.len() - 1
            };

            for device in &devices {
                assert_eq!(*device, *device);

                assert_eq!(
                    device_count_minus_one,
                    devices.iter().filter(|device_| device != *device_).count()
                );
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn queue_families() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                let queue_families = device.queue_families();

                assert!(!queue_families.is_empty());
            }
        }
    }
}
