use {ash::vk::CompareOp, fenrir_hal::compare::Operation};

#[inline]
pub(crate) fn ash_to_hal(operation: CompareOp) -> Operation {
    if CompareOp::NEVER == operation {
        Operation::Never
    } else if CompareOp::LESS == operation {
        Operation::Less
    } else if CompareOp::EQUAL == operation {
        Operation::Equal
    } else if CompareOp::LESS_OR_EQUAL == operation {
        Operation::LessOrEqual
    } else if CompareOp::GREATER == operation {
        Operation::Greater
    } else if CompareOp::NOT_EQUAL == operation {
        Operation::NotEqual
    } else if CompareOp::GREATER_OR_EQUAL == operation {
        Operation::GreaterOrEqual
    } else {
        Operation::Always
    }
}

#[inline]
pub(crate) const fn hal_to_ash(operation: Operation) -> CompareOp {
    match operation {
        Operation::Never => CompareOp::NEVER,
        Operation::Less => CompareOp::LESS,
        Operation::Equal => CompareOp::EQUAL,
        Operation::LessOrEqual => CompareOp::LESS_OR_EQUAL,
        Operation::Greater => CompareOp::GREATER,
        Operation::NotEqual => CompareOp::NOT_EQUAL,
        Operation::GreaterOrEqual => CompareOp::GREATER_OR_EQUAL,
        Operation::Always => CompareOp::ALWAYS,
    }
}
