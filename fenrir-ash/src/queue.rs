pub mod family;
pub mod submitter;

use core::ops::Deref;

pub(crate) type Family = crate::queue::family::Family;

pub(crate) struct Queue {
    queue: ash::vk::Queue,
}

impl Queue {
    #[inline]
    pub(crate) const fn new(queue: ash::vk::Queue) -> Self {
        Self { queue }
    }
}

impl Deref for Queue {
    type Target = ash::vk::Queue;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.queue
    }
}
