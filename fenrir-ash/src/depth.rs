use {
    crate::{command::buffer::Recorder, device::Guard as DeviceGuard, trace_called, trace_calling},
    abi_stable::std_types::RArc,
    ash::{
        prelude::VkResult,
        vk::{CommandBuffer, CompareOp},
    },
    fenrir_hal::depth::{Bias as HalBias, Depth as HalDepth},
    float_eq::float_eq,
    tracing::debug_span,
};

#[derive(Clone, Copy, Debug)]
pub(crate) struct Bias {
    clamp: f32,
    constant_factor: f32,
    enable: bool,
    slope_factor: f32,
}

#[derive(Clone, Copy, Debug)]
pub(crate) struct Depth {
    test: bool,
    write: bool,
    compare_op: CompareOp,
    bounds_test: bool,
    min_bounds: f32,
    max_bounds: f32,
}

impl Bias {
    #[inline]
    pub(crate) const fn new(
        enable: bool,
        clamp: f32,
        constant_factor: f32,
        slope_factor: f32,
    ) -> Self {
        Self {
            clamp,
            constant_factor,
            enable,
            slope_factor,
        }
    }

    #[cold]
    #[inline]
    fn format_cmd_set_depth_bias(
        args: (&RArc<DeviceGuard>, CommandBuffer, f32, f32, f32),
    ) -> String {
        let (device, command, constant_factor, clamp, slope_factor) = args;

        format!(
            "cmd_set_depth_bias(&self: {device:?}, command: {command:?}, constant_factor: \
            {constant_factor:?}, clamp: {clamp:?}, slope_factor: {slope_factor:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_depth_bias_enable(args: (&RArc<DeviceGuard>, CommandBuffer, bool)) -> String {
        let (device, command, enable) = args;

        format!(
            "cmd_set_depth_bias_enable(&self: {device:?}, command: {command:?}, depth_bias_enable: \
            {enable:?})"
        )
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let device = recorder.buffer().device();

        let command_buffer = recorder.buffer().ash();

        {
            let trace_args = (
                &device,
                command_buffer,
                self.constant_factor,
                self.clamp,
                self.slope_factor,
            );

            trace_calling(Self::format_cmd_set_depth_bias, trace_args);

            {
                let _span = debug_span!("cmd_set_depth_bias");

                unsafe {
                    device.cmd_set_depth_bias(
                        command_buffer,
                        self.constant_factor,
                        self.clamp,
                        self.slope_factor,
                    );
                }
            }

            trace_called(
                Self::format_cmd_set_depth_bias,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (&device, command_buffer, self.enable);

            trace_calling(Self::format_cmd_set_depth_bias_enable, trace_args);

            {
                let _span = debug_span!("cmd_set_depth_bias_enable");

                unsafe {
                    device.cmd_set_depth_bias_enable(command_buffer, self.enable);
                }
            }

            trace_called(
                Self::format_cmd_set_depth_bias_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
    }
}

impl Depth {
    #[inline]
    pub(crate) const fn bounds_test(&self) -> bool {
        self.bounds_test
    }

    #[inline]
    pub(crate) const fn compare_op(&self) -> CompareOp {
        self.compare_op
    }

    #[inline]
    pub(crate) const fn max_bounds(&self) -> f32 {
        self.max_bounds
    }

    #[inline]
    pub(crate) const fn min_bounds(&self) -> f32 {
        self.min_bounds
    }

    #[inline]
    pub(crate) const fn test(&self) -> bool {
        self.test
    }

    #[inline]
    pub(crate) const fn write(&self) -> bool {
        self.write
    }
}

impl Eq for Bias {}

#[allow(clippy::from_over_into)]
impl Into<HalBias> for Bias {
    #[inline]
    fn into(self) -> HalBias {
        HalBias::new(
            self.enable,
            self.clamp,
            self.constant_factor,
            self.slope_factor,
        )
    }
}

impl PartialEq for Bias {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            float_eq!(self.clamp, other.clamp, r2nd <= f32::EPSILON),
            float_eq!(
                self.constant_factor,
                other.constant_factor,
                r2nd <= f32::EPSILON
            ),
            self.enable == other.enable,
            float_eq!(self.slope_factor, other.slope_factor, r2nd <= f32::EPSILON),
        ];

        values.iter().all(|value| *value)
    }
}

impl Eq for Depth {}

impl From<HalDepth> for Depth {
    #[inline]
    fn from(depth: HalDepth) -> Self {
        Self {
            test: depth.test(),
            write: depth.write(),
            compare_op: crate::compare::operation::hal_to_ash(depth.compare_operation()),
            bounds_test: depth.bounds_test(),
            min_bounds: depth.min_bounds(),
            max_bounds: depth.max_bounds(),
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<HalDepth> for Depth {
    #[inline]
    fn into(self) -> HalDepth {
        HalDepth::new(
            self.test,
            self.write,
            crate::compare::operation::ash_to_hal(self.compare_op),
            self.bounds_test,
            self.min_bounds,
            self.max_bounds,
        )
    }
}

impl PartialEq for Depth {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.bounds_test == other.bounds_test,
            self.compare_op == other.compare_op,
            self.test == other.test,
            self.write == other.write,
            self.max_bounds == other.max_bounds,
            self.min_bounds == other.min_bounds,
        ];

        values.iter().all(|value| *value)
    }
}
