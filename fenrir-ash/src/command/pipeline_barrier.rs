use {crate::image::MemoryBarrier as ImageMemoryBarrier, smallvec::SmallVec};

#[derive(Default)]
pub(crate) struct PipelineBarrier {
    image_memory_barriers: SmallVec<[ImageMemoryBarrier; 1]>,
}

impl PipelineBarrier {
    #[inline]
    pub(crate) fn add_image_memory_barriers(
        mut self,
        image_memory_barriers: impl IntoIterator<Item = ImageMemoryBarrier>,
    ) -> Self {
        self.image_memory_barriers.extend(image_memory_barriers);

        self
    }
}

#[allow(clippy::from_over_into)]
impl Into<SmallVec<[ImageMemoryBarrier; 1]>> for PipelineBarrier {
    #[inline]
    fn into(self) -> SmallVec<[ImageMemoryBarrier; 1]> {
        self.image_memory_barriers
    }
}
