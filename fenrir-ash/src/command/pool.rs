use {
    crate::{
        command::{buffer::InheritanceInfo, Buffer as CommandBuffer},
        queue::Family,
        result::{out_of_device_memory, out_of_host_memory, unexpected_error},
        semaphore::Semaphore,
        trace_called, trace_calling,
    },
    abi_stable::std_types::RArc,
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            CommandBufferAllocateInfo, CommandBufferInheritanceRenderingInfo, CommandBufferLevel,
            CommandPool, CommandPoolCreateFlags, CommandPoolCreateInfo, CommandPoolResetFlags,
        },
    },
    async_lock::RwLock,
    core::{
        fmt::{Debug, Formatter},
        future::Future,
        ops::{Deref, Range},
        pin::{pin, Pin},
        sync::atomic::{AtomicBool, Ordering},
        task::{Context, Poll},
    },
    fenrir_error::{argument_out_of_domain, state_not_recoverable, Error},
    futures_util::StreamExt,
    sleipnir::ffi::runtime::Handle,
    tracing::debug_span,
};

extern crate alloc;

#[derive(Debug)]
struct CreateInfo {
    flags: CommandPoolCreateFlags,
    queue_family_index: u32,
}

impl CreateInfo {
    #[inline]
    fn ash<'a>(&self) -> CommandPoolCreateInfo<'a> {
        CommandPoolCreateInfo::default()
            .flags(self.flags)
            .queue_family_index(self.queue_family_index)
    }
}

#[derive(Debug)]
pub(crate) struct Guard {
    semaphore: RwLock<Semaphore>,
    command_pool: CommandPool,
    // create_info: CreateInfo,
    submitted: AtomicBool,
    queue_family: Family,
    device: RArc<crate::device::Guard>,
}

#[must_use = "futures do nothing unless you `.await` or poll them"]
pub(crate) struct Pool {
    next: Option<Pin<Box<dyn Future<Output = Result<Arc<Guard>, Error>> + Send>>>,
    queue_family_push: Option<Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>>,
    inner: Option<Arc<Guard>>,
    semaphore: Arc<RwLock<crate::semaphore::Guard>>,
    runtime: Handle,
}

impl Guard {
    #[inline]
    pub(crate) fn new(
        queue_family: Family,
        device: RArc<crate::device::Guard>,
    ) -> Result<Self, Error> {
        let semaphore = RwLock::new(Semaphore::new(device.clone())?);

        let submitted = AtomicBool::new(false);

        let queue_family_index = queue_family.index();

        let create_info = CreateInfo {
            flags: CommandPoolCreateFlags::default(),
            queue_family_index,
        };

        let ash_create_info = create_info.ash();

        let create_command_pool = || {
            let _spane = debug_span!("create_command_pool");

            unsafe { device.create_command_pool(&ash_create_info, None) }
        };

        let trace_args = (device.deref(), ash_create_info);

        trace_calling(Self::format_create_command_pool, trace_args);

        let command_pool_ = create_command_pool();

        trace_called(
            Self::format_create_command_pool,
            trace_args,
            Some(&command_pool_),
        );

        let command_pool = match command_pool_ {
            Ok(command_pool_) => Ok(command_pool_),
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }?;

        Ok(Self {
            semaphore,
            command_pool,
            submitted,
            queue_family,
            device,
        })
    }

    #[inline]
    pub(crate) fn device(&self) -> RArc<crate::device::Guard> {
        self.device.clone()
    }

    #[cold]
    #[inline]
    fn format_create_command_pool(args: (&crate::device::Guard, CommandPoolCreateInfo)) -> String {
        let (device, create_info) = args;

        format!("create_command_pool(&self: {device:?}, create_info: {create_info:?}, allocation_callbacks: None)")
    }

    #[cold]
    #[inline]
    fn format_destroy_command_pool(args: (&crate::device::Guard, CommandPool)) -> String {
        let (device, command_pool) = args;

        format!("destroy_command_pool(&self: {device:?}, pool: {command_pool:?}, allocation_callbacks: None)")
    }

    #[inline]
    pub(crate) fn queue_family(&self) -> crate::queue::family::Family {
        self.queue_family.clone()
    }

    #[inline]
    pub(crate) fn queue_submitter(&self) -> crate::queue::submitter::Submitter {
        self.queue_family.queue_submitter()
    }

    #[inline]
    pub(crate) async fn semaphore(&self) -> Arc<RwLock<crate::semaphore::Guard>> {
        self.semaphore.read().await.guard()
    }

    #[inline]
    pub(crate) const fn submitted(&self) -> &AtomicBool {
        &self.submitted
    }
}

impl Pool {
    #[inline]
    pub(crate) async fn new(pool: Arc<Guard>, runtime: Handle) -> Self {
        let semaphore = pool.semaphore.read().await.guard();

        let inner = Some(pool);

        let queue_family_push = None;

        let next = None;

        Self {
            next,
            queue_family_push,
            inner,
            semaphore,
            runtime,
        }
    }

    #[inline]
    pub(crate) fn buffer(
        &self,
        level: CommandBufferLevel,
        inheritance_info: Option<CommandBufferInheritanceRenderingInfo>,
    ) -> Result<crate::command::Buffer, Error> {
        let inner = self.get_inner_as_ref()?;

        let allocate_info = CommandBufferAllocateInfo::default()
            .command_pool(***inner)
            .level(level)
            .command_buffer_count(1);

        let device = &*inner.device;

        let trace_args = (device, allocate_info);

        let allocate_command_buffer = || {
            let _span = debug_span!("allocate_command_buffers");

            unsafe { device.allocate_command_buffers(&allocate_info) }
        };

        trace_calling(Self::format_allocate_command_buffers, trace_args);

        let buffer_result = allocate_command_buffer();

        trace_called(
            Self::format_allocate_command_buffers,
            trace_args,
            Some(&buffer_result),
        );

        match buffer_result {
            Ok(buffer_vec) => {
                let maybe_inheritance_info = inheritance_info.map(InheritanceInfo::from);

                Ok(CommandBuffer::new(
                    buffer_vec[0],
                    level,
                    maybe_inheritance_info,
                    self,
                )?)
            }
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    pub(crate) fn buffers(
        &self,
        level: CommandBufferLevel,
        inheritance_info: Option<CommandBufferInheritanceRenderingInfo>,
        count: usize,
    ) -> Result<Vec<CommandBuffer>, Error> {
        let inner = self.get_inner_as_ref()?;

        let count_u32 = count.try_into().map_err(|_| {
            argument_out_of_domain(
                Range {
                    start: 0.into(),
                    end: u32::MAX.into(),
                },
                count.into(),
            )
        })?;

        let allocate_info = CommandBufferAllocateInfo::default()
            .command_pool(***inner)
            .level(level)
            .command_buffer_count(count_u32);

        let device = &*inner.device;

        let trace_args = (device, allocate_info);

        let allocate_command_buffer = || {
            let _span = debug_span!("allocate_command_buffers");

            unsafe { device.allocate_command_buffers(&allocate_info) }
        };

        trace_calling(Self::format_allocate_command_buffers, trace_args);

        let buffer_result = allocate_command_buffer();

        trace_called(
            Self::format_allocate_command_buffers,
            trace_args,
            Some(&buffer_result),
        );

        match buffer_result {
            Ok(buffers) => {
                let maybe_inheritance_info = inheritance_info.map(InheritanceInfo::from);

                buffers
                    .into_iter()
                    .map(|buffer| {
                        crate::command::Buffer::new(
                            buffer,
                            level,
                            maybe_inheritance_info.clone(),
                            self,
                        )
                    })
                    .collect::<Result<Vec<_>, Error>>()
            }
            Err(result) => Err(match result {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    fn check_is_submited_semaphore_poll_next_reset_command_pool_and_push_back_to_queue_family(
        &mut self,
        cx: &mut Context<'_>,
    ) -> Poll<Result<&mut Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>, Error>> {
        self.inner.take().map_or_else(
            || {
                Poll::Ready(Err(state_not_recoverable(
                    "inner must not be taken before awaited",
                )))
            },
            |inner| {
                if !inner.submitted.load(Ordering::Relaxed) {
                    Poll::Ready(Ok(self.queue_family_push(inner)))
                } else {
                    match pin!(self.semaphore_poll_next(inner)).poll(cx) {
                        Poll::Pending => Poll::Pending,
                        Poll::Ready(result) => match result {
                            Err(error) => Poll::Ready(Err(error)),
                            Ok(pool) => Poll::Ready(
                                self.reset_command_pool_and_push_back_to_queue_family(pool),
                            ),
                        },
                    }
                }
            },
        )
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn device(&self) -> RArc<crate::device::Guard> {
        self.inner
            .as_ref()
            .expect("pool must not be taken before awaited")
            .device()
    }

    #[cold]
    #[inline]
    fn format_allocate_command_buffers(
        args: (&crate::device::Guard, CommandBufferAllocateInfo),
    ) -> String {
        let (device, allocate_info) = args;

        format!("allocate_command_buffers({device:?}, allocate_info: {allocate_info:?})")
    }

    #[cold]
    #[inline]
    fn format_reset_command_pool(
        args: (&crate::device::Guard, CommandPool, CommandPoolResetFlags),
    ) -> String {
        let (device, command_pool, flags) = args;

        format!("reset_command_pool(&self: {device:?}, command_pool: {command_pool:?}, flags: {flags:?})")
    }

    #[inline]
    fn get_inner_as_ref(&self) -> Result<&Arc<Guard>, Error> {
        self.inner
            .as_ref()
            .ok_or_else(|| state_not_recoverable("inner must not be taken before awaited"))
    }

    #[inline]
    pub(crate) fn guard(&self) -> Result<Arc<Guard>, Error> {
        Ok(self.get_inner_as_ref()?.clone())
    }

    #[inline]
    fn queue_family_push(
        &mut self,
        pool: Arc<Guard>,
    ) -> &mut Pin<Box<dyn Future<Output = Result<(), Error>> + Send>> {
        let queue_family_push_future = self.queue_family_push.insert(Box::pin(async move {
            pool.clone().queue_family.push(pool).await;
            Ok(())
        }));

        self.next = None;

        queue_family_push_future
    }

    #[inline]
    fn poll_future_result(
        future_result: Result<&mut Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>, Error>,
        cx: &mut Context<'_>,
    ) -> Poll<Result<(), Error>> {
        match future_result {
            Ok(future) => pin!(future).poll(cx),
            Err(error) => Poll::Ready(Err(error)),
        }
    }

    #[inline]
    fn reset_command_pool_and_push_back_to_queue_family(
        &mut self,
        pool: Arc<Guard>,
    ) -> Result<&mut Pin<Box<dyn Future<Output = Result<(), Error>> + Send>>, Error> {
        let device = &*pool.device;

        let flags = CommandPoolResetFlags::empty();

        let reset_command_pool = || {
            let _span = debug_span!("reset_command_pool");

            unsafe { pool.device.reset_command_pool(**pool, flags) }
        };

        let trace_args = (device, pool.command_pool, flags);

        trace_calling(Self::format_reset_command_pool, trace_args);

        let reset_result = reset_command_pool();

        trace_called(
            Self::format_reset_command_pool,
            trace_args,
            Some(&reset_result),
        );

        if let Err(error) = reset_result {
            Err(match error {
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                error => unexpected_error(error),
            })
        } else {
            Ok(self.queue_family_push(pool))
        }
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) fn semaphore(&self) -> Arc<RwLock<crate::semaphore::Guard>> {
        self.semaphore.clone()
    }

    #[inline]
    fn semaphore_poll_next(
        &mut self,
        pool: Arc<Guard>,
    ) -> &mut Pin<Box<dyn Future<Output = Result<Arc<Guard>, Error>> + Send>> {
        let next: Pin<Box<dyn Future<Output = Result<Arc<Guard>, Error>> + Send>> =
            Box::pin(async {
                let mut semaphore = pool.semaphore.write().await;

                let maybe_next_result = semaphore.next().await;

                match maybe_next_result {
                    None => Err(state_not_recoverable(&format!(
                        "({:?} as StreamExt)::next returned None",
                        *semaphore
                    ))),
                    Some(next_result) => match next_result {
                        Ok(()) => {
                            drop(semaphore);

                            Ok(pool)
                        }
                        Err(error) => Err(error),
                    },
                }
            });

        self.next.insert(next)
    }
}

impl Deref for Guard {
    type Target = CommandPool;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.command_pool
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        let device = &*self.device;

        let command_pool = self.command_pool;

        let trace_args = (device, command_pool);

        trace_calling(Self::format_destroy_command_pool, trace_args);

        {
            let _span = debug_span!("destroy_command_pool");

            unsafe { device.destroy_command_pool(self.command_pool, None) };
        }

        trace_called(
            Self::format_destroy_command_pool,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

unsafe impl Send for Guard {}
unsafe impl Sync for Guard {}

impl Debug for Pool {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Poll")
            .field("inner", &self.inner)
            .field("runtime", &self.runtime)
            .finish_non_exhaustive()
    }
}

impl Future for Pool {
    type Output = Result<(), Error>;

    #[inline]
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        match this.queue_family_push.as_mut() {
            None => match this.next.as_mut() {
                None => {
                    let future_poll_result =
                        this.check_is_submited_semaphore_poll_next_reset_command_pool_and_push_back_to_queue_family(cx);

                    match future_poll_result {
                        Poll::Pending => Poll::Pending,
                        Poll::Ready(future_result) => Self::poll_future_result(future_result, cx),
                    }
                }
                Some(next_future) => match pin!(next_future).poll(cx) {
                    Poll::Pending => Poll::Pending,
                    Poll::Ready(next_result) => match next_result {
                        Err(error) => Poll::Ready(Err(error)),
                        Ok(pool) => Self::poll_future_result(
                            this.reset_command_pool_and_push_back_to_queue_family(pool),
                            cx,
                        ),
                    },
                },
            },
            Some(queue_family_push_future) => {
                Self::poll_future_result(Ok(queue_family_push_future), cx)
            }
        }
    }
}

unsafe impl Send for Pool {}
unsafe impl Sync for Pool {}

impl Unpin for Pool {}

#[cfg(test)]
mod tests {
    use {
        crate::instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
        ash::vk::CommandBufferLevel,
        futures_time::{future::FutureExt, time::Duration},
        tokio::test,
    };

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                for queue_family in device.queue_families() {
                    let command_pool = queue_family.command_pool().await;

                    assert!(command_pool.is_ok());

                    assert!(command_pool.unwrap().await.is_ok());
                }
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new_commmand_buffer() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                for queue_family in device.queue_families() {
                    let command_pool_result = queue_family.command_pool().await;

                    assert!(command_pool_result.is_ok());

                    let command_pool = command_pool_result.unwrap();

                    let command_buffer = command_pool.buffer(CommandBufferLevel::PRIMARY, None);

                    assert!(command_buffer.is_ok());

                    assert!(command_pool.await.is_ok());
                }
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn record_submit() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                for queue_family in device.queue_families() {
                    let command_pool_result = queue_family.command_pool().await;

                    assert!(command_pool_result.is_ok());

                    let command_pool = command_pool_result.unwrap();

                    let command_buffer_result =
                        command_pool.buffer(CommandBufferLevel::PRIMARY, None);

                    assert!(command_buffer_result.is_ok());

                    let mut command_buffer = command_buffer_result.unwrap();

                    {
                        let recorder = command_buffer.record();

                        assert!(recorder.is_ok());
                    }

                    let submit_result = command_buffer.submit().await;

                    assert!(submit_result.is_ok());

                    let timeout_result = command_pool.timeout(Duration::from_secs(1)).await;

                    assert!(timeout_result.is_ok());

                    assert!(timeout_result.unwrap().is_ok());
                }
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn submit_not_recorded() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                for queue_family in device.queue_families() {
                    let command_pool_result = queue_family.command_pool().await;

                    assert!(command_pool_result.is_ok());

                    let command_pool = command_pool_result.unwrap();

                    let commmand_buffer_result =
                        command_pool.buffer(CommandBufferLevel::PRIMARY, None);

                    assert!(commmand_buffer_result.is_ok());

                    let command_buffer = commmand_buffer_result.unwrap();

                    let submit_result = command_buffer.submit().await;

                    assert!(submit_result.is_err());

                    assert!(command_pool.await.is_ok());
                }
            }
        }
    }
}
