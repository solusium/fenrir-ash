use {
    crate::{
        command::{pool::Guard as CommandPoolGuard, PipelineBarrier, Pool as CommandPool},
        device::Guard as DeviceGuard,
        graphics::pipeline::{Guard as GraphicsPipelineGuard, Renderer},
        queue::submitter::Metadata,
        result::{out_of_device_memory, out_of_host_memory, unexpected_error},
        semaphore::Guard as SemaphoreGuard,
        trace_called, trace_calling,
    },
    abi_stable::std_types::RArc,
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            CommandBuffer, CommandBufferBeginInfo, CommandBufferInheritanceInfo,
            CommandBufferInheritanceRenderingInfo, CommandBufferLevel, CommandBufferSubmitInfo,
            CommandBufferUsageFlags, DependencyInfo, Format, PipelineBindPoint,
            PipelineStageFlags2, RenderingFlags, RenderingInfo, SampleCountFlags,
        },
    },
    async_lock::RwLock,
    core::{
        ops::Deref,
        slice::{from_raw_parts, from_ref},
        sync::atomic::Ordering,
    },
    fenrir_error::{state_not_recoverable, Error},
    futures_util::{stream, StreamExt},
    smallvec::SmallVec,
    tracing::debug_span,
};

extern crate alloc;

#[derive(Debug)]
enum State {
    Initial,
    Recording,
    Executable,
    Pending,
}

#[derive(Debug)]
pub(crate) struct Buffer {
    inner: CommandBuffer,
    signal_semaphores: SmallVec<[(Arc<RwLock<SemaphoreGuard>>, PipelineStageFlags2); 2]>,
    level: CommandBufferLevel,
    inheritance_info: Option<InheritanceInfo>,
    pool: Arc<CommandPoolGuard>,
    state: State,
}

#[derive(Clone, Debug)]
pub(crate) struct InheritanceInfo {
    flags: RenderingFlags,
    view_mask: u32,
    color_attachments: SmallVec<[Format; 3]>,
    depth_attachment: Format,
    stencil_attachment: Format,
    rasterization_samples: SampleCountFlags,
}

pub(crate) struct Recorder<'buffer> {
    buffer: &'buffer mut Buffer,
}

pub(crate) struct SubmitInfo<'a> {
    submit_info: CommandBufferSubmitInfo<'a>,
}

impl Buffer {
    pub(crate) fn new(
        inner: CommandBuffer,
        level: CommandBufferLevel,
        inheritance_info: Option<InheritanceInfo>,
        pool: &CommandPool,
    ) -> Result<Self, Error> {
        let signal_semaphores = SmallVec::new();

        let pool = pool.guard()?;

        let state = State::Initial;

        Ok(Self {
            inner,
            signal_semaphores,
            level,
            inheritance_info,
            pool,
            state,
        })
    }

    #[inline]
    pub(crate) const fn ash(&self) -> CommandBuffer {
        self.inner
    }

    #[inline]
    pub(crate) fn device(&self) -> RArc<crate::device::Guard> {
        self.pool.device()
    }

    #[cold]
    #[inline]
    fn format_begin_command_buffer(
        args: (&DeviceGuard, CommandBuffer, CommandBufferBeginInfo),
    ) -> String {
        let (device, command_buffer, begin_info) = args;

        let begin_info_string = if begin_info.p_inheritance_info.is_null() {
            format!("{:?}", begin_info.p_inheritance_info)
        } else {
            let inheritance_info = unsafe { *begin_info.p_inheritance_info };

            format!(
                "CommandBufferInheritanceInfo {{ s_type: {:?}, p_next: {:?}, render_pass: \
                {:?}, subpass: {:?}, framebuffer: {:?}, occlusion_query_enable: {:?}, \
                query_flags: {:?}, pipeline_statistics: {:?} }}",
                inheritance_info.s_type,
                inheritance_info.p_next,
                inheritance_info.render_pass,
                inheritance_info.subpass,
                inheritance_info.framebuffer,
                inheritance_info.occlusion_query_enable,
                inheritance_info.query_flags,
                inheritance_info.pipeline_statistics
            )
        };

        format!(
            "begin_command_buffer(&self: {device:?}, command_buffer: {command_buffer:?}, \
             begin_info: {begin_info_string:?})"
        )
    }

    #[inline]
    pub(crate) const fn level(&self) -> CommandBufferLevel {
        self.level
    }

    #[inline]
    pub(crate) fn record(&mut self) -> Result<Recorder<'_>, Error> {
        match self.state {
            State::Initial => {
                let device = self.pool.device();

                let command_buffer = self.inner;

                let mut maybe_rendering_inheritance_info = self
                    .inheritance_info
                    .as_ref()
                    .map(|inheritance_info| inheritance_info.ash());

                let maybe_inheritance_info =
                    maybe_rendering_inheritance_info
                        .as_mut()
                        .map(|rendering_inheritance_info| {
                            CommandBufferInheritanceInfo::default()
                                .push_next(rendering_inheritance_info)
                        });

                let mut flags = CommandBufferUsageFlags::ONE_TIME_SUBMIT;

                if self.inheritance_info.is_some() {
                    flags |= CommandBufferUsageFlags::RENDER_PASS_CONTINUE;
                }

                let mut begin_info = CommandBufferBeginInfo::default().flags(flags);

                if let Some(inheritance_info) = maybe_inheritance_info.as_ref() {
                    begin_info = begin_info.inheritance_info(inheritance_info);
                }

                let begin_command_buffer = || {
                    let _span = debug_span!("begin_command_buffer");

                    unsafe { device.begin_command_buffer(command_buffer, &begin_info) }
                };

                let trace_args = (device.deref(), command_buffer, begin_info);

                trace_calling(Self::format_begin_command_buffer, trace_args);

                let begin_result = begin_command_buffer();

                trace_called(
                    Self::format_begin_command_buffer,
                    trace_args,
                    Some(&begin_result),
                );

                match begin_result {
                    Ok(()) => {
                        self.state = State::Recording;

                        let buffer = self;

                        Ok(Recorder { buffer })
                    }
                    Err(result) => Err(match result {
                        ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                        ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                        error => unexpected_error(error),
                    }),
                }
            }
            _ => Err(state_not_recoverable(&format!(
                "expected a command buffer to be in State::Initial before recording commands: \
                command buffer: {self:?}"
            ))),
        }
    }

    #[inline]
    pub(crate) async fn submit(mut self) -> Result<(), Error> {
        match self.state {
            State::Executable => {
                let pool = &self.pool;

                let submitted_result = pool.submitted().compare_exchange(
                    false,
                    true,
                    Ordering::Relaxed,
                    Ordering::Relaxed,
                );

                if submitted_result.is_err() {
                    Err(state_not_recoverable(&format!(
                        "submitting from one command pool multiple times before resetting the \
                        pool is not allowed: command buffer: {self:?}, command pool: {pool:?}"
                    )))
                } else {
                    self.signal_semaphores
                        .push((pool.semaphore().await, PipelineStageFlags2::BOTTOM_OF_PIPE));

                    let metadata = Metadata::new(
                        self.pool.queue_family(),
                        from_ref(&self.inner),
                        &self.signal_semaphores,
                    );

                    match self.pool.queue_submitter().submit(metadata).await {
                        Ok(result) => {
                            self.state = State::Pending;

                            Ok(result)
                        }
                        Err(error) => Err(error),
                    }
                }
            }
            _ => Err(state_not_recoverable(&format!(
                "expected {self:?} to be in State::Recording before finshing recording \
                commands",
            ))),
        }
    }
}

impl InheritanceInfo {
    fn ash(&self) -> CommandBufferInheritanceRenderingInfo {
        CommandBufferInheritanceRenderingInfo::default()
            .flags(self.flags)
            .view_mask(self.view_mask)
            .color_attachment_formats(&self.color_attachments)
            .depth_attachment_format(self.depth_attachment)
            .stencil_attachment_format(self.stencil_attachment)
            .rasterization_samples(self.rasterization_samples)
    }
}

impl<'buffer> Recorder<'buffer> {
    #[inline]
    pub(crate) fn buffer(&self) -> &Buffer {
        self.buffer
    }

    #[inline]
    pub(crate) async fn draw(&self, pipeline: &Arc<GraphicsPipelineGuard>) {
        let device = pipeline.device();

        let device_guard = device.guard();

        let command_buffer = self.buffer().ash();

        {
            let bind_point = PipelineBindPoint::GRAPHICS;

            let trace_args = (&device_guard, command_buffer, bind_point, ***pipeline);

            trace_calling(Self::format_cmd_bind_pipeline, trace_args);

            {
                let _span = debug_span!("cmd_bind_pipeline");

                unsafe {
                    device_guard.cmd_bind_pipeline(
                        command_buffer,
                        PipelineBindPoint::GRAPHICS,
                        ***pipeline,
                    );
                };
            }

            trace_called(
                Self::format_cmd_bind_pipeline,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        {
            let draw_calls = pipeline.draw_calls().read().await;

            draw_calls.iter().for_each(|(id, _)| {
                id.color_blend().set_dynamics(self);

                id.depth_stencil().set_dynamics(self);

                id.input_assembly().set_dynamics(self);

                id.multisample().set_dynamics(self);

                id.rasterization().set_dynamics(self);

                id.tessellation().set_dynamics(self);

                id.vertex_input().set_dynamics(self);

                id.viewport().set_dynamics(self);
            });
        }

        unsafe {
            device_guard.cmd_draw(command_buffer, 3, 1, 0, 0);
        }
    }

    #[cold]
    #[inline]
    fn format_cmd_bind_pipeline(
        args: (
            &RArc<DeviceGuard>,
            CommandBuffer,
            PipelineBindPoint,
            ash::vk::Pipeline,
        ),
    ) -> String {
        let (device, command_buffer, bind_point, pipeline) = args;

        format!(
            "cmd_bind_pipeline(&self: {device:?}, command_buffer: {command_buffer:?}, \
            pipeline_bind_point: {bind_point:?}, pipeline: {pipeline:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_execute_commands(
        args: (&RArc<DeviceGuard>, CommandBuffer, &[CommandBuffer]),
    ) -> String {
        let (device, primary_command_buffer, secondary_command_buffers) = args;

        format!("cmd_execute_commands(&self: {device:?}, primary_command_buffer: {primary_command_buffer:?}, \
            secondary_command_buffers: {secondary_command_buffers:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_pipeline_barrier2(
        args: (&DeviceGuard, CommandBuffer, &DependencyInfo),
    ) -> String {
        let (device, command_buffer, dependency_info) = args;

        let memory_barriers = if dependency_info.memory_barrier_count == 0 {
            &[]
        } else {
            unsafe {
                from_raw_parts(
                    dependency_info.p_memory_barriers,
                    dependency_info.memory_barrier_count as usize,
                )
            }
        };

        let buffer_memory_barriers = if dependency_info.buffer_memory_barrier_count == 0 {
            &[]
        } else {
            unsafe {
                from_raw_parts(
                    dependency_info.p_buffer_memory_barriers,
                    dependency_info.buffer_memory_barrier_count as usize,
                )
            }
        };

        let image_memory_barriers = if dependency_info.image_memory_barrier_count == 0 {
            &[]
        } else {
            unsafe {
                from_raw_parts(
                    dependency_info.p_image_memory_barriers,
                    dependency_info.image_memory_barrier_count as usize,
                )
            }
        };

        let dependency_info_string =
            format!(
                "DependencyInfo {{ s_type: {:?}, p_next: {:?}, memory_barrier_count: {:?}, \
                p_memory_barriers: {:?}, buffer_memory_barrier_count: {:?}, p_buffer_memory_barriers: {:?}, \
                image_memory_barrier_count: {:?}, p_image_memory_barriers: {:?} }}", dependency_info.s_type,
                dependency_info.p_next,
                dependency_info.memory_barrier_count,
                memory_barriers,
                dependency_info.buffer_memory_barrier_count,
                buffer_memory_barriers,
                dependency_info.image_memory_barrier_count,
                image_memory_barriers,
            );

        format!(
            "cmd_pipeline_barrier2(&self: {device:?}, command_buffer: {command_buffer:?}, dependency_info: \
            {dependency_info_string:?})",
        )
    }

    #[cold]
    #[inline]
    fn format_end_command_buffer(args: (&RArc<DeviceGuard>, CommandBuffer)) -> String {
        let (device, command_buffer) = args;

        format!("end_command_buffer(&self: {device:?}, command_buffer: {command_buffer:?})")
    }

    #[inline]
    pub(crate) fn execute(
        &self,
        secondary_command_buffers: impl IntoIterator<Item = Buffer>,
    ) -> Result<(), Error> {
        let secondary_command_buffers_inner = secondary_command_buffers
            .into_iter()
            .map(|buffer| {
                if CommandBufferLevel::SECONDARY != buffer.level() {
                    Err(state_not_recoverable(
                        "expected a secondary command buffer, instead got: {buffer:?}",
                    ))
                } else {
                    Ok(buffer.inner)
                }
            })
            .collect::<Result<Vec<_>, Error>>()?;

        let device = self.buffer.pool.device();

        let inner = self.buffer.inner;

        let trace_args = (&device, inner, secondary_command_buffers_inner.as_slice());

        trace_calling(Self::format_cmd_execute_commands, trace_args);

        {
            let _span = debug_span!("cmd_execute_commands");

            unsafe {
                self.buffer
                    .pool
                    .device()
                    .cmd_execute_commands(self.buffer.inner, &secondary_command_buffers_inner);
            };
        }

        trace_called(
            Self::format_cmd_execute_commands,
            trace_args,
            None::<&VkResult<()>>,
        );

        Ok(())
    }

    #[inline]
    pub(crate) async fn pipeline_barrier(&self, pipeline_barrier: PipelineBarrier) {
        let device = self.buffer().device();

        let buffer = self.buffer().ash();

        let image_memory_barriers: SmallVec<[_; 1]> = pipeline_barrier.into();

        let ash_image_memory_barriers = stream::iter(image_memory_barriers.iter())
            .filter(|memory_barrier| async {
                memory_barrier.image().layout().await != memory_barrier.destination()
            })
            .then(|memory_barrier| async { memory_barrier.ash().await })
            .collect::<SmallVec<[_; 1]>>()
            .await;

        if ash_image_memory_barriers.is_empty() {
            return;
        }

        let dependency_info =
            DependencyInfo::default().image_memory_barriers(&ash_image_memory_barriers);

        let trace_args = (&*device, buffer, &dependency_info);

        trace_calling(Self::format_cmd_pipeline_barrier2, trace_args);

        {
            let _span = debug_span!("cmd_pipeline_barrier2");

            unsafe {
                device.cmd_pipeline_barrier2(buffer, &dependency_info);
            }
        }

        trace_called(
            Self::format_cmd_pipeline_barrier2,
            trace_args,
            None::<&VkResult<()>>,
        );

        stream::iter(image_memory_barriers.iter())
            .for_each(|memory_barrier| async move {
                memory_barrier
                    .image()
                    .set_layout(memory_barrier.destination())
                    .await;
            })
            .await;
    }

    #[inline]
    pub(crate) fn renderer<'recorder>(
        &'recorder self,
        rendering_info: RenderingInfo<'recorder>,
    ) -> Renderer<'recorder, 'buffer> {
        Renderer::new(self, rendering_info)
    }
}

impl SubmitInfo<'_> {
    #[inline]
    pub(crate) fn new(command_buffer: CommandBuffer) -> Self {
        let submit_info = CommandBufferSubmitInfo::default().command_buffer(command_buffer);

        Self { submit_info }
    }
}

unsafe impl Send for Buffer {}
unsafe impl Sync for Buffer {}

impl From<CommandBufferInheritanceRenderingInfo<'_>> for InheritanceInfo {
    #[inline]
    fn from(info: CommandBufferInheritanceRenderingInfo) -> Self {
        let flags = info.flags;
        let view_mask = info.view_mask;
        let color_attachments = unsafe {
            from_raw_parts(
                info.p_color_attachment_formats,
                info.color_attachment_count as usize,
            )
        }
        .iter()
        .copied()
        .collect::<SmallVec<[Format; 3]>>();
        let depth_attachment = info.depth_attachment_format;
        let stencil_attachment = info.stencil_attachment_format;
        let rasterization_samples = info.rasterization_samples;

        Self {
            flags,
            view_mask,
            color_attachments,
            depth_attachment,
            stencil_attachment,
            rasterization_samples,
        }
    }
}

impl Deref for Recorder<'_> {
    type Target = CommandBuffer;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.buffer.inner
    }
}

impl Drop for Recorder<'_> {
    fn drop(&mut self) {
        match self.buffer.state {
            State::Recording => {
                let device = self.buffer.pool.device();

                let command_buffer = self.buffer.inner;

                let end_command_buffer = || unsafe {
                    let _span = debug_span!("end_command_buffer");

                    device.end_command_buffer(command_buffer)
                };

                let trace_args = (&device, command_buffer);

                trace_calling(Self::format_end_command_buffer, trace_args);

                let end_result = end_command_buffer();

                trace_called(
                    Self::format_end_command_buffer,
                    trace_args,
                    Some(&end_result),
                );

                if end_result.is_err() {
                    unreachable!()
                }

                self.buffer.state = State::Executable;
            }
            _ => {
                unreachable!()
            }
        }
    }
}

impl<'a> Deref for SubmitInfo<'a> {
    type Target = CommandBufferSubmitInfo<'a>;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.submit_info
    }
}

unsafe impl Send for SubmitInfo<'_> {}
unsafe impl Sync for SubmitInfo<'_> {}

#[cfg(test)]
mod tests {
    use {
        crate::instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
        ash::vk::CommandBufferLevel,
        tokio::test,
    };

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn recorded_not_submitted() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                for queue_family in device.queue_families() {
                    let command_pool_result = queue_family.command_pool().await;

                    assert!(command_pool_result.is_ok());

                    let command_pool = command_pool_result.unwrap();

                    let commmand_buffer_result =
                        command_pool.buffer(CommandBufferLevel::PRIMARY, None);

                    assert!(commmand_buffer_result.is_ok());

                    let mut command_buffer = commmand_buffer_result.unwrap();

                    {
                        let recorder = command_buffer.record();

                        assert!(recorder.is_ok());
                    }

                    assert!(command_pool.await.is_ok());
                }
            }
        }
    }
}
