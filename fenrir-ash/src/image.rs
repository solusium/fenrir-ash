pub(super) use crate::image::view::View;
use {
    crate::{
        device::Guard as DeviceGuard,
        failed_to_cast,
        image::view::Metadata as ViewMetadata,
        result::{
            compression_exhausted, invalid_opaque_capture_address, out_of_device_memory,
            out_of_host_memory, unexpected_error,
        },
        trace_called, trace_calling, Device,
    },
    abi_stable::{
        rtry,
        sabi_trait::TD_CanDowncast,
        std_types::{
            RArc,
            RResult::{self, ROk},
        },
    },
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            Extent3D, Format, Image as AshImage, ImageCreateFlags, ImageCreateInfo, ImageLayout,
            ImageMemoryRequirementsInfo2, ImageTiling, ImageType, ImageUsageFlags,
            MemoryRequirements2, Result as AshResult, SampleCountFlags, SharingMode,
        },
    },
    async_lock::RwLock,
    bytemuck::checked::try_from_bytes,
    core::{
        fmt::Debug,
        marker::PhantomData,
        mem::take,
        ops::{Deref, Range},
    },
    fenrir_ash_interface::device::Memory,
    fenrir_error::{argument_out_of_domain, result_out_of_range, Error},
    fenrir_hal::{
        allocation::{
            self, Allocation, Metadata as HalAllocationMetadata,
            Requirements as HalAllocationRequirements,
        },
        allocator::Allocator,
        color::component::Mapping,
        texture::{
            guard::stable_abi::Trait as HalTextureGuardTrait, stable_abi::Trait,
            view::stable_abi::Trait_TO as HalViewTrait, Guard as HalTextureGuard,
            Metadata as HalTextureMetadata, SubresourceRange, Usage as HalTextureUsage,
            View as HalView,
        },
        Device as HalDevice,
    },
    smallvec::SmallVec,
    strum::IntoEnumIterator,
    tracing::debug_span,
};

extern crate alloc;

pub(super) mod view;

mod memory_barrier;

pub(crate) type MemoryBarrier = crate::image::memory_barrier::MemoryBarrier;

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct CreateInfo {
    flags: ImageCreateFlags,
    image_type: ImageType,
    format: Format,
    extent: Extent3D,
    mip_levels: u32,
    array_layers: u32,
    samples: SampleCountFlags,
    tiling: ImageTiling,
    usage: ImageUsageFlags,
    sharing_mode: SharingMode,
    queue_family_indices: SmallVec<[u32; 4]>,
}

#[derive(Debug)]
pub(crate) struct Guard {
    allocation: Option<Arc<Allocation>>,
    image: AshImage,
    layout: RwLock<ImageLayout>,
    device: RArc<DeviceGuard>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct Image {
    create_info: CreateInfo,
    name: String,
    inner: Arc<Guard>,
    allocator: Option<Allocator>,
    device: HalDevice,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct Metadata {
    device: HalDevice,
    allocator: Option<Allocator>,
    depth: usize,
    height: usize,
    width: usize,
    usage: ImageUsageFlags,
    name: String,
}

impl CreateInfo {
    #[inline]
    pub(crate) fn ash(&self) -> ImageCreateInfo<'_> {
        ImageCreateInfo::default()
            .flags(self.flags)
            .image_type(self.image_type)
            .format(self.format)
            .extent(self.extent)
            .mip_levels(self.mip_levels)
            .array_layers(self.array_layers)
            .samples(self.samples)
            .tiling(self.tiling)
            .usage(self.usage)
            .sharing_mode(self.sharing_mode)
            .queue_family_indices(&self.queue_family_indices)
    }
}

impl Guard {
    #[inline]
    fn device(&self) -> RArc<DeviceGuard> {
        self.device.clone()
    }

    #[cold]
    #[inline]
    fn format_destroy_image(args: (&DeviceGuard, AshImage)) -> String {
        let (device, image) = args;

        format!("destroy_image(&self: {device:?}, image: {image:?}, allocation_callbacks: None)")
    }
}

impl Image {
    pub(crate) async fn new(metadata: Metadata) -> Result<Self, Error> {
        let device = match metadata.device.state().obj.downcast_as::<Device>() {
            Ok(device_) => Ok(device_),
            Err(error) => Err(failed_to_cast(
                &metadata.device.state(),
                PhantomData::<Device>,
                error,
            )),
        }?
        .guard();

        let image_type = if (1 == metadata.depth) && (1 == metadata.height) {
            ImageType::TYPE_1D
        } else if 1 == metadata.depth {
            ImageType::TYPE_2D
        } else {
            ImageType::TYPE_3D
        };

        let u32_extend = [metadata.height, metadata.width, metadata.depth]
            .into_iter()
            .zip([
                u32::try_from(metadata.height),
                u32::try_from(metadata.width),
                u32::try_from(metadata.depth),
            ])
            .map(|(usize_size, u32_size_result)| {
                u32_size_result.map_or_else(
                    |_| {
                        Err(result_out_of_range(
                            0u32.into()..u32::MAX.into(),
                            usize_size.into(),
                        ))
                    },
                    Ok,
                )
            })
            .collect::<Result<SmallVec<[u32; 3]>, Error>>()?;

        let extent = Extent3D::default()
            .height(u32_extend[0])
            .width(u32_extend[1])
            .depth(u32_extend[2]);

        let create_info = Self::new_create_info(image_type, extent, metadata.usage);

        let initial_layout = ImageLayout::UNDEFINED;

        let layout = RwLock::new(initial_layout);

        let ash_image = Self::create_image(
            device.deref(),
            create_info.ash().initial_layout(initial_layout),
        )?;

        let allocator = metadata.allocator.clone();

        let name = metadata.name.clone();

        let allocation = match allocator.as_ref() {
            None => None,
            Some(allocator_) => Some(
                Self::allocate(device.clone(), ash_image, allocator_.clone(), &name)
                    .await
                    .map(Arc::new),
            ),
        }
        .transpose()?;

        let inner = Arc::new(Guard {
            allocation,
            image: ash_image,
            layout,
            device,
        });

        let device = metadata.device.clone();

        Ok(Self {
            create_info,
            name,
            inner,
            allocator,
            device,
        })
    }

    async fn allocate(
        device: RArc<DeviceGuard>,
        image: AshImage,
        allocator: Allocator,
        name: &str,
    ) -> Result<Allocation, Error> {
        let new_memory_metadata = || {
            let device_ = device.deref();

            let image_memory_requirements_info =
                ImageMemoryRequirementsInfo2::default().image(image);

            let mut image_memory_requirements = MemoryRequirements2::default();

            trace_calling(
                Self::format_get_image_memory_requirements2,
                (
                    device_,
                    &image_memory_requirements_info,
                    &image_memory_requirements,
                ),
            );

            unsafe {
                device_.get_image_memory_requirements2(
                    &image_memory_requirements_info,
                    &mut image_memory_requirements,
                );
            };

            trace_called(
                Self::format_get_image_memory_requirements2,
                (
                    device_,
                    &image_memory_requirements_info,
                    &image_memory_requirements,
                ),
                None::<&VkResult<()>>,
            );

            let memory_requirements = image_memory_requirements.memory_requirements;

            let usize_memory_requirements =
                [memory_requirements.size, memory_requirements.alignment]
                    .into_iter()
                    .map(|u64_memory_requirement| {
                        u64_memory_requirement.try_into().map_or_else(
                            |_| {
                                Err(argument_out_of_domain(
                                    Range {
                                        start: usize::MIN.into(),
                                        end: usize::MAX.into(),
                                    },
                                    u64_memory_requirement.into(),
                                ))
                            },
                            Ok,
                        )
                    })
                    .collect::<Result<SmallVec<[usize; 2]>, Error>>()?;

            Ok(HalAllocationMetadata {
                requirements: HalAllocationRequirements {
                    size: usize_memory_requirements[0],
                    alignment: usize_memory_requirements[1],
                    flags: memory_requirements.memory_type_bits as usize,
                },
                allocation_type: allocation::Type::GpuOnly,
                name: name.into(),
            })
        };

        let memory_metadata = new_memory_metadata();

        let allocation = allocator.allocate(memory_metadata?).await?;

        let memory = match try_from_bytes::<Memory>(allocation.handle()) {
            Ok(memory_) => Ok(*memory_),
            Err(error) => Err(failed_to_cast(
                &allocation.handle(),
                PhantomData::<Memory>,
                error,
            )),
        }?;

        let offset = allocation.offset() as u64;

        match unsafe { device.bind_image_memory(image, *memory, offset) } {
            Ok(()) => Ok(allocation),
            Err(result) => Err(match result {
                AshResult::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                AshResult::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    pub(crate) fn ash(&self) -> AshImage {
        self.inner.image
    }

    #[inline]
    fn create_image(device: &DeviceGuard, create_info: ImageCreateInfo) -> Result<AshImage, Error> {
        let trace_args = (device, &create_info);

        let create_image = || {
            let _span = debug_span!("create_image");

            unsafe { device.create_image(&create_info, None) }
        };

        trace_calling(Self::format_create_image, trace_args);

        let image_result = create_image();

        trace_called(Self::format_create_image, trace_args, Some(&image_result));

        match image_result {
            Ok(image) => Ok(image),
            Err(error) => Err(match error {
                AshResult::ERROR_COMPRESSION_EXHAUSTED_EXT => compression_exhausted(),
                AshResult::ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS => invalid_opaque_capture_address(),
                AshResult::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                AshResult::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                error => unexpected_error(error),
            }),
        }
    }

    #[inline]
    #[must_use]
    pub(crate) const fn create_info(&self) -> &CreateInfo {
        &self.create_info
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn format(&self) -> Format {
        self.create_info.format
    }

    #[cold]
    #[inline]
    fn format_create_image(args: (&DeviceGuard, &ImageCreateInfo)) -> String {
        let (device, create_info) = args;

        format!(
            "create_image(&self: {device:?}, create_info: {create_info:?}, allocation_callbacks: None)"
        )
    }

    #[cold]
    #[inline]
    fn format_get_image_memory_requirements2(
        args: (
            &crate::device::Guard,
            &ImageMemoryRequirementsInfo2,
            &MemoryRequirements2,
        ),
    ) -> String {
        let (device, info, out) = args;

        format!("get_image_memory_requirements2(&self: {device:?}, info: {info:?}, out: {out:?})")
    }

    #[inline]
    pub(crate) fn guard(&self) -> RArc<Guard> {
        self.inner.clone().into()
    }

    #[inline]
    pub(crate) async fn layout(&self) -> ImageLayout {
        *self.inner.layout.read().await.deref()
    }

    #[inline]
    pub(crate) fn metadata(&self) -> Metadata {
        Metadata {
            device: self.device.clone(),
            allocator: self.allocator.clone(),
            depth: self.create_info.extent.depth as usize,
            height: self.create_info.extent.height as usize,
            width: self.create_info.extent.width as usize,
            usage: self.create_info.usage,
            name: self.name.clone(),
        }
    }

    #[inline]
    #[must_use]
    fn new_create_info(
        image_type: ImageType,
        extent: Extent3D,
        usage: ImageUsageFlags,
    ) -> CreateInfo {
        CreateInfo {
            flags: ImageCreateFlags::default(),
            image_type,
            format: Format::R8G8B8A8_SRGB,
            extent,
            mip_levels: 1,
            array_layers: 1,
            samples: SampleCountFlags::TYPE_1,
            tiling: ImageTiling::OPTIMAL,
            usage: ImageUsageFlags::TRANSFER_SRC | ImageUsageFlags::TRANSFER_DST | usage,
            sharing_mode: SharingMode::EXCLUSIVE,
            queue_family_indices: SmallVec::new(),
        }
    }

    #[inline]
    pub(crate) async fn set_layout(&self, layout: ImageLayout) {
        *self.inner.layout.write().await = layout;
    }
}

impl Deref for Guard {
    type Target = AshImage;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.image
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        take(&mut self.allocation).map_or_else(|| unreachable!(), drop);

        let device = &self.device;

        let image = self.image;

        let trace_args = (device.deref(), image);

        trace_calling(Self::format_destroy_image, trace_args);

        unsafe { device.destroy_image(image, None) };

        trace_called(
            Self::format_destroy_image,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

impl Eq for Guard {}

impl HalTextureGuardTrait for Guard {}

impl PartialEq for Guard {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.allocation == other.allocation
            && self.image == other.image
            && self.layout.upgradable_read_blocking().deref()
                == other.layout.upgradable_read_blocking().deref()
            && self.device == other.device
    }
}

impl Deref for Image {
    type Target = AshImage;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.inner.image
    }
}

unsafe impl Send for Image {}
unsafe impl Sync for Image {}

impl Trait for Image {
    fn guard(&self) -> HalTextureGuard {
        HalTextureGuard::new(self.guard())
    }

    fn metadata(&self) -> HalTextureMetadata {
        let metadata = self.metadata();

        let device = metadata.device.clone();

        let depth = metadata.depth;
        let height = metadata.height;
        let width = metadata.width;

        let usage = HalTextureUsage::iter()
            .filter(|usage| {
                let ash_usage = match usage {
                    HalTextureUsage::ColorAttachment => ImageUsageFlags::COLOR_ATTACHMENT,
                    HalTextureUsage::DepthStencilAttachment => {
                        ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
                    }
                    HalTextureUsage::Destination => ImageUsageFlags::TRANSFER_DST,
                    HalTextureUsage::InputAttachment => ImageUsageFlags::INPUT_ATTACHMENT,
                    HalTextureUsage::Sampled => ImageUsageFlags::SAMPLED,
                    HalTextureUsage::Source => ImageUsageFlags::TRANSFER_SRC,
                    HalTextureUsage::Storage => ImageUsageFlags::STORAGE,
                    HalTextureUsage::TransientAttachment => ImageUsageFlags::TRANSIENT_ATTACHMENT,
                };

                (self.create_info.usage & ash_usage) == ash_usage
            })
            .collect();

        let name = self.name.clone();

        HalTextureMetadata::new(device, width, height, depth, usage, name)
    }

    #[inline]
    fn view(&self, components: Mapping, subresource: SubresourceRange) -> RResult<HalView, Error> {
        ROk(HalView::new(HalViewTrait::from_ptr(
            RArc::<View>::new(rtry!(View::new(ViewMetadata::new(
                self.clone(),
                components,
                subresource,
            )))),
            TD_CanDowncast,
        )))
    }
}

impl TryFrom<HalTextureMetadata> for Metadata {
    type Error = Error;

    #[inline]
    fn try_from(metadata: HalTextureMetadata) -> Result<Self, Self::Error> {
        let device = metadata.device();

        let allocator = match device.state().obj.downcast_as::<Device>() {
            Ok(device_) => Ok(device_),
            Err(error) => Err(failed_to_cast(
                &device.state(),
                PhantomData::<crate::Device>,
                error,
            )),
        }?
        .allocator();

        let usage = metadata
            .usage()
            .iter()
            .map(|usage| match usage {
                HalTextureUsage::ColorAttachment => ImageUsageFlags::COLOR_ATTACHMENT,
                HalTextureUsage::DepthStencilAttachment => {
                    ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT
                }
                HalTextureUsage::Destination => ImageUsageFlags::TRANSFER_DST,
                HalTextureUsage::InputAttachment => ImageUsageFlags::INPUT_ATTACHMENT,
                HalTextureUsage::Sampled => ImageUsageFlags::SAMPLED,
                HalTextureUsage::Source => ImageUsageFlags::TRANSFER_SRC,
                HalTextureUsage::Storage => ImageUsageFlags::STORAGE,
                HalTextureUsage::TransientAttachment => ImageUsageFlags::TRANSIENT_ATTACHMENT,
            })
            .fold(ImageUsageFlags::empty(), |sum, next| sum | next);

        Ok(Self {
            device: metadata.device(),
            allocator: Some(allocator),
            depth: metadata.depth(),
            height: metadata.height(),
            width: metadata.width(),
            usage,
            name: metadata.name().to_string(),
        })
    }
}

unsafe impl Send for Metadata {}
unsafe impl Sync for Metadata {}

#[cfg(test)]
mod tests {
    use {
        crate::{
            device::Device,
            image::{Image, Metadata},
            instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
        },
        abi_stable::{sabi_trait::TD_CanDowncast, std_types::RArc},
        ash::vk::ImageUsageFlags,
        pretty_assertions::{assert_eq, assert_ne},
        rand::{rng, Rng},
    };

    pub(crate) fn new_metadata(
        device: Device,
        depth: usize,
        height: usize,
        width: usize,
        usage: ImageUsageFlags,
        name: &str,
    ) -> Metadata {
        let allocator = Some(device.allocator());

        let device = fenrir_hal::device::Device::new(
            fenrir_hal::device::stable_abi::Trait_TO::from_ptr(RArc::new(device), TD_CanDowncast),
        );

        let name = name.to_string();

        Metadata {
            device,
            allocator,
            depth,
            height,
            width,
            usage,
            name,
        }
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            let mut rng = rng();

            for device in devices {
                let depth = rng.random_range(1..4);
                let height = 2usize.pow(rng.random_range(0..4));
                let width = 2usize.pow(rng.random_range(0..4));

                let metadata = new_metadata(
                    device,
                    depth,
                    height,
                    width,
                    ImageUsageFlags::empty(),
                    "image::tests::new",
                );

                let image = Image::new(metadata);

                assert!(image.await.is_ok());
            }
        }
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn queries() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            let mut rng = rng();

            for device in devices {
                let width = 2usize.pow(rng.random_range(0..4));
                let height = 2usize.pow(rng.random_range(0..4));
                let depth = rng.random_range(1..4);

                let metadata = new_metadata(
                    device,
                    depth,
                    height,
                    width,
                    ImageUsageFlags::empty(),
                    "image::tests::queries",
                );

                let image_result = Image::new(metadata).await;

                assert!(image_result.is_ok());

                let image = image_result.unwrap();

                let metadata = image.metadata();

                assert_eq!(depth, metadata.depth);

                assert_eq!(height, metadata.height);

                assert_eq!(width, metadata.width);
            }
        }
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn eq() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            let mut rng = rng();

            for device in devices {
                let depth = rng.random_range(1..4);
                let height = 2usize.pow(rng.random_range(0..4));
                let width = 2usize.pow(rng.random_range(0..4));

                let metadata = new_metadata(
                    device.clone(),
                    depth,
                    height,
                    width,
                    ImageUsageFlags::empty(),
                    "image::tests::eq",
                );

                let image_result = Image::new(metadata.clone()).await;

                assert!(image_result.is_ok());

                let image = image_result.unwrap();

                assert_eq!(image, image);

                let generate_different_metadata = || loop {
                    let other_metadata = new_metadata(
                        device.clone(),
                        depth,
                        height,
                        width,
                        ImageUsageFlags::empty(),
                        "image::tests::eq - other image",
                    );

                    if metadata != other_metadata {
                        return other_metadata;
                    }
                };

                let other_image_result = Image::new(generate_different_metadata()).await;

                assert!(other_image_result.is_ok());

                let other_image = other_image_result.unwrap();

                assert_ne!(image, other_image);
            }
        }
    }
}
