use {
    crate::{failed_to_cast, image::View},
    abi_stable::{sabi_trait::TD_CanDowncast, std_types::RArc},
    ash::vk::{
        AttachmentLoadOp, AttachmentStoreOp, ClearColorValue as AshClearColorValue,
        ClearDepthStencilValue, ClearValue as AshClearValue, RenderingAttachmentInfo,
    },
    core::{fmt::Debug, marker::PhantomData, ops::Range},
    fenrir_error::{argument_out_of_domain, Error, IntegerWrapper},
    fenrir_hal::{
        clear::Value as HalClearValue,
        color::{
            attachment::{LoadOperation, StoreOperation},
            blend::State as ColorBlendState,
            Attachment as HalColorAttachment, RGBA,
        },
        texture::view::{stable_abi::Trait_TO as HalViewTrait, View as HalView},
    },
    float_eq::float_eq,
};

#[derive(Clone, Eq, PartialEq)]
pub(crate) struct Attachment {
    view: View,
    resolve_target: Option<View>,
    load_operation: AttachmentLoadOp,
    store_operation: AttachmentStoreOp,
    clear_value: ClearValue,
    color_blend_state: ColorBlendState,
}

#[derive(Clone, Copy, Debug)]
pub(crate) enum ClearValue {
    Color(ClearColorValue),
    DepthStencil(ClearDepthStencilValue),
}

#[derive(Clone, Copy, Debug)]
pub(crate) enum ClearColorValue {
    Float32([f32; 4]),
    Int32([i32; 4]),
    Uint32([u32; 4]),
}

impl Attachment {
    #[inline]
    pub(crate) const fn new(
        view: View,
        resolve_target: Option<View>,
        load_operation: AttachmentLoadOp,
        store_operation: AttachmentStoreOp,
        clear_value: ClearValue,
        color_blend_state: ColorBlendState,
    ) -> Self {
        Self {
            view,
            resolve_target,
            load_operation,
            store_operation,
            clear_value,
            color_blend_state,
        }
    }

    #[inline]
    fn cast_integers<T, F, U>(value: T, generate_range: F) -> Result<U, Error>
    where
        T: Copy + Into<IntegerWrapper>,
        F: FnOnce() -> Range<IntegerWrapper>,
        U: TryFrom<T>,
    {
        value.try_into().map_or_else(
            |_| Err(argument_out_of_domain(generate_range(), value.into())),
            Ok,
        )
    }

    #[inline]
    const fn ash_to_hal_clear_value(clear_value: ClearValue) -> HalClearValue {
        match clear_value {
            ClearValue::Color(clear_color_value) => match clear_color_value {
                ClearColorValue::Float32([r, g, b, a]) => {
                    HalClearValue::FColor(RGBA::new(r, g, b, a))
                }
                ClearColorValue::Int32([r, g, b, a]) => {
                    HalClearValue::SColor(RGBA::new(r as isize, g as isize, b as isize, a as isize))
                }
                ClearColorValue::Uint32([r, g, b, a]) => {
                    HalClearValue::UColor(RGBA::new(r as usize, g as usize, b as usize, a as usize))
                }
            },
            ClearValue::DepthStencil(clear_depth_stencil_value) => HalClearValue::DepthStencil {
                depth: clear_depth_stencil_value.depth,
                stencil: clear_depth_stencil_value.stencil as usize,
            },
        }
    }

    #[inline]
    fn ash_to_hal_load_operation(load_operation: AttachmentLoadOp) -> LoadOperation {
        match load_operation {
            AttachmentLoadOp::LOAD => LoadOperation::Load,
            AttachmentLoadOp::CLEAR => LoadOperation::Clear,
            AttachmentLoadOp::DONT_CARE => LoadOperation::DontCare,
            AttachmentLoadOp::NONE_KHR => LoadOperation::None,
            _ => unreachable!("unknown ash::vk::AttachmentLoadOp: {load_operation:?}"),
        }
    }

    #[inline]
    fn ash_to_hal_store_operation(store_operation: AttachmentStoreOp) -> StoreOperation {
        match store_operation {
            AttachmentStoreOp::STORE => StoreOperation::Store,
            AttachmentStoreOp::DONT_CARE => StoreOperation::DontCare,
            AttachmentStoreOp::NONE => StoreOperation::None,
            _ => unreachable!("unknown ash::vk::AttachmentStoreOp: {store_operation:?}"),
        }
    }

    #[inline]
    pub(crate) const fn color_blend_state(&self) -> &ColorBlendState {
        &self.color_blend_state
    }

    #[inline]
    fn hal_to_ash_clear_value(clear_value: HalClearValue) -> Result<ClearValue, Error> {
        let isize_to_int32 = |value: isize| {
            Self::cast_integers(value, || Range {
                start: 0.into(),
                end: ((i64::from(i32::MAX)) + 1).into(),
            })
        };

        let usize_to_uint32 = |value: usize| {
            Self::cast_integers(value, || Range {
                start: 0.into(),
                end: ((u64::from(u32::MAX)) + 1).into(),
            })
        };

        Ok(match clear_value {
            fenrir_hal::clear::Value::FColor(float) => {
                ClearValue::Color(ClearColorValue::Float32([
                    float.r(),
                    float.g(),
                    float.b(),
                    float.a(),
                ]))
            }
            fenrir_hal::clear::Value::SColor(signed) => {
                ClearValue::Color(ClearColorValue::Int32([
                    isize_to_int32(signed.r())?,
                    isize_to_int32(signed.g())?,
                    isize_to_int32(signed.b())?,
                    isize_to_int32(signed.a())?,
                ]))
            }
            fenrir_hal::clear::Value::UColor(unsigned) => {
                ClearValue::Color(ClearColorValue::Uint32([
                    usize_to_uint32(unsigned.r())?,
                    usize_to_uint32(unsigned.g())?,
                    usize_to_uint32(unsigned.b())?,
                    usize_to_uint32(unsigned.a())?,
                ]))
            }
            fenrir_hal::clear::Value::DepthStencil { depth, stencil } => {
                ClearValue::DepthStencil(ClearDepthStencilValue {
                    depth,
                    stencil: usize_to_uint32(stencil)?,
                })
            }
        })
    }

    #[inline]
    const fn hal_to_ash_load_op(load_operation: LoadOperation) -> AttachmentLoadOp {
        match load_operation {
            LoadOperation::Load => AttachmentLoadOp::LOAD,
            LoadOperation::Clear => AttachmentLoadOp::CLEAR,
            LoadOperation::DontCare => AttachmentLoadOp::DONT_CARE,
            LoadOperation::None => AttachmentLoadOp::NONE_KHR,
        }
    }

    #[inline]
    const fn hal_to_ash_store_op(store_operation: StoreOperation) -> AttachmentStoreOp {
        match store_operation {
            StoreOperation::Store => AttachmentStoreOp::STORE,
            StoreOperation::DontCare => AttachmentStoreOp::DONT_CARE,
            StoreOperation::None => AttachmentStoreOp::NONE,
        }
    }

    #[inline]
    pub(crate) async fn rendering_info(&self) -> RenderingAttachmentInfo {
        let clear_value = match self.clear_value {
            ClearValue::Color(clear_color_value) => match clear_color_value {
                ClearColorValue::Float32(float32) => AshClearValue {
                    color: AshClearColorValue { float32 },
                },
                ClearColorValue::Int32(int32) => AshClearValue {
                    color: AshClearColorValue { int32 },
                },
                ClearColorValue::Uint32(uint32) => AshClearValue {
                    color: AshClearColorValue { uint32 },
                },
            },
            ClearValue::DepthStencil(depth_stencil) => AshClearValue { depth_stencil },
        };

        let mut rendering_info = RenderingAttachmentInfo::default()
            .image_view(self.view.ash())
            .image_layout(self.view.image().layout().await)
            .load_op(self.load_operation)
            .store_op(self.store_operation)
            .clear_value(clear_value);

        if let Some(resolve_target) = self.resolve_target.as_ref() {
            rendering_info = rendering_info
                .resolve_image_view(resolve_target.ash())
                .resolve_image_layout(resolve_target.image().layout().await);
        }

        rendering_info
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn resolve_target(&self) -> Option<&View> {
        self.resolve_target.as_ref()
    }

    #[inline]
    pub(crate) const fn view(&self) -> &View {
        &self.view
    }
}

impl Debug for Attachment {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Attachment")
            .field("view", &self.view)
            .field("resolve_target", &self.resolve_target)
            .field("load_operation", &self.load_operation)
            .field("store_operation", &self.store_operation)
            .field("clear_value", &self.clear_value)
            .field("color_blend_state", &self.color_blend_state)
            .finish()
    }
}

unsafe impl Send for Attachment {}
unsafe impl Sync for Attachment {}

impl TryFrom<HalColorAttachment> for Attachment {
    type Error = Error;

    #[inline]
    fn try_from(value: HalColorAttachment) -> Result<Self, Self::Error> {
        let downcast_view = |view: &HalView| {
            let inner = view.inner();

            match inner.obj.downcast_as::<View>() {
                Err(error) => Err(failed_to_cast(&inner, PhantomData::<View>, error)),
                Ok(view_) => Ok(view_.clone()),
            }
        };

        let view = downcast_view(value.view())?;

        let resolve_target = value.resolve_target().map_or_else(
            || Ok(None),
            |hal_resolve_target| match downcast_view(hal_resolve_target) {
                Ok(resolve_target_) => Ok(Some(resolve_target_)),
                Err(error) => Err(error),
            },
        )?;

        let load_operation = Self::hal_to_ash_load_op(value.load_operation());
        let store_operation = Self::hal_to_ash_store_op(value.store_operation());

        let clear_value = Self::hal_to_ash_clear_value(value.clear_value())?;

        let color_blend_state = value.color_blend_state().clone();

        Ok(Self::new(
            view,
            resolve_target,
            load_operation,
            store_operation,
            clear_value,
            color_blend_state,
        ))
    }
}

#[allow(clippy::from_over_into)]
impl Into<HalColorAttachment> for Attachment {
    #[inline]
    fn into(self) -> HalColorAttachment {
        let new_hal_view =
            |view| HalView::new(HalViewTrait::from_ptr(RArc::new(view), TD_CanDowncast));

        let view = new_hal_view(self.view);

        let resolve_target = self.resolve_target.map(new_hal_view);

        HalColorAttachment::new(
            view,
            resolve_target,
            Self::ash_to_hal_load_operation(self.load_operation),
            Self::ash_to_hal_store_operation(self.store_operation),
            Self::ash_to_hal_clear_value(self.clear_value),
            self.color_blend_state,
        )
    }
}

impl Eq for ClearValue {}

impl PartialEq for ClearValue {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Color(left), Self::Color(right)) => left == right,
            (Self::DepthStencil(left), Self::DepthStencil(right)) => {
                float_eq!(left.depth, right.depth, r2nd <= f32::EPSILON)
                    && (left.stencil == right.stencil)
            }
            _ => false,
        }
    }
}

impl Eq for ClearColorValue {}

impl PartialEq for ClearColorValue {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Float32(left), Self::Float32(right)) => left
                .iter()
                .zip(right.iter())
                .all(|(left, right)| float_eq!(left, right, r2nd <= f32::EPSILON)),
            (Self::Int32(left), Self::Int32(right)) => left == right,
            (Self::Uint32(left), Self::Uint32(right)) => left == right,
            _ => false,
        }
    }
}
