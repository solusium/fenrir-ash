mod call;
mod calls;

pub(crate) type Call = crate::draw::call::Call;
pub(crate) type Calls = crate::draw::calls::Calls;
