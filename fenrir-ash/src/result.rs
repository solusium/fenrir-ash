use {
    ash::vk::Result,
    fenrir_error::{state_not_recoverable, Error},
};

#[cold]
#[inline]
pub(crate) const fn compression_exhausted() -> Error {
    Error::CompressionExhausted
}

#[cold]
#[inline]
pub(crate) const fn device_lost() -> Error {
    Error::DeviceLost
}

#[cold]
#[inline]
pub(crate) fn extension_not_present(extension: &str) -> Error {
    Error::ExtensionNotPresent(extension.to_string().into())
}

#[cold]
#[inline]
pub(crate) const fn incompatible_driver() -> Error {
    Error::IncompatibleDriver
}

#[cold]
#[inline]
pub(crate) fn initialization_failed(reason: &str) -> Error {
    Error::InitializationFailed(reason.to_string().into())
}

#[cold]
#[inline]
pub(crate) const fn invalid_opaque_capture_address() -> Error {
    Error::InvalidOpaqueCaptureAddress
}

#[cold]
#[inline]
pub(crate) const fn out_of_host_memory() -> Error {
    Error::OutOfHostMemory
}

#[cold]
#[inline]
pub(crate) const fn out_of_device_memory() -> Error {
    Error::OutOfDeviceMemory
}

#[cold]
#[inline]
pub(crate) const fn too_many_objects() -> Error {
    Error::TooManyObjects
}

#[cold]
#[inline]
pub(crate) fn unexpected_error(error: Result) -> Error {
    state_not_recoverable(&format!("unexpected error: {error}"))
}
