use {
    crate::{
        device::{Device, Guard as DeviceGuard},
        draw::Calls,
        failed_to_cast,
        result::{out_of_device_memory, out_of_host_memory, unexpected_error},
        shader::module::Module,
        trace_called, trace_calling, Attachment, RenderPass,
    },
    abi_stable::{
        sabi_trait::TD_CanDowncast,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
            RVec,
        },
    },
    alloc::sync::Arc,
    ash::{
        prelude::VkResult,
        vk::{
            FrontFace, GraphicsPipelineCreateInfo, PipelineCache, PipelineLayout,
            PipelineLayoutCreateInfo, PipelineRenderingCreateInfo, PipelineShaderStageCreateInfo,
            PipelineVertexInputStateCreateInfo,
        },
    },
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::RwLock,
    core::{
        ffi::CStr,
        marker::PhantomData,
        num::NonZero,
        ops::Deref,
        slice::{from_raw_parts, from_ref},
    },
    fenrir_error::{state_not_recoverable, Error as FenrirError},
    fenrir_hal::{
        front::Face,
        graphics::pipeline::{stable_abi::Trait, Metadata},
        RenderPass as HalRenderPass,
    },
    fenrir_shader_compiler::Error,
    futures_util::future::join_all,
    smallvec::SmallVec,
    std::ops::DerefMut,
    tracing::debug_span,
};

extern crate alloc;

pub(crate) mod color_blend;

mod depth_stencil;
mod dynamic_states;
mod input_assembly;
mod multisample;
mod rasterization;
mod renderer;
mod tessellation;
mod vertex_input;
mod viewport;

pub(crate) type ColorBlend = crate::graphics::pipeline::color_blend::ColorBlend;
pub(crate) type DepthStencil = crate::graphics::pipeline::depth_stencil::DepthStencil;
pub(crate) type DynamicStates = crate::graphics::pipeline::dynamic_states::DynamicStates;
pub(crate) type InputAssembly = crate::graphics::pipeline::input_assembly::InputAssembly;
pub(crate) type Multisample = crate::graphics::pipeline::multisample::Multisample;
pub(crate) type Rasterization = crate::graphics::pipeline::rasterization::Rasterization;
pub(crate) type Renderer<'recorder, 'buffer> =
    crate::graphics::pipeline::renderer::Renderer<'recorder, 'buffer>;
pub(crate) type Tessellation = crate::graphics::pipeline::tessellation::Tessellation;
pub(crate) type VertexInput = crate::graphics::pipeline::vertex_input::VertexInput;
pub(crate) type Viewport = crate::graphics::pipeline::viewport::Viewport;

#[derive(Debug)]
pub(crate) struct Guard {
    pipeline: ash::vk::Pipeline,
    layout: PipelineLayout,
    draw_calls: Calls,
    render_pass: RwLock<RenderPass>,
    device: Device,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct Pipeline {
    inner: Arc<Guard>,
    shaders: SmallVec<[Module; 4]>,
}

impl Guard {
    #[inline]
    pub(crate) const fn device(&self) -> &Device {
        &self.device
    }

    #[inline]
    pub(crate) const fn draw_calls(&self) -> &Calls {
        &self.draw_calls
    }
}

impl Pipeline {
    pub(crate) async fn new(
        metadata: Metadata<'_, '_>,
        render_pass: RenderPass,
    ) -> Result<Self, Error> {
        let hal_device = metadata.device();

        let hal_device_inner = hal_device.state();

        let device = match hal_device_inner.obj.downcast_as::<Device>() {
            Err(error) => Err(Error::FenrirError(failed_to_cast(
                &hal_device.state(),
                PhantomData::<Device>,
                error,
            ))),
            Ok(device_) => Ok(device_.clone()),
        }?;

        let render_pass = RwLock::new(render_pass);

        let shaders = join_all(
            metadata
                .shaders()
                .iter()
                .map(|shader| async { Module::new(device.clone(), shader).await }),
        )
        .await
        .into_iter()
        .collect::<Result<SmallVec<[Module; 4]>, _>>()?;

        let device_guard = device.guard();

        let layout = Self::construct_pipeline_layout(&device_guard)?;

        let mut rendering = PipelineRenderingCreateInfo::default();

        let view_mask = rendering.view_mask;

        let (
            color_blend,
            depth_stencil,
            dynamic_states,
            input_assembly,
            multisample,
            rasterization,
            tessellation,
            vertex_input,
            viewport,
        ) = Self::new_states(
            &device,
            &metadata,
            render_pass.read().await.color_attachments().await.as_ref(),
        )
        .map_err(Error::FenrirError)?;

        let pipeline = Self::construct_pipelines((
            &device_guard,
            &mut rendering,
            &shaders,
            &color_blend,
            &depth_stencil,
            &input_assembly,
            &multisample,
            &rasterization,
            &tessellation,
            &vertex_input,
            &viewport,
            &dynamic_states,
            layout,
        ))?[0];

        let draw_calls = Calls::new(
            color_blend,
            depth_stencil,
            dynamic_states,
            input_assembly,
            multisample,
            rasterization,
            tessellation,
            vertex_input,
            viewport,
            view_mask,
        );

        let inner = Arc::new(Guard {
            pipeline,
            layout,
            draw_calls,
            render_pass,
            device,
        });

        Ok(Self { inner, shaders })
    }

    #[inline]
    fn construct_pipelines(
        (
            device,
            rendering,
            shader_modules,
            color_blend,
            depth_stencil,
            input_assembly,
            multisample,
            rasterization,
            tessellation,
            vertex_input,
            viewport,
            dynamic_states,
            layout,
        ): (
            &DeviceGuard,
            &mut PipelineRenderingCreateInfo,
            &[Module],
            &ColorBlend,
            &DepthStencil,
            &InputAssembly,
            &Multisample,
            &Rasterization,
            &Tessellation,
            &VertexInput,
            &Viewport,
            &DynamicStates,
            PipelineLayout,
        ),
    ) -> Result<Vec<ash::vk::Pipeline>, Error> {
        let entry_points = shader_modules
            .iter()
            .map(|shader_module| {
                let mut entry_point = shader_module.entry_point().as_bytes().to_vec();

                entry_point.push(0);

                entry_point
            })
            .collect::<SmallVec<[Vec<u8>; 4]>>();

        let shader_stages = shader_modules
            .iter()
            .zip(entry_points.iter())
            .map(|(shader_module, entry_point)| {
                Ok(PipelineShaderStageCreateInfo::default()
                    .stage(shader_module.stage())
                    .module(**shader_module)
                    .name(match CStr::from_bytes_until_nul(entry_point) {
                        Ok(name) => Ok(name),
                        Err(error) => Err(Error::FenrirError(state_not_recoverable(&format!(
                            "couldn't read entry point name of {shader_module:?}: {error}"
                        )))),
                    }?))
            })
            .collect::<Result<SmallVec<[_; 4]>, Error>>()?;

        let dynamic_states_create_info = dynamic_states.create_info();

        let mut create_info = GraphicsPipelineCreateInfo::default()
            .push_next(rendering)
            .stages(&shader_stages)
            .dynamic_state(&dynamic_states_create_info)
            .layout(layout);

        let maybe_color_blend_create_info = color_blend.create_info();

        if let Some(color_blend_create_info) = maybe_color_blend_create_info.as_ref() {
            create_info = create_info.color_blend_state(color_blend_create_info);
        }

        let maybe_depth_stencil_create_info = depth_stencil.create_info();

        if let Some(depth_stencil_create_info) = maybe_depth_stencil_create_info.as_ref() {
            create_info = create_info.depth_stencil_state(depth_stencil_create_info);
        }

        let maybe_input_assembly_create_info = input_assembly.create_info();

        if let Some(input_assembly_create_info) = maybe_input_assembly_create_info.as_ref() {
            create_info = create_info.input_assembly_state(input_assembly_create_info);
        }

        let maybe_multisample_create_info = multisample.create_info();

        if let Some(multisample_create_info) = maybe_multisample_create_info.as_ref() {
            create_info = create_info.multisample_state(multisample_create_info);
        }

        let maybe_rasterization_create_info = rasterization.create_info();

        if let Some(rasterization_create_info) = maybe_rasterization_create_info.as_ref() {
            create_info = create_info.rasterization_state(rasterization_create_info);
        }

        let maybe_tessellation_create_info = tessellation.create_info();

        if let Some(tessellation_create_info) = maybe_tessellation_create_info.as_ref() {
            create_info = create_info.tessellation_state(tessellation_create_info);
        }

        let maybe_vertex_input_create_info = vertex_input.create_info();

        let ash_vertex_input_create_info = maybe_vertex_input_create_info.as_ref().map_or_else(
            PipelineVertexInputStateCreateInfo::default,
            |vertex_input_create_info| vertex_input_create_info.ash(),
        );

        create_info = create_info.vertex_input_state(&ash_vertex_input_create_info);

        let maybe_viewport_create_info = viewport.create_info();

        if let Some(viewport_create_info) = maybe_viewport_create_info.as_ref() {
            create_info = create_info.viewport_state(viewport_create_info);
        }

        let create_infos = from_ref(&create_info);

        let create_graphics_pipelines = || {
            debug_span!("create_graphics_pipelines");

            unsafe {
                device.create_graphics_pipelines(PipelineCache::default(), create_infos, None)
            }
        };

        let pipeline_cache = PipelineCache::default();

        let trace_args = (device, &pipeline_cache, create_infos);

        trace_calling(Self::format_create_graphic_pipeline, trace_args);

        let pipelines_result = create_graphics_pipelines().map_err(|(_, error)| error);

        trace_called(
            Self::format_create_graphic_pipeline,
            trace_args,
            Some(&pipelines_result),
        );

        match pipelines_result {
            Ok(pipelines) => Ok(pipelines),
            Err(error) => Err(Error::FenrirError(match error {
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                ash::vk::Result::ERROR_INVALID_SHADER_NV => fenrir_error::Error::InvalidShader,
                _ => unexpected_error(error),
            })),
        }
    }

    #[inline]
    fn construct_pipeline_layout(device: &ash::Device) -> Result<PipelineLayout, Error> {
        let create_info = PipelineLayoutCreateInfo::default();

        let pipeline_layout_result = unsafe { device.create_pipeline_layout(&create_info, None) };

        match pipeline_layout_result {
            Ok(pipeline_layout) => Ok(pipeline_layout),
            Err(error) => Err(Error::FenrirError(match error {
                ash::vk::Result::ERROR_OUT_OF_HOST_MEMORY => out_of_host_memory(),
                ash::vk::Result::ERROR_OUT_OF_DEVICE_MEMORY => out_of_device_memory(),
                _ => unexpected_error(error),
            })),
        }
    }

    #[cold]
    #[inline]
    fn format_create_graphic_pipeline(
        args: (&DeviceGuard, &PipelineCache, &[GraphicsPipelineCreateInfo]),
    ) -> String {
        let (device, pipeline_cache, create_infos) = args;

        let create_infos_string = create_infos
            .iter()
            .map(|create_info| {
                let rendering_create_info =
                    unsafe { *create_info.p_next.cast::<PipelineRenderingCreateInfo>() };

                let shader_stage_create_infos = Self::format_shader_stage_create_infos(unsafe {
                    from_raw_parts(create_info.p_stages, create_info.stage_count as usize)
                });

                let vertex_input_state_create_info = unsafe { *create_info.p_vertex_input_state };

                let input_assembly_state_create_info_string =
                    if create_info.p_input_assembly_state.is_null() {
                        "0x0".to_string()
                    } else {
                        format!("{:?}", unsafe { *create_info.p_input_assembly_state })
                    };

                let tessellation_state_create_info_string =
                    if create_info.p_tessellation_state.is_null() {
                        "0x0".to_string()
                    } else {
                        format!("{:?}", unsafe { *create_info.p_tessellation_state })
                    };

                let rasterization_state_create_info_string =
                    if create_info.p_rasterization_state.is_null() {
                        "0x0".to_string()
                    } else {
                        format!("{:?}", unsafe { *create_info.p_rasterization_state })
                    };

                let multisample_state_create_info_string =
                    if create_info.p_multisample_state.is_null() {
                        "0x0".to_string()
                    } else {
                        format!("{:?}", unsafe { *create_info.p_multisample_state })
                    };

                let dynamic_state_create_info = unsafe { *create_info.p_dynamic_state };

                let dynamic_states = unsafe {
                    from_raw_parts(
                        dynamic_state_create_info.p_dynamic_states,
                        dynamic_state_create_info.dynamic_state_count as usize,
                    )
                };

                let dynamic_state_create_info_string = format!(
                    "PipelineDynamicStateCreateInfo {{ s_type: {:?}, \
                    p_next: {:?}, flags: {:?}, dynamic_state_count: {}, p_dynamic_states: {:?} }}",
                    dynamic_state_create_info.s_type,
                    dynamic_state_create_info.p_next,
                    dynamic_state_create_info.flags,
                    dynamic_state_create_info.dynamic_state_count,
                    dynamic_states
                );

                format!(
                    "GraphicsPipelineCreateInfo {{ s_type: {:?}, p_next: \
                    {rendering_create_info:?}, flags: {:?}, \
                    stage_count: {}, p_stages: {shader_stage_create_infos}, p_vertex_input_state: \
                    {vertex_input_state_create_info:?}, p_input_assembly_state: \
                    {input_assembly_state_create_info_string}, p_tessellation_state: \
                    {tessellation_state_create_info_string}, p_viewport_state: {:?}, \
                    p_rasterization_state: {rasterization_state_create_info_string}, \
                    p_multisample_state: {multisample_state_create_info_string}\
                    , p_depth_stencil_state: {:?}, p_color_blend_state: {:?}, p_dynamic_state: \
                    {dynamic_state_create_info_string}, layout: {:?}, render_pass: {:?}, subpass: \
                    {}, base_pipeline_handle: {:?} base_pipeline_index: {} }}",
                    create_info.s_type,
                    create_info.flags,
                    create_info.stage_count,
                    create_info.p_viewport_state,
                    create_info.p_depth_stencil_state,
                    create_info.p_color_blend_state,
                    create_info.layout,
                    create_info.render_pass,
                    create_info.subpass,
                    create_info.base_pipeline_handle,
                    create_info.base_pipeline_index
                )
            })
            .fold(String::new(), |mut create_infos, create_info| {
                if !create_infos.is_empty() {
                    create_infos.push_str(", ");
                }

                create_infos.push_str(&create_info);

                create_infos
            });

        format!(
            "create_graphics_pipeline(&self: {device:?}, pipeline_cache: {pipeline_cache:?}, \
            create_infos: {create_infos_string}, allocation_callbacks: None)"
        )
    }

    #[inline]
    fn format_shader_stage_create_infos(
        shader_stage_create_infos: &[PipelineShaderStageCreateInfo],
    ) -> String {
        shader_stage_create_infos
            .iter()
            .map(|shader_stage_create_info| {
                let c_name = unsafe { CStr::from_ptr(shader_stage_create_info.p_name) };

                let name = c_name.to_string_lossy();

                format!(
                    "PipelineShaderStageCreateInfo {{ s_type: {:?}, p_next: {:?}, flags: {:?}, \
                    stage: {:?}, module: {:?}, p_name: {name}, p_specialization_info: {:?} }}",
                    shader_stage_create_info.s_type,
                    shader_stage_create_info.p_next,
                    shader_stage_create_info.flags,
                    shader_stage_create_info.stage,
                    shader_stage_create_info.module,
                    shader_stage_create_info.p_specialization_info
                )
            })
            .fold(String::new(), |mut create_infos, create_info| {
                if !create_infos.is_empty() {
                    create_infos.push_str(", ");
                }

                create_infos.push_str(&create_info);

                create_infos
            })
    }

    #[inline]
    pub(crate) fn guard(&self) -> Arc<Guard> {
        self.inner.clone()
    }

    #[inline]
    async fn metadata(&self) -> Vec<Metadata> {
        self.inner
            .draw_calls
            .read()
            .await
            .iter()
            .map(|(id, _)| {
                let mut builder = Metadata::builder(fenrir_hal::device::Device::new(
                    fenrir_hal::device::stable_abi::Trait_TO::from_ptr(
                        RArc::new(self.inner.device.clone()),
                        TD_CanDowncast,
                    ),
                ))
                .set_color_blend(id.color_blend().clone().into())
                .set_cull_mode(crate::cull::mode::ash_to_hal(
                    id.rasterization().cull_mode(),
                ))
                .set_depth(id.depth_stencil().depth().into())
                .set_depth_bias(id.rasterization().depth_bias().into())
                .set_depth_clamp(id.rasterization().depth_clamp())
                .set_front_face(if FrontFace::CLOCKWISE == id.rasterization().front_face() {
                    Face::Clockwise
                } else {
                    Face::CounterClockwise
                })
                .set_line_width(id.rasterization().line_width())
                .set_multisample(id.multisample().clone().into())
                .set_polygon_mode(crate::polygon::mode::ash_to_hal(
                    id.rasterization().polygon_mode(),
                ))
                .set_primitive(id.input_assembly().clone().into())
                .set_rasterizer_discard(id.rasterization().rasterizer_discard())
                .set_scissors(
                    &id.viewport()
                        .scissors()
                        .iter()
                        .map(crate::scissor::ash_to_hal)
                        .collect::<SmallVec<[_; 2]>>(),
                )
                .set_stencil(id.depth_stencil().stencil().hal())
                .set_tessellation_patch_control_points(unsafe {
                    NonZero::new_unchecked(id.tessellation().patch_control_points() as usize)
                })
                .set_viewports(
                    &id.viewport()
                        .viewports()
                        .iter()
                        .map(crate::viewport::ash_to_hal)
                        .collect::<SmallVec<[_; 2]>>(),
                )
                .set_vertex_input(id.vertex_input().clone().into());

                for shader in &self.shaders {
                    builder = builder.add_shader(shader.hal());
                }

                builder.build()
            })
            .collect()
    }

    #[inline]
    fn new_states(
        device: &Device,
        metadata: &Metadata<'_, '_>,
        color_attachments: &[Attachment],
    ) -> Result<
        (
            ColorBlend,
            DepthStencil,
            DynamicStates,
            InputAssembly,
            Multisample,
            Rasterization,
            Tessellation,
            VertexInput,
            Viewport,
        ),
        fenrir_error::Error,
    > {
        let mut features = device.features();

        let available_dynamic_state_2_features = features.dynamic_state_2()?;

        let available_dynamic_state_3_features = features.dynamic_state_3()?;

        let available_vertex_input_dynamic_state_features =
            features.vertex_input_dynamic_state()?;

        let available_features = &mut features;

        let viewport = Viewport::new(device, metadata)?;

        let vertex_input = VertexInput::new(
            device,
            metadata,
            &available_vertex_input_dynamic_state_features,
        )?;

        let tessellation =
            Tessellation::new(device, metadata, &available_dynamic_state_2_features)?;

        let rasterization = Rasterization::new(metadata, &available_dynamic_state_3_features);

        let multisample = Multisample::new(
            metadata,
            available_features,
            &available_dynamic_state_3_features,
        )?;

        let input_assembly = InputAssembly::new(device, metadata)?;

        let color_blend = ColorBlend::new(
            device,
            metadata,
            color_attachments,
            &available_dynamic_state_2_features,
            &available_dynamic_state_3_features,
        )?;

        let dynamic_states = DynamicStates::new(
            &color_blend,
            &multisample,
            &rasterization,
            &tessellation,
            &vertex_input,
            &available_dynamic_state_3_features,
        );

        let depth_stencil = DepthStencil::new(device, metadata)?;

        Ok((
            color_blend,
            depth_stencil,
            dynamic_states,
            input_assembly,
            multisample,
            rasterization,
            tessellation,
            vertex_input,
            viewport,
        ))
    }
}

impl Guard {
    #[cold]
    #[inline]
    fn format_destroy_pipeline(
        args: (
            &crate::device::Guard,
            ash::vk::Pipeline,
            Option<&ash::vk::AllocationCallbacks<'_>>,
        ),
    ) -> String {
        let (device, pipeline, allocation_callbacks) = args;

        format!(
            "destroy_pipeline(&self: {device:?}, pipeline: {pipeline:?},
                allocation_callbacks: {allocation_callbacks:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_destroy_pipeline_layout(
        args: (
            &crate::device::Guard,
            ash::vk::PipelineLayout,
            Option<&ash::vk::AllocationCallbacks<'_>>,
        ),
    ) -> String {
        let (device, pipeline_layout, allocation_callbacks) = args;

        format!(
            "destroy_pipeline_layout(&self: {device:?}, pipeline_layout: {pipeline_layout:?},
                allocation_callbacks: {allocation_callbacks:?})"
        )
    }
}

impl Deref for Guard {
    type Target = ash::vk::Pipeline;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.pipeline
    }
}

impl Drop for Guard {
    #[inline]
    fn drop(&mut self) {
        let device = self.device.guard();

        let allocation_callbacks = None;

        {
            let trace_args = (&*device, self.pipeline, allocation_callbacks);

            trace_calling(Self::format_destroy_pipeline, trace_args);

            {
                debug_span!("destroy_pipeline");

                unsafe {
                    device.destroy_pipeline(self.pipeline, allocation_callbacks);
                }
            }

            trace_called(
                Self::format_destroy_pipeline,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        let trace_args = (&*device, self.layout, allocation_callbacks);

        trace_calling(Self::format_destroy_pipeline_layout, trace_args);

        {
            debug_span!("destroy_pipeline_layout");

            unsafe {
                device.destroy_pipeline_layout(self.layout, None);
            }
        }

        trace_called(
            Self::format_destroy_pipeline_layout,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

impl Eq for Guard {}

impl PartialEq for Guard {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.pipeline == other.pipeline,
            self.layout == other.layout,
            self.draw_calls == other.draw_calls,
            self.render_pass.read_blocking().deref() == other.render_pass.read_blocking().deref(),
            self.device == other.device,
        ];

        values.into_iter().all(|value| value)
    }
}

impl Trait for Pipeline {
    #[inline]
    fn metadata(&self) -> BorrowingFfiFuture<RVec<Metadata>> {
        async move { self.metadata().await.into() }.into_ffi()
    }

    #[inline]
    fn new_render_pass(
        &self,
        render_pass: HalRenderPass,
    ) -> BorrowingFfiFuture<RResult<(), FenrirError>> {
        async move {
            let render_pass_state = render_pass.state();

            let render_pass_state_obj = render_pass_state.obj;

            match render_pass_state_obj.downcast_as::<RenderPass>() {
                Err(error) => RErr(failed_to_cast(
                    &render_pass_state_obj,
                    PhantomData::<RenderPass>,
                    error,
                )),
                Ok(render_pass) => {
                    {
                        let mut this_render_pass = self.inner.render_pass.write().await;

                        *this_render_pass.deref_mut() = render_pass.clone();
                    }

                    ROk(())
                }
            }
        }
        .into_ffi()
    }

    #[inline]
    fn render_pass(&self) -> BorrowingFfiFuture<HalRenderPass> {
        async move { self.inner.render_pass.read().await.clone().into() }.into_ffi()
    }
}

unsafe impl Send for Pipeline {}
unsafe impl Sync for Pipeline {}

#[cfg(test)]
mod tests {
    use {
        crate::{
            graphics::Pipeline,
            instance::tests::{devices_and_dependencies, init_tracing, load_shader_compilers},
            queue::Family as QueueFamily,
            render_pass::Metadata as RenderPassMetadata,
            RenderPass,
        },
        abi_stable::{sabi_trait::TD_CanDowncast, std_types::RArc},
        ash::vk::{Extent2D, Offset2D, PipelineBindPoint, Rect2D},
        fenrir_hal::{
            device::stable_abi::Trait_TO, graphics::pipeline::Metadata, shader::Stage,
            Device as HalDevice,
        },
        fenrir_shader_compiler::Language,
        smallvec::SmallVec,
        tokio::test,
        tracing::error,
    };

    fn render_pass(queue_family: QueueFamily, device: HalDevice) -> RenderPass {
        let render_area = Rect2D {
            offset: Offset2D::default().x(0).y(0),
            extent: Extent2D::default().width(1).height(1),
        };

        let metadata = RenderPassMetadata::new(
            render_area,
            1,
            0,
            PipelineBindPoint::GRAPHICS,
            SmallVec::new(),
            queue_family,
            device,
        );

        let render_pass_result = RenderPass::new(metadata);

        assert!(render_pass_result.is_ok());

        render_pass_result.unwrap()
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn new() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                let hal_device = HalDevice::new(Trait_TO::from_ptr(
                    RArc::new(device.clone()),
                    TD_CanDowncast,
                ));

                for queue_family in device.queue_families() {
                    let shaders = ["#version 450\n\
                    \n\
                    vec2 positions[3] = vec2[](\n\
                        vec2(0.0, -0.5),\n\
                        vec2(0.5, 0.5),\n\
                        vec2(-0.5, 0.5)\n\
                    );\n\
                    \n\
                    void main() {\n\
                        gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
                    }"
                    .as_bytes()];

                    let pipeline_result = Box::pin(Pipeline::new(
                        Metadata::builder(hal_device.clone())
                            .add_shader((Stage::Vertex, shaders[0], Language::GLSL, "main").into())
                            .build(),
                        render_pass(queue_family.clone(), hal_device.clone()),
                    ))
                    .await;

                    if pipeline_result.is_err() {
                        error!("{}", pipeline_result.as_ref().unwrap_err());
                    }

                    assert!(pipeline_result.is_ok());
                }
            }
        }
    }

    #[test(flavor = "multi_thread", worker_threads = 1)]
    async fn eq() {
        init_tracing().await;

        for shader_compiler in load_shader_compilers() {
            let (devices, _, _) = devices_and_dependencies(shader_compiler);

            for device in devices {
                let hal_device =
                    HalDevice::new(fenrir_hal::device::stable_abi::Trait_TO::from_ptr(
                        RArc::new(device.clone()),
                        TD_CanDowncast,
                    ));

                for queue_family in device.queue_families() {
                    let shaders = [
                        "#version 450\n\
                    \n\
                    vec2 positions[3] = vec2[](\n\
                        vec2(0.0, -0.5),\n\
                        vec2(0.5, 0.5),\n\
                        vec2(-0.5, 0.5)\n\
                    );\n\
                    \n\
                    void main() {\n\
                        gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
                    }"
                        .as_bytes(),
                        "#version 450\n\
                    \n\
                    vec2 positions[3] = vec2[](\n\
                        vec2(-0.5, 0.0),\n\
                        vec2(0.5, 0.5),\n\
                        vec2(0.5, -0.5)\n\
                    );\n\
                    \n\
                    void main() {\n\
                        gl_Position = vec4(positions[gl_VertexIndex], 0.0, 1.0);\n\
                    }"
                        .as_bytes(),
                    ];

                    let render_pass = render_pass(queue_family.clone(), hal_device.clone());

                    let pipeline_results = vec![
                        Box::pin(Pipeline::new(
                            Metadata::builder(hal_device.clone())
                                .add_shader(
                                    (Stage::Vertex, shaders[0], Language::GLSL, "main").into(),
                                )
                                .build(),
                            render_pass.clone(),
                        ))
                        .await,
                        Box::pin(Pipeline::new(
                            Metadata::builder(hal_device.clone())
                                .add_shader(
                                    (Stage::Vertex, shaders[1], Language::GLSL, "main").into(),
                                )
                                .build(),
                            render_pass,
                        ))
                        .await,
                    ];

                    assert!(pipeline_results.iter().all(Result::is_ok));

                    let pipelines = pipeline_results
                        .into_iter()
                        .map(|result| result.unwrap())
                        .collect::<SmallVec<[_; 2]>>();

                    pipelines
                        .iter()
                        .enumerate()
                        .zip(pipelines.iter().enumerate())
                        .for_each(|((first_index, first), (second_index, second))| {
                            assert_eq!(first_index == second_index, first == second);
                        });
                }
            }
        }
    }
}
