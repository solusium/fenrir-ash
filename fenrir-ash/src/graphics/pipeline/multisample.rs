use {
    crate::{command::buffer::Recorder, device_does_not_support, trace_called, trace_calling},
    abi_stable::std_types::RArc,
    ash::{
        ext::extended_dynamic_state3::Device as DynamicState3Device,
        prelude::VkResult,
        vk::{
            CommandBuffer, PhysicalDeviceExtendedDynamicState3FeaturesEXT, PhysicalDeviceFeatures,
            PipelineMultisampleStateCreateFlags, PipelineMultisampleStateCreateInfo,
            SampleCountFlags, SampleMask, TRUE,
        },
    },
    fenrir_error::Error,
    fenrir_hal::{graphics::pipeline::Metadata, sample::Count, Multisample as HalMultisample},
    float_eq::float_eq,
    smallvec::{smallvec, SmallVec},
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
struct CreateInfo {
    flags: PipelineMultisampleStateCreateFlags,
}

#[derive(Clone, Debug)]
#[allow(clippy::struct_excessive_bools)]
pub(crate) struct Multisample {
    create_info: Option<CreateInfo>,
    dynamic_alpha_to_coverage: bool,
    alpha_to_coverage: bool,
    dynamic_alpha_to_one: bool,
    alpha_to_one: bool,
    min_sample_shading: f32,
    dynamic_rasterization_samples: bool,
    rasterization_samples: SampleCountFlags,
    dynamic_sample_masks: bool,
    sample_masks: SmallVec<[SampleMask; 2]>,
    sample_shading: bool,
}

impl Multisample {
    #[inline]
    pub(super) fn new(
        metadata: &Metadata,
        available_device_features: &PhysicalDeviceFeatures,
        available_dynamic_state3_features: &PhysicalDeviceExtendedDynamicState3FeaturesEXT,
    ) -> Result<Self, Error> {
        let hal_multisampling = metadata.multisample();

        let alpha_to_coverage = hal_multisampling.alpha_to_coverage();

        let alpha_to_one = hal_multisampling.alpha_to_one();

        let alpha_to_one_available = TRUE == available_device_features.alpha_to_one;

        if alpha_to_one && !alpha_to_one_available {
            return Err(device_does_not_support("Multisample::alpha_to_one"));
        }

        let min_sample_shading = hal_multisampling.min_sample_shading();

        let rasterization_samples = match hal_multisampling.rasterization_samples() {
            Count::One => SampleCountFlags::TYPE_1,
            Count::Two => SampleCountFlags::TYPE_2,
            Count::Four => SampleCountFlags::TYPE_4,
            Count::Eight => SampleCountFlags::TYPE_8,
            Count::Sixteen => SampleCountFlags::TYPE_16,
            Count::ThirtyTwo => SampleCountFlags::TYPE_32,
            Count::SixtyFour => SampleCountFlags::TYPE_64,
        };

        let sample_masks = smallvec![0u32; (rasterization_samples.as_raw() as usize + 31) / 32];

        let sample_shading = hal_multisampling.sample_shading();

        let sample_shading_available = TRUE == available_device_features.sample_rate_shading;

        if sample_shading && !sample_shading_available {
            return Err(device_does_not_support("Multisample::sample_shading"));
        }

        let dynamic_alpha_to_coverage = TRUE
            == available_dynamic_state3_features.extended_dynamic_state3_alpha_to_coverage_enable;

        let dynamic_alpha_to_one =
            TRUE == available_dynamic_state3_features.extended_dynamic_state3_alpha_to_one_enable;

        let dynamic_rasterization_samples =
            TRUE == available_dynamic_state3_features.extended_dynamic_state3_rasterization_samples;

        let dynamic_sample_masks =
            TRUE == available_dynamic_state3_features.extended_dynamic_state3_sample_mask;

        let create_info_can_be_none = dynamic_alpha_to_coverage
            && ((dynamic_alpha_to_one || !alpha_to_one) && !sample_shading)
            && dynamic_rasterization_samples
            && dynamic_sample_masks;

        let create_info = if create_info_can_be_none {
            None
        } else {
            let flags = PipelineMultisampleStateCreateFlags::default();

            Some(CreateInfo { flags })
        };

        Ok(Self {
            create_info,
            dynamic_alpha_to_coverage,
            alpha_to_coverage,
            dynamic_alpha_to_one,
            alpha_to_one,
            min_sample_shading,
            dynamic_rasterization_samples,
            rasterization_samples,
            dynamic_sample_masks,
            sample_masks,
            sample_shading,
        })
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineMultisampleStateCreateInfo<'_>> {
        self.create_info.as_ref().map(|create_info| {
            let mut ash_create_info = PipelineMultisampleStateCreateInfo::default()
                .flags(create_info.flags)
                .sample_shading_enable(self.sample_shading);

            if !self.dynamic_alpha_to_coverage {
                ash_create_info = ash_create_info.alpha_to_coverage_enable(self.alpha_to_coverage);
            }

            if !self.dynamic_alpha_to_one {
                ash_create_info = ash_create_info.alpha_to_one_enable(self.alpha_to_one);
            }

            if !self.dynamic_rasterization_samples {
                ash_create_info = ash_create_info.rasterization_samples(self.rasterization_samples);
            }

            if !self.dynamic_sample_masks {
                ash_create_info = ash_create_info.sample_mask(&self.sample_masks);
            }

            ash_create_info
        })
    }

    #[inline]
    pub(super) const fn dynamic_alpha_to_coverage(&self) -> Option<bool> {
        if self.dynamic_alpha_to_coverage {
            Some(self.alpha_to_coverage)
        } else {
            None
        }
    }

    #[inline]
    pub(super) const fn dynamic_alpha_to_one(&self) -> Option<bool> {
        if self.dynamic_alpha_to_one {
            Some(self.alpha_to_one)
        } else {
            None
        }
    }

    #[inline]
    pub(super) const fn dynamic_rasterization_samples(&self) -> Option<SampleCountFlags> {
        if self.dynamic_rasterization_samples {
            Some(self.rasterization_samples)
        } else {
            None
        }
    }

    #[inline]
    pub(super) fn dynamic_sample_masks(&self) -> Option<&[SampleMask]> {
        if self.dynamic_sample_masks {
            Some(&self.sample_masks)
        } else {
            None
        }
    }

    #[cold]
    #[inline]
    fn format_cmd_set_alpha_to_coverage_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, alpha_to_coverage) = args;

        format!("cmd_set_alpha_to_coverage_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            alpha_to_coverage_enable: {alpha_to_coverage})")
    }

    #[cold]
    #[inline]
    fn format_cmd_set_alpha_to_one_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, alpha_to_one) = args;

        format!(
            "cmd_set_alpha_to_one_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            alpha_to_one_enable: {alpha_to_one})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_rasterization_samples(
        args: (&RArc<crate::device::Guard>, CommandBuffer, SampleCountFlags),
    ) -> String {
        let (device, command_buffer, rasterization_samples) = args;

        format!(
            "cmd_set_rasterization_samples(&self: {device:?}, command_buffer: {command_buffer:?}, \
            rasterization_samples: {rasterization_samples:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_sample_mask(
        args: (
            &RArc<crate::device::Guard>,
            CommandBuffer,
            SampleCountFlags,
            &[SampleMask],
        ),
    ) -> String {
        let (device, command_buffer, samples, sample_mask) = args;

        format!(
            "cmd_set_sample_mask(&self: {device:?}, command_buffer: {command_buffer:?}, \
            samples: {samples:?}, sample_mask: {sample_mask:?})"
        )
    }

    #[inline]
    #[allow(dead_code)]
    pub(super) const fn min_sample_shading(&self) -> f32 {
        self.min_sample_shading
    }

    #[inline]
    #[allow(dead_code)]
    pub(super) const fn sample_shading(&self) -> bool {
        self.sample_shading
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let any_dynamics = [
            self.dynamic_alpha_to_coverage,
            self.dynamic_alpha_to_one,
            self.dynamic_rasterization_samples,
            self.dynamic_sample_masks,
        ]
        .into_iter()
        .any(|x| x);

        if !any_dynamics {
            return;
        }

        let device_guard = recorder.buffer().device();

        let instance = device_guard.instance();

        let device = DynamicState3Device::new(&instance, &device_guard);

        let command_buffer = recorder.buffer().ash();

        if self.dynamic_alpha_to_coverage {
            let trace_args = (&device_guard, command_buffer, self.alpha_to_coverage);

            trace_calling(Self::format_cmd_set_alpha_to_coverage_enable, trace_args);

            {
                let _span = debug_span!("cmd_set_alpha_to_coverage_enable");

                unsafe {
                    device.cmd_set_alpha_to_coverage_enable(command_buffer, self.alpha_to_coverage);
                }
            }

            trace_called(
                Self::format_cmd_set_alpha_to_coverage_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        if self.dynamic_alpha_to_one {
            let trace_args = (&device_guard, command_buffer, self.alpha_to_one);

            trace_calling(Self::format_cmd_set_alpha_to_one_enable, trace_args);

            {
                let _span = debug_span!("cmd_set_alpha_to_one_enable");

                unsafe {
                    device.cmd_set_alpha_to_one_enable(command_buffer, self.alpha_to_one);
                }
            }

            trace_called(
                Self::format_cmd_set_alpha_to_one_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        if self.dynamic_rasterization_samples {
            let trace_args = (&device_guard, command_buffer, self.rasterization_samples);

            trace_calling(Self::format_cmd_set_rasterization_samples, trace_args);

            {
                let _span = debug_span!("cmd_set_rasterization_samples");

                unsafe {
                    device
                        .cmd_set_rasterization_samples(command_buffer, self.rasterization_samples);
                }
            }

            trace_called(
                Self::format_cmd_set_rasterization_samples,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        if self.dynamic_sample_masks {
            let sample_mask = self.sample_masks.as_slice();

            let trace_args = (
                &device_guard,
                command_buffer,
                self.rasterization_samples,
                sample_mask,
            );

            trace_calling(Self::format_cmd_set_sample_mask, trace_args);

            {
                let _span = debug_span!("cmd_set_sample_mask");

                unsafe {
                    device.cmd_set_sample_mask(
                        command_buffer,
                        self.rasterization_samples,
                        sample_mask,
                    );
                }
            }

            trace_called(
                Self::format_cmd_set_sample_mask,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
    }
}

impl Eq for Multisample {}

#[allow(clippy::from_over_into)]
impl Into<HalMultisample> for Multisample {
    #[inline]
    fn into(self) -> HalMultisample {
        let rasterization_samples = if SampleCountFlags::TYPE_1 == self.rasterization_samples {
            Count::One
        } else if SampleCountFlags::TYPE_2 == self.rasterization_samples {
            Count::Two
        } else if SampleCountFlags::TYPE_4 == self.rasterization_samples {
            Count::Four
        } else if SampleCountFlags::TYPE_8 == self.rasterization_samples {
            Count::Eight
        } else if SampleCountFlags::TYPE_16 == self.rasterization_samples {
            Count::Sixteen
        } else if SampleCountFlags::TYPE_32 == self.rasterization_samples {
            Count::ThirtyTwo
        } else {
            Count::SixtyFour
        };

        HalMultisample::new(
            rasterization_samples,
            self.sample_shading,
            self.min_sample_shading,
            self.alpha_to_coverage,
            self.alpha_to_one,
        )
    }
}

impl PartialEq for Multisample {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.create_info == other.create_info
            && self.dynamic_alpha_to_coverage == other.dynamic_alpha_to_coverage
            && self.alpha_to_coverage == other.alpha_to_coverage
            && self.dynamic_alpha_to_one == other.dynamic_alpha_to_one
            && self.alpha_to_one == other.alpha_to_one
            && float_eq!(
                self.min_sample_shading,
                other.min_sample_shading,
                r2nd <= f32::EPSILON
            )
            && self.dynamic_rasterization_samples == other.dynamic_rasterization_samples
            && self.rasterization_samples == other.rasterization_samples
            && self.dynamic_sample_masks == other.dynamic_sample_masks
            && self.sample_masks == other.sample_masks
            && self.sample_shading == other.sample_shading
    }
}

unsafe impl Send for Multisample {}
unsafe impl Sync for Multisample {}
