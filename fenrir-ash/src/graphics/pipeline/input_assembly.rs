use {
    crate::{command::buffer::Recorder, device::Device, trace_called, trace_calling},
    abi_stable::std_types::RArc,
    ash::{
        prelude::VkResult,
        vk::{
            CommandBuffer, PipelineInputAssemblyStateCreateFlags,
            PipelineInputAssemblyStateCreateInfo, PrimitiveTopology, FALSE, TRUE,
        },
    },
    fenrir_error::{not_supported, Error},
    fenrir_hal::{graphics::pipeline::Metadata, primitive::Topology, Primitive},
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct InputAssembly {
    flags: Option<PipelineInputAssemblyStateCreateFlags>,
    primitive_restart: bool,
    topology: PrimitiveTopology,
}

impl InputAssembly {
    #[inline]
    pub(super) fn new(device: &Device, metadata: &Metadata) -> Result<Self, Error> {
        let primitive = metadata.primitive();

        {
            let supports_tessellation_shader = TRUE == device.features().tessellation_shader;

            if (Topology::PatchList == primitive.topology()) && !supports_tessellation_shader {
                return Err(not_supported(
                    "device doesn't support Topology::PatchList without tessellation shader support",
                ));
            }
        }

        if FALSE == device.features().geometry_shader {
            match primitive.topology() {
                Topology::LineListWithAdjacency
                | Topology::LineStripWithAdjacency
                | Topology::TriangleListWithAdjacency
                | Topology::TriangleStripWithAdjacency => {
                    return Err(not_supported(&format!(
                        "device doesn't support Topology::{:?}, without geometry shader support",
                        primitive.topology()
                    )));
                }
                _ => {}
            }
        }

        let topology = match primitive.topology() {
            Topology::PointList => PrimitiveTopology::POINT_LIST,
            Topology::LineList => PrimitiveTopology::LINE_LIST,
            Topology::LineStrip => PrimitiveTopology::LINE_STRIP,
            Topology::TriangleList => PrimitiveTopology::TRIANGLE_LIST,
            Topology::TriangleStrip => PrimitiveTopology::TRIANGLE_STRIP,
            Topology::TriangleFan => PrimitiveTopology::TRIANGLE_FAN,
            Topology::LineListWithAdjacency => PrimitiveTopology::LINE_LIST_WITH_ADJACENCY,
            Topology::LineStripWithAdjacency => PrimitiveTopology::LINE_STRIP_WITH_ADJACENCY,
            Topology::TriangleListWithAdjacency => PrimitiveTopology::TRIANGLE_LIST_WITH_ADJACENCY,
            Topology::TriangleStripWithAdjacency => {
                PrimitiveTopology::TRIANGLE_STRIP_WITH_ADJACENCY
            }
            Topology::PatchList => PrimitiveTopology::PATCH_LIST,
        };

        let primitive_restart = primitive.restart();

        let create_info_can_be_none = TRUE
            == device
                .properties()
                .dynamic_state_3()
                .dynamic_primitive_topology_unrestricted;

        let flags = if create_info_can_be_none {
            None
        } else {
            Some(PipelineInputAssemblyStateCreateFlags::default())
        };

        Ok(Self {
            flags,
            primitive_restart,
            topology,
        })
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineInputAssemblyStateCreateInfo<'_>> {
        self.flags.map(|flags| {
            PipelineInputAssemblyStateCreateInfo::default()
                .flags(flags)
                .topology(self.topology)
                .primitive_restart_enable(self.primitive_restart)
        })
    }

    #[cold]
    #[inline]
    fn format_cmd_set_primitive_restart_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, primitive_restart) = args;

        format!(
            "cmd_set_primitive_restart_enable(device={device:?}, command_buffer={command_buffer:?}, \
            primitive_restart={primitive_restart:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_primitive_topology(
        args: (
            &RArc<crate::device::Guard>,
            CommandBuffer,
            PrimitiveTopology,
        ),
    ) -> String {
        let (device, command_buffer, topology) = args;

        format!(
            "cmd_set_primitive_topology(device={device:?}, command_buffer={command_buffer:?}, \
            primitive_topology={topology:?})"
        )
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let device = recorder.buffer().device();

        let command_buffer = recorder.buffer().ash();

        {
            let trace_args = (&device, command_buffer, self.primitive_restart);

            trace_calling(Self::format_cmd_set_primitive_restart_enable, trace_args);

            {
                debug_span!("cmd_set_primitive_restart_enable");

                unsafe {
                    device.cmd_set_primitive_restart_enable(command_buffer, self.primitive_restart);
                }
            }

            trace_called(
                Self::format_cmd_set_primitive_restart_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (&device, command_buffer, self.topology);

            trace_calling(Self::format_cmd_set_primitive_topology, trace_args);

            {
                debug_span!("cmd_set_primitive_topology");

                unsafe {
                    device.cmd_set_primitive_topology(command_buffer, self.topology);
                }
            }

            trace_called(
                Self::format_cmd_set_primitive_topology,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<Primitive> for InputAssembly {
    #[inline]
    fn into(self) -> Primitive {
        let topology = if PrimitiveTopology::POINT_LIST == self.topology {
            Topology::PointList
        } else if PrimitiveTopology::LINE_LIST == self.topology {
            Topology::LineList
        } else if PrimitiveTopology::LINE_STRIP == self.topology {
            Topology::LineStrip
        } else if PrimitiveTopology::TRIANGLE_LIST == self.topology {
            Topology::TriangleList
        } else if PrimitiveTopology::TRIANGLE_STRIP == self.topology {
            Topology::TriangleStrip
        } else if PrimitiveTopology::TRIANGLE_FAN == self.topology {
            Topology::TriangleFan
        } else if PrimitiveTopology::LINE_LIST_WITH_ADJACENCY == self.topology {
            Topology::LineListWithAdjacency
        } else if PrimitiveTopology::LINE_STRIP_WITH_ADJACENCY == self.topology {
            Topology::LineStripWithAdjacency
        } else if PrimitiveTopology::TRIANGLE_LIST_WITH_ADJACENCY == self.topology {
            Topology::TriangleListWithAdjacency
        } else if PrimitiveTopology::TRIANGLE_STRIP_WITH_ADJACENCY == self.topology {
            Topology::TriangleStripWithAdjacency
        } else {
            Topology::PatchList
        };

        Primitive::new(topology, self.primitive_restart)
    }
}

unsafe impl Send for InputAssembly {}
unsafe impl Sync for InputAssembly {}
