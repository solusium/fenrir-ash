use {
    crate::{command::buffer::Recorder, trace_called, trace_calling, Device},
    abi_stable::std_types::RArc,
    ash::{
        prelude::VkResult,
        vk::{
            CommandBuffer, Extent2D, Offset2D, PipelineViewportStateCreateFlags,
            PipelineViewportStateCreateInfo, Rect2D as AshRect2D, Viewport as AshViewport, TRUE,
        },
    },
    core::ops::Range,
    fenrir_error::{argument_out_of_domain, Error},
    fenrir_hal::{
        geometry::Rect2D as HalRect2D, graphics::pipeline::Metadata, Viewport as HalViewport,
    },
    float_eq::float_eq,
    smallvec::SmallVec,
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
struct CreateInfo {
    flags: PipelineViewportStateCreateFlags,
}

#[derive(Clone, Debug)]
pub(crate) struct Viewport {
    create_info: Option<CreateInfo>,
    scissors: SmallVec<[AshRect2D; 2]>,
    viewports: SmallVec<[AshViewport; 2]>,
}

impl Viewport {
    #[inline]
    pub(super) fn new(device: &Device, metadata: &Metadata) -> Result<Self, Error> {
        let max_count = if TRUE == device.features().multi_viewport {
            usize::try_from(device.limits().max_viewports).map_err(|_| {
                argument_out_of_domain(
                    Range {
                        start: 0.into(),
                        end: usize::MAX.into(),
                    },
                    device.limits().max_viewports.into(),
                )
            })
        } else {
            Ok(1)
        }?;

        let out_of_domain = |result: usize| {
            Err(argument_out_of_domain(
                Range {
                    start: 0.into(),
                    end: (max_count + 1).into(),
                },
                result.into(),
            ))
        };

        if metadata.viewports().len() > max_count {
            return out_of_domain(metadata.viewports().len());
        }

        let viewports = Self::hal_to_ash_viewports(metadata.viewports());

        if metadata.scissors().len() > max_count {
            return out_of_domain(metadata.scissors().len());
        }

        let scissors = Self::hal_to_ash_scissors(metadata.scissors())?;

        let enabled_extended_dynamic_state_3 = device
            .extension_names()?
            .any(|extension_name| "VK_EXT_extended_dynamic_state3" == extension_name);

        let create_info = if enabled_extended_dynamic_state_3 {
            None
        } else {
            let flags = PipelineViewportStateCreateFlags::default();

            Some(CreateInfo { flags })
        };

        Ok(Self {
            create_info,
            scissors,
            viewports,
        })
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineViewportStateCreateInfo<'_>> {
        self.create_info.as_ref().map(|create_info| {
            PipelineViewportStateCreateInfo::default()
                .flags(create_info.flags)
                .viewports(&self.viewports)
                .scissors(&self.scissors)
        })
    }

    #[inline]
    fn format_cmd_set_scissor_with_count(
        args: (&RArc<crate::device::Guard>, CommandBuffer, &[AshRect2D]),
    ) -> String {
        let (device, command_buffer, scissors) = args;

        format!(
            "cmd_set_scissor_with_count(&self: {device:?}, command_buffer: {command_buffer:?}, scissors: \
            {scissors:?})"
        )
    }

    #[inline]
    fn format_cmd_set_viewport_with_count(
        args: (&RArc<crate::device::Guard>, CommandBuffer, &[AshViewport]),
    ) -> String {
        let (device, command_buffer, viewports) = args;

        format!(
            "cmd_set_viewport(&self: {device:?}, command_buffer: {command_buffer:?}, viewports: \
            {viewports:?})"
        )
    }

    #[inline]
    fn hal_to_ash_scissors(input: &[HalRect2D]) -> Result<SmallVec<[AshRect2D; 2]>, Error> {
        let signed_cast = |input: isize| -> Result<i32, Error> {
            input.try_into().map_or_else(
                |_| {
                    Err(argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: (u64::from(u32::MAX) + 1).into(),
                        },
                        input.into(),
                    ))
                },
                Ok,
            )
        };

        let unsigned_cast = |input: usize| -> Result<u32, Error> {
            input.try_into().map_or_else(
                |_| {
                    Err(argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: (u64::from(u32::MAX) + 1).into(),
                        },
                        input.into(),
                    ))
                },
                Ok,
            )
        };

        input
            .iter()
            .map(|scissor| {
                Ok(AshRect2D::default()
                    .offset(
                        Offset2D::default()
                            .x(signed_cast(scissor.offset().x())?)
                            .y(signed_cast(scissor.offset().y())?),
                    )
                    .extent(
                        Extent2D::default()
                            .height(unsigned_cast(scissor.extent().height())?)
                            .width(unsigned_cast(scissor.extent().width())?),
                    ))
            })
            .collect()
    }

    #[inline]
    fn hal_to_ash_viewports(input: &[HalViewport]) -> SmallVec<[AshViewport; 2]> {
        input
            .iter()
            .map(|viewport| {
                AshViewport::default()
                    .x(viewport.x())
                    .y(viewport.y())
                    .width(viewport.width())
                    .height(viewport.height())
                    .min_depth(viewport.min_depth())
                    .max_depth(viewport.max_depth())
            })
            .collect()
    }

    #[inline]
    pub(super) fn scissors(&self) -> &[AshRect2D] {
        &self.scissors
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let device = recorder.buffer().device();

        let command_buffer = recorder.buffer().ash();

        {
            let viewports = self.viewports.as_slice();

            let trace_args = (&device, command_buffer, viewports);

            trace_calling(Self::format_cmd_set_viewport_with_count, trace_args);

            {
                let _span = debug_span!("cmd_set_viewport_with_count");

                unsafe {
                    device.cmd_set_viewport_with_count(command_buffer, viewports);
                };
            }

            trace_called(
                Self::format_cmd_set_viewport_with_count,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        let scissors = self.scissors.as_slice();

        let trace_args = (&device, command_buffer, scissors);

        trace_calling(Self::format_cmd_set_scissor_with_count, trace_args);

        {
            let _span = debug_span!("cmd_set_scissor_count");

            unsafe {
                device.cmd_set_scissor_with_count(command_buffer, scissors);
            }
        }

        trace_called(
            Self::format_cmd_set_scissor_with_count,
            trace_args,
            None::<&VkResult<()>>,
        );
    }

    #[inline]
    pub(super) fn viewports(&self) -> &[AshViewport] {
        &self.viewports
    }
}

impl Eq for Viewport {}

impl PartialEq for Viewport {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.create_info == other.create_info
            && self.scissors == other.scissors
            && self
                .viewports
                .iter()
                .zip(other.viewports.iter())
                .all(|(left, right)| {
                    float_eq!(left.x, right.x, r2nd <= f32::EPSILON)
                        && float_eq!(left.y, right.y, r2nd <= f32::EPSILON)
                        && float_eq!(left.width, right.width, r2nd <= f32::EPSILON)
                        && float_eq!(left.height, right.height, r2nd <= f32::EPSILON)
                        && float_eq!(left.min_depth, right.min_depth, r2nd <= f32::EPSILON)
                        && float_eq!(left.max_depth, right.max_depth, r2nd <= f32::EPSILON)
                })
    }
}

unsafe impl Send for Viewport {}
unsafe impl Sync for Viewport {}
