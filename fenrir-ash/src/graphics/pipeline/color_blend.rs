use {
    crate::{
        command::buffer::Recorder, device_does_not_support, trace_called, trace_calling,
        Attachment as ColorAttachment, Device,
    },
    abi_stable::std_types::RArc,
    ash::{
        ext::{
            extended_dynamic_state2::Device as DynamicState2Device,
            extended_dynamic_state3::Device as DynamicState3Device,
        },
        prelude::VkResult,
        vk::{
            ColorBlendAdvancedEXT, ColorBlendEquationEXT, CommandBuffer, LogicOp,
            PhysicalDeviceExtendedDynamicState2FeaturesEXT,
            PhysicalDeviceExtendedDynamicState3FeaturesEXT, PipelineColorBlendAttachmentState,
            PipelineColorBlendStateCreateFlags, PipelineColorBlendStateCreateInfo, FALSE, TRUE,
        },
    },
    fenrir_error::Error,
    fenrir_hal::{
        color::{blend::State, Blend, RGBA},
        graphics::pipeline::Metadata,
        logical::Operation as LogicalOperation,
    },
    smallvec::{smallvec, SmallVec},
    tracing::debug_span,
};

mod attachments;
mod factor;
mod operation;

pub(crate) type Attachments = crate::graphics::pipeline::color_blend::attachments::Attachments;

#[derive(Clone, Debug, Eq, PartialEq)]
struct CreateInfo {
    flags: PipelineColorBlendStateCreateFlags,
}

#[derive(Clone, Debug)]
pub(crate) struct ColorBlend {
    create_info: Option<CreateInfo>,
    _advanced: Option<SmallVec<[ColorBlendAdvancedEXT; 3]>>,
    attachments: Attachments,
    dynamic_logic_op: bool,
    logic_op: LogicOp,
    dynamic_logic_op_enable: bool,
    logic_op_enable: bool,
    constants: [f32; 4],
}

impl ColorBlend {
    pub(super) fn new(
        device: &Device,
        metadata: &Metadata,
        color_attachments: &[ColorAttachment],
        available_dynamic_state_2_features: &PhysicalDeviceExtendedDynamicState2FeaturesEXT,
        available_dynamic_state_3_features: &PhysicalDeviceExtendedDynamicState3FeaturesEXT,
    ) -> Result<Self, Error> {
        let color_blend = metadata.color_blend();

        if (FALSE == device.features().logic_op) && color_blend.logical_operation().is_some() {
            return Err(device_does_not_support("Blend::logical_operation"));
        }

        let attachments =
            Self::new_attachments(available_dynamic_state_3_features, color_attachments);

        let constants = [
            color_blend.constants().r(),
            color_blend.constants().g(),
            color_blend.constants().b(),
            color_blend.constants().a(),
        ];

        let (logic_op, logic_op_enable) = color_blend.logical_operation().map_or_else(
            || (LogicOp::default(), false),
            |logical_operation| {
                let logic_op_ = match logical_operation {
                    LogicalOperation::Clear => LogicOp::CLEAR,
                    LogicalOperation::And => LogicOp::AND,
                    LogicalOperation::AndReverse => LogicOp::AND_REVERSE,
                    LogicalOperation::Copy => LogicOp::COPY,
                    LogicalOperation::AndInverted => LogicOp::AND_INVERTED,
                    LogicalOperation::NoOp => LogicOp::NO_OP,
                    LogicalOperation::Xor => LogicOp::XOR,
                    LogicalOperation::Or => LogicOp::OR,
                    LogicalOperation::Not => unreachable!(),
                    LogicalOperation::Nor => LogicOp::NOR,
                    LogicalOperation::Equivalent => LogicOp::EQUIVALENT,
                    LogicalOperation::Invert => LogicOp::INVERT,
                    LogicalOperation::OrReverse => LogicOp::OR_REVERSE,
                    LogicalOperation::CopyInverted => LogicOp::COPY_INVERTED,
                    LogicalOperation::OrInverted => LogicOp::OR_INVERTED,
                    LogicalOperation::Nand => LogicOp::NAND,
                    LogicalOperation::Set => LogicOp::SET,
                };

                (logic_op_, true)
            },
        );

        let dynamic_logic_op_enable =
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_logic_op_enable;

        let dynamic_logic_op =
            TRUE == available_dynamic_state_2_features.extended_dynamic_state2_logic_op;

        let dynamic_attachments = matches!(
            &attachments,
            Attachments::Dynamics {
                enables: _,
                equations: _,
                write_masks: _,
            }
        );

        let create_info = if dynamic_attachments && dynamic_logic_op && dynamic_logic_op_enable {
            None
        } else {
            Some(CreateInfo {
                flags: PipelineColorBlendStateCreateFlags::default(),
            })
        };

        Ok(Self {
            create_info,
            _advanced: None,
            attachments,
            constants,
            dynamic_logic_op,
            logic_op,
            dynamic_logic_op_enable,
            logic_op_enable,
        })
    }

    #[inline]
    fn add_enables(
        ash_state: PipelineColorBlendAttachmentState,
        hal_state: &State,
    ) -> PipelineColorBlendAttachmentState {
        ash_state.blend_enable(hal_state.enabled())
    }

    #[inline]
    fn add_equations(
        ash_state: PipelineColorBlendAttachmentState,
        hal_state: &State,
    ) -> PipelineColorBlendAttachmentState {
        ash_state
            .src_color_blend_factor(factor::hal_to_ash(hal_state.source_color_blend_factor()))
            .dst_color_blend_factor(factor::hal_to_ash(
                hal_state.destination_color_blend_factor(),
            ))
            .color_blend_op(operation::hal_to_ash(hal_state.color_blend_operation()))
            .src_alpha_blend_factor(factor::hal_to_ash(hal_state.source_alpha_blend_factor()))
            .dst_alpha_blend_factor(factor::hal_to_ash(
                hal_state.destination_alpha_blend_factor(),
            ))
            .alpha_blend_op(operation::hal_to_ash(hal_state.alpha_blend_operation()))
    }

    #[inline]
    fn add_write_masks(
        ash_state: PipelineColorBlendAttachmentState,
        hal_state: &State,
    ) -> PipelineColorBlendAttachmentState {
        ash_state.color_write_mask(crate::color::component::hal_to_ash(
            hal_state.color_write_mask(),
        ))
    }

    #[inline]
    pub(super) const fn attachments(
        &self,
    ) -> crate::graphics::pipeline::color_blend::attachments::State<'_> {
        crate::graphics::pipeline::color_blend::attachments::State::new(&self.attachments)
    }

    #[inline]
    #[allow(dead_code)]
    pub(crate) const fn constants(&self) -> &[f32] {
        &self.constants
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineColorBlendStateCreateInfo<'_>> {
        self.create_info.as_ref().map(|create_info| {
            let mut ash_create_info =
                PipelineColorBlendStateCreateInfo::default().flags(create_info.flags);

            if self.dynamic_logic_op {
                ash_create_info = ash_create_info.logic_op(self.logic_op);
            }

            if self.dynamic_logic_op_enable {
                ash_create_info = ash_create_info.logic_op_enable(self.logic_op_enable);
            }

            let maybe_states = match &self.attachments {
                Attachments::States { states }
                | Attachments::DynamicEnables { enables: _, states }
                | Attachments::DynamicEquations {
                    equations: _,
                    states,
                }
                | Attachments::DynamicWriteMasks {
                    write_masks: _,
                    states,
                }
                | Attachments::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states,
                }
                | Attachments::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states,
                }
                | Attachments::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states,
                }
                | Attachments::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states,
                } => Some(states),
                Attachments::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                } => None,
            };

            if let Some(states) = maybe_states {
                ash_create_info = ash_create_info.attachments(states);
            }

            ash_create_info
        })
    }

    #[inline]
    pub(crate) const fn dynamic_logic_op(&self) -> Option<LogicOp> {
        if self.dynamic_logic_op {
            Some(self.logic_op)
        } else {
            None
        }
    }

    #[inline]
    pub(crate) const fn dynamic_logic_op_enable(&self) -> Option<bool> {
        if self.dynamic_logic_op_enable {
            Some(self.logic_op_enable)
        } else {
            None
        }
    }

    #[inline]
    fn format_cmd_set_logic_op(
        args: (&RArc<crate::device::Guard>, CommandBuffer, LogicOp),
    ) -> String {
        let (device, command_buffer, logic_op) = args;

        format!(
            "cmd_set_logic_op(&self: {device:?}, command_buffer: {command_buffer:?}, logic_op: \
            {logic_op:?})",
        )
    }

    #[inline]
    fn format_cmd_set_logic_op_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, logic_op_enable) = args;

        format!(
            "cmd_set_logic_op_enable(&self: {device:?}, command_buffer: {command_buffer:?}, logic_op_enable: \
            {logic_op_enable:?})",
        )
    }

    #[inline]
    fn from_enables(state: &State) -> PipelineColorBlendAttachmentState {
        PipelineColorBlendAttachmentState::default().blend_enable(state.enabled())
    }

    #[inline]
    fn from_equations(state: &State) -> PipelineColorBlendAttachmentState {
        Self::add_equations(PipelineColorBlendAttachmentState::default(), state)
    }

    #[inline]
    fn from_enables_and_equations(state: &State) -> PipelineColorBlendAttachmentState {
        Self::add_enables(
            Self::add_equations(PipelineColorBlendAttachmentState::default(), state),
            state,
        )
    }

    #[inline]
    fn from_enables_and_equations_and_write_masks(
        state: &State,
    ) -> PipelineColorBlendAttachmentState {
        Self::add_enables(
            Self::add_equations(
                Self::add_write_masks(PipelineColorBlendAttachmentState::default(), state),
                state,
            ),
            state,
        )
    }

    #[inline]
    fn from_enables_and_write_masks(state: &State) -> PipelineColorBlendAttachmentState {
        Self::add_enables(
            Self::add_write_masks(PipelineColorBlendAttachmentState::default(), state),
            state,
        )
    }

    #[inline]
    fn from_equations_and_write_masks(state: &State) -> PipelineColorBlendAttachmentState {
        Self::add_equations(
            Self::add_write_masks(PipelineColorBlendAttachmentState::default(), state),
            state,
        )
    }

    #[inline]
    fn from_write_masks(state: &State) -> PipelineColorBlendAttachmentState {
        Self::add_write_masks(PipelineColorBlendAttachmentState::default(), state)
    }

    #[inline]
    fn new_attachments(
        available_dynamic_state_3_features: &PhysicalDeviceExtendedDynamicState3FeaturesEXT,
        color_attachments: &[ColorAttachment],
    ) -> Attachments {
        let dynamic_attachments_settings = (
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_color_blend_advanced,
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_color_blend_enable,
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_color_blend_equation,
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_color_write_mask,
        );

        let color_blend_states = color_attachments
            .iter()
            .map(ColorAttachment::color_blend_state)
            .collect::<SmallVec<[_; 3]>>();

        let new_enables = || {
            color_blend_states
                .iter()
                .map(|color_blend_state| {
                    if color_blend_state.enabled() {
                        TRUE
                    } else {
                        FALSE
                    }
                })
                .collect::<SmallVec<[_; 3]>>()
        };

        let new_equations = || {
            color_blend_states
                .iter()
                .map(Self::new_color_blend_equation)
                .collect()
        };

        let new_write_masks = || {
            color_blend_states
                .iter()
                .map(|state| crate::color::component::hal_to_ash(state.color_write_mask()))
                .collect()
        };

        match dynamic_attachments_settings {
            (true, true, true, true) => Attachments::Dynamics {
                enables: new_enables(),
                equations: new_equations(),
                write_masks: new_write_masks(),
            },
            (_, true, true, false) => Attachments::DynamicEnablesAndEquations {
                enables: new_enables(),
                equations: new_equations(),
                states: Self::new_state(color_attachments, Self::from_write_masks),
            },
            (_, true, false, true) => Attachments::DynamicEnablesAndWriteMasks {
                enables: new_enables(),
                write_masks: new_write_masks(),
                states: Self::new_state(color_attachments, Self::from_equations),
            },
            (_, true, false, false) => Attachments::DynamicEnables {
                enables: new_enables(),
                states: Self::new_state(color_attachments, Self::from_equations_and_write_masks),
            },
            (_, false, true, true) => Attachments::DynamicEquationsAndWriteMasks {
                equations: new_equations(),
                write_masks: new_write_masks(),
                states: Self::new_state(color_attachments, Self::from_enables),
            },
            (_, false, true, false) => Attachments::DynamicEquations {
                equations: new_equations(),
                states: Self::new_state(color_attachments, Self::from_enables_and_write_masks),
            },
            (_, false, false, true) => Attachments::DynamicWriteMasks {
                write_masks: new_write_masks(),
                states: Self::new_state(color_attachments, Self::from_enables_and_equations),
            },
            (_, false, false, false) => Attachments::States {
                states: Self::new_state(
                    color_attachments,
                    Self::from_enables_and_equations_and_write_masks,
                ),
            },
            (false, true, true, true) => Attachments::DynamicEnablesAndEquationsAndWriteMasks {
                enables: new_enables(),
                equations: new_equations(),
                write_masks: new_write_masks(),
                states: smallvec!(PipelineColorBlendAttachmentState::default();
                                  color_attachments.len()),
            },
        }
    }

    #[inline]
    #[allow(clippy::trivially_copy_pass_by_ref)]
    fn new_color_blend_equation(state: &&State) -> ColorBlendEquationEXT {
        ColorBlendEquationEXT::default()
            .src_color_blend_factor(factor::hal_to_ash(state.source_color_blend_factor()))
            .dst_color_blend_factor(factor::hal_to_ash(state.destination_color_blend_factor()))
            .color_blend_op(operation::hal_to_ash(state.color_blend_operation()))
            .src_alpha_blend_factor(factor::hal_to_ash(state.source_alpha_blend_factor()))
            .dst_alpha_blend_factor(factor::hal_to_ash(state.destination_alpha_blend_factor()))
            .alpha_blend_op(operation::hal_to_ash(state.alpha_blend_operation()))
    }

    #[inline]
    fn new_state<F>(
        color_attachments: &[ColorAttachment],
        f: F,
    ) -> SmallVec<[PipelineColorBlendAttachmentState; 3]>
    where
        F: Fn(&State) -> PipelineColorBlendAttachmentState,
    {
        color_attachments
            .iter()
            .map(ColorAttachment::color_blend_state)
            .map(f)
            .collect()
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let device_guard = recorder.buffer().device();

        let instance = device_guard.instance();

        let command_buffer = recorder.buffer().ash();

        if self.dynamic_logic_op_enable {
            let device = DynamicState3Device::new(&instance, &device_guard);

            let trace_args = (&device_guard, command_buffer, self.logic_op_enable);

            trace_calling(Self::format_cmd_set_logic_op_enable, trace_args);

            {
                let _span = debug_span!("cmd_set_logic_op_enable");

                unsafe {
                    device.cmd_set_logic_op_enable(command_buffer, self.logic_op_enable);
                }
            }

            trace_called(
                Self::format_cmd_set_logic_op_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        if self.dynamic_logic_op {
            let device = DynamicState2Device::new(&instance, &device_guard);

            let trace_args = (&device_guard, command_buffer, self.logic_op);

            trace_calling(Self::format_cmd_set_logic_op, trace_args);

            {
                let _span = debug_span!("cmd_set_logic_op");

                unsafe {
                    device.cmd_set_logic_op(command_buffer, self.logic_op);
                }
            }

            trace_called(
                Self::format_cmd_set_logic_op,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        self.attachments
            .set_dynamics(&instance, &device_guard, command_buffer);
    }
}

impl Eq for ColorBlend {}

#[allow(clippy::from_over_into)]
impl Into<Blend> for ColorBlend {
    #[inline]
    fn into(self) -> Blend {
        Blend::new(
            None,
            RGBA::new(
                self.constants[0],
                self.constants[1],
                self.constants[2],
                self.constants[3],
            ),
            Some(crate::logical::ash_to_hal(self.logic_op)),
        )
    }
}

impl PartialEq for ColorBlend {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.create_info == other.create_info,
            self.attachments == other.attachments,
            self.dynamic_logic_op == other.dynamic_logic_op,
            self.logic_op == other.logic_op,
            self.dynamic_logic_op_enable == other.dynamic_logic_op_enable,
            self.logic_op_enable == other.logic_op_enable,
            self.constants == other.constants,
        ];

        values.iter().all(|value| *value)
    }
}
