use {
    crate::graphics::pipeline::{
        ColorBlend, Multisample, Rasterization, Tessellation, VertexInput,
    },
    ash::vk::{
        DynamicState, PhysicalDeviceExtendedDynamicState3FeaturesEXT,
        PipelineDynamicStateCreateFlags, PipelineDynamicStateCreateInfo, TRUE,
    },
    smallvec::{smallvec, SmallVec},
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct DynamicStates {
    flags: PipelineDynamicStateCreateFlags,
    dynamic_states: SmallVec<[DynamicState; 31]>,
}

impl DynamicStates {
    #[inline]
    pub(super) fn new(
        color_blend: &ColorBlend,
        multisample: &Multisample,
        rasterization: &Rasterization,
        tessellation: &Tessellation,
        vertex_input: &VertexInput,
        available_dynamic_state_3_features: &PhysicalDeviceExtendedDynamicState3FeaturesEXT,
    ) -> Self {
        let mut dynamic_states = smallvec![
            DynamicState::BLEND_CONSTANTS,
            DynamicState::CULL_MODE,
            DynamicState::DEPTH_BIAS,
            DynamicState::DEPTH_BIAS_ENABLE,
            DynamicState::DEPTH_BOUNDS,
            DynamicState::DEPTH_BOUNDS_TEST_ENABLE,
            DynamicState::DEPTH_COMPARE_OP,
            DynamicState::DEPTH_TEST_ENABLE,
            DynamicState::DEPTH_WRITE_ENABLE,
            DynamicState::FRONT_FACE,
            DynamicState::LINE_WIDTH,
            DynamicState::PRIMITIVE_RESTART_ENABLE,
            DynamicState::PRIMITIVE_TOPOLOGY,
            DynamicState::RASTERIZER_DISCARD_ENABLE,
            DynamicState::SCISSOR_WITH_COUNT,
            DynamicState::STENCIL_OP,
            DynamicState::STENCIL_TEST_ENABLE,
            DynamicState::VIEWPORT_WITH_COUNT,
        ];

        {
            if color_blend.dynamic_logic_op().is_some() {
                dynamic_states.push(DynamicState::LOGIC_OP_EXT);
            }

            if color_blend.dynamic_logic_op_enable().is_some() {
                dynamic_states.push(DynamicState::LOGIC_OP_ENABLE_EXT);
            }

            let color_blend_attachments = color_blend.attachments();

            if color_blend_attachments.dynamic_enables().is_some() {
                dynamic_states.push(DynamicState::COLOR_BLEND_ENABLE_EXT);
            }

            if color_blend_attachments.dynamic_equations().is_some() {
                dynamic_states.push(DynamicState::COLOR_BLEND_EQUATION_EXT);
            }

            if color_blend_attachments.dynamic_write_masks().is_some() {
                dynamic_states.push(DynamicState::COLOR_WRITE_MASK_EXT);
            }

            let dynamic_color_blend_advanced = TRUE
                == available_dynamic_state_3_features.extended_dynamic_state3_color_blend_advanced;

            if dynamic_color_blend_advanced {
                dynamic_states.push(DynamicState::COLOR_BLEND_ADVANCED_EXT);
            }
        }

        if multisample.dynamic_alpha_to_coverage().is_some() {
            dynamic_states.push(DynamicState::ALPHA_TO_COVERAGE_ENABLE_EXT);
        }

        if multisample.dynamic_alpha_to_one().is_some() {
            dynamic_states.push(DynamicState::ALPHA_TO_ONE_ENABLE_EXT);
        }

        if multisample.dynamic_rasterization_samples().is_some() {
            dynamic_states.push(DynamicState::RASTERIZATION_SAMPLES_EXT);
        }

        if multisample.dynamic_sample_masks().is_some() {
            dynamic_states.push(DynamicState::SAMPLE_MASK_EXT);
        }

        if rasterization.dynamic_depth_clamp() {
            dynamic_states.push(DynamicState::DEPTH_CLAMP_ENABLE_EXT);
        }

        if rasterization.dynamic_polygon_mode() {
            dynamic_states.push(DynamicState::POLYGON_MODE_EXT);
        }

        if tessellation.dynamic_patch_control_points().is_some() {
            dynamic_states.push(DynamicState::PATCH_CONTROL_POINTS_EXT);
        }

        if vertex_input.dynamic_vertex_input().is_some() {
            dynamic_states.push(DynamicState::VERTEX_INPUT_EXT);
        }

        let flags = PipelineDynamicStateCreateFlags::default();

        Self {
            flags,
            dynamic_states,
        }
    }

    #[inline]
    pub(super) fn create_info(&self) -> PipelineDynamicStateCreateInfo<'_> {
        PipelineDynamicStateCreateInfo::default()
            .flags(self.flags)
            .dynamic_states(&self.dynamic_states)
    }
}

unsafe impl Send for DynamicStates {}
unsafe impl Sync for DynamicStates {}
