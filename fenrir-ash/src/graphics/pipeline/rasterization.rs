use {
    crate::{command::buffer::Recorder, depth::Bias, trace_called, trace_calling},
    abi_stable::std_types::RArc,
    ash::{
        ext::extended_dynamic_state3::Device as DynamicState3Device,
        prelude::VkResult,
        vk::{
            CommandBuffer, CullModeFlags, FrontFace,
            PhysicalDeviceExtendedDynamicState3FeaturesEXT, PipelineRasterizationStateCreateFlags,
            PipelineRasterizationStateCreateInfo, PolygonMode as AshPolygonMode, TRUE,
        },
    },
    fenrir_hal::{
        cull::Mode as HalCullMode, front::Face, graphics::pipeline::Metadata,
        polygon::Mode as HalPolygonMode,
    },
    float_eq::float_eq,
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
struct CreateInfo {
    flags: PipelineRasterizationStateCreateFlags,
}

#[derive(Clone, Debug)]
#[allow(clippy::struct_excessive_bools)]
pub(crate) struct Rasterization {
    create_info: Option<CreateInfo>,
    cull_mode: CullModeFlags,
    depth_bias: Bias,
    depth_clamp: bool,
    dynamic_depth_clamp: bool,
    front_face: FrontFace,
    line_width: f32,
    dynamic_polygon_mode: bool,
    polygon_mode: AshPolygonMode,
    rasterizer_discard: bool,
}

impl Rasterization {
    pub(super) fn new(
        metadata: &Metadata,
        available_dynamic_state_3_features: &PhysicalDeviceExtendedDynamicState3FeaturesEXT<'_>,
    ) -> Self {
        let rasterizer_discard = metadata.rasterizer_discard();

        let polygon_mode = match metadata.polygon_mode() {
            HalPolygonMode::Fill => AshPolygonMode::FILL,
            HalPolygonMode::Line => AshPolygonMode::LINE,
            HalPolygonMode::Point => AshPolygonMode::POINT,
        };

        let dynamic_polygon_mode =
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_polygon_mode;

        let line_width = metadata.line_width();

        let front_face = match metadata.front_face() {
            Face::Clockwise => FrontFace::CLOCKWISE,
            Face::CounterClockwise => FrontFace::COUNTER_CLOCKWISE,
        };

        let dynamic_depth_clamp =
            TRUE == available_dynamic_state_3_features.extended_dynamic_state3_depth_clamp_enable;

        let depth_clamp = metadata.depth_clamp();

        let depth_bias = Bias::new(
            metadata.depth_bias().enabled(),
            metadata.depth_bias().clamp(),
            metadata.depth_bias().constant_factor(),
            metadata.depth_bias().slope_factor(),
        );

        let cull_mode = match metadata.cull_mode() {
            HalCullMode::None => CullModeFlags::NONE,
            HalCullMode::Front => CullModeFlags::FRONT,
            HalCullMode::Back => CullModeFlags::BACK,
            HalCullMode::FrontAndBack => CullModeFlags::FRONT_AND_BACK,
        };

        let create_info = if dynamic_depth_clamp && dynamic_polygon_mode {
            None
        } else {
            let flags = PipelineRasterizationStateCreateFlags::default();

            Some(CreateInfo { flags })
        };

        Self {
            create_info,
            cull_mode,
            depth_bias,
            depth_clamp,
            dynamic_depth_clamp,
            front_face,
            line_width,
            dynamic_polygon_mode,
            polygon_mode,
            rasterizer_discard,
        }
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineRasterizationStateCreateInfo<'_>> {
        self.create_info.as_ref().map(|create_info| {
            let mut ash_create_info =
                PipelineRasterizationStateCreateInfo::default().flags(create_info.flags);

            if !self.dynamic_depth_clamp {
                ash_create_info = ash_create_info.depth_clamp_enable(self.depth_clamp);
            }

            if !self.dynamic_polygon_mode {
                ash_create_info = ash_create_info.polygon_mode(self.polygon_mode);
            }

            ash_create_info
        })
    }

    #[inline]
    pub(super) const fn cull_mode(&self) -> CullModeFlags {
        self.cull_mode
    }

    #[inline]
    pub(super) const fn depth_bias(&self) -> Bias {
        self.depth_bias
    }

    #[inline]
    pub(super) const fn depth_clamp(&self) -> bool {
        self.depth_clamp
    }

    #[inline]
    pub(super) const fn dynamic_depth_clamp(&self) -> bool {
        self.dynamic_depth_clamp
    }

    #[inline]
    pub(super) const fn dynamic_polygon_mode(&self) -> bool {
        self.dynamic_polygon_mode
    }

    #[cold]
    #[inline]
    fn format_cmd_set_cull_mode(
        args: (&RArc<crate::device::Guard>, CommandBuffer, CullModeFlags),
    ) -> String {
        let (device, command_buffer, cull_mode) = args;

        format!(
            "cmd_set_cull_mode(&self: {device:?}, command_buffer: {command_buffer:?}, cull_mode: \
            {cull_mode:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_depth_clamp_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, depth_clamp_enable) = args;

        format!(
            "cmd_set_depth_clamp_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            depth_clamp_enable: {depth_clamp_enable:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_fron_face(
        args: (&RArc<crate::device::Guard>, CommandBuffer, FrontFace),
    ) -> String {
        let (device, command_buffer, front_face) = args;

        format!(
            "cmd_set_front_face(&self: {device:?}, command_buffer: {command_buffer:?}, front_face: \
            {front_face:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_line_width(
        args: (&RArc<crate::device::Guard>, CommandBuffer, f32),
    ) -> String {
        let (device, command_buffer, line_width) = args;

        format!(
            "cmd_set_line_width(&self: {device:?}, command_buffer: {command_buffer:?}, line_width: \
            {line_width:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_polygon_mode(
        args: (&RArc<crate::device::Guard>, CommandBuffer, AshPolygonMode),
    ) -> String {
        let (device, command_buffer, polygon_mode) = args;

        format!(
            "cmd_set_polygon_mode(&self: {device:?}, command_buffer: {command_buffer:?}, polygon_mode: \
            {polygon_mode:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_rasterizer_discard_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command, enable) = args;

        format!(
            "cmd_set_rasterizer_discard_enable(&self: {device:?}, command: {command:?}, \
            rasterizer_discard_enable: {enable:?})"
        )
    }

    #[inline]
    pub(super) const fn front_face(&self) -> FrontFace {
        self.front_face
    }

    #[inline]
    pub(super) const fn line_width(&self) -> f32 {
        self.line_width
    }

    #[inline]
    pub(super) const fn polygon_mode(&self) -> AshPolygonMode {
        self.polygon_mode
    }

    #[inline]
    pub(super) const fn rasterizer_discard(&self) -> bool {
        self.rasterizer_discard
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let device_guard = recorder.buffer().device();

        let instance = device_guard.instance();

        let device = DynamicState3Device::new(&instance, &device_guard);

        let command_buffer = recorder.buffer().ash();

        self.depth_bias.set_dynamics(recorder);

        {
            let trace_args = (&device_guard, command_buffer, self.cull_mode);

            trace_calling(Self::format_cmd_set_cull_mode, trace_args);

            {
                debug_span!("cmd_set_cull_mode");

                unsafe {
                    device_guard.cmd_set_cull_mode(command_buffer, self.cull_mode);
                }
            }

            trace_called(
                Self::format_cmd_set_cull_mode,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        if self.dynamic_depth_clamp {
            let trace_args = (&device_guard, command_buffer, self.depth_clamp);

            trace_calling(Self::format_cmd_set_depth_clamp_enable, trace_args);

            {
                debug_span!("cmd_set_depth_clamp_enable");

                unsafe {
                    device.cmd_set_depth_clamp_enable(command_buffer, self.depth_clamp);
                }
            }

            trace_called(
                Self::format_cmd_set_depth_clamp_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        {
            let trace_args = (&device_guard, command_buffer, self.front_face);

            trace_calling(Self::format_cmd_set_fron_face, trace_args);

            {
                debug_span!("cmd_set_front_face");

                unsafe {
                    device_guard.cmd_set_front_face(command_buffer, self.front_face);
                }
            }

            trace_called(
                Self::format_cmd_set_fron_face,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (&device_guard, command_buffer, self.line_width);

            trace_calling(Self::format_cmd_set_line_width, trace_args);

            {
                debug_span!("cmd_set_line_width");

                unsafe {
                    device_guard.cmd_set_line_width(command_buffer, self.line_width);
                }
            }

            trace_called(
                Self::format_cmd_set_line_width,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        if self.dynamic_polygon_mode {
            let trace_args = (&device_guard, command_buffer, self.polygon_mode);

            trace_calling(Self::format_cmd_set_polygon_mode, trace_args);

            {
                debug_span!("cmd_set_polygon_mode");

                unsafe {
                    device.cmd_set_polygon_mode(command_buffer, self.polygon_mode);
                }
            }

            trace_called(
                Self::format_cmd_set_polygon_mode,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        {
            let trace_args = (&device_guard, command_buffer, self.rasterizer_discard);

            trace_calling(Self::format_cmd_set_rasterizer_discard_enable, trace_args);

            {
                debug_span!("cmd_set_rasterizer_discard_enable");

                unsafe {
                    device_guard
                        .cmd_set_rasterizer_discard_enable(command_buffer, self.rasterizer_discard);
                }
            }

            trace_called(
                Self::format_cmd_set_rasterizer_discard_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
    }
}

impl Eq for Rasterization {}

impl PartialEq for Rasterization {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.create_info == other.create_info,
            self.cull_mode == other.cull_mode,
            self.depth_bias == other.depth_bias,
            self.depth_clamp == other.depth_clamp,
            self.front_face == other.front_face,
            self.line_width == other.line_width,
            self.dynamic_depth_clamp == other.dynamic_depth_clamp,
            self.depth_clamp == other.depth_clamp,
            self.front_face == other.front_face,
            float_eq!(self.line_width, other.line_width, r2nd <= f32::EPSILON),
            self.dynamic_polygon_mode == other.dynamic_polygon_mode,
            self.polygon_mode == other.polygon_mode,
            self.rasterizer_discard == other.rasterizer_discard,
        ];

        values.iter().all(|value| *value)
    }
}

unsafe impl Send for Rasterization {}
unsafe impl Sync for Rasterization {}
