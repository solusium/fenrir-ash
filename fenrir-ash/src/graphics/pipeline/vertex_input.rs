use {
    crate::{
        command::buffer::Recorder,
        device::{Device, Guard as DeviceGuard, Properties},
        trace_called, trace_calling,
    },
    ash::{
        ext::vertex_input_dynamic_state::Device as VertexInputDynamicStateDevice,
        prelude::VkResult,
        vk::{
            CommandBuffer, Format as AshFormat, PhysicalDeviceLimits,
            PhysicalDeviceVertexInputDynamicStateFeaturesEXT, PipelineVertexInputStateCreateFlags,
            PipelineVertexInputStateCreateInfo, VertexInputAttributeDescription,
            VertexInputAttributeDescription2EXT, VertexInputBindingDescription,
            VertexInputBindingDescription2EXT, VertexInputRate, TRUE,
        },
    },
    core::ops::Range,
    fenrir_error::{argument_out_of_domain, Error},
    fenrir_hal::{
        format::Format as HalFormat,
        graphics::pipeline::Metadata,
        vertex::{
            input::{Attribute as HalAttribute, Binding as HalBinding, Rate},
            Input,
        },
    },
    smallvec::SmallVec,
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub(super) struct Attribute {
    location: u32,
    binding: u32,
    format: AshFormat,
    offset: u32,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(super) struct Binding {
    index: u32,
    stride: u32,
    input_rate: VertexInputRate,
    divisor: u32,
}

pub(super) struct CreateInfo {
    flags: PipelineVertexInputStateCreateFlags,
    attribute_descriptions: SmallVec<[VertexInputAttributeDescription; 3]>,
    binding_descriptions: SmallVec<[VertexInputBindingDescription; 3]>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct Inner {
    flags: PipelineVertexInputStateCreateFlags,
    attribute_descriptions: SmallVec<[Attribute; 3]>,
    binding_descriptions: SmallVec<[Binding; 3]>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct VertexInput {
    create_info: Inner,
    dynamic_vertex_input: bool,
}

impl Attribute {
    fn ash(&self) -> VertexInputAttributeDescription2EXT {
        VertexInputAttributeDescription2EXT::default()
            .binding(self.binding)
            .format(self.format)
            .location(self.location)
            .offset(self.offset)
    }
}

impl Binding {
    fn ash(&self) -> VertexInputBindingDescription2EXT {
        VertexInputBindingDescription2EXT::default()
            .binding(self.index)
            .input_rate(self.input_rate)
            .stride(self.stride)
            .divisor(self.divisor)
    }
}

impl CreateInfo {
    #[inline]
    pub(crate) fn ash(&self) -> PipelineVertexInputStateCreateInfo {
        PipelineVertexInputStateCreateInfo::default()
            .flags(self.flags)
            .vertex_attribute_descriptions(&self.attribute_descriptions)
            .vertex_binding_descriptions(&self.binding_descriptions)
    }
}

impl VertexInput {
    #[inline]
    pub(super) fn new(
        device: &Device,
        metadata: &Metadata,
        available_vertex_input_dynamic_state_features: &PhysicalDeviceVertexInputDynamicStateFeaturesEXT<'_>,
    ) -> Result<Self, Error> {
        let limits = device.limits();

        let properties = device.properties();

        let binding_descriptions =
            Self::construct_vertex_input_binding_descriptions(metadata, limits, properties)?;

        let attribute_descriptions =
            Self::construct_vertex_input_attribute_descriptions(metadata, limits)?;

        let flags = PipelineVertexInputStateCreateFlags::default();

        let create_info = Inner {
            flags,
            attribute_descriptions,
            binding_descriptions,
        };

        let dynamic_vertex_input =
            TRUE == available_vertex_input_dynamic_state_features.vertex_input_dynamic_state;

        Ok(Self {
            create_info,
            dynamic_vertex_input,
        })
    }

    #[inline]
    fn construct_vertex_input_attribute_descriptions(
        metadata: &Metadata,
        limits: &PhysicalDeviceLimits,
    ) -> Result<SmallVec<[Attribute; 3]>, Error> {
        metadata
            .vertex_input()
            .attributes()
            .iter()
            .map(|attribute| {
                let max_vertex_input_bindings_reached_error = || {
                    argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: limits.max_vertex_input_bindings.into(),
                        },
                        attribute.binding().into(),
                    )
                };

                match attribute.binding().try_into() {
                    Err(_) => Err(max_vertex_input_bindings_reached_error()),
                    Ok(binding) => {
                        if binding >= limits.max_vertex_input_bindings {
                            return Err(max_vertex_input_bindings_reached_error());
                        }

                        let format = match attribute.format() {
                            HalFormat::X32Sfloat => AshFormat::R32_SFLOAT,
                            HalFormat::X32Y32Sfloat => AshFormat::R32G32_SFLOAT,
                            HalFormat::X32Y32Z32Sfloat => AshFormat::R32G32B32_SFLOAT,
                            HalFormat::X32Y32Z32W32Sfloat => AshFormat::R32G32B32A32_SFLOAT,
                        };

                        let max_vertex_input_attributes_reached_error = || {
                            argument_out_of_domain(
                                Range {
                                    start: 0.into(),
                                    end: limits.max_vertex_input_attributes.into(),
                                },
                                attribute.location().into(),
                            )
                        };

                        match attribute.location().try_into() {
                            Err(_) => Err(max_vertex_input_attributes_reached_error()),
                            Ok(location) => {
                                if location >= limits.max_vertex_input_attributes {
                                    return Err(max_vertex_input_attributes_reached_error());
                                }

                                let max_vertex_input_attribute_offset_reached_error = || {
                                    argument_out_of_domain(
                                        Range {
                                            start: 0.into(),
                                            end: (limits.max_vertex_input_attribute_offset + 1)
                                                .into(),
                                        },
                                        attribute.offset().into(),
                                    )
                                };

                                match attribute.offset().try_into() {
                                    Err(_) => {
                                        Err(max_vertex_input_attribute_offset_reached_error())
                                    }
                                    Ok(offset) => {
                                        if offset > limits.max_vertex_input_attribute_offset {
                                            return Err(
                                                max_vertex_input_attribute_offset_reached_error(),
                                            );
                                        }

                                        Ok(Attribute {
                                            location,
                                            binding,
                                            format,
                                            offset,
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
            })
            .collect()
    }

    #[inline]
    fn construct_vertex_input_binding_descriptions(
        metadata: &Metadata,
        limits: &PhysicalDeviceLimits,
        properties: &Properties,
    ) -> Result<SmallVec<[Binding; 3]>, Error> {
        metadata
            .vertex_input()
            .bindings()
            .iter()
            .map(|binding| {
                let max_vertex_input_bindings_reached_error = || {
                    argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: limits.max_vertex_input_bindings.into(),
                        },
                        binding.index().into(),
                    )
                };

                let index = binding
                    .index()
                    .try_into()
                    .map_err(|_| max_vertex_input_bindings_reached_error())?;

                if index >= limits.max_vertex_input_bindings {
                    return Err(max_vertex_input_bindings_reached_error());
                }

                let input_rate = match binding.rate() {
                    Rate::Vertex => VertexInputRate::VERTEX,
                    Rate::Instance => VertexInputRate::INSTANCE,
                };

                let max_vertex_input_stide_reached_error = || {
                    argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: (limits.max_vertex_input_binding_stride + 1).into(),
                        },
                        binding.stride().into(),
                    )
                };

                let stride = binding
                    .stride()
                    .try_into()
                    .map_err(|_| max_vertex_input_stide_reached_error())?;

                if stride > limits.max_vertex_input_binding_stride {
                    return Err(max_vertex_input_stide_reached_error());
                }

                let max_vertex_input_instance_rate_divisor_error = || {
                    argument_out_of_domain(
                        Range {
                            start: 0.into(),
                            end: properties
                                .vertex_attribute_divisor()
                                .max_vertex_attrib_divisor
                                .into(),
                        },
                        binding.divisor().into(),
                    )
                };

                let divisor = binding
                    .divisor()
                    .try_into()
                    .map_err(|_| max_vertex_input_stide_reached_error())?;

                if divisor
                    > properties
                        .vertex_attribute_divisor()
                        .max_vertex_attrib_divisor
                {
                    return Err(max_vertex_input_instance_rate_divisor_error());
                }

                Ok(Binding {
                    index,
                    stride,
                    input_rate,
                    divisor,
                })
            })
            .collect()
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<CreateInfo> {
        if self.dynamic_vertex_input {
            None
        } else {
            Some(CreateInfo {
                flags: self.create_info.flags,
                attribute_descriptions: self
                    .create_info
                    .attribute_descriptions
                    .iter()
                    .map(|attribute| attribute.clone().into())
                    .collect(),
                binding_descriptions: self
                    .create_info
                    .binding_descriptions
                    .iter()
                    .map(|binding| binding.clone().into())
                    .collect(),
            })
        }
    }

    #[inline]
    pub(super) fn dynamic_vertex_input(&self) -> Option<(&[Attribute], &[Binding])> {
        if self.dynamic_vertex_input {
            Some((
                &self.create_info.attribute_descriptions,
                &self.create_info.binding_descriptions,
            ))
        } else {
            None
        }
    }

    #[cold]
    #[inline]
    fn format_set_vertex_input_state(
        args: (
            &DeviceGuard,
            CommandBuffer,
            &[VertexInputBindingDescription2EXT],
            &[VertexInputAttributeDescription2EXT],
        ),
    ) -> String {
        let (device, command_buffer, vertex_binding_descriptions, vertex_attribute_descriptions) =
            args;

        format!(
            "cmd_set_vertex_input(self: {device:?}, command_buffer: {command_buffer:?}, \
            vertex_binding_descriptions: {vertex_binding_descriptions:?}, vertex_attribute_descriptions: \
            {vertex_attribute_descriptions:?})",
        )
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        if !self.dynamic_vertex_input {
            return;
        }

        let device_guard = recorder.buffer().device();

        let instance = device_guard.instance();

        let device = VertexInputDynamicStateDevice::new(&instance, &device_guard);

        let command_buffer = recorder.buffer().ash();

        let vertex_binding_descriptions = self
            .create_info
            .binding_descriptions
            .iter()
            .map(|binding| binding.ash())
            .collect::<SmallVec<[_; 3]>>();

        let vertex_attribute_descriptions = self
            .create_info
            .attribute_descriptions
            .iter()
            .map(|attribute| attribute.ash())
            .collect::<SmallVec<[_; 3]>>();

        let trace_args = (
            &*device_guard,
            command_buffer,
            vertex_binding_descriptions.as_slice(),
            vertex_attribute_descriptions.as_slice(),
        );

        trace_calling(Self::format_set_vertex_input_state, trace_args);

        {
            let _span = debug_span!("cmd_set_vertex_input");

            unsafe {
                device.cmd_set_vertex_input(
                    command_buffer,
                    &vertex_binding_descriptions,
                    &vertex_attribute_descriptions,
                );
            }
        }

        trace_called(
            Self::format_set_vertex_input_state,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

#[allow(clippy::from_over_into)]
impl Into<VertexInputAttributeDescription> for Attribute {
    #[inline]
    fn into(self) -> VertexInputAttributeDescription {
        VertexInputAttributeDescription::default()
            .binding(self.binding)
            .format(self.format)
            .location(self.location)
            .offset(self.offset)
    }
}

#[allow(clippy::from_over_into)]
impl Into<VertexInputBindingDescription> for Binding {
    #[inline]
    fn into(self) -> VertexInputBindingDescription {
        VertexInputBindingDescription::default()
            .binding(self.index)
            .input_rate(self.input_rate)
            .stride(self.stride)
    }
}

#[allow(clippy::from_over_into)]
impl Into<Input> for VertexInput {
    #[inline]
    fn into(self) -> Input {
        let attributes = self
            .create_info
            .attribute_descriptions
            .iter()
            .map(|attribute| {
                let format = if AshFormat::R32_SFLOAT == attribute.format {
                    HalFormat::X32Sfloat
                } else if AshFormat::R32G32_SFLOAT == attribute.format {
                    HalFormat::X32Y32Sfloat
                } else if AshFormat::R32G32B32_SFLOAT == attribute.format {
                    HalFormat::X32Y32Z32Sfloat
                } else {
                    HalFormat::X32Y32Z32W32Sfloat
                };

                HalAttribute::new(
                    attribute.location as usize,
                    attribute.binding as usize,
                    format,
                    attribute.offset as usize,
                )
            })
            .collect::<SmallVec<[_; 3]>>();

        let bindings = self
            .create_info
            .binding_descriptions
            .iter()
            .map(|binding| {
                HalBinding::new(
                    binding.index as usize,
                    binding.stride as usize,
                    if VertexInputRate::VERTEX == binding.input_rate {
                        Rate::Vertex
                    } else {
                        Rate::Instance
                    },
                    binding.divisor as usize,
                )
            })
            .collect::<SmallVec<[_; 3]>>();

        Input::new(&attributes, &bindings)
    }
}

unsafe impl Send for VertexInput {}
unsafe impl Sync for VertexInput {}
