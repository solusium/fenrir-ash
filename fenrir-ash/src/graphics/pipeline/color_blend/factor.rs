use {ash::vk::BlendFactor, fenrir_hal::color::blend::Factor};

#[inline]
pub(super) fn ash_to_hal(factor: BlendFactor) -> Factor {
    if BlendFactor::ZERO == factor {
        Factor::Zero
    } else if BlendFactor::ONE == factor {
        Factor::One
    } else if BlendFactor::SRC_COLOR == factor {
        Factor::SourceColor
    } else if BlendFactor::ONE_MINUS_SRC_COLOR == factor {
        Factor::OneMinusSourceColor
    } else if BlendFactor::DST_COLOR == factor {
        Factor::DestinationColor
    } else if BlendFactor::ONE_MINUS_DST_COLOR == factor {
        Factor::OneMinusDestinationColor
    } else if BlendFactor::SRC_ALPHA == factor {
        Factor::SourceAlpha
    } else if BlendFactor::ONE_MINUS_SRC_ALPHA == factor {
        Factor::OneMinusSourceAlpha
    } else if BlendFactor::DST_ALPHA == factor {
        Factor::DestinationAlpha
    } else if BlendFactor::ONE_MINUS_DST_ALPHA == factor {
        Factor::OneMinusDestinationAlpha
    } else if BlendFactor::CONSTANT_COLOR == factor {
        Factor::ConstantColor
    } else if BlendFactor::ONE_MINUS_CONSTANT_COLOR == factor {
        Factor::OneMinusConstantColor
    } else if BlendFactor::CONSTANT_ALPHA == factor {
        Factor::ConstantAlpha
    } else if BlendFactor::ONE_MINUS_CONSTANT_ALPHA == factor {
        Factor::OneMinusConstantAlpha
    } else if BlendFactor::SRC_ALPHA_SATURATE == factor {
        Factor::SourceAlphaSaturate
    } else if BlendFactor::SRC1_COLOR == factor {
        Factor::Source1Color
    } else if BlendFactor::ONE_MINUS_SRC1_COLOR == factor {
        Factor::OneMinusSource1Color
    } else if BlendFactor::SRC1_ALPHA == factor {
        Factor::Source1Alpha
    } else {
        Factor::OneMinusSource1Alpha
    }
}

#[inline]
pub(super) const fn hal_to_ash(factor: Factor) -> BlendFactor {
    match factor {
        Factor::Zero => BlendFactor::ZERO,
        Factor::One => BlendFactor::ONE,
        Factor::SourceColor => BlendFactor::SRC_COLOR,
        Factor::OneMinusSourceColor => BlendFactor::ONE_MINUS_SRC_COLOR,
        Factor::DestinationColor => BlendFactor::DST_COLOR,
        Factor::OneMinusDestinationColor => BlendFactor::ONE_MINUS_DST_COLOR,
        Factor::SourceAlpha => BlendFactor::SRC_ALPHA,
        Factor::OneMinusSourceAlpha => BlendFactor::ONE_MINUS_SRC_ALPHA,
        Factor::DestinationAlpha => BlendFactor::DST_ALPHA,
        Factor::OneMinusDestinationAlpha => BlendFactor::ONE_MINUS_DST_ALPHA,
        Factor::ConstantColor => BlendFactor::CONSTANT_COLOR,
        Factor::OneMinusConstantColor => BlendFactor::ONE_MINUS_CONSTANT_COLOR,
        Factor::ConstantAlpha => BlendFactor::CONSTANT_ALPHA,
        Factor::OneMinusConstantAlpha => BlendFactor::ONE_MINUS_CONSTANT_ALPHA,
        Factor::SourceAlphaSaturate => BlendFactor::SRC_ALPHA_SATURATE,
        Factor::Source1Color => BlendFactor::SRC1_COLOR,
        Factor::OneMinusSource1Color => BlendFactor::ONE_MINUS_SRC1_COLOR,
        Factor::Source1Alpha => BlendFactor::SRC1_ALPHA,
        Factor::OneMinusSource1Alpha => BlendFactor::ONE_MINUS_SRC1_ALPHA,
    }
}
