use {
    crate::{trace_called, trace_calling},
    abi_stable::std_types::RArc,
    alloc::sync::Arc,
    ash::{
        ext::extended_dynamic_state3::Device as DynamicState3Device,
        prelude::VkResult,
        vk::{
            Bool32, ColorBlendEquationEXT, ColorComponentFlags, CommandBuffer,
            PipelineColorBlendAttachmentState, TRUE,
        },
    },
    fenrir_hal::color::Component,
    smallvec::SmallVec,
    tracing::debug_span,
};

extern crate alloc;

#[derive(Clone, Debug)]
pub(crate) enum Attachments {
    States {
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicEnables {
        enables: SmallVec<[Bool32; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicEquations {
        equations: SmallVec<[ColorBlendEquationEXT; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicWriteMasks {
        write_masks: SmallVec<[ColorComponentFlags; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicEnablesAndEquations {
        enables: SmallVec<[Bool32; 3]>,
        equations: SmallVec<[ColorBlendEquationEXT; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicEnablesAndWriteMasks {
        enables: SmallVec<[Bool32; 3]>,
        write_masks: SmallVec<[ColorComponentFlags; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicEquationsAndWriteMasks {
        equations: SmallVec<[ColorBlendEquationEXT; 3]>,
        write_masks: SmallVec<[ColorComponentFlags; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    DynamicEnablesAndEquationsAndWriteMasks {
        enables: SmallVec<[Bool32; 3]>,
        equations: SmallVec<[ColorBlendEquationEXT; 3]>,
        write_masks: SmallVec<[ColorComponentFlags; 3]>,
        states: SmallVec<[PipelineColorBlendAttachmentState; 3]>,
    },
    Dynamics {
        enables: SmallVec<[Bool32; 3]>,
        equations: SmallVec<[ColorBlendEquationEXT; 3]>,
        write_masks: SmallVec<[ColorComponentFlags; 3]>,
    },
}

pub(crate) struct State<'a> {
    attachments: &'a Attachments,
}

impl Attachments {
    #[inline]
    fn compare_equations(this: &[ColorBlendEquationEXT], other: &[ColorBlendEquationEXT]) -> bool {
        this.iter().zip(other).all(|(this, other)| {
            let values = [
                this.src_color_blend_factor == other.src_color_blend_factor,
                this.dst_color_blend_factor == other.dst_color_blend_factor,
                this.color_blend_op == other.color_blend_op,
                this.src_alpha_blend_factor == other.src_alpha_blend_factor,
                this.dst_alpha_blend_factor == other.dst_alpha_blend_factor,
                this.alpha_blend_op == other.alpha_blend_op,
            ];

            values.iter().all(|value| *value)
        })
    }

    #[inline]
    fn compare_states(
        this: &[PipelineColorBlendAttachmentState],
        other: &[PipelineColorBlendAttachmentState],
    ) -> bool {
        this.iter().zip(other).all(|(this, other)| {
            let values = [
                this.blend_enable == other.blend_enable,
                this.src_color_blend_factor == other.src_color_blend_factor,
                this.dst_color_blend_factor == other.dst_color_blend_factor,
                this.color_blend_op == other.color_blend_op,
                this.src_alpha_blend_factor == other.src_alpha_blend_factor,
                this.dst_alpha_blend_factor == other.dst_alpha_blend_factor,
                this.alpha_blend_op == other.alpha_blend_op,
                this.color_write_mask == other.color_write_mask,
            ];

            values.iter().all(|value| *value)
        })
    }

    #[inline]
    #[allow(dead_code)]
    #[allow(clippy::too_many_lines)]
    pub(super) fn hal(&self) -> Vec<fenrir_hal::color::blend::State> {
        match self {
            Self::States { states } => states
                .iter()
                .map(|state| {
                    Self::fill_builder_from_state(fenrir_hal::color::blend::State::builder(), state)
                        .build()
                })
                .collect(),
            Self::DynamicEnables { enables, states } => enables
                .iter()
                .zip(states.iter())
                .map(|(enable, state)| {
                    Self::fill_builder_from_enable(
                        Self::fill_builder_from_state(
                            fenrir_hal::color::blend::State::builder(),
                            state,
                        ),
                        *enable,
                    )
                    .build()
                })
                .collect(),
            Self::DynamicEquations { equations, states } => equations
                .iter()
                .zip(states.iter())
                .map(|(equation, state)| {
                    Self::fill_builder_from_equation(
                        Self::fill_builder_from_state(
                            fenrir_hal::color::blend::State::builder(),
                            state,
                        ),
                        equation,
                    )
                    .build()
                })
                .collect(),
            Self::DynamicWriteMasks {
                write_masks,
                states,
            } => write_masks
                .iter()
                .zip(states.iter())
                .map(|(mask, state)| {
                    Self::fill_builder_from_write_mask(
                        Self::fill_builder_from_state(
                            fenrir_hal::color::blend::State::builder(),
                            state,
                        ),
                        *mask,
                    )
                    .build()
                })
                .collect(),
            Self::DynamicEnablesAndEquations {
                enables,
                equations,
                states,
            } => enables
                .iter()
                .zip(equations.iter())
                .zip(states.iter())
                .map(|((enable, equation), state)| {
                    Self::fill_builder_from_enable(
                        Self::fill_builder_from_equation(
                            Self::fill_builder_from_state(
                                fenrir_hal::color::blend::State::builder(),
                                state,
                            ),
                            equation,
                        ),
                        *enable,
                    )
                    .build()
                })
                .collect(),
            Self::DynamicEnablesAndWriteMasks {
                enables,
                write_masks,
                states,
            } => enables
                .iter()
                .zip(write_masks.iter())
                .zip(states.iter())
                .map(|((enable, mask), state)| {
                    Self::fill_builder_from_enable(
                        Self::fill_builder_from_write_mask(
                            Self::fill_builder_from_state(
                                fenrir_hal::color::blend::State::builder(),
                                state,
                            ),
                            *mask,
                        ),
                        *enable,
                    )
                    .build()
                })
                .collect(),
            Self::DynamicEquationsAndWriteMasks {
                equations,
                write_masks,
                states,
            } => equations
                .iter()
                .zip(write_masks.iter())
                .zip(states.iter())
                .map(|((equation, mask), state)| {
                    Self::fill_builder_from_equation(
                        Self::fill_builder_from_write_mask(
                            Self::fill_builder_from_state(
                                fenrir_hal::color::blend::State::builder(),
                                state,
                            ),
                            *mask,
                        ),
                        equation,
                    )
                    .build()
                })
                .collect(),
            Self::DynamicEnablesAndEquationsAndWriteMasks {
                enables,
                equations,
                write_masks,
                states: _,
            }
            | Self::Dynamics {
                enables,
                equations,
                write_masks,
            } => enables
                .iter()
                .zip(equations.iter())
                .zip(write_masks.iter())
                .map(|((enable, equation), mask)| {
                    Self::fill_builder_from_enable(
                        Self::fill_builder_from_equation(
                            Self::fill_builder_from_write_mask(
                                fenrir_hal::color::blend::State::builder(),
                                *mask,
                            ),
                            equation,
                        ),
                        *enable,
                    )
                    .build()
                })
                .collect(),
        }
    }

    #[inline]
    #[allow(dead_code)]
    const fn fill_builder_from_enable(
        builder: fenrir_hal::color::blend::state::Builder<'_>,
        enable: Bool32,
    ) -> fenrir_hal::color::blend::state::Builder<'_> {
        builder.enabled(TRUE == enable)
    }

    #[inline]
    #[allow(dead_code)]
    fn fill_builder_from_equation<'a>(
        builder: fenrir_hal::color::blend::state::Builder<'a>,
        equation: &ColorBlendEquationEXT,
    ) -> fenrir_hal::color::blend::state::Builder<'a> {
        builder
            .alpha_blend_operation(
                crate::graphics::pipeline::color_blend::operation::ash_to_hal(
                    equation.alpha_blend_op,
                ),
            )
            .color_blend_operation(
                crate::graphics::pipeline::color_blend::operation::ash_to_hal(
                    equation.color_blend_op,
                ),
            )
            .destination_alpha_blend_factor(
                crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                    equation.dst_alpha_blend_factor,
                ),
            )
            .destination_alpha_blend_factor(
                crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                    equation.dst_color_blend_factor,
                ),
            )
            .source_alpha_blend_factor(crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                equation.src_alpha_blend_factor,
            ))
            .source_color_blend_factor(crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                equation.src_color_blend_factor,
            ))
    }

    #[inline]
    #[allow(dead_code)]
    fn fill_builder_from_state<'a>(
        builder: fenrir_hal::color::blend::state::Builder<'a>,
        state: &PipelineColorBlendAttachmentState,
    ) -> fenrir_hal::color::blend::state::Builder<'a> {
        let ash_flags = [
            ColorComponentFlags::R,
            ColorComponentFlags::G,
            ColorComponentFlags::B,
            ColorComponentFlags::A,
        ];

        let hal_flags = ash_flags
            .into_iter()
            .filter(|flag| state.color_write_mask.contains(*flag))
            .map(|flag| {
                if ColorComponentFlags::R == flag {
                    Component::R
                } else if ColorComponentFlags::G == flag {
                    Component::G
                } else if ColorComponentFlags::B == flag {
                    Component::B
                } else {
                    Component::A
                }
            })
            .collect();

        builder
            .enabled(TRUE == state.blend_enable)
            .alpha_blend_operation(
                crate::graphics::pipeline::color_blend::operation::ash_to_hal(state.alpha_blend_op),
            )
            .color_blend_operation(
                crate::graphics::pipeline::color_blend::operation::ash_to_hal(state.color_blend_op),
            )
            .destination_alpha_blend_factor(
                crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                    state.dst_alpha_blend_factor,
                ),
            )
            .destination_alpha_blend_factor(
                crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                    state.dst_color_blend_factor,
                ),
            )
            .source_alpha_blend_factor(crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                state.src_alpha_blend_factor,
            ))
            .source_color_blend_factor(crate::graphics::pipeline::color_blend::factor::ash_to_hal(
                state.src_color_blend_factor,
            ))
            .color_write_mask(hal_flags)
    }

    #[inline]
    #[allow(dead_code)]
    fn fill_builder_from_write_mask(
        builder: fenrir_hal::color::blend::state::Builder<'_>,
        write_mask: ColorComponentFlags,
    ) -> fenrir_hal::color::blend::state::Builder<'_> {
        let ash_flags = [
            ColorComponentFlags::R,
            ColorComponentFlags::G,
            ColorComponentFlags::B,
            ColorComponentFlags::A,
        ];

        let hal_flags = ash_flags
            .into_iter()
            .filter(|flag| write_mask.contains(*flag))
            .map(|flag| {
                if ColorComponentFlags::R == flag {
                    Component::R
                } else if ColorComponentFlags::G == flag {
                    Component::G
                } else if ColorComponentFlags::B == flag {
                    Component::B
                } else {
                    Component::A
                }
            })
            .collect();

        builder.color_write_mask(hal_flags)
    }

    #[cold]
    #[inline]
    fn format_cmd_set_color_blend_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, u32, &[Bool32]),
    ) -> String {
        let (device, command_buffer, first_attachment, color_blend_enables) = args;

        format!(
            "cmd_set_color_blend_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            first_attachment: {first_attachment}, color_blend_enables: {color_blend_enables:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_color_blend_equation(
        args: (
            &RArc<crate::device::Guard>,
            CommandBuffer,
            u32,
            &[ColorBlendEquationEXT],
        ),
    ) -> String {
        let (device, command_buffer, first_attachment, color_blend_equations) = args;

        format!(
            "cmd_set_color_blend_equation(&self: {device:?}, command_buffer: {command_buffer:?}, \
            first_attachment: {first_attachment}, color_blend_equations: {color_blend_equations:?})"
        )
    }

    #[cold]
    #[inline]
    fn format_cmd_set_color_write_mask(
        args: (
            &RArc<crate::device::Guard>,
            CommandBuffer,
            u32,
            &[ColorComponentFlags],
        ),
    ) -> String {
        let (device, command_buffer, first_attachment, color_write_masks) = args;

        format!(
            "cmd_set_color_write_mask(&self: {device:?}, command_buffer: {command_buffer:?}, \
            first_attachment: {first_attachment}, color_write_masks: {color_write_masks:?})"
        )
    }

    #[inline]
    fn maybe_set_dynamic_enables(
        &self,
        instance: &Arc<crate::instance::Guard>,
        device_guard: &RArc<crate::device::Guard>,
        command_buffer: CommandBuffer,
    ) {
        match self {
            Self::DynamicEnables { enables, states: _ }
            | Self::DynamicEnablesAndEquations {
                enables,
                equations: _,
                states: _,
            }
            | Self::DynamicEnablesAndWriteMasks {
                enables,
                write_masks: _,
                states: _,
            }
            | Self::DynamicEnablesAndEquationsAndWriteMasks {
                enables,
                equations: _,
                write_masks: _,
                states: _,
            }
            | Self::Dynamics {
                enables,
                equations: _,
                write_masks: _,
            } => {
                let device = DynamicState3Device::new(instance, device_guard);

                let first_attachment = 0u32;

                let color_blend_enables = enables.as_slice();

                let trace_args = (
                    device_guard,
                    command_buffer,
                    first_attachment,
                    color_blend_enables,
                );

                trace_calling(Self::format_cmd_set_color_blend_enable, trace_args);

                {
                    debug_span!("cmd_set_color_blend_enable");

                    unsafe {
                        device.cmd_set_color_blend_enable(
                            command_buffer,
                            first_attachment,
                            color_blend_enables,
                        );
                    }
                }

                trace_called(
                    Self::format_cmd_set_color_blend_enable,
                    trace_args,
                    None::<&VkResult<()>>,
                );
            }
            _ => {}
        }
    }

    #[inline]
    fn maybe_set_dynamic_equations(
        &self,
        instance: &Arc<crate::instance::Guard>,
        device_guard: &RArc<crate::device::Guard>,
        command_buffer: CommandBuffer,
    ) {
        match self {
            Self::DynamicEquations {
                equations,
                states: _,
            }
            | Self::DynamicEnablesAndEquations {
                enables: _,
                equations,
                states: _,
            }
            | Self::DynamicEquationsAndWriteMasks {
                equations,
                write_masks: _,
                states: _,
            }
            | Self::DynamicEnablesAndEquationsAndWriteMasks {
                enables: _,
                equations,
                write_masks: _,
                states: _,
            }
            | Self::Dynamics {
                enables: _,
                equations,
                write_masks: _,
            } => {
                let device = DynamicState3Device::new(instance, device_guard);

                let first_attachment = 0u32;

                let color_blend_equations = equations.as_slice();

                let trace_args = (
                    device_guard,
                    command_buffer,
                    first_attachment,
                    color_blend_equations,
                );

                trace_calling(Self::format_cmd_set_color_blend_equation, trace_args);

                {
                    debug_span!("cmd_set_color_blend_equation");

                    unsafe {
                        device.cmd_set_color_blend_equation(
                            command_buffer,
                            first_attachment,
                            color_blend_equations,
                        );
                    }
                }

                trace_called(
                    Self::format_cmd_set_color_blend_equation,
                    trace_args,
                    None::<&VkResult<()>>,
                );
            }
            _ => {}
        }
    }

    #[inline]
    fn maybe_set_dynamic_write_masks(
        &self,
        instance: &Arc<crate::instance::Guard>,
        device_guard: &RArc<crate::device::Guard>,
        command_buffer: CommandBuffer,
    ) {
        match self {
            Self::DynamicWriteMasks {
                write_masks,
                states: _,
            }
            | Self::DynamicEnablesAndWriteMasks {
                enables: _,
                write_masks,
                states: _,
            }
            | Self::DynamicEquationsAndWriteMasks {
                equations: _,
                write_masks,
                states: _,
            }
            | Self::DynamicEnablesAndEquationsAndWriteMasks {
                enables: _,
                equations: _,
                write_masks,
                states: _,
            }
            | Self::Dynamics {
                enables: _,
                equations: _,
                write_masks,
            } => {
                let device = DynamicState3Device::new(instance, device_guard);

                let first_attachment = 0u32;

                let write_masks = write_masks.as_slice();

                let trace_args = (device_guard, command_buffer, first_attachment, write_masks);

                trace_calling(Self::format_cmd_set_color_write_mask, trace_args);

                {
                    debug_span!("cmd_set_color_write_mask");

                    unsafe {
                        device.cmd_set_color_write_mask(
                            command_buffer,
                            first_attachment,
                            write_masks,
                        );
                    }
                }

                trace_called(
                    Self::format_cmd_set_color_write_mask,
                    trace_args,
                    None::<&VkResult<()>>,
                );
            }
            _ => {}
        }
    }

    #[inline]
    pub(super) fn set_dynamics(
        &self,
        instance: &Arc<crate::instance::Guard>,
        device_guard: &RArc<crate::device::Guard>,
        command_buffer: CommandBuffer,
    ) {
        self.maybe_set_dynamic_enables(instance, device_guard, command_buffer);

        self.maybe_set_dynamic_equations(instance, device_guard, command_buffer);

        self.maybe_set_dynamic_write_masks(instance, device_guard, command_buffer);
    }
}

impl<'a> State<'a> {
    #[inline]
    pub(super) const fn new(attachments: &'a Attachments) -> Self {
        Self { attachments }
    }

    #[inline]
    pub(crate) fn dynamic_enables(&self) -> Option<SmallVec<[Bool32; 3]>> {
        match self.attachments {
            Attachments::States { states: _ }
            | Attachments::DynamicEquations {
                equations: _,
                states: _,
            }
            | Attachments::DynamicWriteMasks {
                write_masks: _,
                states: _,
            }
            | Attachments::DynamicEquationsAndWriteMasks {
                equations: _,
                write_masks: _,
                states: _,
            } => None,
            Attachments::DynamicEnables { enables, states: _ }
            | Attachments::DynamicEnablesAndEquations {
                enables,
                equations: _,
                states: _,
            }
            | Attachments::DynamicEnablesAndWriteMasks {
                enables,
                write_masks: _,
                states: _,
            }
            | Attachments::DynamicEnablesAndEquationsAndWriteMasks {
                enables,
                equations: _,
                write_masks: _,
                states: _,
            }
            | Attachments::Dynamics {
                enables,
                equations: _,
                write_masks: _,
            } => Some(enables.clone()),
        }
    }

    #[inline]
    pub(crate) fn dynamic_equations(&self) -> Option<SmallVec<[ColorBlendEquationEXT; 3]>> {
        match self.attachments {
            Attachments::States { states: _ }
            | Attachments::DynamicEnables {
                enables: _,
                states: _,
            }
            | Attachments::DynamicWriteMasks {
                write_masks: _,
                states: _,
            }
            | Attachments::DynamicEnablesAndWriteMasks {
                enables: _,
                write_masks: _,
                states: _,
            } => None,
            Attachments::DynamicEquations {
                equations,
                states: _,
            }
            | Attachments::DynamicEnablesAndEquations {
                enables: _,
                equations,
                states: _,
            }
            | Attachments::DynamicEquationsAndWriteMasks {
                equations,
                write_masks: _,
                states: _,
            }
            | Attachments::DynamicEnablesAndEquationsAndWriteMasks {
                enables: _,
                equations,
                write_masks: _,
                states: _,
            }
            | Attachments::Dynamics {
                enables: _,
                equations,
                write_masks: _,
            } => Some(equations.clone()),
        }
    }

    #[inline]
    pub(crate) fn dynamic_write_masks(&self) -> Option<SmallVec<[ColorComponentFlags; 3]>> {
        match self.attachments {
            Attachments::States { states: _ }
            | Attachments::DynamicEnables {
                enables: _,
                states: _,
            }
            | Attachments::DynamicEquations {
                equations: _,
                states: _,
            }
            | Attachments::DynamicEnablesAndEquations {
                enables: _,
                equations: _,
                states: _,
            } => None,
            Attachments::DynamicWriteMasks {
                write_masks,
                states: _,
            }
            | Attachments::DynamicEnablesAndWriteMasks {
                enables: _,
                write_masks,
                states: _,
            }
            | Attachments::DynamicEquationsAndWriteMasks {
                equations: _,
                write_masks,
                states: _,
            }
            | Attachments::DynamicEnablesAndEquationsAndWriteMasks {
                enables: _,
                equations: _,
                write_masks,
                states: _,
            }
            | Attachments::Dynamics {
                enables: _,
                equations: _,
                write_masks,
            } => Some(write_masks.clone()),
        }
    }
}

impl Eq for Attachments {}

impl PartialEq for Attachments {
    #[inline]
    #[allow(clippy::too_many_lines)]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (
                Self::States { states },
                Self::States {
                    states: other_states,
                },
            ) => Self::compare_states(states, other_states),
            (
                Self::DynamicEnables { enables, states },
                Self::DynamicEnables {
                    enables: other_enables,
                    states: other_states,
                },
            ) => enables == other_enables && Self::compare_states(states, other_states),
            (
                Self::DynamicEquations { equations, states },
                Self::DynamicEquations {
                    equations: other_equations,
                    states: other_states,
                },
            ) => {
                Self::compare_equations(equations, other_equations)
                    && Self::compare_states(states, other_states)
            }
            (
                Self::DynamicWriteMasks {
                    write_masks,
                    states,
                },
                Self::DynamicWriteMasks {
                    write_masks: other_write_masks,
                    states: other_states,
                },
            ) => write_masks == other_write_masks && Self::compare_states(states, other_states),
            (
                Self::DynamicEnablesAndEquations {
                    enables,
                    equations,
                    states,
                },
                Self::DynamicEnablesAndEquations {
                    enables: other_enables,
                    equations: other_equations,
                    states: other_states,
                },
            ) => {
                enables == other_enables
                    && Self::compare_equations(equations, other_equations)
                    && Self::compare_states(states, other_states)
            }

            (
                Self::DynamicEnablesAndWriteMasks {
                    enables,
                    write_masks,
                    states,
                },
                Self::DynamicEnablesAndWriteMasks {
                    enables: other_enables,
                    write_masks: other_write_masks,
                    states: other_states,
                },
            ) => {
                enables == other_enables
                    && write_masks == other_write_masks
                    && Self::compare_states(states, other_states)
            }
            (
                Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables,
                    equations,
                    write_masks,
                    states,
                },
                Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: other_enables,
                    equations: other_equations,
                    write_masks: other_write_masks,
                    states: other_states,
                },
            ) => {
                enables == other_enables
                    && Self::compare_equations(equations, other_equations)
                    && write_masks == other_write_masks
                    && Self::compare_states(states, other_states)
            }
            (
                Self::Dynamics {
                    enables,
                    equations,
                    write_masks,
                },
                Self::Dynamics {
                    enables: other_enables,
                    equations: other_equations,
                    write_masks: other_write_masks,
                },
            ) => {
                enables == other_enables
                    && Self::compare_equations(equations, other_equations)
                    && write_masks == other_write_masks
            }
            (
                Self::States { states: _ }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicEnables {
                    enables: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicEquations {
                    equations: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                },
            )
            | (
                Self::States { states: _ }
                | Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                },
                Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
            )
            | (
                Self::DynamicEnables {
                    enables: _,
                    states: _,
                }
                | Self::DynamicEquations {
                    equations: _,
                    states: _,
                }
                | Self::DynamicWriteMasks {
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquations {
                    enables: _,
                    equations: _,
                    states: _,
                }
                | Self::DynamicEnablesAndWriteMasks {
                    enables: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEquationsAndWriteMasks {
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::DynamicEnablesAndEquationsAndWriteMasks {
                    enables: _,
                    equations: _,
                    write_masks: _,
                    states: _,
                }
                | Self::Dynamics {
                    enables: _,
                    equations: _,
                    write_masks: _,
                },
                Self::States { states: _ },
            ) => false,
        }
    }
}
