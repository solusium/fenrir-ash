use {ash::vk::BlendOp, fenrir_hal::color::blend::Operation};

#[inline]
pub(super) fn ash_to_hal(operation: BlendOp) -> Operation {
    if BlendOp::ADD == operation {
        Operation::Add
    } else if BlendOp::SUBTRACT == operation {
        Operation::Subtract
    } else if BlendOp::REVERSE_SUBTRACT == operation {
        Operation::ReverseSubtract
    } else if BlendOp::MIN == operation {
        Operation::Min
    } else {
        Operation::Max
    }
}

#[inline]
pub(super) const fn hal_to_ash(operation: Operation) -> BlendOp {
    match operation {
        Operation::Add => BlendOp::ADD,
        Operation::Subtract => BlendOp::SUBTRACT,
        Operation::ReverseSubtract => BlendOp::REVERSE_SUBTRACT,
        Operation::Min => BlendOp::MIN,
        Operation::Max => BlendOp::MAX,
    }
}
