use {
    super::{trace_called, trace_calling},
    crate::{command::buffer::Recorder, Depth, Device},
    abi_stable::std_types::RArc,
    ash::{
        prelude::VkResult,
        vk::{
            CommandBuffer, CompareOp, PipelineDepthStencilStateCreateFlags,
            PipelineDepthStencilStateCreateInfo, StencilFaceFlags, StencilOp, StencilOpState,
            FALSE,
        },
    },
    fenrir_error::{not_supported, Error},
    fenrir_hal::{
        graphics::pipeline::Metadata,
        stencil::{Operation as StencilOperation, Operations},
        Stencil as HalStencil,
    },
    smallvec::{smallvec, SmallVec},
    tracing::debug_span,
};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(super) enum StencilFrontAndOrBack {
    FrontAndBack {
        stencil: Stencil,
        test: bool,
    },
    FrontBack {
        front: Stencil,
        back: Stencil,
        test: bool,
    },
}

#[derive(Clone, Copy, Debug, Default)]
#[allow(clippy::struct_excessive_bools)]
struct CreateInfo {
    flags: PipelineDepthStencilStateCreateFlags,
    depth_test_enable: bool,
    depth_write_enable: bool,
    depth_compare_op: CompareOp,
    depth_bounds_test_enable: bool,
    stencil_test_enable: bool,
    front: StencilOpState,
    back: StencilOpState,
    min_depth_bounds: f32,
    max_depth_bounds: f32,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(crate) struct DepthStencil {
    create_info: Option<CreateInfo>,
    depth: Depth,
    stencil: StencilFrontAndOrBack,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(super) struct Stencil {
    compare_op: CompareOp,
    depth_fail: StencilOp,
    fail_op: StencilOp,
    pass_op: StencilOp,
}

impl CreateInfo {
    #[inline]
    fn ash(&self) -> PipelineDepthStencilStateCreateInfo<'_> {
        PipelineDepthStencilStateCreateInfo::default()
            .flags(self.flags)
            .depth_test_enable(self.depth_test_enable)
            .depth_write_enable(self.depth_write_enable)
            .depth_compare_op(self.depth_compare_op)
            .depth_bounds_test_enable(self.depth_bounds_test_enable)
            .stencil_test_enable(self.stencil_test_enable)
            .front(self.front)
            .back(self.back)
            .min_depth_bounds(self.min_depth_bounds)
            .max_depth_bounds(self.max_depth_bounds)
    }

    #[inline]
    fn compare_op_states(this: &StencilOpState, other: &StencilOpState) -> bool {
        this.fail_op == other.fail_op
            && this.pass_op == other.pass_op
            && this.depth_fail_op == other.depth_fail_op
            && this.compare_op == other.compare_op
            && this.write_mask == other.write_mask
            && this.reference == other.reference
    }
}

impl DepthStencil {
    #[inline]
    pub(super) fn new(device: &Device, metadata: &Metadata) -> Result<Self, Error> {
        let test = metadata.stencil().test();

        let stencil_back = metadata.stencil().back();

        let stencil_front = metadata.stencil().front();

        let stencil = if stencil_back == stencil_front {
            StencilFrontAndOrBack::FrontAndBack {
                stencil: Self::hal_to_ash_stencil(stencil_back),
                test,
            }
        } else {
            StencilFrontAndOrBack::FrontBack {
                front: Self::hal_to_ash_stencil(stencil_front),
                back: Self::hal_to_ash_stencil(stencil_back),
                test,
            }
        };

        let hal_depth = metadata.depth();

        if hal_depth.bounds_test() && (FALSE == device.features().depth_bounds) {
            return Err(not_supported("device doesn't support Depth::bounds_test"));
        }

        let depth = (*metadata.depth()).into();

        let enabled_extended_dynamic_state_3 = device
            .extension_names()?
            .any(|extension_name| "VK_EXT_extended_dynamic_state3" == extension_name);

        let create_info = if enabled_extended_dynamic_state_3 {
            None
        } else {
            Some(CreateInfo::default())
        };

        Ok(Self {
            create_info,
            depth,
            stencil,
        })
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineDepthStencilStateCreateInfo<'_>> {
        self.create_info
            .as_ref()
            .map(|create_info| create_info.ash())
    }

    #[inline]
    pub(super) const fn depth(&self) -> Depth {
        self.depth
    }

    #[inline]
    fn format_cmd_set_depth_bounds(
        args: (&RArc<crate::device::Guard>, CommandBuffer, f32, f32),
    ) -> String {
        let (device, command_buffer, min_depth_bounds, max_depth_bounds) = args;

        format!(
            "cmd_set_depth_bounds(&self: {device:?}, command_buffer: {command_buffer:?}, \
            min_depth_bounds: {min_depth_bounds:?}, max_depth_bounds: {max_depth_bounds:?})"
        )
    }

    #[inline]
    fn format_cmd_set_depth_bounds_test_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, depth_bounds_test_enable) = args;

        format!(
            "cmd_set_depth_bounds_test_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            depth_bounds_test_enable: {depth_bounds_test_enable:?})"
        )
    }

    #[inline]
    fn format_cmd_set_depth_compare_op(
        args: (&RArc<crate::device::Guard>, CommandBuffer, CompareOp),
    ) -> String {
        let (device, command_buffer, depth_compare_op) = args;

        format!(
            "cmd_set_depth_compare_op(&self: {device:?}, command_buffer: {command_buffer:?}, \
            depth_compare_op: {depth_compare_op:?})",
        )
    }

    #[inline]
    fn format_cmd_set_depth_test_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, depth_test_enable) = args;

        format!(
            "cmd_set_depth_test_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            depth_test_enable: {depth_test_enable:?})",
        )
    }

    #[inline]
    fn format_cmd_set_depth_write_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, depth_write_enable) = args;

        format!(
            "cmd_set_depth_write_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            depth_write_enable: {depth_write_enable:?})"
        )
    }

    #[inline]
    const fn hal_to_ash_stencil(stencil: &Operations) -> Stencil {
        let pass_op = Self::hal_to_ash_stencil_operation(stencil.pass());

        let fail_op = Self::hal_to_ash_stencil_operation(stencil.fail());

        let depth_fail = Self::hal_to_ash_stencil_operation(stencil.depth_fail());

        let compare_op = crate::compare::operation::hal_to_ash(stencil.compare());

        Stencil {
            compare_op,
            depth_fail,
            fail_op,
            pass_op,
        }
    }

    #[inline]
    const fn hal_to_ash_stencil_operation(stencil_operation: StencilOperation) -> StencilOp {
        match stencil_operation {
            StencilOperation::Keep => StencilOp::KEEP,
            StencilOperation::Zero => StencilOp::ZERO,
            StencilOperation::Replace => StencilOp::REPLACE,
            StencilOperation::IncrementAndClamp => StencilOp::INCREMENT_AND_CLAMP,
            StencilOperation::DecrementAndClamp => StencilOp::DECREMENT_AND_CLAMP,
            StencilOperation::Invert => StencilOp::INVERT,
            StencilOperation::IncrementAndWrap => StencilOp::INCREMENT_AND_WRAP,
            StencilOperation::DecrementAndWrap => StencilOp::DECREMENT_AND_WRAP,
        }
    }

    #[inline]
    pub(super) const fn stencil(&self) -> StencilFrontAndOrBack {
        self.stencil
    }

    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        let device = recorder.buffer().device();

        let command_buffer = recorder.buffer().ash();

        {
            let trace_args = (&device, command_buffer, self.depth.test());

            trace_calling(Self::format_cmd_set_depth_test_enable, trace_args);

            {
                debug_span!("cmd_set_depth_test_enable");

                unsafe {
                    device.cmd_set_depth_test_enable(command_buffer, self.depth.test());
                }
            }

            trace_called(
                Self::format_cmd_set_depth_test_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (&device, command_buffer, self.depth.write());

            trace_calling(Self::format_cmd_set_depth_write_enable, trace_args);

            {
                debug_span!("cmd_set_depth_write_enable");

                unsafe {
                    device.cmd_set_depth_write_enable(command_buffer, self.depth.write());
                }
            }

            trace_called(
                Self::format_cmd_set_depth_write_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (&device, command_buffer, self.depth.compare_op());

            trace_calling(Self::format_cmd_set_depth_compare_op, trace_args);

            {
                debug_span!("cmd_set_depth_compare_op");

                unsafe {
                    device.cmd_set_depth_compare_op(command_buffer, self.depth.compare_op());
                }
            }

            trace_called(
                Self::format_cmd_set_depth_compare_op,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (&device, command_buffer, self.depth.bounds_test());

            trace_calling(Self::format_cmd_set_depth_bounds_test_enable, trace_args);

            {
                debug_span!("cmd_set_depth_bounds_test_enable");

                unsafe {
                    device
                        .cmd_set_depth_bounds_test_enable(command_buffer, self.depth.bounds_test());
                }
            }

            trace_called(
                Self::format_cmd_set_depth_bounds_test_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let trace_args = (
                &device,
                command_buffer,
                self.depth.min_bounds(),
                self.depth.max_bounds(),
            );

            trace_calling(Self::format_cmd_set_depth_bounds, trace_args);

            {
                debug_span!("cmd_set_depth_bounds");

                unsafe {
                    device.cmd_set_depth_bounds(
                        command_buffer,
                        self.depth.min_bounds(),
                        self.depth.max_bounds(),
                    );
                }
            }

            trace_called(
                Self::format_cmd_set_depth_bounds,
                trace_args,
                None::<&VkResult<()>>,
            );
        }

        self.stencil.set_dynamics(recorder);
    }
}

impl StencilFrontAndOrBack {
    #[inline]
    fn format_cmd_set_stencil_op(
        args: (
            &RArc<crate::device::Guard>,
            CommandBuffer,
            StencilFaceFlags,
            &Stencil,
        ),
    ) -> String {
        let (device, command_buffer, face_mask, stencil) = args;

        format!(
            "cmd_set_stencil_op(&self: {device:?}, command_buffer: {command_buffer:?}, \
            face_mask: {face_mask:?}, fail_op: {:?}, pass_op: {:?}, depth_fail_op: {:?}, \
            compare_op: {:?})",
            stencil.fail_op, stencil.pass_op, stencil.depth_fail, stencil.compare_op
        )
    }

    #[inline]
    fn format_cmd_set_stencil_test_enable(
        args: (&RArc<crate::device::Guard>, CommandBuffer, bool),
    ) -> String {
        let (device, command_buffer, stencil_test_enable) = args;

        format!(
            "cmd_set_stencil_test_enable(&self: {device:?}, command_buffer: {command_buffer:?}, \
            stencil_test_enable: {stencil_test_enable:?})"
        )
    }

    pub(super) fn hal(&self) -> HalStencil {
        let (test, front, back) = match self {
            Self::FrontAndBack { stencil, test } => (test, (*stencil).into(), (*stencil).into()),
            Self::FrontBack { front, back, test } => (test, (*front).into(), (*back).into()),
        };

        HalStencil::new(*test, front, back)
    }

    fn set_dynamics(&self, recorder: &Recorder) {
        let device = recorder.buffer().device();

        let command_buffer = recorder.buffer().ash();

        {
            let stencil_test_enable = match self {
                Self::FrontAndBack { stencil: _, test }
                | Self::FrontBack {
                    front: _,
                    back: _,
                    test,
                } => *test,
            };

            let trace_args = (&device, command_buffer, stencil_test_enable);

            trace_calling(Self::format_cmd_set_stencil_test_enable, trace_args);

            {
                debug_span!("cmd_set_stencil_test_enable");

                unsafe {
                    device.cmd_set_stencil_test_enable(command_buffer, stencil_test_enable);
                }
            }

            trace_called(
                Self::format_cmd_set_stencil_test_enable,
                trace_args,
                None::<&VkResult<()>>,
            );
        }
        {
            let face_masks_and_stencils: SmallVec<[(StencilFaceFlags, &Stencil); 2]> = match self {
                Self::FrontAndBack { stencil, test: _ } => {
                    smallvec![(StencilFaceFlags::FRONT_AND_BACK, stencil)]
                }
                Self::FrontBack {
                    front,
                    back,
                    test: _,
                } => {
                    smallvec![
                        (StencilFaceFlags::FRONT, front),
                        (StencilFaceFlags::BACK, back)
                    ]
                }
            };

            face_masks_and_stencils
                .into_iter()
                .for_each(|(face_mask, stencil)| {
                    let trace_args = (&device, command_buffer, face_mask, stencil);

                    trace_calling(Self::format_cmd_set_stencil_op, trace_args);

                    {
                        debug_span!("cmd_set_stencil_op");

                        unsafe {
                            device.cmd_set_stencil_op(
                                command_buffer,
                                face_mask,
                                stencil.fail_op,
                                stencil.pass_op,
                                stencil.depth_fail,
                                stencil.compare_op,
                            );
                        };
                    }

                    trace_called(
                        Self::format_cmd_set_stencil_op,
                        trace_args,
                        None::<&VkResult<()>>,
                    );
                });
        }
    }
}

impl Eq for CreateInfo {}

impl PartialEq for CreateInfo {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let values = [
            self.flags == other.flags,
            self.depth_test_enable == other.depth_test_enable,
            self.depth_write_enable == other.depth_write_enable,
            self.depth_compare_op == other.depth_compare_op,
            self.depth_bounds_test_enable == other.depth_bounds_test_enable,
            self.stencil_test_enable == other.stencil_test_enable,
            Self::compare_op_states(&self.front, &other.front),
            Self::compare_op_states(&self.back, &other.back),
            self.min_depth_bounds == other.min_depth_bounds,
            self.max_depth_bounds == other.max_depth_bounds,
        ];

        values.iter().all(|value| *value)
    }
}

unsafe impl Send for DepthStencil {}
unsafe impl Sync for DepthStencil {}

#[allow(clippy::from_over_into)]
impl Into<Operations> for Stencil {
    fn into(self) -> Operations {
        Operations::new(
            crate::stencil::operation::ash_to_hal(self.fail_op),
            crate::stencil::operation::ash_to_hal(self.pass_op),
            crate::stencil::operation::ash_to_hal(self.depth_fail),
            crate::compare::operation::ash_to_hal(self.compare_op),
            0,
            0,
            0,
        )
    }
}
