use {
    crate::{command::buffer::Recorder, trace_called, trace_calling},
    abi_stable::std_types::RArc,
    ash::{
        prelude::VkResult,
        vk::{CommandBuffer, RenderingInfo},
    },
    std::slice::from_raw_parts,
    tracing::debug_span,
};

pub(crate) struct Renderer<'recorder, 'buffer> {
    recorder: &'recorder Recorder<'buffer>,
}

impl<'recorder, 'buffer> Renderer<'recorder, 'buffer> {
    #[inline]
    pub(crate) fn new(
        recorder: &'recorder Recorder<'buffer>,
        rendering_info: RenderingInfo<'recorder>,
    ) -> Self {
        let command_buffer = recorder.buffer();

        let device = command_buffer.device();

        let ash_command_buffer = command_buffer.ash();

        let trace_args = (&device, ash_command_buffer, &rendering_info);

        trace_calling(Self::format_cmd_begin_rendering, trace_args);

        {
            debug_span!("cmd_begin_rendering");

            unsafe {
                device.cmd_begin_rendering(ash_command_buffer, &rendering_info);
            }
        }

        trace_called(
            Self::format_cmd_begin_rendering,
            trace_args,
            None::<&VkResult<()>>,
        );

        Self { recorder }
    }

    #[cold]
    #[inline]
    fn format_cmd_begin_rendering(
        args: (&RArc<crate::device::Guard>, CommandBuffer, &RenderingInfo),
    ) -> String {
        let (device, command_buffer, rendering_info) = args;

        let color_attachments = if 0 == rendering_info.color_attachment_count {
            format!("{:?}", rendering_info.p_color_attachments)
        } else {
            format!("{:?}", unsafe {
                from_raw_parts(
                    rendering_info.p_color_attachments,
                    rendering_info.color_attachment_count as usize,
                )
            })
        };

        let depth_attachment = if rendering_info.p_depth_attachment.is_null() {
            format!("{:?}", rendering_info.p_depth_attachment)
        } else {
            format!("{:?}", unsafe { *rendering_info.p_depth_attachment })
        };

        let stencil_attachment = if rendering_info.p_stencil_attachment.is_null() {
            format!("{:?}", rendering_info.p_stencil_attachment)
        } else {
            format!("{:?}", unsafe { *rendering_info.p_stencil_attachment })
        };

        let rendering_info_string = format!(
            "RenderingInfo {{ s_type: {:?}, p_next: 0x0, flags: {:?}, render_area: {:?}, layer_count: {:?}, \
            view_mask: {:?}, color_attachment_count: {:?}, p_color_attachments: {:?}, p_depth_attachment: \
            {:?}, p_stencil_attachment: {:?} }}",rendering_info.s_type, rendering_info.flags, rendering_info.render_area,
            rendering_info.layer_count, rendering_info.view_mask, rendering_info.color_attachment_count,
            color_attachments, depth_attachment, stencil_attachment
        );

        format!(
            "cmd_begin_rendering(&self: {device:?}, command_buffer: {command_buffer:?}, rendering_info: \
            {rendering_info_string})"
        )
    }

    #[cold]
    #[inline]
    fn format_end_command_buffer(args: (&RArc<crate::device::Guard>, CommandBuffer)) -> String {
        let (device, command_buffer) = args;

        format!("end_command_buffer(&self: {device:?}, command_buffer: {command_buffer:?})")
    }
}

impl Drop for Renderer<'_, '_> {
    #[inline]
    fn drop(&mut self) {
        let command_buffer = self.recorder.buffer();

        let device = command_buffer.device();

        let ash_command_buffer = command_buffer.ash();

        let trace_args = (&device, ash_command_buffer);

        trace_calling(Self::format_end_command_buffer, trace_args);

        {
            debug_span!("cmd_end_rendering");

            unsafe {
                device.cmd_end_rendering(ash_command_buffer);
            }
        }

        trace_called(
            Self::format_end_command_buffer,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}
