use {
    crate::{command::buffer::Recorder, device::Device, trace_called, trace_calling},
    abi_stable::std_types::RArc,
    ash::{
        ext::extended_dynamic_state2::Device as DynamicState2Device,
        prelude::VkResult,
        vk::{
            CommandBuffer, PhysicalDeviceExtendedDynamicState2FeaturesEXT,
            PipelineTessellationStateCreateFlags, PipelineTessellationStateCreateInfo, TRUE,
        },
    },
    core::ops::Range,
    fenrir_error::{argument_out_of_domain, Error},
    fenrir_hal::graphics::pipeline::Metadata,
    tracing::debug_span,
};

#[derive(Clone, Debug, Eq, PartialEq)]
struct CreateInfo {
    flags: PipelineTessellationStateCreateFlags,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct Tessellation {
    create_info: Option<CreateInfo>,
    dynamic_patch_control_points: bool,
    patch_control_points: u32,
}

impl Tessellation {
    #[inline]
    pub(super) fn new(
        device: &Device,
        metadata: &Metadata,
        available_dynamic_state2_features: &PhysicalDeviceExtendedDynamicState2FeaturesEXT,
    ) -> Result<Self, Error> {
        let limits = device.limits();

        let patch_control_points_out_of_domain_error = || {
            argument_out_of_domain(
                Range {
                    start: 1.into(),
                    end: (limits.max_tessellation_patch_size + 1).into(),
                },
                metadata.tessellation_patch_control_points().into(),
            )
        };

        let patch_control_points_usize: usize = metadata.tessellation_patch_control_points().into();

        let patch_control_points = patch_control_points_usize
            .try_into()
            .map_err(|_| patch_control_points_out_of_domain_error())?;

        {
            let valid_patch_control_points_range = 1..=limits.max_tessellation_patch_size;

            if !valid_patch_control_points_range.contains(&patch_control_points) {
                return Err(patch_control_points_out_of_domain_error());
            }
        }

        let dynamic_patch_control_points =
            TRUE == available_dynamic_state2_features.extended_dynamic_state2_patch_control_points;

        let create_info = if TRUE
            == available_dynamic_state2_features.extended_dynamic_state2_patch_control_points
        {
            None
        } else {
            let flags = PipelineTessellationStateCreateFlags::default();

            Some(CreateInfo { flags })
        };

        Ok(Self {
            create_info,
            dynamic_patch_control_points,
            patch_control_points,
        })
    }

    #[inline]
    pub(super) fn create_info(&self) -> Option<PipelineTessellationStateCreateInfo<'_>> {
        self.create_info.as_ref().map(|create_info| {
            let mut ash_create_info =
                PipelineTessellationStateCreateInfo::default().flags(create_info.flags);

            if !self.dynamic_patch_control_points {
                ash_create_info = ash_create_info.patch_control_points(self.patch_control_points);
            }

            ash_create_info
        })
    }

    #[inline]
    pub(super) const fn dynamic_patch_control_points(&self) -> Option<u32> {
        if self.dynamic_patch_control_points {
            Some(self.patch_control_points)
        } else {
            None
        }
    }

    #[cold]
    #[inline]
    fn format_cmd_set_patch_control_points(
        args: (&RArc<crate::device::Guard>, CommandBuffer, u32),
    ) -> String {
        let (device, command_buffer, patch_control_points) = args;

        format!(
            "cmd_set_patch_control_points(&self: {device:?}, command_buffer: {command_buffer:?}, \
            patch_control_points: {patch_control_points:?})"
        )
    }

    #[inline]
    pub(super) const fn patch_control_points(&self) -> u32 {
        self.patch_control_points
    }

    #[inline]
    pub(crate) fn set_dynamics(&self, recorder: &Recorder) {
        if !self.dynamic_patch_control_points {
            return;
        }

        let device_guard = recorder.buffer().device();

        let instance = device_guard.instance();

        let device = DynamicState2Device::new(&instance, &device_guard);

        let command_buffer = recorder.buffer().ash();

        let trace_args = (&device_guard, command_buffer, self.patch_control_points);

        trace_calling(Self::format_cmd_set_patch_control_points, trace_args);

        {
            debug_span!("cmd_set_patch_control_points");

            unsafe {
                device.cmd_set_patch_control_points(command_buffer, self.patch_control_points);
            }
        }

        trace_called(
            Self::format_cmd_set_patch_control_points,
            trace_args,
            None::<&VkResult<()>>,
        );
    }
}

unsafe impl Send for Tessellation {}
unsafe impl Sync for Tessellation {}
