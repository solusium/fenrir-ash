use {
    crate::rendering::AttachmentInfo,
    ash::vk::{Rect2D, RenderingFlags, RenderingInfo},
    core::{ops::Range, slice::from_raw_parts},
    fenrir_error::{argument_out_of_domain, Error},
    smallvec::SmallVec,
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub(crate) struct Info {
    flags: RenderingFlags,
    render_area: Rect2D,
    layer_count: u32,
    view_mask: u32,
    color_attachments: SmallVec<[AttachmentInfo; 3]>,
    depth_attachment: Option<AttachmentInfo>,
    stencil_attachment: Option<AttachmentInfo>,
}

impl Info {
    #[inline]
    pub(crate) fn ash(&self) -> RenderingInfo<'_> {
        RenderingInfo::default()
            .flags(self.flags)
            .render_area(self.render_area)
            .layer_count(self.layer_count)
            .view_mask(self.view_mask)
    }

    #[inline]
    #[must_use]
    pub(crate) const fn layer_count(&self) -> u32 {
        self.layer_count
    }

    #[inline]
    pub(crate) const fn render_area(&self) -> &Rect2D {
        &self.render_area
    }

    #[inline]
    pub(crate) const fn view_mask(&self) -> u32 {
        self.view_mask
    }
}

impl TryFrom<RenderingInfo<'_>> for Info {
    type Error = Error;

    #[inline]
    fn try_from(value: RenderingInfo) -> Result<Self, Error> {
        let color_attachment_count = value.color_attachment_count.try_into().map_err(|_| {
            argument_out_of_domain(
                Range {
                    start: usize::MIN.into(),
                    end: usize::MAX.into(),
                },
                value.color_attachment_count.into(),
            )
        })?;

        let color_attachments =
            unsafe { from_raw_parts(value.p_color_attachments, color_attachment_count) }
                .iter()
                .map(|color_attachment| AttachmentInfo {
                    image_view: color_attachment.image_view,
                    image_layout: color_attachment.image_layout,
                    resolve_mode: color_attachment.resolve_mode,
                    resolve_image_view: color_attachment.resolve_image_view,
                    resolve_image_layout: color_attachment.resolve_image_layout,
                    load_operation: color_attachment.load_op,
                    store_operation: color_attachment.store_op,
                    clear_value: color_attachment.clear_value,
                })
                .collect::<SmallVec<_>>();

        let [depth_attachment, stencil_attachment] =
            [value.p_depth_attachment, value.p_stencil_attachment].map(|pointer| {
                unsafe { pointer.as_ref() }.map(|attachment| AttachmentInfo {
                    image_view: attachment.image_view,
                    image_layout: attachment.image_layout,
                    resolve_mode: attachment.resolve_mode,
                    resolve_image_view: attachment.resolve_image_view,
                    resolve_image_layout: attachment.resolve_image_layout,
                    load_operation: attachment.load_op,
                    store_operation: attachment.store_op,
                    clear_value: attachment.clear_value,
                })
            });

        Ok(Self {
            flags: value.flags,
            render_area: value.render_area,
            layer_count: value.layer_count,
            view_mask: value.view_mask,
            color_attachments,
            depth_attachment,
            stencil_attachment,
        })
    }
}
