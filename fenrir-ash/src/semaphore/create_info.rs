use ash::vk::{SemaphoreCreateFlags, SemaphoreCreateInfo, SemaphoreType, SemaphoreTypeCreateInfo};

#[derive(Debug)]
pub(crate) struct CreateInfo {
    flags: SemaphoreCreateFlags,
    semaphore_type: SemaphoreType,
    initial_value: u64,
}

pub(crate) struct Wrapper<'a> {
    create_info: SemaphoreCreateInfo<'a>,
    type_create_info: SemaphoreTypeCreateInfo<'a>,
}

impl CreateInfo {
    #[inline]
    pub(crate) fn new() -> Self {
        Self {
            flags: SemaphoreCreateFlags::default(),
            semaphore_type: SemaphoreType::TIMELINE,
            initial_value: 0,
        }
    }

    pub(crate) fn ash<'a>(&'a self) -> Wrapper<'a> {
        let type_create_info = SemaphoreTypeCreateInfo::default()
            .semaphore_type(self.semaphore_type)
            .initial_value(self.initial_value);

        let create_info = SemaphoreCreateInfo::default().flags(self.flags);

        Wrapper {
            create_info,
            type_create_info,
        }
    }
}

impl<'a> Wrapper<'a> {
    #[inline]
    #[must_use]
    pub(crate) fn deref_mut(&mut self) -> &SemaphoreCreateInfo {
        self.create_info.p_next = <*const SemaphoreTypeCreateInfo>::cast(&self.type_create_info);

        &self.create_info
    }
}
