use {ash::vk::StencilOp, fenrir_hal::stencil::Operation};

#[inline]
pub(crate) fn ash_to_hal(operation: StencilOp) -> Operation {
    if StencilOp::KEEP == operation {
        Operation::Keep
    } else if StencilOp::ZERO == operation {
        Operation::Zero
    } else if StencilOp::REPLACE == operation {
        Operation::Replace
    } else if StencilOp::INCREMENT_AND_CLAMP == operation {
        Operation::IncrementAndClamp
    } else if StencilOp::DECREMENT_AND_CLAMP == operation {
        Operation::DecrementAndClamp
    } else if StencilOp::INVERT == operation {
        Operation::Invert
    } else if StencilOp::INCREMENT_AND_WRAP == operation {
        Operation::IncrementAndWrap
    } else {
        Operation::DecrementAndWrap
    }
}
