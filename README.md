# fenrir-ash

An implementation of the fenrir-traits
(https://gitlab.com/solusium/fenrir/-/tree/main/fenrir-traits) GPU abstraction layer based
on ash (https://github.com/ash-rs/ash).

