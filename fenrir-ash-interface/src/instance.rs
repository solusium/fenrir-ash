use {
    ash::Instance,
    bytemuck::{Pod, Zeroable},
    std::{ops::Deref, ptr::null},
};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Handle {
    instance: *const Instance,
}

impl Handle {
    pub fn new(instance: *const Instance) -> Self {
        Handle { instance }
    }
}

impl Deref for Handle {
    type Target = ash::Instance;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.instance }
    }
}

unsafe impl Pod for Handle {}

unsafe impl Send for Handle {}
unsafe impl Sync for Handle {}

unsafe impl Zeroable for Handle {
    fn zeroed() -> Self {
        Handle { instance: null() }
    }
}
