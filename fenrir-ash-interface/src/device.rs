use {
    ash::{
        vk::{DeviceMemory, PhysicalDevice},
        Device,
    },
    bytemuck::{bytes_of, checked::try_from_bytes, Pod, Zeroable},
    core::{ops::Deref, ptr::null},
    fenrir_error::{initialization_failed, Error},
};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Handle {
    physical_device: PhysicalDevice,
    device: *const Device,
    instance: crate::instance::Handle,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub struct Memory {
    device_memory: DeviceMemory,
}

impl Handle {
    #[inline]
    pub fn new(
        physical_device: PhysicalDevice,
        device: *const Device,
        instance: crate::instance::Handle,
    ) -> Self {
        Handle {
            physical_device,
            device,
            instance,
        }
    }

    #[inline]
    pub fn as_core_handle(&self) -> fenrir_hal::device::Handle {
        fenrir_hal::device::Handle::new(bytes_of(self))
    }

    #[inline]
    pub fn instance(&self) -> &crate::instance::Handle {
        &self.instance
    }

    #[inline]
    pub fn physical_device(&self) -> PhysicalDevice {
        self.physical_device
    }
}

impl Memory {
    #[inline]
    pub fn new(device_memory: DeviceMemory) -> Self {
        Memory { device_memory }
    }

    #[inline]
    pub fn handle(&self) -> DeviceMemory {
        self.device_memory
    }
}

impl Default for Handle {
    #[inline]
    fn default() -> Self {
        Handle {
            physical_device: PhysicalDevice::default(),
            device: null(),
            instance: crate::instance::Handle::zeroed(),
        }
    }
}

impl Deref for Handle {
    type Target = ash::Device;

    #[inline]
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.device }
    }
}

unsafe impl Pod for Handle {}

unsafe impl Send for Handle {}
unsafe impl Sync for Handle {}

impl TryFrom<fenrir_hal::device::Handle<'_>> for Handle {
    type Error = Error;

    #[inline]
    fn try_from(core_handle: fenrir_hal::device::Handle) -> Result<Self, Self::Error> {
        match try_from_bytes::<Self>(core_handle.data()) {
            Ok(handle) => Ok(*handle),
            Err(_) => Err(initialization_failed("could not cast bytes to Handle")),
        }
    }
}

unsafe impl Zeroable for Handle {
    #[inline]
    fn zeroed() -> Self {
        Handle {
            physical_device: PhysicalDevice::default(),
            device: null(),
            instance: crate::instance::Handle::zeroed(),
        }
    }
}

impl Deref for Memory {
    type Target = DeviceMemory;

    fn deref(&self) -> &Self::Target {
        &self.device_memory
    }
}

unsafe impl Pod for Memory {}

unsafe impl Send for Memory {}
unsafe impl Sync for Memory {}

unsafe impl Zeroable for Memory {
    fn zeroed() -> Self {
        Memory {
            device_memory: DeviceMemory::default(),
        }
    }
}
